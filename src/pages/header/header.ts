import { Component, Input } from '@angular/core';
import { Platform } from 'ionic-angular';

//providers services
import { CommonService } from '../../providers/common-service/common.service';
import { Constants } from '../../providers/appSettings/constant-settings';

@Component({
    selector: 'header',
    templateUrl: 'header.html'
})
export class HeaderPage {
    @Input() firstName: any;
    @Input() lastName: any;
    @Input() pageTitle: any;
    @Input() link: any;
    @Input() linkLabel: any;
    linkLabelText: any;
    @Input() showButton:boolean;
    @Input()showFilter:boolean;
    
    constructor( public commonService: CommonService, private constants: Constants, public platform: Platform ){
    }
    
    
    comingSoon = () => {
        this.commonService.presentToast();
    }
}
