import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { LandingCopyPage } from './landing-copy';
// import * from 'bootstrap/dist/css'

@NgModule({
  declarations: [
    LandingCopyPage,
  ],
  imports: [
    IonicPageModule.forChild(LandingCopyPage),
    TranslateModule.forChild()
  ],
  exports:[
    LandingCopyPage
  ]
})
export class LandingPageModule {}
