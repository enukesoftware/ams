import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { reportsPage } from './reports';
import {ChartModule} from "primeng/chart";
import { CalendarModule } from 'primeng/calendar';
import {TableModule} from 'primeng/table';
import { SharedModule } from "../components/shared.module";

@NgModule({
  declarations: [
    reportsPage,
  ],
  imports: [
    IonicPageModule.forChild(reportsPage),
    TranslateModule.forChild(),
    ChartModule,
    CalendarModule,
    TableModule,
    SharedModule
  ],
  exports: [
    reportsPage
  ]
})
export class reportsPagetModule {}
