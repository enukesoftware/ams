import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Content, Events ,Platform} from 'ionic-angular';
import { Location } from '@angular/common';

//providers
import { CommonService } from '../../providers/common-service/common.service';
import { Constants } from '../../providers/appSettings/constant-settings';
import { UserDataDTO } from '../../interfaces/user-data-dto';
import { PatientRoleServiceProvider } from "../../providers/patient-role-service/patient-role-service";
import { LocalStorageService } from '../../providers/localStorage-service/localStorage.service';

//PrimeNg Modules
import {TableModule} from 'primeng/table';
import {ChartModule} from 'primeng/chart';

@IonicPage( {
    name: 'reports',
    segment: 'reports/:from'
} )
@Component( {
    selector: 'reports',
    templateUrl: 'reports.html',
} )
export class reportsPage {
    @ViewChild( Content ) content: Content;
/*    userData: UserDataDTO = {};
    appointmentList: any;
    loadFinished: boolean = false;
    pageNo: number = 0;
    pageName: any;
    formattedDate: any;
    formattedTime: any;
    isUpcominAppointments: boolean = false;
    protected patientId: any;*/
    startValueStr: string;
    endValueStr: string;
    fromPage:any;
    doctorProfileId:any;
    hospitalId:any;
    reportData:any;
    names=[];
    values=[];
    revenueNames=[];
    revenueValues = [];
    appointmentChartColors = [];
    revenueChartColors = [];
    newlyGeneratedAppointmentColor:any;
    newlyGeneratedRevenueColor:any;
    appointmentsData=[];
    revenueDetails=[];
    isSysAdmRole = false;
    isDocRole =false;
    isHospitalAdminRole=false;
    sysAdmTableData=[];
    startValue:any;
    endValue:any;
    colorPicker=[];
    isDateSelected=false;
    goBtnClicked = false;
    errorMSG:any;
    errorMsgEndDate:any;
    userData: UserDataDTO = {};
    selectedDates:any ={
        fromDate:'',
        toDate:''  
    }

    
    data:any={
            labels:[],
            datasets:[]
    };  
    
    revenueData:any={
            labels:[],
            datasets:[]
    };  
    totalRevenue:any;
    constructor( private commonService: CommonService, private evts: Events, private patientRoleService: PatientRoleServiceProvider, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private constants: Constants,
                private locstr: LocalStorageService,private location: Location, public platform: Platform) {
        
        this.menu.enable( true );
        this.startValueStr='';
        this.endValueStr='';
        this.platform.registerBackButtonAction(() => {
            this.location.back();
          });
        if( this.navParams.get('from')){
            this.fromPage = this.navParams.get('from');
            console.log(" from",this.fromPage);
        } 
        
        this.colorPicker =[
              "#230300","#18003e","#084000", "#441b00","#2f2100", "#002c2c","#1e0014","#3a0500",
              "#270066","#0c5e00","#6c2b00", "#503901","#004444","#320021","#570700","#390094",
              "#107a00", "#923a00","#785809","#006161","#4f0034","#a40d00","#4e00cc","#149700",
              "#ba4a00","#b78816","#007f7f","#71004b","#ff0000","#6100ff","#1ac000","#ff6600",
              "e2a718", "#00a3a3","#990066","#ff3939","#8d47ff","#45d42f","#ff893a", "#ffb500",
              "#1cbfbf","#c11588", "#ff6363","#a974ff","#6fe85d", "#ffad76","#ffd162","#3ddddd",
              "#e922a7","#ff8b8b"];
       
    }

    ionViewWillEnter = () => {
        console.log("in reports");
        this.menu.enable( true );
        this.getIds();
        this.getRandomColor();
        this.isAuthorized();
      /*  setTimeout( () => {
            this.update();
        },500);*/
    }
    
    convertDate = () =>{
        var d = new Date();
        
        return [d.getFullYear(), this.pad(d.getMonth()+1), this.pad(d.getDate())].join('-');
    }
    
    pad = (s) => {
        return (s < 10) ? '0' + s : s;
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                console.log("userData--->",this.userData);
            }
        } );
    }

    comingSoonPopup = () => {
        this.commonService.presentToast();
    }
    
    
    /*function to choose role as per fromPage */
    public getIds = () => { 
        let unformattedStartValue = "";
        let unformattedEndValue ="";
        this.names=[];
        this.values=[];
        this.appointmentsData=[];
        this.revenueNames=[];
        this.revenueValues=[];
        this.revenueDetails =[];
        //this.appointmentChartColors=[];
        this.revenueChartColors=[];
        
        if(this.startValueStr && this.endValueStr){
            console.log("startDate any...", this.startValueStr);
            if( this.startValueStr){
                if(!this.commonService.checkIsWeb()){
                    var now = new Date(this.startValueStr);
                    var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                    unformattedStartValue = utc_now.getTime().toString();
                }
                else{
                    console.log("startValue",this.startValueStr);
                    unformattedStartValue = new Date(this.startValueStr).getTime().toString();
                    console.log("unformattedStartValue",unformattedStartValue);
                }
            } 
            
            if(this.endValueStr){
                if(!this.commonService.checkIsWeb()){
                    var now = new Date(this.endValueStr);
                    var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                    unformattedEndValue = utc_now.getTime().toString();
                }
                else{
                    new Date(this.endValueStr).setHours(11, 59, 59);
                    
                    unformattedEndValue = new Date(this.endValueStr).getTime().toString();
                }
            }
            /*new Date(this.endValueStr).setHours(11, 59, 59);*/
           
           // unformattedStartValue = new Date(this.startValueStr).getTime().toString();
           // unformattedEndValue = new Date(this.endValueStr).getTime().toString();
            console.log("startValue",unformattedStartValue);
            console.log( "endValue",unformattedEndValue);
            this.goBtnClicked=false;

        }
        
        this.selectedDates={
                fromDate: unformattedStartValue,
                toDate:unformattedEndValue  
        }
        console.log(" selectedDates--->",this.selectedDates);
        if(this.fromPage == "systemAdmin"){
            console.log("in syadmin role ");
                this.isSysAdmRole = true;
                this.isDocRole=false;
                this.isHospitalAdminRole=false;
                this.getReportForSysAdmin(this.selectedDates);
              
        }
        else if(this.fromPage == "doctor"  ||  this.fromPage == "sysAdmDoctor" ){
            console.log("in doctor role ");
            this.isSysAdmRole = false;
            this.isDocRole=true;
            this.isHospitalAdminRole=false;
           this.commonService.getFromStorage('doctorProfileId').then((value)=>{
                console.log("doctorProfileId---> ",value);
                this.doctorProfileId = value;
                this.getReportForDoctor(this.doctorProfileId,this.selectedDates);
            });
        }
        else if(this.fromPage == "hospitalAdmin" || this.fromPage == "sysAdmHospitalAdmin"){
            console.log("in hopital admn role ");
            this.isSysAdmRole = false;
            this.isDocRole=false;
            this.isHospitalAdminRole = true;
            this.commonService.getFromStorage('hospitalId').then((value)=>{
                console.log("hospitalId---> ",value);
                this.hospitalId = value;
                this.getReportForHospitalAdmin(this.hospitalId, this.selectedDates);
            })
            
        }
        else{
        }
        this.commonService.setPaddingTopScroll( this.content );
    }
    
    
    
    /*
     * function to get reports for doctor role or sysAdm as doctor
     * */
    
    getReportForDoctor = (id,selectedDates?:any) => { 
        console.log("selectedDates for doc... ",selectedDates);
        this.patientRoleService.getReportForDoctor( id,selectedDates).subscribe(
                res => {
                    if ( res.status == "success" ) {
                        this.commonService.hideLoading();
                        //this.commonService.presentToast( res.message );
                        this.reportData = res.data;
                        console.log("reportData revenue available",this.reportData);
                       this.getTotalRevenue( this.reportData.appointmentsRevenueCountReport);
                       if(this.reportData){
                             this.getAppointmentsRevenueCount(this.reportData);
                             this.showChart();
                       }
                    
                    }
                    else {
                        this.commonService.hideLoading();
                        this.commonService.presentToast( res.message );
                    }
                     
                },
                error => {
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);
                });
        }
    
    
    /*
     * function to get reports for Hospital Admin role or sysAdm as Hospital Admin
     * */
    
    getReportForHospitalAdmin = (id,selectedDates?:any) => { 
        console.log("selectedDates for hospiadmin... ",selectedDates);
        this.patientRoleService.getReportForHospitalAdmin( id,selectedDates ).subscribe(
                res => {
                    if ( res.status == "success" ) {
                        this.commonService.hideLoading();
                        //this.commonService.presentToast( res.message );
                        console.log("res",res);
                        this.reportData = res.data;
                        this.getTotalRevenue( this.reportData.appointmentsRevenueCountReport);
                        console.log("res",this.reportData);
                        if(this.reportData){
                            this.getAppointmentsRevenueCount(this.reportData);
                            this.showChart();
                        }
                    }
                    else {
                        this.commonService.hideLoading();
                        this.commonService.presentToast( res.message );
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);
                }
            );
        
        }
    
    
    
    /*function to get report for SysAdmin*/
    
    public getReportForSysAdmin = (selectedDates?:any) =>{
        console.log("selectedDates for sysadmin... ",selectedDates);
            this.patientRoleService.getReportForSysAdmin(selectedDates).subscribe(      
                res => {
                if ( res.status == "success" ) {
                    this.commonService.hideLoading();
                    //this.commonService.presentToast( res.message );
                    console.log("res for sys admin",res);
                    this.sysAdmTableData = res.data.list;
                    this.getTotalRevenue(this.sysAdmTableData)
                    console.log("sysAdmTableData", this.sysAdmTableData);
                }
                else {
                    this.commonService.hideLoading();
                    this.commonService.presentToast( res.message );
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }
    
    
    
    
    
    /*
     *function to get name & values for appointment and revenue   
     * */
    
        public getAppointmentsRevenueCount = (reportData) => {
            //for appointments pie chart
            if(reportData.appointmentsCountReport.length >0){
                for(let i=0; i < reportData.appointmentsCountReport.length; i++){
                    if(reportData.appointmentsCountReport[i].name ){
                        //console.log("name---> ",reportData.appointmentsCountReport[i].name);
                        this.names.push(reportData.appointmentsCountReport[i].name);
                        this.values.push(reportData.appointmentsCountReport[i].sum);
                        //this.newlyGeneratedAppointmentColor = this.getRandomColor();
                       // this.appointmentChartColors.push(this.newlyGeneratedAppointmentColor);
                        this.appointmentsData.push({name : reportData.appointmentsCountReport[i].name ,color:this.colorPicker[i], sum:reportData.appointmentsCountReport[i].sum});     
                    }
                    else if(reportData.appointmentsCountReport[i].first_name){
                        this.names.push(reportData.appointmentsCountReport[i].first_name+" "+reportData.appointmentsRevenueCountReport[i].last_name);
                        this.values.push(reportData.appointmentsCountReport[i].sum);
                       // this.newlyGeneratedAppointmentColor = this.getRandomColor();
                        //this.appointmentChartColors.push(this.newlyGeneratedAppointmentColor);
                        this.appointmentsData.push({name : reportData.appointmentsCountReport[i].first_name ,color:this.colorPicker[i], sum:reportData.appointmentsCountReport[i].sum});
                        
                    }
               
                console.log("names reportData.appointmentsCountReport..", reportData.appointmentsCountReport);
                }
            }
            
            //for revenue pie chart
            if(reportData.appointmentsRevenueCountReport.length >0){
              for(let i=0; i < reportData.appointmentsRevenueCountReport.length; i++){
                  if(reportData.appointmentsRevenueCountReport[i].name){
                  this.revenueNames.push(reportData.appointmentsRevenueCountReport[i].name);
                  this.revenueValues.push(reportData.appointmentsRevenueCountReport[i].amount);
                  //this.newlyGeneratedAppointmentColor = this.getRandomColor();
                  //this.revenueChartColors.push(this.newlyGeneratedAppointmentColor);
                  this.revenueDetails.push({name : reportData.appointmentsRevenueCountReport[i].name ,color:this.colorPicker[i], amount:reportData.appointmentsRevenueCountReport[i].amount});
                 }else if(reportData.appointmentsRevenueCountReport[i].first_name ){
                     this.revenueNames.push(reportData.appointmentsRevenueCountReport[i].first_name +" "+reportData.appointmentsRevenueCountReport[i].last_name);
                     this.revenueValues.push(reportData.appointmentsRevenueCountReport[i].amount);
                     //this.newlyGeneratedAppointmentColor = this.getRandomColor();
                    // this.revenueChartColors.push(this.newlyGeneratedAppointmentColor);
                     this.revenueDetails.push({name : reportData.appointmentsRevenueCountReport[i].name ,color:this.colorPicker[i], amount:reportData.appointmentsRevenueCountReport[i].amount});
                 }
             }
            }
        } 

        
       /*function to generate color for reports*/
        public getRandomColor  =() => {
            let letters = '0123456789ABCDEF'.split('');
            let color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.round(Math.random() * 15)];
            }
            console.log("color",color);
            return color;
        }


        
        /*function to integrate report data in chart*/
        public showChart = () => {
               console.log("event ",this.data);
               this.data = {
                    labels: this.names,
                    datasets: [
                        {
                            data: this.values,
                            backgroundColor:this.colorPicker,
                            hoverBackgroundColor:this.colorPicker
                        }]    
                };
            
               console.log(" appointment data",this.data);
               console.log(" appointment values",this.values);
        
            this.revenueData = {
                    labels: this.revenueNames,
                    datasets: [
                        {
                            data: this.revenueValues,
                            backgroundColor: this.colorPicker,
                            hoverBackgroundColor: this.colorPicker
                        }]    
                };
        
            console.log(" revenueData",this.revenueData);
            console.log(" revenue values",this.values);
      }
        
      
    /*function to get total revenue*/  
        public getTotalRevenue = (revenueValues) => {
            console.log("in get Total revenue ",revenueValues);
            this.totalRevenue=0;
            for(let i=0; i<revenueValues.length; i++) { 
            if(this.fromPage == "systemAdmin"){
            this.totalRevenue =  this.totalRevenue + revenueValues[i].revenueGenerated;
            }
            else if(this.fromPage == "doctor"  ||  this.fromPage == "sysAdmDoctor"){
            this.totalRevenue =  this.totalRevenue + revenueValues[i].amount;
            }
            else{
            this.totalRevenue =  this.totalRevenue + revenueValues[i].amount;
            }
        }
        console.log("revenueValues totalRevenue",this.totalRevenue);
    }
            
    
    /*function to validate and get date specific reports*/  
    public getReportsAsPerDates = () =>{
        if(!this.startValueStr){
            this.goBtnClicked =true;
            this.errorMsgEndDate='';
            this.errorMSG = this.commonService.getTranslate('SELECTSTARTDATE',{});
            this.commonService.presentToast(this.errorMSG );
        }
        else if(!this.endValueStr && this.startValueStr){
            this.goBtnClicked =true;
            this.errorMSG = '';
            this.errorMsgEndDate = this.commonService.getTranslate('SELECTENDDATE',{});
            this.commonService.presentToast(this.errorMsgEndDate );
        }
        else{
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.getIds();
        }
       
    }        
}
