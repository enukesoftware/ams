import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, Platform, Events  } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Location } from '@angular/common';


//providers services
import { CommonService } from '../../providers/common-service/common.service';
import { AuthService } from '../../providers/authService/authService';
import { Constants } from '../../providers/appSettings/constant-settings';
import { UserDataDTO } from '../../interfaces/user-data-dto';
import { HttpResponseDTO } from '../../interfaces/http-response-dto';
import { LocalStorageService } from "../../providers/localStorage-service/localStorage.service";
import { CameraService } from '../../providers/camera-service/camera.service';

@IonicPage()
@Component({
    selector: 'page-sign-up',
    templateUrl: 'sign-up.html'
})
export class SignUpPage {
    signupForm: FormGroup;
    passwordEyeIcon: any = "eye-icon";
    inputType: any = "password";
    isSubmit =false;
    firstNamePlaceholder: any;
    lastNamePlaceholder: any;
    optionalPlaceholder: any;
    usernamePlaceholder: any;
    mobilePlaceholder: any;
    emailPlaceholder: any;
    cityPlaceholder: any;
    passwordPlaceholder: any;
    signUpData: UserDataDTO = {};
    imageUpload: any = {};
    rank: number;
    isDefault: boolean;
    isUpload:boolean = false;
    
    constructor( private commonService: CommonService, public cameraService : CameraService, private location: Location, private constants: Constants, private platform: Platform, private authService: AuthService, private navCtrl: NavController, private menu: MenuController,
            private formBuilder: FormBuilder, private evts: Events, private locstr: LocalStorageService ) {
   this.menu.enable(false);
   
   this.platform.registerBackButtonAction(() => {
       this.location.back();
     });
       
            
        this.signupForm = formBuilder.group({

            FirstName: ['', Validators.compose([
                Validators.maxLength(20),
                Validators.minLength(1),
                Validators.required,
                Validators.pattern(/^[a-zA-Z ]+$/),
                
            ])
            ],

            LastName: ['', Validators.compose([
                   Validators.maxLength(20),
                   Validators.minLength(1),
                   //Validators.required,
                ])
            ],

            Email: ['', Validators.compose([
                Validators.pattern(/^\s*(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/),
              //  Validators.required,
                Validators.maxLength(256)
            ])
            ],
            Username: [
                '', Validators.compose([
                    Validators.required,
                    Validators.maxLength(50),
                ])
            ],

            ContactNumber: [
                '', Validators.compose([
                    Validators.required,
                    Validators.minLength(10),
                    Validators.pattern(/^\d{3}\d{3}\d{4}$/)
                ])
            ],

            City: [
                '', Validators.compose([
                    Validators.required,
                    Validators.maxLength(20),
                ])
            ],
            Password: [
                   '', Validators.compose([
                       Validators.required,
                       //Validators.minLength(8),
                       //Validators.maxLength(20),
                       Validators.pattern(/^(?=.*\d)(?=.*[A-Z])(?!.*[^a-zA-Z0-9@#$^+=])(.{8,20})$/)
                   ])
               ]
        });
    }
    
    ionViewWillEnter = () => {
        this.menu.enable(false);
        this.rank = 0;
        if( this.platform.is('cordova') ){
            this.signUpData.signupType = "APP";
        }else{
            this.signUpData.signupType = "WEB";
        }
        this.translateTheText().then(()=>{});
        this.isAuthorized();
    }
    
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                console.log("login isAuthorized not ==============================>");
            }else{
                if( this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN ){
                    this.navCtrl.setRoot( 'SADashboardPage' );
                }else if( this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN ){
                    this.navCtrl.setRoot( 'HADashboardPage' );
                }else if( this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR ){
                    this.navCtrl.setRoot( 'DoctorDashboardPage' );
                }else if( this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT ){
                    this.navCtrl.setRoot( 'PatientRoleHomePage' );
                }else if(this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_STAFF){
                    this.navCtrl.setRoot( 'SRDashboardPage');
                }
            }
        });
    }
    
    /*
     * Function to translate the text used through components
     * */
    translateTheText = () => {
       return new Promise((resolve, reject) => {
            let promisesArr = [];
            
            //Set placeholder labels from translated files
            promisesArr.push(this.commonService.translateText("FN_LABEL").toPromise().then((translatedText)=>{
                this.firstNamePlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("LN_LABEL").toPromise().then((translatedText)=>{
                this.lastNamePlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("OPTIONAL_LABEL").toPromise().then((translatedText)=>{
                this.lastNamePlaceholder = this.lastNamePlaceholder + " " + translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("USERNAME_LABEL").toPromise().then((translatedText)=>{
                this.usernamePlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("MOBILE_LABEL").toPromise().then((translatedText)=>{
                this.mobilePlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("EMAIL_LABEL").toPromise().then((translatedText)=>{
                this.emailPlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("OPTIONAL_LABEL").toPromise().then((translatedText)=>{
                //this.emailPlaceholder = this.emailPlaceholder + " " + translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("CITY_LABEL").toPromise().then((translatedText)=>{
                this.cityPlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("PASSWORD_LABEL").toPromise().then((translatedText)=>{
                this.passwordPlaceholder = translatedText;
            }));
            
            Promise.all(promisesArr).then( () => {
                resolve();
            });
        });
    }
    
    /*
     * Function to handle toogle behaviour of the password field
     * */
    showPasswordToggle = () => {
        console.log( "in toggle" );
        this.commonService.passwordToggle( this.passwordEyeIcon,this.inputType, ( eyeIcon,inputType ) => {
            this.passwordEyeIcon = eyeIcon;
            this.inputType = inputType;
        });
    }
    
    /*
     * Function to sign up user to app and save the sign up details to backend
     * */
    doSignUp = () => {
        console.log("doSignUp clicked ==========================>",this.signUpData);
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.authService.signup( this.signUpData ).subscribe(
            res => {
                this.handleResponse( res );
                if(res.status == 'success'){
                    console.log("doSignUp clicked res==========================>",res);
                    if(this.isUpload){
                        this.uploadImage( res.data.userId );
                    }
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }
    
    /**
     * Function to handle response of sign up web service
     * param: response
     */
    private handleResponse = ( res: HttpResponseDTO ) => {
        console.log( "in handleResponse");
        if ( res.status == "success" ) {
            this.commonService.hideLoading();
            this.commonService.setInStorage( 'userData',res.data );
            this.locstr.setObj('accessToken', res.data.token);
            this.commonService.setInStorage( 'userActiveRole',res.data.role );
            this.commonService.setActiveUserRole( res.data.role );
            this.evts.publish('fire-after-login-event');
            this.navCtrl.setRoot( 'PatientRoleHomePage' );
        }
        else {
            console.log( "error has occured" );
            this.commonService.hideLoading();
        }
    }
    
   
    /*
     * Function to handle sign up submit/click event
     * */
    
    public onSubmit = (form:any) =>{
        this.isSubmit =true;
        if(form.valid){
            console.log( "error has occured" ,form);
            this.doSignUp();
        }
        else{}
    }
    
    /*
     * Function to handle click event on profile photo
     * */
    public addProfilePhoto =()=>{
        this.commonService.presentToast();
    }
    
    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault: boolean, isUpload: boolean) {
        this.rank++;
        this.isDefault = isDefault;
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        var that = this;
        this.imageUpload = {
            "file":"",
            "fileName":""
        }
        myReader.onloadend = (loadEvent:any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback)=>{
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if( callback ){
                    this.isUpload = isUpload;
                }
            });
        };
         myReader.readAsDataURL(file);
    }

     /**
     * Function to upload image 
     **/
     uploadImage = ( userId: number ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                    console.log("upload image sys admin=================", res);
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
        });
    }
    
     /***
      * Function to open camera/photo gallery
      */
      protected onCamera = () => {
          try{
             // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
              this.cameraService.loadImage(this.successCallback,this.errorCallback);
          }catch(e){
              
          }
      }

      /**
       * Function to handle camera success callback
       * @param success
       */
      private successCallback = ( base64Image: any ) => {
          this.isUpload = true;
          this.rank++;
          this.isDefault = true;
          //this.imageUpload.file = base64Image;
          this.commonService.convertImgToBase64(base64Image, (callback)=>{
              this.imageUpload.file = callback;
          })
      }
      
      /**
       * Function to handle camera error callback
       * @param error
       */
      private errorCallback = ( error: any ) => {
         console.log( 'Unable to load profile picture.' ,error);
     }
      
      /*
       * Function to show privacy policy
       **/
      openPrivacyPolicy( title:string ){
          let popover = this.commonService.showPrivacyPolicyAlert( title , "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
          this.commonService.closePopupOnBackBtn(popover);
       }
}


