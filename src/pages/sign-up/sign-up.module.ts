import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SignUpPage } from './sign-up';
 
@NgModule({
    declarations: [
       SignUpPage,
    ],
    imports: [
       IonicPageModule.forChild(SignUpPage),
       TranslateModule.forChild()
    ],
    exports: [
       SignUpPage
    ]
})
export class SignUpPageModule {}
