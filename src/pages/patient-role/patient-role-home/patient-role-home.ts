import { Component, ViewChild, NgZone, HostListener } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events, Content, ModalController, Platform} from 'ionic-angular';
import { MapServiceProvider } from "../../../providers/map-integration-service/map-integration-service";
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorListDTO, HospitalListDTO } from '../../../interfaces/user-data-dto';
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CallNumber } from "@ionic-native/call-number";
import { SearchFilterComponent } from "../../components/search-filter/search-filter";

@IonicPage( {
    name: 'PatientRoleHomePage',
    segment: 'patient-role-home/:id'
} )
@Component( {
    selector: 'page-patient-role-home',
    templateUrl: 'patient-role-home.html',
} )
export class PatientRoleHomePage {
    searchedData: any;
    setHospitalFilter: any;
    @ViewChild( Content ) content: Content;
    
    cityId: any;
    city:any
    cities: any[];
    listOfDoctors: DoctorListDTO[];
    currentSegment: any;
    hospitals: HospitalListDTO[];
    userData: UserDataDTO = {};
    hospitalAvailable: any;
    doctorPageNo: number = 0;
    hospitalPageNo: number = 0;
    patientId: any = "";
    patientData:UserDataDTO = {};
    
    loadFinishedDoctor: boolean = false;
    loadFinishedHospital: boolean = false;
    
    isPatientDoctorList:any;
    isAssociateDoctorlist:any;
    isSADoctorlist:any;
    public startingTime:any;
    public endingTime:any;
    public startTimeSuffix = "am";
    public endTimeSuffix = "am";
    todaysAvailArray:any;
    searchDocText: any;
    searchHospitalText: any;
    protected address:any;
    protected matchCity:any;
    protected isCityChanges:boolean = false;
    showSearchResultContainer: boolean = false;
    fromString: string;
    seeAllClicked: boolean = false;
    isSearchHeader:boolean = false;
    isSkipAndExplore=false;
    popover:any;
    searchFilterComponent:any;
    fromPROnboardPatient:any;
    fromPage:any;
    
    constructor( private evts: Events, public modalCtrl: ModalController, private location: Location, public platform: Platform, private _ngZone: NgZone, public mapService : MapServiceProvider, public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
        private constants: Constants, private patientRoleService: PatientRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider, private callNumber: CallNumber ) {
   
    }

    @HostListener('document:click', ['$event'])
    andClickEvent(event) { 
        this.showSearchResultContainer = false;
    }
    
    hideSearchContainer(event: Event) {
        if (event && event.stopPropagation) {
            event.stopPropagation();
        }
    }
    
    ionViewDidLoad() {
        this.menu.enable( true );
        /**
         *handle device back button
         */
         this.platform.registerBackButtonAction(() => {
             if(this.showSearchResultContainer){
                 this.showSearchResultContainer = false;
             }
           });
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        this.getListOfCities();
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        this.currentSegment = 'doctorList';
        this.fromString = "patientRoleDoctorList";
        this.evts.subscribe( 'system-admin-search-more-hospitals', (data)=>{
            this.showSearchResultContainer = false;
            if(this.currentSegment == 'doctorList'){
                this.listOfDoctors = data.searchObj;
                this.formatDoctorAvailability();
            } else{
                this.hospitals = data.searchObj;
                this.formatAvailability();
            }
            this.seeAllClicked = true;
        });
        //remove appointment confirmed status from local storage
        this.commonService.removeFromStorage('isConfirmed');
        this.commonService.getFromStorage('isSkipAndExplore').then((value)=>{
            this.isSkipAndExplore =value;
        })
    }
    
    ionViewDidLeave(){
        if(this.searchFilterComponent){
            this.searchFilterComponent.dismiss();
        }
    }
    

    /* To hide search result popup on widow click
     **/
    public hideSerchContainer = (e) => {
     console.log("e.................", e);
        //this.showSearchResultContainer = false;
    }
    
    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public plotAddressOnMap = ( hospital: any ) => {
        let currentHospitalAddress = hospital.name+ "+" +hospital.address.street + "+" + hospital.address.cityName + "+" + 
                                     hospital.address.state + "+" + hospital.address.zipCode;
        this.commonService.setAddressOnMap(currentHospitalAddress);
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                if(this.navParams.get('fromPage')){
                    this.fromPage = this.navParams.get('fromPage');
                    console.log("FROM:",this.fromPage);
                    if(this.fromPage == 'PROnboardPatient'){
                        this.fromPROnboardPatient = true;
                    }else{
                        this.fromPROnboardPatient = false;
                    }
                }
                this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_PATIENT );
                this.commonService.setActiveUserRole( this.constants.ROLE_PATIENT );
                this.commonService.fireSelectedEvent( '#/patient-role-home/0' );
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_PATIENT );
                this.evts.publish('fire-after-login-event');
                this.getPatientProfile();
            }
        } );
    }

    /*
     * Function to get doctor details based on id
     * */
    getPatientProfile = () => {
        if( this.navParams.get('id') && this.navParams.get('id') != ":id" && this.navParams.get('id') != "0" ){
            this.patientId = this.navParams.get('id');
            this.commonService.setInStorage("patientId", this.patientId );
            this.patientRoleService.getPatientProfile(this.patientId).subscribe(res=>{
                this.patientData = res.data;
                this.commonService.setInStorage("patientId", res.data.userId);
            }, err=>{
                let errorMsg = err.message ? err.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                if(!this.isSkipAndExplore){
                    this.commonService.presentToast(errorMsg);
                }
//                this.commonService.showAlert("Error", err.message);
            });
        }else{
            this.commonService.getFromStorage("patientId").then(( value ) => {
                if( value ) {
                    this.patientId = value;
                }else{
                    this.patientId = "";
                }
                this.patientRoleService.getPatientProfile(this.patientId).subscribe(res=>{
                    this.patientData = res.data;
                    this.commonService.setInStorage("patientId", res.data.userId);
                }, err=>{
                    let errorMsg = err.message ? err.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                   
                    if(!this.isSkipAndExplore){
                        this.commonService.presentToast(errorMsg);
                    }
                });
            });
        }
    }
    
    /**
     * Function To get List of Cities
     **/
    getListOfCities = () => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.commonService.getListOfCities().subscribe(
            res => {
                this.cities = res.data.list;
                console.log("cities..", this.cities);
                if(this.cities && this.cities.length > 0 ){
                    this.city = {id:this.cities[0].id,name:this.cities[0].name};
                    this.cityId = this.cities[0].id;
                    console.log("cityId..",  this.cityId);
                }else{
                    this.cityId="";
                }
                console.log("cityId getcities..",  this.cityId);
                this.commonService.getFromStorage("SelectedPatientCity").then((value)=>{
                    if(value){
                        this.address = value;  
                        this.city = {id:this.address.id,name:this.address.name};
                        this.searchCity(this.address.name,this.address.id); 
                        console.log("address local storage..",  this.address);
                        this.commonService.setInStorage("SelectedPatientCity", this.address );
                    
                    }
                    else{
                        this.getCurrentLocationAddress();
                    }
                 });
//                this.getCurrentLocationAddress();
                this.commonService.setPaddingTopScroll( this.content );
            }, error => {
                this.cityId="";
                console.log( "error....", error );
                this.commonService.setPaddingTopScroll( this.content );
            } );
    }
    
    /**
     * Function for get Current Location Address
     * */
    protected getCurrentLocationAddress = () => {
        this.mapService.getCurrentLocation( response => {
            if ( response && response.coords && response.coords.latitude && response.coords.longitude ) {
                this.mapService.reverseGeocoding( response.coords.latitude, response.coords.longitude, ( status, result ) => {
                    if ( status ) {
                        this._ngZone.run(() => {
                            this.address = result.address;
                            this.commonService.setInStorage("SelectedPatientCity", this.address );
                            this.searchCity(this.address,this.cityId); 
                        });
                    } else {
                        this.getDoctorList(this.cityId,true);  
                    }
                });
            } else {
                console.log( 'error2==>' );
                this.getDoctorList(this.cityId,true);
            }
        }, error => {
            console.log( 'error3==>' );
            this.getDoctorList(this.cityId,true);
        });
    }


    /*
     * function to find city 
     */
    public searchCity = (city:any,cityId:any) => {
        if(this.address!=undefined){
            this.matchCity = this.cities.find( item => item.name === this.address );
            if(this.matchCity){
                this.cityId = this.matchCity.id;  
            }else{
                this.cityId = cityId;
            }
        }else{
            this.cityId = cityId; 
        }
        this.getDoctorList( this.cityId,true );
    }
    
    onCityChange = (cityId:any) => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.isCityChanges = true;
        this.cityId = cityId;
        
        for(let i=0; i<this.cities.length; i++){
            if(this.cities[i].id == cityId){
            this.matchCity = this.cities[i];
            this.commonService.setInStorage("SelectedPatientCity",this.matchCity );
            }
        }
        
        
        if(this.currentSegment == 'doctorList'){
            this.loadFinishedDoctor = false;
            this.fromString = "patientRoleDoctorList";
            this.getDoctorList( this.cityId,true );
        }else{
            this.loadFinishedHospital =false;
            this.fromString = "patientRoleHospitalList";
            this.getHospitalList( this.cityId,true );
        }
     
    }



    onCityChangeMobile= (event:any) =>{
        // this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        // this.cityId = event.value;
        // for(let i=0; i<this.cities.length; i++){
        //     if(this.cities[i].id == event.value){
        //         this.matchCity = this.cities[i];
        //         this.commonService.setInStorage("SelectedHospCity",this.matchCity );
        //     }
        // }
        // this.loadFinished = false;
        // this.getHospitalList(event.value.id, true );


        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.isCityChanges = true;
        this.cityId = event.value.id;
        
        for(let i=0; i<this.cities.length; i++){
            if(this.cities[i].id == event.value.id){
            this.matchCity = this.cities[i];
            this.commonService.setInStorage("SelectedPatientCity",this.matchCity );
            }
        }
        
        
        if(this.currentSegment == 'doctorList'){
            this.loadFinishedDoctor = false;
            this.fromString = "patientRoleDoctorList";
            this.getDoctorList( this.cityId,true );
        }else{
            this.loadFinishedHospital =false;
            this.fromString = "patientRoleHospitalList";
            this.getHospitalList( this.cityId,true );
        }
      }


    protected doctorListClick = () =>{
        this.loadFinishedDoctor = false;
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.currentSegment = 'doctorList';
        this.fromString = "patientRoleDoctorList";
        this.getDoctorList( this.cityId,true );
        this.isSearchHeader = false;
    }
    protected hospitalListClick = () =>{
        this.loadFinishedHospital =false;
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.currentSegment = 'hospitalList';
        this.fromString = "patientRoleHospitalList";
        this.getHospitalList( this.cityId,true );
        this.isSearchHeader = false;
    }
    /**
     * Format doctor availability time
     **/
    
    formatDoctorAvailability = () => {
        console.log("in formatDoctorAvailability")
        var currentDate = new Date();
        for ( let i = 0; i < this.listOfDoctors.length; i++ ) {
            var todayAvailability;
            let availabilities = this.listOfDoctors[i].timeAvailabilityList;
            //let todaysTimeAvailabilityList = this.listOfDoctors[i].todaysTimeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime( availabilities );
            this.listOfDoctors[i].formattedAvailabilities = this.commonService.calculateTime( availabilities );
            let availabilityLength = this.listOfDoctors[i].formattedAvailabilities.length;
            this.listOfDoctors[i].todayAvailability = this.listOfDoctors[i].todaysTimeAvailabilityList;
            if(this.listOfDoctors[i].todaysTimeAvailabilityList && this.listOfDoctors[i].todaysTimeAvailabilityList.length > 0){
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = this.listOfDoctors[i].todaysTimeAvailabilityList.length;
                for(let j=0; j<todayTimeLen; j++){
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].toTime), true);
                    if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                        timeSlotFound = true;
                        this.listOfDoctors[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if(!timeSlotFound){
                    this.listOfDoctors[i].todaysAvailabilityString = "Unavailable";
                }
//                this.listOfDoctors[i]s.todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[0].fromTime)) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[0].toTime));
            } else{
                this.listOfDoctors[i].todaysAvailabilityString = "Unavailable";
            }
        }
    }
    
    /* function to get images in async */ 
    public getImageBaseDoctorList =()=>{
        let startIndex = this.doctorPageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.listOfDoctors.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.listOfDoctors[i].doctorId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.listOfDoctors.length; j++ ){
                    if(this.listOfDoctors[j].doctorId == res.data.userId){
                        this.listOfDoctors[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.listOfDoctors);
            }
        },
        error => {
            this.commonService.hideLoading();
            /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }

    /**
     * Get doctors list to associate them with particular hospital
     **/
    getDoctorList( cityId: any,firstDoctorPageCall?: boolean ) {
        //this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.patientRoleService.getDoctorsList( cityId,this.doctorPageNo, this.constants.ROLE_PATIENT ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempDoctorPageNo = this.doctorPageNo;
                if( res.data.list && res.data.list.length > 0 && firstDoctorPageCall ){
                    this.listOfDoctors = res.data.list;
                    this.getImageBaseDoctorList();
                    if ( ( tempDoctorPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinishedDoctor = true;
                    }
                    this.formatDoctorAvailability();
                }else if( res.data.list && res.data.list.length > 0 ){
                    for( let j=0;j< res.data.list.length;j++ ){
                        this.listOfDoctors.push(res.data.list[j]);
                    }
                    this.getImageBaseDoctorList();
                    if ( ( tempDoctorPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinishedDoctor = true;
                    }
                    this.formatDoctorAvailability();
                }else{
                    this.loadFinishedDoctor = true;
                    if(res.data.list.length ==0){
                        this.listOfDoctors = [];
                    }
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                if(!this.isSkipAndExplore){
                this.commonService.presentToast(errorMsg);
                }
            }
        );
    }
    
    /*
     * Function for load more(pagination) of doctor data
     * */
    loadMoreDoctorData = () => {
        this.doctorPageNo = this.doctorPageNo + 1;
        this.getDoctorList(this.cityId);
    }
    
    /*
     * Function to do calculations for time related 
     * */
    formatAvailability = () => {
        var currentDate = new Date();
        var today = currentDate.getDay();
        let hospitalLength = this.hospitals.length;
        for(let i=0; i< hospitalLength; i++){
            var todayAvailability;
            let availabilities = this.hospitals[i].timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime(availabilities);
            this.hospitals[i].formattedAvailabilities = this.commonService.calculateTime(availabilities);
            let availabilityLength = this.hospitals[i].formattedAvailabilities.length;
            
            this.hospitals[i].todayAvailability = this.hospitals[i].todaysTimeAvailabilityList;
            if(this.hospitals[i].todaysTimeAvailabilityList && this.hospitals[i].todaysTimeAvailabilityList.length > 0){
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = this.hospitals[i].todayAvailability.length;
                for(let j=0; j< todayTimeLen; j++){
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].toTime), true);
                
                    if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                        timeSlotFound = true;
                        this.hospitals[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if(!timeSlotFound){
                    this.hospitals[i].todaysAvailabilityString = "Closed";
                }
            } else{
                this.hospitals[i].todaysAvailabilityString = "Closed";
            }
            if(this.hospitals[i].todaysAvailabilityString == "00:00AM - 11:59PM"){
                this.hospitals[i].todaysAvailabilityString = "24 Hrs Open";
            }
        }
    }


    /**
     * Function to show Timing popup
     **/
    openTimingDetails = ( hospital ) => {
        let title = hospital.name;
        let subTitle = hospital.address.street + ", " + hospital.address.cityName;
        this.popover = this.commonService.showTimingList( title, subTitle, hospital.formattedAvailabilities );
        this.commonService.closePopupOnBackBtn(this.popover);
    }
    
    /* function to get images in async */ 
    public getImageBaseHospitalList =()=>{
        let startIndex = this.hospitalPageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.hospitals.length; i++) {
            let imageData = {
                    'userType': "HOSPITAL_IMAGE",
                    'id': this.hospitals[i].hospitalId,
                    'rank': 1,
                    'isDefault': true
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.hospitals.length; j++ ){
                    if(this.hospitals[j].hospitalId == res.data.userId){
                        this.hospitals[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.hospitals);
            }
        },
        error => {
            this.commonService.hideLoading();
           /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }

    /**
       * Function To get List of Hospitals
       **/
    getHospitalList( cityId: any,firstHospitalPageCall?: boolean ) {
        //this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.manageHospitalServiceProvider.getHospitalList( cityId,this.hospitalPageNo ).subscribe( res => {
            this.commonService.hideLoading();
            let tempHospitalPageNo = this.hospitalPageNo;
            if( res.data.list && res.data.list.length > 0 && firstHospitalPageCall ){
                this.hospitals = res.data.list;
                this.getImageBaseHospitalList();
                if ( ( tempHospitalPageNo + 1 ) >= res.data.totalPages ) {
                    this.loadFinishedHospital = true;
                }
                this.formatAvailability();
            }else if( res.data.list && res.data.list.length > 0 ){
                for( let j=0;j< res.data.list.length;j++ ){
                    this.hospitals.push(res.data.list[j]);
                }
                this.getImageBaseHospitalList();
                if ( ( tempHospitalPageNo + 1 ) >= res.data.totalPages ) {
                    this.loadFinishedHospital = true;
                }
                this.formatAvailability();
            }else{
                this.loadFinishedHospital = true;
                if(res.data.list.length ==0){
                    this.hospitals = [];
                }
            }
        }, error => {
            this.commonService.hideLoading();
            let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            if(!this.isSkipAndExplore){
            this.commonService.presentToast(errorMsg);
            }
        } );
    }
   
    
    /*
     * Function for load more(pagination) of hospital data
     * */
    loadMoreHospitalData = () => {
        this.hospitalPageNo = this.hospitalPageNo + 1;
        this.getHospitalList(this.cityId);
    }
    
    /*
     * function to handle search
     * */
    onChangeSearchText = (mode) => {
        let searchText = "";
        let type = "";
        if(mode == undefined){
            mode = "";
        }
        if(mode == "doctor"){
            searchText = this.searchDocText;
        } else if(mode == "hospital"){
            searchText = this.searchHospitalText;
        }
        if( searchText.length > 2 ){
            if(!this.matchCity){
                this.matchCity = this.cities[0];
            }
            console.log("searchText at 463...", searchText);
            this.showSearchResultContainer = true;
            let searchObj = {
                searchText: searchText,
                city:  this.matchCity.name,
                currentDocIndex: 0,
                currentPage: 0,
                sortField:"",
                type: mode,
                filters:[{
                         filterName: "",
                         rangeFrom: "",
                         rangeTo: "",
                         selectedValue: [{}]
                    }]
            }
            
            this.commonService.getFromStorage("doctorFiltersArray").then((value)=>{
                if(value){
                    searchObj.filters = value;  
                }
                this.evts.publish( 'system-admin-search-hospitals',{searchObj: searchObj} );
             });
        }else{
            this.showSearchResultContainer = false;
            if(this.seeAllClicked && mode == "hospital"){
                this.getHospitalList(this.cityId, true);
            } else if(this.seeAllClicked && mode == "doctor"){
                this.getDoctorList(this.cityId,true);
            }
            this.evts.publish('unsub-system-admin-search-hospitals');
        }
    }
    
    /*
     * function to get filters
     * */
  
    getFilters(){
        if(!this.matchCity){
            this.matchCity = this.cities[0];
        }
        if(this.currentSegment == "doctorList"){
            let filtersForDoctor = this.commonService.getDoctorFilters(this.matchCity.name);
            this.searchFilterComponent = this.modalCtrl.create( SearchFilterComponent, filtersForDoctor, {showBackdrop:true, enableBackdropDismiss: true});
            this.searchFilterComponent.present();
            if(this.searchedData){
                if(this.searchedData.feesSlots){
                    filtersForDoctor.feesSlots = this.searchedData.feesSlots; 
                }
                if(this.searchedData.doctorExp){
                    filtersForDoctor.doctorExp = this.searchedData.doctorExp; 
                }
                if(this.searchedData.availability){
                    filtersForDoctor.availability = this.searchedData.availability;
                }
            }
            this.searchFilterComponent.onDidDismiss((data) => {
                if(data){
                    this.listOfDoctors = data.doctors;
                    this.searchedData = data.searchParams;
                    this.formatDoctorAvailability();
                }
            });
        } else{
            let filtersForHospital = this.commonService.getHospitalFilters(this.matchCity.name);
            if(this.setHospitalFilter && this.setHospitalFilter.allDayOpen){
                filtersForHospital.allDayOpen = this.setHospitalFilter.allDayOpen
            } else {
                filtersForHospital.allDayOpen = false;
            }
            if(this.setHospitalFilter && this.setHospitalFilter.multiSpeciality){
                filtersForHospital.multiSpeciality = this.setHospitalFilter.multiSpeciality;
            } else{
                filtersForHospital.multiSpeciality = false;
            }
            this.searchFilterComponent = this.modalCtrl.create( SearchFilterComponent, filtersForHospital, {showBackdrop:true, enableBackdropDismiss: true});
            this.searchFilterComponent.present();
            
            this.searchFilterComponent.onDidDismiss((data) => {
                if(data){
                    this.hospitals = data.hospitals;
                    this.setHospitalFilter = data.searchParams;
                    this.formatAvailability();
                }
            });
        }
    }
    
    /**
     * function to make a phone call
     **/
    makeCall(number){
        this.callNumber.callNumber(number, true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
    }

    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
       // console.log("on comingSoonPopup 288.....");
        this.commonService.presentToast();
    }
    /*
     * searchHeaderClick for mobile
     * 
     * */
    
    searchHeaderClick(){
        this._ngZone.run(()=>{
            this.isSearchHeader = true; 
        });
    }
    
    
    manageHospital = ( currentHospital ) => {
        //console.log( "currentHospital",currentHospital );
    }
    
}