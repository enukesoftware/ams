import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { PatientRoleHomePage } from './patient-role-home';

import { SharedModule } from "../../components/shared.module";
import { IonicSelectableModule } from 'ionic-selectable';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    PatientRoleHomePage,
  ],
  imports: [
    IonicPageModule.forChild(PatientRoleHomePage),
    SharedModule,
    IonicSelectableModule,
    DropdownModule,
    TranslateModule.forChild()
  ],
  exports: [
    PatientRoleHomePage
  ]
})
export class PatientRoleHomePageModule {}
