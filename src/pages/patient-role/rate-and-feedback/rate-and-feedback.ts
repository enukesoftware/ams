/**
 * rate and feedback controller module
 * rate and feedback related functionality
 * Created By : #1166
 * Date :29 August. 2018
 */

/*------------------------ angular component ----------------------------*/
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

/*------------------------ Providers ----------------------------*/
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';
import { UserDataDTO,FeedbackDataDTO,DoctorDataDto,HospitalDTO} from '../../../interfaces/user-data-dto';
import { DoctorRoleServiceProvider } from "../../../providers/doctor-role-service/doctor-role-service";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";


@IonicPage({
    name: 'RateAndFeedbackPage',
    segment: 'rate-and-feedback/:mode/:doctorId/:hospitalId/:appointmentId'
})
@Component({
  selector: 'page-rate-and-feedback',
  templateUrl: 'rate-and-feedback.html',
})
export class RateAndFeedbackPage {
    @ViewChild(Content) content: Content;
    userData: UserDataDTO = {};
    protected drRating: number = 3;
    protected hospitalRating: number = 3;
    protected mode:any;
    doctorId:any;
    hospitalId:any;
    appointmentId:any;
    doctorProfileId:any;
    doctorData: DoctorDataDto = {};
    hospitalData :HospitalDTO = {};
    feedbackData: FeedbackDataDTO = {};
    ratedBtn =false;
    patientId:any;
    pageNo: number = 0;
    pageName: any;
    
  constructor(public navCtrl: NavController, public navParams: NavParams,private commonService: CommonService,
              public doctorRoleServiceProvider: DoctorRoleServiceProvider,  private constants: Constants, public platform: Platform, public location: Location,
              public manageHospitalServiceProvider: ManageHospitalServiceProvider,private patientRoleService: PatientRoleServiceProvider) {
      this.mode = this.navParams.get("mode");
      this.doctorId = this.navParams.get('doctorId');
      this.hospitalId = this.navParams.get('hospitalId');
      this.appointmentId =  this.navParams.get('appointmentId');
      console.log('this.mode==>',this.mode);
      console.log('this.doctorId==>',this.doctorId);
      console.log('this.hospitalId==>',this.hospitalId);
      console.log('this.appointmentId==>',this.appointmentId);
      this.feedbackData.appointmentId = this.appointmentId;
      
      this.platform.registerBackButtonAction(() => {
          this.location.back();
      });
  }
  
  ionViewWillEnter = () => {
      this.isAuthorized();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RateAndFeedbackPage');
  }

  /*
   * Function to check if user is authorized or not to access the screen
   * */
  isAuthorized = () => {
      this.commonService.isLoggedIn(( result ) => {
          if ( !result ) {
              this.navCtrl.setRoot( 'LoginPage' );
          } else {
              this.userData = result;
                  console.log("on 55...");
                  this.commonService.fireSelectedEvent( '#/rate-and-feedback');
                  this.getPatientID();
                  this.getDoctorInfo(this.doctorId);
                  this.getHospitalDetails(this.hospitalId);
                  this.commonService.setPaddingTopScroll( this.content );
                  if(this.mode == "updateFeedback"){
                      this.getFeedback(this.appointmentId);
                  }
          }
      } );
  }
  
  
  
  /*
   * Function to get doctor details based on id
   * */
  /*
   * Function to get doctor details based on id
   * */
  getPatientID = () => {
      console.log(" in get patient profile");
      if( this.navParams.get('id') && this.navParams.get('id') != ":id" && this.navParams.get('id') != "0" ){
          console.log( "in getPatientProfile  if================>");
          this.patientId = this.navParams.get('id');
          this.commonService.setInStorage("patientId", this.patientId );
          console.log("this.patientId ",this.patientId);
          this.feedbackData.patientId = this.patientId;
      }else{
          console.log( "in getPatientProfile  else================>");
          this.commonService.getFromStorage("patientId").then(( value ) => {
              if( value ) {
                  this.patientId = value;
                  console.log(" patientId ", this.patientId);
                  this.feedbackData.patientId = this.patientId;
              }else{
                  this.patientId = "";
              }
          });
      }
  }
  
  
  /*
   * get Doctor Info
   * */
  
  getDoctorInfo(id){
      this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
      this.doctorRoleServiceProvider.getDoctorProfile(this.doctorId).subscribe(
          res => {
            this.commonService.hideLoading();  
            if(res.status == 'success'){
              this.doctorData = res.data;
              this.getImageBaseForDoctor();
              console.log("doctorData . .",this.doctorData);
              this.feedbackData.doctorProfileId = res.data.doctorProfileId;
            }
          }, error => {
              this.commonService.hideLoading();
              this.commonService.presentToast(error.message);
              console.log("error....", error);
          });
  }
  
  
  
  /* function to get images in async */ 
  public getImageBaseForDoctor =()=>{
      //let startIndex = this.pageNo * this.constants.PAGE_SIZE;
          let imageData1 = {
                  'userType': "USER_PROFILE_IMAGE",
                  'id': this.doctorData.doctorId,
                  'rank': 1,
           }   
      this.commonService.downloadImage( imageData1 ).subscribe(
      res => {
          console.log("on get image base res")
          if ( res.status == "success" ) {
              console.log("image base 64=======>", res);
              this.doctorData.imageBase64 = res.data.file;
          }
      },
      error => {
          this.commonService.hideLoading();
         /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
          this.commonService.presentToast(errorMsg);*/
  });
  }
  
  /*
   * get hospital details
   * */   
      getHospitalDetails = ( hospitalId: any ) => {
          this.manageHospitalServiceProvider.getHospitalDetails(this.hospitalId).subscribe(
              res => {
              if ( res.status == "success" ) {
                  this.hospitalData = res.data;
                  this.getImageBaseForHospital();
                  console.log("hospitalData",this.hospitalData);
                  this.feedbackData.hospitalId = res.data.hospitalId;
              }
              else {
                  this.commonService.hideLoading();
                  this.commonService.presentToast( res.message );
              }
                  this.commonService.setPaddingTopScroll( this.content );
            },
          
          error => {
              this.commonService.hideLoading();
              //this.commonService.showAlert( "Error", error.message );
              let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
              this.commonService.presentToast(errorMsg);
              this.commonService.setPaddingTopScroll( this.content );
          }
      );
  }
  
      /* function to get images in async */ 
      public getImageBaseForHospital =()=>{
          //let startIndex = this.pageNo * this.constants.PAGE_SIZE;
              let imageData1 = {
                      'userType': "HOSPITAL_IMAGE",
                      'id': this.hospitalData.hospitalId,
                      'rank': 1,
               }   
          this.commonService.downloadImage( imageData1 ).subscribe(
          res => {
              console.log("on get image base res")
              if ( res.status == "success" ) {
                  console.log("image base 64=======>", res);
                  this.hospitalData.imageBase64 = res.data.file;
              }
          },
          error => {
              this.commonService.hideLoading();
             /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
              this.commonService.presentToast(errorMsg);*/
      });
      }
  /*
   *function to  create feedback
   * */
      
  createFeedback = () => {
      if(!this.feedbackData.doctorFeedbackRating || !this.feedbackData.hospitalFeedbackRating){
          let feedbackRequiredMsg = this.constants.FEEDBACK_ERROR;
          this.commonService.presentToast(feedbackRequiredMsg);
      }else{
          this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
          this.patientRoleService.createFeedback(this.feedbackData).subscribe(
                  res => {
                      if ( res.status == "success" ) {
                          this.commonService.hideLoading();
                          this.commonService.presentToast( res.message );
                          console.log("res",res);
                          this.feedbackData.doctorFeedbackRating = this.feedbackData.doctorFeedbackRating;
                          this.feedbackData.hospitalFeedbackRating = this.feedbackData.hospitalFeedbackRating;
                          this.ratedBtn =true;
                          this.navCtrl.setRoot('AppointmentList',{fromPage:'patientHistory'});
                      }
                      else {
                          this.commonService.hideLoading();
                          this.commonService.presentToast( res.message );
                      }
                  },
                  error => {
                      this.commonService.hideLoading();
                      let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                      this.commonService.presentToast(errorMsg);
    
                  }
              );
      }
      }
  
  
  
      /*
       * function to get feedback  
       * */
      
      getFeedback = (appointmentId) => {
          console.log("feedbackData....",this.feedbackData);
          this.patientRoleService.getFeedback(appointmentId).subscribe(
                  res => {
                      if ( res.status == "success" ) {
                          this.commonService.hideLoading();
                          console.log("res feedback details..",res);
                          this.feedbackData =res.data;
                          console.log("res feedback details..", this.feedbackData);
                       /*   if(this.mode=="update"){
                              this.feedbackData.doctorFeedbackRating = this.feedbackData.doctorFeedbackRatingPoints;
                          }
                          else{
                              
                          }*/
                      }
                      else {
                          this.commonService.hideLoading();
                          this.commonService.presentToast( res.message );
                      }
                  },
                  error => {
                      this.commonService.hideLoading();
                      let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                      this.commonService.presentToast(errorMsg);
    
                  }
              );
      }
  
  
  
      /*
       *function to  update feedback
       * */
          
      updateFeedback = () => {
              this.feedbackData.appointmentId = this.appointmentId;
              console.log("feedbackData",this.feedbackData);
              this.feedbackData.doctorFeedbackRating = this.feedbackData.doctorFeedbackRatingPoints;
              this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
              this.patientRoleService.updateFeedback(this.feedbackData).subscribe(
                      res => {
                          if ( res.status == "success" ) {
                              this.commonService.hideLoading();
                              this.commonService.presentToast( res.message );
                              console.log("res updated code",res);
                              this.navCtrl.setRoot( 'PatientRoleFeedbackHistoryPage' );
                          }
                          else {
                              this.commonService.hideLoading();
                              this.commonService.presentToast( res.message );
                          }
                      },
                      error => {
                          this.commonService.hideLoading();
                          let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                          this.commonService.presentToast(errorMsg);
        
                      }
                  );
          }
  
}
