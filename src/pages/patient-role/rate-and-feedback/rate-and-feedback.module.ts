import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RateAndFeedbackPage } from './rate-and-feedback';
import { TranslateModule } from '@ngx-translate/core';
import {RatingModule} from 'primeng/rating';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    RateAndFeedbackPage,
  ],
  imports: [
    IonicPageModule.forChild(RateAndFeedbackPage),
    TranslateModule.forChild(),
    RatingModule,
    SharedModule
  ],
  exports: [
    RateAndFeedbackPage
  ]
})
export class RateAndFeedbackPageModule {}
