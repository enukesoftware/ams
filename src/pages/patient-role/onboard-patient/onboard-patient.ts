import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform, MenuController } from 'ionic-angular';
import { UserDataDTO, DoctorListDTO, PatientDependentDataDTO } from "../../../interfaces/user-data-dto";
import { AuthService } from "../../../providers/authService/authService";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CommonService } from "../../../providers/common-service/common.service";
import { Location } from '@angular/common';
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';
import * as _ from 'lodash';
// import {Router} from '@angular/router';

/**
 * Generated class for the OnboardPatientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
    name: 'OnboardPatientPage',
    segment: 'onboard-patient/:fromPage/:redirectPage/:redirectParam/:doctorId'
})
@Component({
    selector: 'page-onboard-patient',
    templateUrl: 'onboard-patient.html',
})
export class OnboardPatientPage {
    @ViewChild(Content) content: Content;

    fromPage: any;
    redirectPage: any;
    redirectParam: any;
    doctorId: any;
    userData: any = {};
    selectButtonTitle = "Select Doctor";
    // patientDependent = 
    isHospitalAdmin: boolean = false;
    isDoctorAdmin: boolean = false;
    isPatientAdmin = false;
    isHospitalStaff = false;
    isSearched = false;

    pageNo: number = 0;
    patientId: any;
    patient: PatientDependentDataDTO;
    patientDependents: any = [];
    selectedPatient: any;
    searchText: any;
    selectDoctorPageUrl: any;
    selectDoctorPageParams:any;
    dependentStorageKey: any;
    addPatientUrl: any;
    addPatientParams:any;

    mode: any = "create"

    constructor(private commonService: CommonService, private patientRoleService: PatientRoleServiceProvider, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private constants: Constants,
        private location: Location, public platform: Platform) {
        this.menu.enable(true);


    }

    ionViewDidLoad() {
        this.menu.enable(true);
        console.log('ionViewDidLoad OnboardPatientPage');

        // this.fromPage = this.navParams.get('fromPage');


    }

    ionViewWillEnter = () => {
        this.menu.enable(true);

        this.isAuthorized();
    }



    getNavigationData() {
        this.fromPage = this.navParams.get('fromPage');
        this.redirectPage = this.navParams.get('redirectPage');
        this.redirectParam = this.navParams.get('redirectParam');
        this.doctorId = this.navParams.get('doctorId');
        console.log("ONBOARD:","FROM:"+this.fromPage + " REDIRECT:" + this.redirectPage + " PARAM:"+ this.redirectParam + " DOCTOR:"+this.doctorId);
        this.setAdminRole();
        this.setPageNavigationData();
    }

    setAdminRole() {
        this.commonService.getFromStorage('userActiveRole').then((value) => {
            if (value) {
                console.log("ROLE:", value);

                if (value == this.constants.ROLE_SYSTEM_ADMIN) {

                } else if (value == this.constants.ROLE_HOSPITAL_ADMIN) {
                    this.isHospitalAdmin = true;
                    this.isDoctorAdmin = false;
                    this.isPatientAdmin = false;
                    this.isHospitalStaff = false;
                    if (this.doctorId == "0") {
                        this.selectButtonTitle = "Select Doctor";
                    } else {
                        this.selectButtonTitle = "Book Appointment";
                    }

                    this.dependentStorageKey = "HospitalDependents";
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN, true);
                    this.getPatientDependents(this.dependentStorageKey);

                } else if (value == this.constants.ROLE_DOCTOR) {
                    this.isHospitalAdmin = false;
                    this.isDoctorAdmin = true;
                    this.isPatientAdmin = false;
                    this.isHospitalStaff = false;
                    this.selectButtonTitle = "Book Appointment";
                    this.dependentStorageKey = "DoctorDependents";
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_DOCTOR, true);
                    this.getPatientDependents(this.dependentStorageKey);

                } else if (value == this.constants.ROLE_PATIENT) {
                    this.isHospitalAdmin = false;
                    this.isDoctorAdmin = false;
                    this.isPatientAdmin = true;
                    this.isHospitalStaff = false;
                    if (this.doctorId == "0") {
                        this.selectButtonTitle = "Select Doctor";
                    } else {
                        this.selectButtonTitle = "Book Appointment";
                    }
                    this.dependentStorageKey = "PatientDependents";
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_PATIENT, true);
                    this.getPatientDependents(this.dependentStorageKey);

                }else if (value == this.constants.ROLE_HOSPITAL_STAFF) {
                    this.isHospitalAdmin = false;
                    this.isDoctorAdmin = false;
                    this.isPatientAdmin = false;
                    this.isHospitalStaff = true;
                    if (this.doctorId == "0") {
                        this.selectButtonTitle = "Select Doctor";
                    } else {
                        this.selectButtonTitle = "Book Appointment";
                    }
                    this.dependentStorageKey = "HospitalStaffDependents";
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_STAFF, true);
                    this.getPatientDependents(this.dependentStorageKey);

                }

            }
        })
        // if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            
        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            

        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
           
        // }
    }

    setPageNavigationData() {
        if (this.fromPage) {
            this.addPatientUrl = 'AddPatientPage'
            this.addPatientParams = {
                fromPage:'OPAddPatient',
                previousFromPage:this.fromPage,
                redirectPage:this.redirectPage,
                redirectParam:this.redirectParam,
                doctorId:this.doctorId,
                mode:this.mode
            }
            if ((this.fromPage == "HADashboard" 
            || this.fromPage == "patientHome" 
            || this.fromPage == "HAAssociatedDoctorList"
            || this.fromPage == "patientHospitalProfile"
            || this.fromPage == "doctorRoleUpcomingAppointment"
            || this.fromPage == "doctorRoleUpcomingAppointmentVDBook"
            || this.fromPage == "doctorRoleAppointmentHistory"
            || this.fromPage == "doctorRoleAppointmentHistoryVisitDetailsBookAgain"
            || this.fromPage == "HAManageAppointment"
            || this.fromPage == "HAManageAppointmentVDBook" 
            || this.fromPage == "patientUpcomingAppointmentBookAgain" 
            || this.fromPage == "patientUpcomingAppointment" 
            || this.fromPage == "patientUpcomingAppointmentViewDetailsBookAgain" 
            || this.fromPage == "patientAppointmentHistory" 
            || this.fromPage == "HAUpdateVisitDetails" 
            || this.fromPage ==  "DoctorRoleUpdateVisitDetails" 
            || this.fromPage == "patientUpcomingAppointmentViewDetails"
            ) && this.redirectPage == "bookStep1") {
                // this.commonService.fireSelectedEvent('#/onboard-patient/' + this.fromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                // this.selectDoctorPageUrl = "#/booking-step1/" + this.doctorId + "/" + this.fromPage + "/" + this.redirectParam;;
                this.selectDoctorPageUrl = "BookingStep1Page";
                this.selectDoctorPageParams = {
                    doctorId: this.doctorId,
                    from: this.fromPage,
                    editParam:this.redirectParam
                  };
            } else if (this.fromPage == "doctorDashboard" && this.redirectPage == "bookStep1") {
                // this.commonService.fireSelectedEvent('#/onboard-patient/' + this.fromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                // this.selectDoctorPageUrl = "#/booking-step1/" + this.doctorId + "/doctorRoleUpcomingAppointment/" + this.redirectParam;;

                this.selectDoctorPageUrl = "BookingStep1Page";
                this.selectDoctorPageParams = {
                    doctorId: this.doctorId,
                    from: 'doctorRoleUpcomingAppointment',
                    editParam:this.redirectParam
                  };

            } 
            else if (this.fromPage == "HADashboard") {

                this.commonService.fireSelectedEvent('#/onboard-patient/' + this.fromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                // this.selectDoctorPageUrl = "#/hospital-admin-associated-doctor-list";
                this.selectDoctorPageUrl = 'HospitalAdminAssociatedDoctorListPage';
                this.selectDoctorPageParams ={
                    fromPage:'HAOnboardPatient'
                };

            } else if (this.fromPage == "doctorDashboard") {

                this.commonService.fireSelectedEvent('#/onboard-patient/' + this.fromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                // this.selectDoctorPageUrl = "#/booking-step1/" + this.doctorId + "/doctorRoleUpcomingAppointment/" + this.redirectParam;;
                this.selectDoctorPageUrl = "BookingStep1Page";
                this.selectDoctorPageParams = {
                    doctorId: this.doctorId,
                    from: 'doctorRoleUpcomingAppointment',
                    editParam:this.redirectParam
                  };

            } else if (this.fromPage == "patientHome") {
                
                this.commonService.fireSelectedEvent('#/onboard-patient/' + this.fromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                // this.selectDoctorPageUrl = "#/patient-role-home/0";
                this.selectDoctorPageUrl = 'PatientRoleHomePage';
                this.selectDoctorPageParams ={
                    id:"0",
                    fromPage:'PROnboardPatient'
                };
            } else if (this.fromPage == "HSRoleDashboard") {
                this.commonService.fireSelectedEvent('#/onboard-patient/HSRoleDashboard/HSRoleAssociatedDoctorList/0/0');
                // this.commonService.fireSelectedEvent('#/onboard-patient/' + this.fromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                // this.selectDoctorPageUrl = "#/patient-role-home/0";
                this.selectDoctorPageUrl = 'HospitalStaffAssociatedDoctorListPage';
                this.selectDoctorPageParams ={
                    fromPage:'HSRoleOnboardPatient'
                };
                // fromPage:'HSRoleDashboard'
            }
            else if (this.fromPage == "HSRoleAssociatedDoctorList" || this.fromPage == "HSRoleManageAppointment") {

                // this.commonService.fireSelectedEvent('#/onboard-patient/' + this.fromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                this.commonService.fireSelectedEvent('#/onboard-patient/HSRoleDashboard/HSRoleAssociatedDoctorList/0/0');
                

                this.selectDoctorPageUrl = "BookingStep1Page";
                this.selectDoctorPageParams = {
                    doctorId: this.doctorId,
                    from: 'HSRoleOnboardPatient',
                    editParam:this.redirectParam
                  };
            }
        } else {
            this.fromPage = ""
        }
    }



    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if (!result) {
                this.navCtrl.setRoot('LoginPage');
            } else {

                this.userData = result;

                console.log("USER:", this.userData);
                this.commonService.setPaddingTopScroll(this.content);
                
                this.getNavigationData();
                // if(this.navParams.get('id') && this.navParams.get('id') != 0 ){
                //     this.doctorId = this.navParams.get('id');

                //     this.getDoctorInfo(this.doctorId);
                //     this.getAssociatedHospitals(this.doctorId ,true);
                //     this.getDoctorFeedbacks(this.doctorId);
                // }else{
                //     this.commonService.getFromStorage( "patientRoleDoctorId" ).then(( value ) => {
                //         if ( value ) {
                //             this.doctorId = value;

                //             this.getDoctorInfo(this.doctorId);
                //             this.getAssociatedHospitals(this.doctorId ,true);
                //             this.getDoctorFeedbacks(this.doctorId);
                //         } else {
                //             this.doctorId = "";
                //         }
                //     } );
                // }

                console.log("ACTIVE:", this.commonService.getActiveUserRole());

            }
        });
    }

    /*
     * Function to get doctor details based on id
     * */
    getPatientProfile = () => {
        let patientId = "";

        this.commonService.getFromStorage("patientId").then((value) => {
            if (value) {
                patientId = value;
            } else {
                patientId = "";
            }

            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.patientRoleService.getPatientProfile(patientId).subscribe(res => {

                if (res.status == "success") {
                    this.commonService.hideLoading();
                    if (res.data) {
                        if (res.data.contactNumber) {
                            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
                            this.searchPatientFromPatient(res.data.contactNumber)
                        } else if (res.data.userName) {
                            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
                            this.searchPatientFromPatient(res.data.userName)
                        } else if (res.data.email) {
                            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
                            this.searchPatientFromPatient(res.data.email)
                        }

                    }
                } else {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }

                // this.patient = res.data;
                // console.log("PAT:",this.patient);

                // this.patient.relation = this.constants.RELATION_SELF_VALUE;

                // this.patientDependents = this.getSampleData();

                // this.patientDependents.forEach(element => {
                //     if (element.relation == this.constants.RELATION_SELF_VALUE) {
                //         element.isSelected = true;
                //     } else {
                //         element.isSelected = false;
                //     }
                // });

                // this.patientDependents.forEach(element => {
                //     if (element.isSelected) {
                //         this.patient = element;
                //     }
                // });


                // this.getImageBase();

            }, err => {
                this.commonService.hideLoading();
                let errorMsg = err.message ? err.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                // if(!this.isSkipAndExplore){
                //     this.commonService.presentToast(errorMsg);
                // }
            });
        });

    }

    getPatientDependents(dependent: any) {

        this.commonService.getFromStorage(dependent).then((value) => {
            if (value) {

                this.patientDependents = value;
                let dependentList = _.cloneDeep(this.patientDependents);
                let indexSelected,indexSelf;
                indexSelected = dependentList.findIndex(dependent => dependent.isSelected == true);
                indexSelf = dependentList.findIndex(dependent => dependent.relation == this.constants.RELATION_SELF_VALUE);
                if(indexSelected){
                    this.patient = this.patientDependents[indexSelected];
                }else{
                    this.patient = this.patientDependents[indexSelf];
                }

                // this.patientDependents.forEach(element => {
                //     if (element.relation == this.constants.RELATION_SELF_VALUE) {
                //         this.patient = element;

                //     }
                // });

                this.getImageBase();

            } else {
                if (this.isPatientAdmin) {
                    this.getPatientProfile();
                }
            }

        });
    }



    /*
     * set data on dependent change
     */

    onDependentSelect = (patient: any) => {
        // this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        // this.cityId = cityId;

        // for(let i=0; i<this.cities.length; i++){
        //     if(this.cities[i].id == cityId){
        //         this.matchCity = this.cities[i];
        //         this.commonService.setInStorage("SelectedHospCity",this.matchCity );
        //     }
        // }

        // this.loadFinished = false;
        // this.getHospitalList(this.cityId,true);


        this.patientDependents.forEach(element => {
            element.isSelected = false;
            patient.isSelected = true;
        });


        // this.setDependentDataInStorage();
    }

    /* function to get images in async */
    public getImageBase = () => {
        
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i < this.patientDependents.length; i++) {
            console.log("USER_ID:",this.patientDependents[i].userId);
            console.log("USER_PROFILE_ID:",this.patientDependents[i].userProfileId);
            let imageData = {
                'userType': "USER_PROFILE_IMAGE",
                'id': this.patientDependents[i].userId,
                'userProfileId': this.patientDependents[i].userProfileId,
                'rank': 1,
            }

            if(this.patientDependents[i].relation == this.constants.RELATION_SELF_VALUE){
                this.commonService.downloadImage(imageData).subscribe(
                    res => {
                        if (res.status == "success") {
                            console.log("image base 64 PATIENT=======>"+i, res);
                            // for (let j = startIndex; j < this.patientDependents.length; j++) {
                                // if (this.patientDependents[j].userId == res.data.userId) {
                                    // if(this.patientDependents[j].imageBase64){
                                        this.patientDependents[i].imageBase64 = res.data.file;
                                    // }
                                    
                                // }
                            // }
                            console.log("this.patientDependents=======>", this.patientDependents);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        /*    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                            this.commonService.presentToast(errorMsg);*/
                    });
            }else{
                this.commonService.downloadDependentImage(imageData).subscribe(
                    res => {
                        if (res.status == "success") {
                            console.log("image base 64 DEPENDENT=======>"+i, res);
                            // for (let j = startIndex; j < this.patientDependents.length; j++) {
                                // if (this.patientDependents[j].userProfileId == res.data.userProfileId) {
                                    // if(this.patientDependents[j].imageBase64){
                                        console.log("RELATION: "+this.patientDependents[i].relation + " NAME:" +this.patientDependents[i].firstName ,i);

                                        this.patientDependents[i].imageBase64 = res.data.file;
                                        // console.log("IMAGE:",this.patientDependents[j].imageBase64);
                                    // }
                                    
                                // }
                            // }
                            console.log("this.patientDependents=======>", this.patientDependents);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        /*    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                            this.commonService.presentToast(errorMsg);*/
                    });
            }
            

        }
    }



    searchPatientFromPatient(searchby: any) {
        let request = { "identifier": searchby }
        this.commonService.searchPatient(request).subscribe(
            res => {
                if (res.status == "success") {
                    this.commonService.hideLoading();
                    console.log("RES:", res.data);

                    if (res.data && res.data.dependentProfiles && res.data.dependentProfiles.length > 0) {
                        this.patientDependents = res.data.dependentProfiles;
                        console.log("DEPENDENT:", this.patientDependents);

                        this.getImageBase();
                        this.patientDependents.forEach(element => {
                            if (element.relation == this.constants.RELATION_SELF_VALUE) {
                                element.isSelected = true;
                                this.patient = element;
                            } else {
                                element.isSelected = false;
                            }
                        });
                    }

                }
                else {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);

            }
        );

    }

    searchPatient() {
        if (this.searchText && this.searchText.length > 0) {
            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            let request = { "identifier": this.searchText }
            this.commonService.searchPatient(request).subscribe(
                res => {
                    if (res.status == "success") {
                        this.commonService.hideLoading();

                        if (res.data && res.data.dependentProfiles && res.data.dependentProfiles.length > 0) {
                            this.isSearched = false;
                            this.searchText = "";
                            this.patientDependents = res.data.dependentProfiles;
                            this.getImageBase();

                            this.patientDependents.forEach(element => {
                                if (element.relation == this.constants.RELATION_SELF_VALUE) {
                                    element.isSelected = true;
                                    this.patient = element;
                                } else {
                                    element.isSelected = false;
                                }
                            });
                        } else {
                            this.isSearched = true;
                            this.patientDependents = [];
                            this.patient = null;
                            this.setDependentDataInStorage();
                        }

                    }
                    else {
                        this.commonService.hideLoading();
                        this.commonService.presentToast(res.message);
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);

                }
            );
        }
    }

    onClickSelectDoctor() {
        this.setDependentDataInStorage();
        // window.location = this.selectDoctorPageUrl;
        // this.navCtrl.push("BookingStep1Page", {
        //     doctorId: this.doctorId,
        //     from: this.fromPage,
        //     editParam:this.redirectParam
        //   });   
        
        if(this.fromPage == "patientHome" && this.redirectPage != "bookStep1" ){
            this.navCtrl.setRoot(this.selectDoctorPageUrl,this.selectDoctorPageParams);

        }else{
            this.navCtrl.push(this.selectDoctorPageUrl,this.selectDoctorPageParams);

        }
        // this.router.navigate = this.selectDoctorPageUrl;
    }

    /**
     * add patient dependents in storage
     */
    onClickAddPatient() {
        this.setDependentDataInStorage();
        if (this.patientDependents && this.patientDependents.length > 0) {
            if (this.isDoctorAdmin) {
                this.commonService.setInStorage("doctorSelected", true);
            } else if (this.isHospitalAdmin) {
            } else if (this.isPatientAdmin) {
            }
        }
        // window.open = this.addPatientUrl;
        this.navCtrl.push(this.addPatientUrl,this.addPatientParams);
    }

    setDependentDataInStorage() {
        if (this.patientDependents && this.patientDependents.length > 0) {
            // console.log("DEPENDENT:",this.patientDependents);
            this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
            
            // if (this.isDoctorAdmin) {
            //     this.commonService.setInStorage("doctorSelected", true);
            // } else if (this.isHospitalAdmin) {
            // } else if (this.isPatientAdmin) {
            // }
        } else {
            this.commonService.removeFromStorage(this.dependentStorageKey);
        }
    }


    onClickDelete(){
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.commonService.deletePatient(this.patient.userProfileId).subscribe(res => {
            console.log("ID:",this.patient.userProfileId);
            console.log("DEL_PAT_RES:",res);
            if (res.status == "success") {
                this.commonService.hideLoading();
                this.commonService.presentToast(res.message);
                // if (res.data) {
                this.deleteDependentFromLocalStorage();

                // }
            } else {
                this.commonService.hideLoading();
                this.commonService.presentToast(res.message);
            }

          

        }, err => {
            this.commonService.hideLoading();
            let errorMsg = err.message ? err.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);
           
        });
    }   

    /**
     * delete dependent from local list
     */
    deleteDependentFromLocalStorage(){
        let deleteList  = _.cloneDeep(this.patientDependents);
        let indexDelete = deleteList.findIndex(dependent => dependent.userProfileId == this.patient.userProfileId);
        deleteList.splice(indexDelete, 1);
        this.patientDependents = deleteList;
        console.log("DEL_DEP:",this.patientDependents);
        let indexSelf = this.patientDependents.findIndex(dependent => dependent.relation == this.constants.RELATION_SELF_VALUE);
        this.patient = this.patientDependents[indexSelf];
        console.log("NEW_PAT:",this.patient);
    }

    onClickEditPatient(){
        this.setDependentDataInStorage();
        this.navCtrl.push(this.addPatientUrl,{
            fromPage:'OPAddPatient',
            previousFromPage:this.fromPage,
            redirectPage:this.redirectPage,
            redirectParam:this.redirectParam,
            doctorId:this.doctorId,
            mode:'edit'
        })
    }

}
