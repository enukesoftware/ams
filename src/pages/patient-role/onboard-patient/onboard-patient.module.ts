import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnboardPatientPage } from './onboard-patient';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../components/shared.module';
import { IonicSelectableModule } from 'ionic-selectable';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    OnboardPatientPage,
  ],
  imports: [
    IonicPageModule.forChild(OnboardPatientPage),
    TranslateModule.forChild(),
    SharedModule,
    IonicSelectableModule,
    DropdownModule
  ],
  exports: [
    OnboardPatientPage
  ]
})
export class OnboardPatientPageModule {}
