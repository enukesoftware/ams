import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "../../components/shared.module";

import { PatientRoleProfilePage } from './patient-role-profile';

@NgModule({
  declarations: [
    PatientRoleProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(PatientRoleProfilePage),
        TranslateModule.forChild(),
        SharedModule
  ],
  exports: [
        PatientRoleProfilePage
   ]
})
export class DoctorRoleProfilePageModule {}
