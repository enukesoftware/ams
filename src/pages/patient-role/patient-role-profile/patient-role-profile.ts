import { Component,ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, MenuController, Platform,Events } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO, DoctorDataDto, AddressDto, serviceAndSpecializationDataDto,PatientDataDTO } from "../../../interfaces/user-data-dto";
import { Constants } from '../../../providers/appSettings/constant-settings';
import { DoctorRoleServiceProvider } from '../../../providers/doctor-role-service/doctor-role-service';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';
import { CameraService } from '../../../providers/camera-service/camera.service';

//3rd party npm module
import * as _ from 'lodash';
import { SelectSearchableComponent } from 'ionic-select-searchable'


@IonicPage({
    name: 'PatientRoleProfilePage',
    segment: 'patient-role-profile/:mode/:id'
} )
@Component({
  selector: 'page-patient-role-profile',
  templateUrl: 'patient-role-profile.html',
})
export class PatientRoleProfilePage {
    scrollHTFlag: boolean;
   @ViewChild(Content) content: Content;
   userData: UserDataDTO = {};
   address: AddressDto = {};
   isProfileEdit: boolean;
   isMyProfile:boolean;
   formSubmitted: boolean;
   isWeb: boolean = false;
   isMobileWeb:boolean = false;
   cityList: any;
   viewFlag: boolean = false;
   editFlag: boolean = false;
   mode: any;
   inputType: any = "text";
   patientData: PatientDataDTO = {};
   imageUpload: any = {};
   patientId: any = "";
   isDefault: boolean;
   rank: number = 0;
   isUpload: boolean = false;
   myProfile: boolean = false;
   getBackEvent:any;
   isSkipAndExplore: boolean = false;
   getMode:any;
   constructor( public navCtrl: NavController, public cameraService : CameraService, private _ngZone: NgZone, private location: Location, public platform: Platform, private constants: Constants, private menu: MenuController, public navParams: NavParams, public commonService: CommonService, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider,
           private patientRoleService: PatientRoleServiceProvider,public evts:Events) {
           this.evts.subscribe('backToPage', () => {
           this._ngZone.run(() => { this.cancel(); });
       });
           
       this.platform.registerBackButtonAction(() => {
           this.location.back();
       });
   }
   
  ionViewWillEnter = () => {
        this.menu.enable( true );
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
      
        this.commonService.getFromStorage('isSkipAndExplore').then((value)=>{
            this.isSkipAndExplore =value;
        });
        
        this.getMode = this.navParams.get('mode');
        console.log("mode--->",this.getMode);
    }

    /**
     * Function to check if user is authorized or not to access the screen
     */
  
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.isProfileEdit = false;
                this.isMyProfile = false;
                this.formSubmitted = false;
                console.log("userData", this.userData);
                this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_PATIENT );
                this.commonService.setActiveUserRole( this.constants.ROLE_PATIENT );
                this.commonService.fireSelectedEvent( '#/patient-role-profile' );
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_PATIENT );
                this.evts.publish('fire-after-login-event');
                this.isWeb = this.commonService.checkIsWeb();
                this.isMobileWeb = this.commonService.checkMobileweb();
                console.log("isMobileWeb..", this.isMobileWeb);
                this.viewFlag = true;
                this.editFlag = false;
                this.getPatientProfile();
            }
        } );
    }

    /**
     * Function to update patient profile
     */
    updatePatientProfile( form: NgForm ){
        this.formSubmitted = true;
         if ( form.valid ) {
            this.formSubmitted = false;
            console.log("doctorData.......", this.patientData);
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.patientData.address=this.address;
            this.patientRoleService.updatePatientProfile( this.patientData ).subscribe(
            res => {
                this.commonService.hideLoading();
                if(res.status == 'success'){
                    this.commonService.presentToast(res.message);
                    this.patientData = res.data;
                    console.log("patientDatanew.......", this.patientData);
                    this.address=this.patientData.address;
                    if( this.isUpload && this.patientData.fileId){
                        console.log('in edit  image id');
                       this.editImage(this.patientData.fileId);
                    }else if( !this.patientData.fileId && this.isUpload){
                        this.uploadImage(this.patientData.userId);
                    } 
                   
                    if(this.patientData.userId == this.userData.userId){
                        this.commonService.setInStorage( 'userData',res.data );
                   }
                   
                    setTimeout(()=>{
                        this.navCtrl.setRoot("PatientRoleProfilePage");
                    }, 3000);
                }
            }, error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            });
        }
    }
    
    /***
     * Function to open camera/photo gallery
     */
     protected onCamera = () => {
         try{
            // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
             this.cameraService.loadImage(this.successCallback,this.errorCallback);
         }catch(e){
             
         }
     }
     
     /**
      * Function to handle camera success callback
      * @param success
      */
     private successCallback = ( base64Image: any ) => {
         this.isUpload = true;
         this.rank++;
         this.isDefault = true;
        // this.imageUpload.file = base64Image;
         this.commonService.convertImgToBase64(base64Image, (callback)=>{
             this.imageUpload.file = callback;
         })
     }
     
     /**
      * Function to handle camera error callback
      * @param error
      */
     private errorCallback = ( error: any ) => {
        console.log( 'Unable to load profile picture.' ,error);
     }
    
    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean ) {
        this.rank++;
        this.isDefault = isDefault;
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        var that = this;
        this.imageUpload = {
            "file":"",
            "fileName":""
        }
        myReader.onloadend = (loadEvent:any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback)=>{
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if( callback ){
                    this.isUpload = isUpload;
                }
            })
         
        };
         myReader.readAsDataURL(file);
    }
    
    
    /**
     * Function to upload image 
     **/
     uploadImage = ( userId: number ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
        });
     }  
    
   
   /*
    * Function to get doctor details based on id
    * */
   getPatientProfile = () => {
       console.log("in getPatientProfile ")
       if( this.navParams.get('id')  && this.navParams.get('id') != ":id" && this.navParams.get('id') != "0" ){
           console.log("in getPatientProfile with id---->")
           this.patientId = this.navParams.get('id');
           this.commonService.setInStorage("patientId", this.patientId );
           this.patientRoleService.getPatientProfile(this.patientId).subscribe(res=>{
               this.patientData = res.data;
               this.getImageBase();
               this.address=this.patientData.address;
               console.log("patientData--->",this.patientData);
           /*   if( this.patientData ){
                   this.imageUpload.file = this.patientData.imageBase64;
                  console.log(" id file", this.imageUpload.file);
               }*/
               this.commonService.setInStorage("patientId", res.data.userId);
               
           }, err=>{
               let errorMsg = err.message ? err.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
               if(!this.isSkipAndExplore){
                   this.commonService.presentToast(errorMsg);
               }
           });
       }else{
           console.log("in getPatientProfile without  id---->")
           this.commonService.getFromStorage("patientId").then(( value ) => {
               if( value ) {
                   this.patientId = value;
               }else{
                   this.patientId = "";
               }
               this.patientRoleService.getPatientProfile(this.patientId).subscribe(res=>{
                   this.patientData = res.data;
                   this.address=this.patientData.address;
                   console.log("patientData--->",this.patientData);
                   //this.patientData.city = this.patientData.address.cityName;
                  /* if( this.patientData ){
                       this.imageUpload.file = this.patientData.imageBase64;
                      // console.log(" storage file", this.imageUpload.file);
                   }*/
                   this.getImageBase();
                   this.commonService.setInStorage("patientId", res.data.userId);
                   this.commonService.setPaddingTopScroll( this.content );
               }, err=>{
                   let errorMsg = err.message ? err.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                   if(!this.isSkipAndExplore){
                       this.commonService.presentToast(errorMsg);
                   }
                   this.commonService.setPaddingTopScroll( this.content );
               });
           });
       }
   }
   
   
   /* function to get images in async */ 
   public getImageBase =()=>{
       //let startIndex = this.pageNo * this.constants.PAGE_SIZE;
           let imageData1 = {
                   'userType': "USER_PROFILE_IMAGE",
                   'id': this.patientData.userId,
                   'rank': 1,
            }   
       this.commonService.downloadImage( imageData1 ).subscribe(
       res => {
           console.log("on get image base res")
           if ( res.status == "success" ) {
               console.log("image base 64=======>", res);
               this.patientData.imageBase64 = res.data.file;
           }
       },
       error => {
           this.commonService.hideLoading();
          /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
           this.commonService.presentToast(errorMsg);*/
   });
   }

   /**
    * Function to upload image 
    **/
    editImage = ( fileId: number ) => {
        console.log("in editImage =======>");
       let imageData = {
           "userType": "USER_PROFILE_IMAGE",
           "mediaType": "IMAGE",
           "id": fileId, //file Id
           "file": this.imageUpload.file,
           "fileName": this.imageUpload.fileName,
           "isDefault": this.isDefault,
           "rank" : this.rank
       }
       this.commonService.editImage(imageData).subscribe(
           res => {
               if ( res.status == "success" ) {
               }
           },
           error => {
               this.commonService.hideLoading();
               let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
               this.commonService.presentToast(errorMsg);
       });
   }
   
    editProfile(){
        console.log("this.imageUpload.file", this.imageUpload.file);
        this.viewFlag = false;
        if( this.userData.userId == this.patientData.userId){
            this.myProfile = true;
        }
       this._ngZone.run(() => {
            this.isProfileEdit = true;
        });
       this.imageUpload.file = this.patientData.imageBase64;
    }

    cancel(){
        this.isProfileEdit = false;
        this.viewFlag = true;
        if( this.userData.userId == this.patientData.userId){
            this.myProfile = true;
        }
        
    }

    /**
     * Function to show coming soon popup
     */
    comingSoonPopup = () => {
        this.commonService.presentToast();
    }
}