import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, PopoverController, NavParams, MenuController, Events, Content, Slides, ModalController, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorListDTO, HospitalListDTO, HospitalDTO } from '../../../interfaces/user-data-dto';
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { PopoverSliderComponent } from "../../components/popover-slider/popover-slider";
import { CallNumber } from "@ionic-native/call-number";

@IonicPage( {
    name: 'PatientRoleHospitalProfilePage',
    segment: 'patient-role-hospital-profile/:hospitalId/:param'
} )

@Component( {
    selector: 'page-patient-hospital-profile',
    templateUrl: 'patient-role-hospital-profile.html',
} )
export class PatientRoleHospitalProfilePage {
    @ViewChild(Content) content: Content;
    @ViewChild( Slides ) slides: Slides;
    popOver: any;
    userData: UserDataDTO = {};
    hospitalData: HospitalDTO = { address: {} };
    listOfDoctors: DoctorListDTO[];
    isSliding: boolean = false;
    clickedImgIndex: number;
    overlayHidden: boolean;
    hospitalId: any;
    imgUrl: any = [];
    rank: number;
    imgCounter: number;
    currentSegment: any;
    loadFinished: boolean =false;
    pageNo: number = 0;
    isPatientRoleDoctorList:any;
    noDefaultImg: boolean = false;
    tempImgArr: any = [];
    hospitalServices: any[];
    hospitalSpecialities: any[];
    viewMoreSpFlag: boolean = false;
    lessThen3Sp: boolean = true;
    initialSpecialities: any[];
    windowWidth: number;
    
    viewMoreServiceFlag: boolean = false;
    lessThen3Service: boolean = true;
    initialServices: any[];
    redirectParam: any;
    isSkipAndExplore:boolean=false;
    noDataFound:boolean = false;
    
    
    constructor( private evts: Events, public navCtrl: NavController, private _ngZone: NgZone, public platform: Platform, public location: Location, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
            private constants: Constants, public popoverCtrl: PopoverController, private patientRoleService: PatientRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider, private callNumber: CallNumber ) {
            this.commonService.closePopupOnBackBtn(this.popOver);
        }
    
    
    
    ionViewDidLoad() {
        this.menu.enable( true );
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        this.clickedImgIndex = 0;
        this.imgCounter = 1;
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        this.currentSegment = 'doctorList';
        this.windowWidth = this.commonService.getWindowWidth();
        this.redirectParam = this.navParams.get('param');
        
        if(this.redirectParam == 'PRDoctorProfileHLProfile'){
            this.currentSegment = 'hospitalDetails';
        }else{
            this.currentSegment = 'doctorList';
        }
        
        console.log("param....................", this.navParams.get('param'));
        this.commonService.getFromStorage('isSkipAndExplore').then((value)=>{
            this.isSkipAndExplore =value;
        })
    }
    
    
    ionViewDidLeave(){
        if(this.popOver){
            this.popOver.dismiss();
        }
    }
    
    /**
     * Function to show image on popover
     * @param index 
     */
     presentPopover(index) {
        this.isSliding = true;
        this.popOver = this.popoverCtrl.create(PopoverSliderComponent, {data: this.imgUrl, index: index}, {
             'enableBackdropDismiss':true
        });
        this.popOver.present();
        this.commonService.closePopupOnBackBtn(this.popOver);
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                console.log("this.hospitalId");
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_PATIENT );
                this.commonService.fireSelectedEvent( '#/patient-role-home/0' );
                if( this.navParams.get('hospitalId') && this.navParams.get('hospitalId') != ":hospitalId" && this.navParams.get('hospitalId') != "0" ){
                    this.hospitalId = this.navParams.get('hospitalId');
                    this.commonService.setInStorage("hospitalId", this.hospitalId );
                    this.getListOfAssociatedDoctors(this.hospitalId,true);
                    this.viewHospitalDetails(this.hospitalId);
                }else{
                    this.commonService.getFromStorage("hospitalId").then(( value ) => {
                        if( value ) {
                            this.hospitalId = value;
                        }else{
                            this.hospitalId = "";
                        }
                       this.viewHospitalDetails(this.hospitalId);
                    });
                }
                
            }
        } );
    }
    
    /*
     * DownLoad hospital images
     * @param imageData 
     */
    downloadImage(){
         let imageData = {
            'userType': "HOSPITAL_IMAGE",
            'id': this.hospitalData.hospitalId,
            'rank': this.imgCounter,
        }
       // this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
       if( this.imgCounter <= this.hospitalData.imagesCount){
            this.commonService.downloadImage( imageData ).subscribe(
                res => {
                    //this.commonService.hideLoading();
                    if ( res.status == "success" ) {
                        this.imgUrl[this.imgCounter-1] = res.data;
                        this.imgCounter ++;
                        this.downloadImage();
                        let isProfileImgFound = false;
                        let profileImgArray = [];
                        for( let i=0; i<this.imgUrl.length; i++ ){
                            if( this.imgUrl[i].isDefault && !this.imgUrl[i].file ){
                                   this.tempImgArr.push(this.imgUrl[i]);
                                   isProfileImgFound = true;
                            }else if( this.imgUrl[i].isDefault && this.imgUrl[i].file){
                                profileImgArray.push(this.imgUrl[i]);
                            }     
                        }
                        
                        if( !isProfileImgFound || (this.tempImgArr && this.tempImgArr.length > 0)){
                            this.noDefaultImg = true;
                        }
            
                        if( profileImgArray && profileImgArray.length > 0){
                            this.noDefaultImg = false;
                        }
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    //this.commonService.showAlert( "Error", error.message );
                   /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    if(!this.isSkipAndExplore){
                        this.commonService.presentToast(errorMsg);
                    }*/
            });
       }
       
    } 
    
    
   
    /*
     * get associated doctor list
     * */   
    getListOfAssociatedDoctors( hospitalId: any ,firstPageCall?: boolean ) {
        //this.manageHospitalService.getAssociatedDoctorsList( hospitalId ).subscribe(
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.manageHospitalServiceProvider.getAssociatedDoctorsList( hospitalId, this.pageNo).subscribe(
            res => {
                //this.commonService.presentToast("List of cities success.......");
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == "success" ) {
                    //this.listOfDoctors = [];
                    if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                        this.listOfDoctors = res.data.list;
                        this.getImageBase();
                        console.log("listOfDoctors",this.listOfDoctors);
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                        this.formatAvailability();
                    }else if( res.data.list && res.data.list.length > 0 ){
                        for( let j=0;j<res.data.list.length;j++ ){
                            this.listOfDoctors.push(res.data.list[j]);
                            console.log("listOfDoctors",this.listOfDoctors);
                        }
                        this.getImageBase();
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                        this.formatAvailability();
                    }else{
                        this.loadFinished = true;
                        this.noDataFound = true;
                    }
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }
    
    /* function to get images in async */ 
    public getImageBase =()=>{
        console.log("getImageBase=======>");
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.listOfDoctors.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.listOfDoctors[i].doctorId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.listOfDoctors.length; j++ ){
                    if(this.listOfDoctors[j].doctorId == res.data.userId){
                        this.listOfDoctors[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.listOfDoctors);
            }
        },
        error => {
            this.commonService.hideLoading();
            /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }
    
    
    
    /*
     * Function to do calculations for time related 
     * */
    formatAvailability = () => {            
        var currentDate = new Date();
        var today = currentDate.getDay();
        for ( let i = 0; i < this.listOfDoctors.length; i++ ) {
            var todayAvailability;
           // console.log("this.listOfDoctors", this.listOfDoctors[i]);
            let availabilities = this.listOfDoctors[i].timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime( availabilities );
            this.listOfDoctors[i].formattedAvailabilities = calculatedTime;
            let availabilityLength = this.listOfDoctors[i].formattedAvailabilities.length;
            for ( let j = 0; j < availabilityLength; j++ ) {
                if ( today == this.listOfDoctors[i].formattedAvailabilities[j].dayCount ) {
                    //console.log( "todayAvailability=========================>", todayAvailability );
                }
            }
            this.listOfDoctors[i].todayAvailability = this.listOfDoctors[i].todaysTimeAvailabilityList;
            if(this.listOfDoctors[i].todaysTimeAvailabilityList && this.listOfDoctors[i].todaysTimeAvailabilityList.length > 0){
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = this.listOfDoctors[i].todayAvailability.length;
                for(let j=0; j<todayTimeLen; j++){
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].toTime), true);
                
                    if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                        timeSlotFound = true;
                        this.listOfDoctors[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if(!timeSlotFound){
                    this.listOfDoctors[i].todaysAvailabilityString = "Unavailable";
                }
            } else{
                this.listOfDoctors[i].todaysAvailabilityString = "Unavailable";
            }
        }
    }
    
    viewHospitalDetails = ( hospitalId: any ) => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.manageHospitalServiceProvider.getHospitalDetails(this.hospitalId).subscribe(
            res => {
                this.commonService.hideLoading();
                if ( res.status == "success" ) {
                    this.hospitalData = res.data;
                    if( this.hospitalData ){
                        this.rank = this.hospitalData.imagesCount;
                        this.downloadImage();
                        
                        this.hospitalSpecialities = this.hospitalData.specializationList;
                        this.hospitalServices = this.hospitalData.serviceList;
                        if(this.windowWidth <= 768){
                            let spLen = this.hospitalSpecialities.length;
                            if(spLen > 3){
                                this.hospitalSpecialities = [];
                                this.viewMoreSpFlag = true;
                                this.lessThen3Sp = false;
                                for(let i=0; i<spLen; i++){
                                    if(i < 3){
                                        this.hospitalSpecialities.push(this.hospitalData.specializationList[i]);
                                    }
                                }
                            } else{
                                this.lessThen3Sp = true;
                            }
                           
                            this.initialSpecialities = this.hospitalSpecialities;
                            
                            let serviceLen = this.hospitalServices.length;
                            if(serviceLen > 3){
                                this.hospitalServices = [];
                                this.viewMoreServiceFlag = true;
                                this.lessThen3Service = false;
                                for(let i=0; i<serviceLen; i++){
                                    if(i < 3){
                                        this.hospitalServices.push(this.hospitalData.serviceList[i]);
                                    }
                                }
                            } else{
                                this.lessThen3Service = true;
                            }
                            this.initialServices = this.hospitalServices;
                        }
                    }
                    
                    var currentDate = new Date();
                    var today = currentDate.getDay();
                    var todayAvailability;
                    let availabilities = this.hospitalData.timeAvailabilityList;
                    let calculatedTime = this.commonService.calculateTime( availabilities );
                    this.hospitalData.formattedAvailabilities = this.commonService.calculateTime( availabilities );
                    let availabilityLength = this.hospitalData.formattedAvailabilities.length;
                    
                    this.hospitalData.todayAvailability = this.hospitalData.todaysTimeAvailabilityList;
                    
                    if(this.hospitalData.todaysTimeAvailabilityList && this.hospitalData.todaysTimeAvailabilityList.length > 0){
                        let timeSlotFound = false;
                        let currentTime = new Date();
                        let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                        let todayTimeLen = this.hospitalData.todayAvailability.length;
                        for(let j=0; j<todayTimeLen; j++){
                            let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitalData.todaysTimeAvailabilityList[j].fromTime), true);
                            let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitalData.todaysTimeAvailabilityList[j].toTime), true);
                        
                            if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                                timeSlotFound = true;
                                this.hospitalData.todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitalData.todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitalData.todaysTimeAvailabilityList[j].toTime), false, true);
                            }
                        }
                        if(!timeSlotFound){
                            this.hospitalData.todaysAvailabilityString = "Closed";
                        }
                    } else{
                        this.hospitalData.todaysAvailabilityString = "Closed";
                    }
                    if(this.hospitalData.todaysAvailabilityString == "00:00AM - 11:59PM"){
                        this.hospitalData.todaysAvailabilityString = "24 Hrs Open";
                    }
                }
            if( this.commonService.checkIsWeb() ){
                this.commonService.setPaddingTopScroll( this.content );
            }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                if( this.commonService.checkIsWeb() ){
                    this.commonService.setPaddingTopScroll( this.content );
                }
            }
        );
    }
   
    
    public loadMoreHospitalData =()=>{
        console.log("in loadMoreHospitalData()");
    }
    
    public loadMoreData = () =>{
        console.log("loadMoreData()");
        this.pageNo = this.pageNo + 1;
        this.getListOfAssociatedDoctors(this.hospitalId);
    }
    
    
    /**
     * Array function for count
     */
    counter = ( i: number ) => {        
        return new Array(i);
    }
    
    /**
     * Function to show Timing popup
     **/
    openTimingDetails = ( hospital ) => {
        //console.log( "doctor",doctor );
        let title = hospital.name;
        console.log("hospital on 374...", hospital);
        let subTitle = hospital.address.street + ", " + hospital.address.cityName;
        this.popOver = this.commonService.showTimingList( title, subTitle, hospital.formattedAvailabilities );
        this.commonService.closePopupOnBackBtn(this.popOver);
    }
    

    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public plotAddressOnMap = ( hospital:any ) => {
        let currentHospitalAddress = hospital.name + "+" + hospital.address.street + "+" + hospital.address.cityName + "+" +
        hospital.address.state + "+" + hospital.address.zipCode;
        this.commonService.setAddressOnMap( currentHospitalAddress );
    }
    
    /*
     * more Specialities List and service list
     * */
                    
     hideMore(type){
         if(type == "speciality"){
             console.log("speciality 185.......");
             this.hospitalSpecialities = this.hospitalData.specializationList;
             this.viewMoreSpFlag = false;
         } else{
             console.log("services 189.......");
             this.hospitalServices = this.hospitalData.serviceList;
             this.viewMoreServiceFlag = false;
         }
     }
     
     /*
      * less Specialities List and service list
      * */
     hideLess(type){
         if(type == "speciality"){
             this.hospitalSpecialities = this.initialSpecialities;
             this.viewMoreSpFlag = true;   
         } else{
             this.hospitalServices = this.initialServices;
             this.viewMoreServiceFlag = true;
         }
     }
     
     
     hospitalDetailsClick(){
         this._ngZone.run(() => {
             this.currentSegment = 'hospitalDetails';
         });
     }
     
     doctorListClick(){
         this._ngZone.run(() => {
             this.currentSegment = 'doctorList';
         });
     }
     
     /**
      * function to make a phone call
      **/
     makeCall(number){
         this.callNumber.callNumber(number, true)
         .then(res => console.log('Launched dialer!', res))
         .catch(err => console.log('Error launching dialer', err));
     }
   
}