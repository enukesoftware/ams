import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { PatientRoleHospitalProfilePage } from './patient-role-hospital-profile';

import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    PatientRoleHospitalProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(PatientRoleHospitalProfilePage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    PatientRoleHospitalProfilePage
  ]
})
export class PatientRoleHomePageModule {}
