import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { PatientRoleFeedbackHistoryPage } from './patient-role-feedback-history';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    PatientRoleFeedbackHistoryPage,
  ],
  imports: [
      IonicPageModule.forChild(PatientRoleFeedbackHistoryPage),
      TranslateModule.forChild(),
      SharedModule
    ],
    exports: [
       PatientRoleFeedbackHistoryPage
    ]
})
export class PatientRoleFeedbackHistoryPageModule {}
