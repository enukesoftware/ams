import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Content, Events, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO } from '../../../interfaces/user-data-dto';
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';

import * as moment from 'moment';

@IonicPage( {
    name: 'PatientRoleFeedbackHistoryPage',
    segment: 'patient-role-feedback-history'
} )
@Component({
  selector: 'page-patient-role-feedback-history',
  templateUrl: 'patient-role-feedback-history.html',
})
export class PatientRoleFeedbackHistoryPage {
  @ViewChild( Content ) content: Content;
  userData: UserDataDTO = {};
  loadFinished: boolean = false;
  patientId:any;
  pageNo: number = 0;
  pageName: any;
  feedbackHistoryData = [];
  formattedFeedbackdate:any;
  feedbackDate:any;
  feedbackTime:any;
  convertedTime:any;
 
 // feedbackData: FeedbackDataDTO = {};

  constructor( private commonService: CommonService, private evts: Events, public platform: Platform, public location: Location, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private constants: Constants,
          private patientRoleService: PatientRoleServiceProvider) {
        this.menu.enable( true );
    }
  
    ionViewWillEnter = () => {
        this.menu.enable( true );
        this.platform.registerBackButtonAction(() => {
            this.location.back();
        });
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                console.log("userData", this.userData);
                this.commonService.fireSelectedEvent( '#/patient-role-feedback-history');
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_PATIENT, true );
                this.getPatientID();
            }
        } );
    }
    
    
    /*
     * Function to load more data when clicked on load more button
     * */
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getfeedbackHistory(this.patientId);
    }
    
    
    /*
     * Function to get doctor details based on id
     * */
    getPatientID = () => {
        console.log(" in get patient profile");
        if( this.navParams.get('id') && this.navParams.get('id') != ":id" && this.navParams.get('id') != "0" ){
            console.log( "in getPatientProfile  if================>");
            this.patientId = this.navParams.get('id');
            this.commonService.setInStorage("patientId", this.patientId );
            this.getfeedbackHistory(this.patientId,true);
        }else{
            console.log( "in getPatientProfile  else================>");
            this.commonService.getFromStorage("patientId").then(( value ) => {
                if( value ) {
                    this.patientId = value;
                    console.log(" patientId ", this.patientId);
                    this.getfeedbackHistory(this.patientId);
                }else{
                    this.patientId = "";
                }
            });
        }
    }
    
    
    /*
     * Function to show coming soon popup
     * */
   comingSoonPopup = () => {
        this.commonService.presentToast();
    }

   
   
   /*
    * Function to get feedback history
    * */
   getfeedbackHistory = (patientId,firstPageCall?: boolean) => {
       console.log("patientId ",patientId);
       this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
       this.patientRoleService.getFeedbackHistory( patientId, this.pageNo ).subscribe(
           res => {
               if ( res.status == "success" ) {
                   this.commonService.hideLoading();
                   let tempPageNo = this.pageNo;
                   //this.feedbackHistoryData = res.data.list;
                   console.log("res",this.feedbackHistoryData);
                   for(let i=0;i<res.data.list.length;i++){
                   
                   var newFeedbackCreateDate = moment(res.data.list[i].feedbackCreatedAt).toDate();
                   
                       res.data.list[i].convertedTime = this.commonService.convertTo12hrsFormat(new Date(newFeedbackCreateDate));
                       res.data.list[i].feedbackCreatedAt = new Date(newFeedbackCreateDate).toLocaleDateString();
                   }
                   if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                       this.feedbackHistoryData = res.data.list;
                       if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                           this.loadFinished = true;
                       }
                   }else if( res.data.list && res.data.list.length > 0 ){
                       for( let j=0;j< res.data.list.length;j++ ){
                           this.feedbackHistoryData.push(res.data.list[j]);
                       }
                       if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                           this.loadFinished = true;
                       }
                   }else{
                       this.loadFinished = true;
                       if(res.data.list.length ==0){
                           this.feedbackHistoryData = [];
                       }
                   }
               }
               else{
                   this.commonService.hideLoading();
                   this.commonService.presentToast(res.message);
               }
               this.commonService.setPaddingTopScroll( this.content );
           }, error => {
                   this.commonService.presentToast( error.message );
                   this.commonService.hideLoading();
                   this.commonService.setPaddingTopScroll( this.content );
               } );
           }
   
   
       public goToUpdate=(data)=>{
               console.log(" go to update  ",data);
               this.navCtrl.setRoot('RateAndFeedbackPage',{mode:'updateFeedback',doctorId: data.doctorsDetails.doctorId,'hospitalId':data.hospitalDetails.hospitalId,'appointmentId':data.userFeedbackId});
           }
   
}
