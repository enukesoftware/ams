import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams } from 'ionic-angular';


//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { UserDataDTO, PatientDataDTO, AddressDto } from '../../../interfaces/user-data-dto';
import { LocalStorageService } from '../../../providers/localStorage-service/localStorage.service';
import { CameraService } from '../../../providers/camera-service/camera.service';

/**
 * Generated class for the AddPatientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage({
    name: 'AddPatientPage',
    segment: 'add-patient/:fromPage/:previousFromPage/:redirectPage/:redirectParam/:doctorId/:mode'
})
@Component({
    selector: 'page-add-patient',
    templateUrl: 'add-patient.html',
})
export class AddPatientPage {
    @ViewChild(Content) content: Content;
    address: AddressDto = {};
    patientData: any = {};
    formSubmitted = false;
    userData: UserDataDTO = {};
    inputType: any = "text";
    isDefault: boolean;
    imageUpload: any = {};
    rank: number;
    isUpload: boolean = false;

    isHospitalAdmin: boolean = false;
    isDoctorAdmin: boolean = false;
    isPatientAdmin = false;
    isHospitalStaff = false;

    viewFlag: boolean = false;
    createFlag: boolean = true;
    editFlag: boolean = false;

    fromPage: any;
    previousFromPage: any;
    redirectPage: any;
    redirectParam: any;
    doctorId: any;
    mode: any;
    backPageUrl: any;
    navParamsComponent:any;

    // patientDependents: any = [{ "firstName": "Gaurav" }, { "firstName": "Thakur" }];

    constructor(public navCtrl: NavController, public cameraService: CameraService, public navParams: NavParams, public commonService: CommonService,
        private constants: Constants, private manageSysAdminServiceProvider: ManageSysAdminServiceProvider,
        private locstr: LocalStorageService) {
        
        this.fromPage = this.navParams.get('fromPage');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddPatientPage');
    }

    ionViewWillEnter = () => {
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.rank = 1;

        this.navParamsComponent = this.navParams;
        console.log("ionViewWillEnter");

        this.isAuthorized();
    }

    /*
    * Function to check if user is authorized or not to access the screen
    * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if (!result) {
                this.navCtrl.setRoot('LoginPage');
            } else {
                this.userData = result;
                this.setAdminRole();
                this.getNavigationData();

                // this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                // this.commonService.fireSelectedEvent('#/system-admin-manage-patients');
                // this.commonService.setPaddingTopScroll( this.content );



            }
        });
    }

    setAdminRole() {
        if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            // this.navCtrl.setRoot( 'HADashboardPage' );

            // this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN, true );
            // this.commonService.fireSelectedEvent('#/onboard-patient');
            this.isHospitalAdmin = true;
            this.isDoctorAdmin = false;
            this.isPatientAdmin = false;
            this.isHospitalStaff = false;

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            // this.navCtrl.setRoot( 'DoctorDashboardPage' );

            // this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR, true );
            // this.commonService.fireSelectedEvent('#/onboard-patient');
            this.isHospitalAdmin = false;
            this.isDoctorAdmin = true;
            this.isPatientAdmin = false;
            this.isHospitalStaff = false;
        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            // this.navCtrl.setRoot( 'PatientRoleHomePage' );

            // this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_PATIENT, true );
            // this.commonService.fireSelectedEvent('#/onboard-patient');
            this.isHospitalAdmin = false;
            this.isDoctorAdmin = false;
            this.isPatientAdmin = true;
            this.isHospitalStaff = false;
        }else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            this.isHospitalAdmin = false;
            this.isDoctorAdmin = false;
            this.isPatientAdmin = false;
            this.isHospitalStaff = true;
        }
    }

    getNavigationData() {
        this.fromPage = this.navParams.get('fromPage');
        this.previousFromPage = this.navParams.get('previousFromPage');
        this.redirectPage = this.navParams.get('redirectPage');
        this.redirectParam = this.navParams.get('redirectParam');
        this.doctorId = this.navParams.get('doctorId');
        this.mode = this.navParams.get('mode');
        this.setPageMode();
        this.backPageUrl = '#/onboard-patient/' + this.previousFromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId;

        // this.setPageNavigationData();
    }

    setPageMode() {
        if (this.mode == "create") {
            this.createFlag = true;
            this.viewFlag = false;
            this.editFlag = false;
        } else if (this.mode == "view") {
            this.createFlag = false;
            this.viewFlag = true;
            this.editFlag = false;
        } else if (this.mode == "edit") {
            this.createFlag = false;
            this.viewFlag = false;
            this.editFlag = true;
        }
    }


}
