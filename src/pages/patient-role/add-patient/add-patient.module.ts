import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPatientPage } from './add-patient';
import { SharedModule } from '../../components/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { DropdownModule } from 'primeng/dropdown';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    AddPatientPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPatientPage),
    SharedModule,
    TranslateModule.forChild(),
    DropdownModule,
    IonicSelectableModule
  ],
  exports: [
    AddPatientPage
  ]
})
export class AddPatientPageModule {}
