import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientRoleDoctorProfilePage } from './patient-role-doctor-profile';
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    PatientRoleDoctorProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(PatientRoleDoctorProfilePage),
    TranslateModule.forChild(),
    SharedModule
  ],
  exports: [
    PatientRoleDoctorProfilePage
  ]
})
export class PatientRoleDoctorProfilePageModule {}
