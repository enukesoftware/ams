import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, MenuController, Content, Platform } from 'ionic-angular';
import { CommonService } from "../../../providers/common-service/common.service";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { PatientRoleServiceProvider } from "../../../providers/patient-role-service/patient-role-service";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { UserDataDTO, DoctorDataDto } from "../../../interfaces/user-data-dto";
import { DoctorRoleServiceProvider } from "../../../providers/doctor-role-service/doctor-role-service";
import { Location } from '@angular/common';

@IonicPage({
    name: 'PatientRoleDoctorProfilePage',
    segment: 'patient-role-doctor-profile/:id/:param'
})

@Component({
  selector: 'page-patient-role-doctor-profile',
  templateUrl: 'patient-role-doctor-profile.html',
})
export class PatientRoleDoctorProfilePage {
    loadFinishedHospital: boolean;
    hospitals: any;
    @ViewChild( Content ) content: Content;
    currentSegment: string = 'hospitalInfo';
    hospitalPageNo: number = 0;
    userData: UserDataDTO = {};
    doctorData: DoctorDataDto = {};
    doctorId:any;
    pageNo: number = 0;
    pageName: any;
    feedbackData =[];
    doctorServices: any[];
    doctorSpecialities: any[];
    viewMoreSpFlag: boolean = false;
    lessThen3Sp: boolean = true;
    initialSpecialities: any[];
    windowWidth: number;
    
    viewMoreServiceFlag: boolean = false;
    lessThen3Service: boolean = true;
    initialServices: any[];
    isSkipAndExplore:boolean=false;
    redirectParam: any;
    popover:any;
    
  constructor(private evts: Events, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public platform: Platform, public location: Location, private _ngZone: NgZone, public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
          private constants: Constants, private patientRoleService: PatientRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider) {
      this.platform.registerBackButtonAction(() => {
          this.location.back();
      });
  }

  ionViewDidLoad() {
      this.menu.enable( true );
  }

  ionViewWillEnter = () => {
      this.menu.enable( true );

      //check if user is authorized or not to access the screen if not redirect him to login screen
      this.isAuthorized();
      this.windowWidth = this.commonService.getWindowWidth();
      this.redirectParam = this.navParams.get('param');
      
      if(this.redirectParam == 'PRHospitalProfileDLProfile'){
          this.currentSegment = 'otherInfo';
      }else{
          this.currentSegment = 'hospitalInfo';
      }
      
      console.log("param....................", this.navParams.get('param'));     


     this.commonService.getFromStorage('isSkipAndExplore').then((value)=>{
          this.isSkipAndExplore =value;
      })
  }
  
  /*
   * Function to check if user is authorized or not to access the screen
   * */
  isAuthorized = () => {
      this.commonService.isLoggedIn(( result ) => {
          if ( !result ) {
              this.navCtrl.setRoot( 'LoginPage' );
          } else {
              console.log
              this.userData = result;
              this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_PATIENT, true );
              this.commonService.fireSelectedEvent( '#/patient-role-home/0' );
              this.commonService.setPaddingTopScroll( this.content );
              if(this.navParams.get('id') && this.navParams.get('id') != 0 ){
                  this.doctorId = this.navParams.get('id');

                  this.getDoctorInfo(this.doctorId);
                  this.getAssociatedHospitals(this.doctorId ,true);
                  this.getDoctorFeedbacks(this.doctorId);
              }else{
                  this.commonService.getFromStorage( "patientRoleDoctorId" ).then(( value ) => {
                      if ( value ) {
                          this.doctorId = value;

                          this.getDoctorInfo(this.doctorId);
                          this.getAssociatedHospitals(this.doctorId ,true);
                          this.getDoctorFeedbacks(this.doctorId);
                      } else {
                          this.doctorId = "";
                      }
                  } );
              }
              
          }
      });
  }
  
  /*
   * getDoctor Info
   * */
  
  getDoctorInfo(id){
      this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
      this.doctorRoleServiceProvider.getDoctorProfile(id).subscribe(
          res => {
            this.commonService.hideLoading();  
            if(res.status == 'success'){
              this.doctorData = res.data;
              this.getImageBase();
              this.commonService.setInStorage("patientRoleDoctorId", this.doctorData.doctorId);
              this.doctorSpecialities = this.doctorData.specializationList;
              this.doctorServices = this.doctorData.serviceList;
              console.log("doctorSpecialities", this.doctorSpecialities);
              if(this.windowWidth <= 768){
                  let spLen = this.doctorSpecialities.length;

                  console.log("spLen", spLen);
                  if(spLen > 3){
                      this.doctorSpecialities = [];
                      this.viewMoreSpFlag = true;
                      this.lessThen3Sp = false;
                      for(let i=0; i<spLen; i++){
                          if(i < 3){
                              this.doctorSpecialities.push(this.doctorData.specializationList[i]);
                          }
                      }
                  } else{
                      this.lessThen3Sp = true;
                  }
                  this.initialSpecialities = this.doctorSpecialities;
                  
                  let serviceLen = this.doctorServices.length;
                  if(serviceLen > 3){
                      this.doctorServices = [];
                      this.viewMoreServiceFlag = true;
                      this.lessThen3Service = false;
                      for(let i=0; i<serviceLen; i++){
                          if(i < 3){
                              this.doctorServices.push(this.doctorData.serviceList[i]);
                          }
                      }
                  } else{
                      this.lessThen3Service = true;
                  }
                  this.initialServices = this.doctorServices;
              }
            }
          }, error => {
              this.commonService.hideLoading();
              if(!this.isSkipAndExplore){
              this.commonService.presentToast(error.message);
              }
              console.log("error....", error);
          });
  }
  
                      
          /* function to get profile image in async */ 
          public getImageBase =()=>{
              console.log("on get image base");
                  let imageData = {
                          'userType': "USER_PROFILE_IMAGE",
                          'id':  this.doctorData.doctorId,
                          'rank': 1,
                   }   
              this.commonService.downloadImage( imageData ).subscribe(
              res => {
                  if ( res.status == "success" ) {
                      console.log("image base 64=======>", res);
                      this.doctorData.imageBase64 = res.data.file;
                  }
              },
              error => {
                  this.commonService.hideLoading();
                 /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                  this.commonService.presentToast(errorMsg);*/
          });
      }                  
                      
                      
          
          
          
          
  /*
   * getDoctor Assosciated Hospitals
   * */
  
  getAssociatedHospitals(doctorId: any,firstHospitalPageCall?: boolean ){
      this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
      this.manageHospitalServiceProvider.getAssociatedHospitalList( doctorId, this.hospitalPageNo ).subscribe( res => {
          
          let tempHospitalPageNo = this.hospitalPageNo;
          if( res.data.list && res.data.list.length > 0 && firstHospitalPageCall ){
              this.commonService.hideLoading();
              this.hospitals = res.data.list;
              this.getImageBaseHospitalList();
              console.log("jospitals---->",this.hospitals);
              if ( ( tempHospitalPageNo + 1 ) >= res.data.totalPages ) {
                  this.loadFinishedHospital = true;
              }
              this.formatAvailability();
          }else if( res.data.list && res.data.list.length > 0 ){
              for( let j=0;j<res.data.list.length;j++ ){
                  this.hospitals.push(res.data.list[j]);
              }
              this.getImageBaseHospitalList();
              if ( ( tempHospitalPageNo + 1 ) >= res.data.totalPages ) {
                  this.loadFinishedHospital = true;
              }
              this.formatAvailability();
          }else{
              this.loadFinishedHospital = true;
          }
      }, error => {
          this.commonService.hideLoading();
          if(!this.isSkipAndExplore){
          this.commonService.presentToast(error);
          }
      } );
  }
  
  
  
  /* function to get images in async */ 
  public getImageBaseHospitalList =()=>{
          let startIndex = this.hospitalPageNo * this.constants.PAGE_SIZE;
              for (let i = startIndex; i <this.hospitals.length; i++) {
                  let imageData = {
                          'userType': "HOSPITAL_IMAGE",
                          'id': this.hospitals[i].hospitalDetails.hospitalId,
                          'rank': 1,
                          'isDefault': true    
                   }   
              this.commonService.downloadImage( imageData ).subscribe(
              res => {
                  if ( res.status == "success" ) {
                      console.log("image base ho 64=======>", res);
                      for( let j=startIndex; j<this.hospitals.length; j++ ){
                          if(this.hospitals[j].hospitalDetails.hospitalId == res.data.userId){
                              this.hospitals[j].hospitalDetails.imageBase64 = res.data.file;
                          }
                      }
                      console.log("this.listOfDoctors=======>", this.hospitals);
                      }
              },
              error => {
                  this.commonService.hideLoading();
                 /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                  this.commonService.presentToast(errorMsg);*/
          });
      }
  }
  
  
  /*
   * Function to do calculations for time related 
   * */
  formatAvailability = () => {
      var currentDate = new Date();
      var today = currentDate.getDay();
      let hospitalLength = this.hospitals.length;
      for(let i=0; i< hospitalLength; i++){
          var todayAvailability;
          let availabilities = this.hospitals[i].hospitalDetails.timeAvailabilityList;
          let calculatedTime = this.commonService.calculateTime(availabilities);
          this.hospitals[i].formattedAvailabilities = this.commonService.calculateTime(availabilities);
          let availabilityLength = this.hospitals[i].formattedAvailabilities.length;
         
          this.hospitals[i].todayAvailability = this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList;
          if(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList && this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList.length > 0){
              let timeSlotFound = false;
              let currentTime = new Date();
              let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
              let todayTimeLen = this.hospitals[i].todayAvailability.length;
              for(let j=0; j<todayTimeLen; j++){
                  let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].fromTime), true);
                  let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].toTime), true);
              
                  if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                      timeSlotFound = true;
                      this.hospitals[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].toTime), false, true);
                  }
              }
              if(!timeSlotFound){
                  this.hospitals[i].todaysAvailabilityString = "Closed";
              }
          } else{
              this.hospitals[i].todaysAvailabilityString = "Closed";
          }
          if(this.hospitals[i].todaysAvailabilityString == "00:00AM - 11:59PM"){
              this.hospitals[i].todaysAvailabilityString = "24 Hrs Open";
          }
      }
  }
  
  
  getDoctorFeedbacks = (doctorId) => {
      console.log("patientId ",doctorId);
      this.patientRoleService.getDoctorFeedbacks( doctorId, this.pageNo ).subscribe(
              res => {
                  if ( res.status == "success" ) {
                  this.commonService.hideLoading();
                  this.feedbackData = res.data.list;
                  for(let i=0;i<this.feedbackData.length;i++){
                  let convertedTime = this.commonService.convertTo12hrsFormat(new Date(this.feedbackData[i].feedbackCreatedAt));
                  this.feedbackData[i].feedbackCreatedAt = new Date(this.feedbackData[i].feedbackCreatedAt).toLocaleDateString() + " "+convertedTime ;
              }
                  console.log("feedbackData -- >",this.feedbackData);
                  }
                  else{
                      this.commonService.hideLoading();
                      this.commonService.presentToast(res.message);
                  }
              }, error => {
                  if(!this.isSkipAndExplore){
                      this.commonService.presentToast( error.message );
                  }
                  this.commonService.hideLoading();
              } );
      }
  
  /*
   * Function to redirect to native map app on mobile device and on web open google map in new tab
   * */
  public plotAddressOnMap = ( hospital: any ) => {
      let currentHospitalAddress = hospital.name+ "+" +hospital.address.street + "+" + hospital.address.cityName + "+" + 
                                   hospital.address.state + "+" + hospital.address.zipCode;
      this.commonService.setAddressOnMap(currentHospitalAddress);
  }
  
  openTimingDetails = (hospital) => {
      let title = hospital.hospitalDetails.name;
      let subTitle= hospital.hospitalDetails.address.street +", "+ hospital.hospitalDetails.address.cityName;
      this.popover = this.commonService.showTimingList(title, subTitle, hospital.formattedAvailabilities);
      this.commonService.closePopupOnBackBtn(this.popover);
  }
  
  /*
   * more Specialities List and service list
   * */
                  
   hideMore(type){
       if(type == "speciality"){
           console.log("speciality 185.......");
           this.doctorSpecialities = this.doctorData.specializationList;
           this.viewMoreSpFlag = false;
       } else{
           console.log("services 189.......");
           this.doctorServices = this.doctorData.serviceList;
           this.viewMoreServiceFlag = false;
       }
       this.content.resize();
   }
   
   /*
    * less Specialities List and service list
    * */
   hideLess(type){
       if(type == "speciality"){
           this.doctorSpecialities = this.initialSpecialities;
           this.viewMoreSpFlag = true;   
       } else{
           this.doctorServices = this.initialServices;
           this.viewMoreServiceFlag = true;
       }
       this.content.resize();
   }
   
   hospitalListClick(){
       this._ngZone.run(() => {
           this.currentSegment = 'hospitalInfo';
       });
   }
   
   otherInfoClick(){
       this._ngZone.run(() => {
           this.currentSegment = 'otherInfo';
       });
   }
   
   showGoToLoginPopup = () =>{
       this.commonService.showGoTOLoginPopup(this.navCtrl);
   }
   
}