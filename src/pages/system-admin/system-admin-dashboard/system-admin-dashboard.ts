import { Component, ViewChild, HostListener, Renderer2, ElementRef } from '@angular/core';
import { IonicPage, NavController, MenuController, Content, Events, Platform } from 'ionic-angular';

//components that are not lazy loaded

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO } from '../../../interfaces/user-data-dto';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";

@IonicPage({
    name: 'SADashboardPage',
    segment: 'system-admin-dashboard'
})
@Component({
    selector: 'page-sa-dashboard',
    templateUrl: 'system-admin-dashboard.html'
})
export class SADashboardPage {
    @ViewChild(Content) content: Content;
    @ViewChild('listGrid') listGrid: ElementRef;
    
    userData: UserDataDTO = {};
    scrollHTFlag: boolean = false;
    
    constructor( private platform: Platform, private commonService: CommonService, private manageSysAdminServiceProvider: ManageSysAdminServiceProvider, private evts: Events, private renderer: Renderer2, private navCtrl: NavController, private menu: MenuController, private constants: Constants ) {
        this.menu.enable(true);
    }
    
    ionViewWillEnter = () => {
        this.menu.enable(true);
        console.log("home ionViewWillEnter ==============================>",this.content.isScrolling);
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        
        /*this.manageSysAdminServiceProvider.getSysAdminFromId().subscribe(
        res=>{
            console.log("on sysAdmin dashboard res......", res);
        },
        error=>{
            console.log("on sysAdmin dashboard error......", error);
        })*/
    }
    
    ionViewDidLoad = () => {
        console.log("home ionViewDidLoad ==============================>",this.content);
        setTimeout(() => {
            this.scrollHTFlag = this.commonService.contentHeight( this.content,this.listGrid );
        }, 100 );
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                this.navCtrl.setRoot( 'LoginPage' );
            }else{
                this.userData = result;
                if(result.role == this.constants.ROLE_SYSTEM_ADMIN){
                    this.commonService.setActiveUserRole(this.constants.ROLE_SYSTEM_ADMIN);
                }
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.fireSelectedEvent('#/system-admin-dashboard');
                console.log("home isLoggedIn ==============================>",this.userData);
                this.commonService.setPaddingTopScroll( this.content );
            }
        });
    }
    
    showComingSoon = () => {
        console.log("home isLoggedIn ==============================>");
        this.commonService.presentToast();        
    }
}