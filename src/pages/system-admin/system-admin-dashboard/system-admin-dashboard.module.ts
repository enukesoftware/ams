import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SADashboardPage } from './system-admin-dashboard';
import { SharedModule } from "../../components/shared.module";
 
@NgModule({
    declarations: [
       SADashboardPage
    ],
    imports: [
       IonicPageModule.forChild(SADashboardPage),
       SharedModule,
       TranslateModule.forChild()
    ],
    exports: [
       SADashboardPage
    ]
})
export class SADashboardPageModule {}
