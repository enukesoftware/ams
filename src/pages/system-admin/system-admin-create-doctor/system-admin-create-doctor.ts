import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonicPage, Content, NavController, NavParams, MenuController } from 'ionic-angular';
import { CommonService } from "../../../providers/common-service/common.service";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { UserDataDTO, DoctorDataDto, AddressDto, serviceAndSpecializationDataDto } from "../../../interfaces/user-data-dto";
//3rd party npm module
import * as _ from 'lodash';
import { DoctorRoleServiceProvider } from "../../../providers/doctor-role-service/doctor-role-service";
import { CameraService } from '../../../providers/camera-service/camera.service';

@IonicPage({
    name: 'SystemAdminCreateDoctorPage',
    segment: 'system-admin-create-doctor/:mode/:id'
})
@Component({
  selector: 'page-system-admin-create-doctor',
  templateUrl: 'system-admin-create-doctor.html',
})
export class SystemAdminCreateDoctorPage {
    @ViewChild(Content) content: Content;
    userData: UserDataDTO = {};
    doctorData: DoctorDataDto = {};
    address: AddressDto = {};
    isProfileEdit: boolean;
    isMyProfile:boolean;
     formSubmitted: boolean;
    servicesString: any;
    specializationString: any;
    doctorEducation: any[];
    selectSpec: boolean = false;
    selectCat: boolean = true;
    selectService: boolean = false;
    selectEdu: boolean = false;
    selectCity: boolean = false;
    isWeb: boolean = false;
    cityList: any;
    doctorEdt: any;
    categories: any;
    category: any;
    selectedCategories: any = [];
    services: any;
    servicesArr: serviceAndSpecializationDataDto[];
    specialization: any;
    specializationArr: serviceAndSpecializationDataDto[];
    viewFlag: boolean = false;
    editFlag: boolean = false;
    createFlag: boolean = false;
    doctorId: any;
    mode: any;
    inputType: any = "text";
    rank: number;
    isDefault: boolean;
    imageUpload: any = {};
    isUpload: boolean = false;
    protected cloneDoctorData:any;
    isViewMore: boolean = false;
    
    constructor( public navCtrl: NavController, private constants: Constants, private menu: MenuController, public navParams: NavParams, public commonService: CommonService, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider,
            public cameraService : CameraService) {
   }

   ionViewWillEnter = () => {
         this.menu.enable( true );
         //this.doctorData.educationList = [];
         //check if user is authorized or not to access the screen if not redirect him to login screen
         this.rank = 0;
         this.isAuthorized();
     }

     /**
      * Function to check if user is authorized or not to access the screen
      */
     isAuthorized = () => {
         this.commonService.isLoggedIn(( result ) => {
             if ( !result ) {
                 this.navCtrl.setRoot( 'LoginPage' );
             } else {
                 this.userData = result;
                 this.isProfileEdit = false;
                 this.isMyProfile = false;
                 this.formSubmitted = false;
               //  console.log("userData", this.userData);
                 this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                 this.commonService.fireSelectedEvent( '#/system-admin-doctor-list' );
                 this.listOfCities();
                 this.isWeb = this.commonService.checkIsWeb();
                 if(this.navParams.get("mode") == "view"){
                     this.viewFlag = true;
                     this.editFlag = false;
                     this.createFlag = false;
                     this.viewDoctorProfile();
                 }else if(this.navParams.get("mode") == "edit"){
                     this.editFlag = true;
                     this.createFlag = false;
                     this.viewFlag = false;
                     this.viewDoctorProfile();
                 }else{
                     this.createFlag = true;
                     this.viewFlag = false;
                     this.editFlag = false;
                     this.getSpecializationServices();
                 }
                 this.getEducation();
             }
         } );
     }
     
     cityChange(){
         if(this.cityList){
             let cityLen = this.cityList.length;
             for(let i=0; i< cityLen; i++){
                 if(this.address.cityId == this.cityList[i].id){
                     this.address.cityName = this.cityList[i].name;
                 }
             }
         }
     }
     
     getSpecializationServices = () => {
         this.manageHospitalServiceProvider.getDoctorSpecializationServices().subscribe( res => {
             try{
                 if ( res.status == "success" ) {
                     this.categories = res.data;
                     if( this.categories ){
                         let categoryLen = this.categories.length;
                         for( let n=0;n<categoryLen;n++ ){
                             this.categories[n].value = this.categories[n];
                             this.categories[n].label = this.categories[n].name;
                         }
                         var catLen = this.categories.length;
                         var catIdArr = [];
                             
                         if(this.doctorData.serviceList){  
                             var serviceLen = this.doctorData.serviceList.length;
                             if(serviceLen > 0){
                                 for(let i=0; i<serviceLen; i++){
                                     if(catIdArr.indexOf( this.doctorData.serviceList[i].categoryId ) == -1 ){
                                        catIdArr.push(this.doctorData.serviceList[i].categoryId);
                                     }
                                 }
                             }
                         }
                        if(this.doctorData.specializationList){
                            var specLen = this.doctorData.specializationList.length;     
                            if(specLen > 0){
                                for(let i=0; i<specLen; i++){
                                    if(catIdArr.indexOf( this.doctorData.specializationList[i].categoryId ) == -1 ){
                                        catIdArr.push(this.doctorData.specializationList[i].categoryId);
                                    }
                                }
                            }
                        }    
                         var catIdLen = catIdArr.length; 
                         var selectedCats = [];  
                         if(catLen > 0){
                            for(let i=0; i<catLen; i++){
                                for(let j=0; j<catIdLen; j++){
                                   if(this.categories[i].id == catIdArr[j]){
                                        selectedCats.push(this.categories[i]);
                                    }
                                }
                            }
                            this.category = selectedCats;
                        }
                            
                        this.onChangeCategory();
                    }
                }
            }catch(err){
              console.log("err on 166...", err);
          }
        this.commonService.setPaddingTopScroll( this.content );

         }, error => {
             console.log( "specialization error.....", error );
             this.commonService.setPaddingTopScroll( this.content );

         } );
     }
     
     /**
      * Function to get list of cities
      */
     listOfCities = () => {
         this.commonService.getListOfCities().subscribe(
             res => {
                 if ( res.status == "success" ) {
                     this.cityList = res.data.list;
                     this.address.cityId = "0";
                 }
             },
             error => {
                 this.commonService.hideLoading();
                 let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                 this.commonService.presentToast(errorMsg);
             }
         );
     }
     
     
     /**
      * Function onChangeCategory
      */
     onChangeCategory(dropDownChange?: boolean, values?: any){
         if( values ){
             this.category = values.value;
         }
         this.specializationArr = [];
         this.servicesArr = [];
         try{
             if( this.category ){
                 for( let h=0;h<this.category.length;h++ ){
                     if( this.category[h].specializationList ){
                         for( let t=0;t<this.category[h].specializationList.length;t++ ){
                             this.category[h].specializationList[t].categoryId = this.category[h].id;
                             this.category[h].specializationList[t].label = this.category[h].specializationList[t].name;
                             this.category[h].specializationList[t].value = this.category[h].specializationList[t];
                             this.specializationArr.push( this.category[h].specializationList[t] );
                             if(this.doctorData.specializationList != null){
                                 for( let r=0;r<this.doctorData.specializationList.length;r++ ){
                                     if( this.doctorData.specializationList[r].id == this.category[h].specializationList[t].id && this.doctorData.specializationList[r].categoryId == this.category[h].id ){
                                         this.doctorData.specializationList.splice( r,1 );
                                         this.doctorData.specializationList.splice( r,0,this.category[h].specializationList[t].value );
                                     }
                                 }
                             }
                         }
                     }
                     if ( this.category[h].serviceList ) {
                         for ( let d=0;d<this.category[h].serviceList.length;d++ ){
                             this.category[h].serviceList[d].categoryId = this.category[h].id;
                             this.category[h].serviceList[d].label = this.category[h].serviceList[d].name;
                             this.category[h].serviceList[d].value = this.category[h].serviceList[d];
                             this.servicesArr.push( this.category[h].serviceList[d] );
                             for( let q=0;q<this.doctorData.serviceList.length;q++ ){
                                 if( this.doctorData.serviceList[q].id == this.category[h].serviceList[d].id && this.doctorData.serviceList[q].categoryId == this.category[h].id ){
                                     this.doctorData.serviceList.splice( q,1 );
                                     this.doctorData.serviceList.splice( q,0,this.category[h].serviceList[d].value );
                                 }
                             }
                         }
                     }
                 }
                 if( dropDownChange ){
                     if( this.specializationArr.length <= 0 ){
                         this.doctorData.specializationList = [];                        
                     }
                 
                     if( this.servicesArr.length <= 0 ){
                         this.doctorData.serviceList = [];
                     }
                     for( let b=0;b<this.doctorData.specializationList.length;b++ ){
                         let foundSpecializationCat = _.findIndex(this.specializationArr,(o) => { return o.categoryId == this.doctorData.specializationList[b].categoryId; });
                         if( foundSpecializationCat == -1 ){
                             this.doctorData.specializationList.splice( b,1 );
                         }
                     }
             
                     for( let z=0;z<this.doctorData.serviceList.length;z++ ){
                         let foundServiceCat = _.findIndex(this.servicesArr,(o) => { return o.categoryId == this.doctorData.serviceList[z].categoryId; });
                         if( foundServiceCat == -1 ){
                             this.doctorData.serviceList.splice( z,1 );
                         }
                     }
                 }
             }
         }catch(err){
             console.log("err on category...", err);
         }                
         
         this.getSpecialization();
         this.getServices();
     }
                     
     /**
      * Function to getSpecialization
      */
                     
     getSpecialization = ( selectedValue?: any ) => {
         try{
             if( selectedValue ){
                 this.doctorData.specializationList = selectedValue.value;
             }
             if(this.doctorData.specializationList){
                 let specLen = this.doctorData.specializationList.length;
                 if ( specLen == 0 ) {
                     this.selectSpec = false
                 }
                 
                 
                 this.specializationString = "";
                 let selectedSpecLen = this.doctorData.specializationList.length;
                 for ( let i = 0; i < selectedSpecLen; i++ ) {
                     if ( i < selectedSpecLen - 1 ) {
                         this.specializationString = this.specializationString + this.doctorData.specializationList[i].name + ", ";
                     } else {
                         this.specializationString = this.specializationString + this.doctorData.specializationList[i].name;
                     }
                 }
             }
         }catch(err){
             console.log("err on specialization......", err)
         }
     }
     
     
     /**
      * Function to getServices
      */
     getServices = ( selectedValue?: any ) => {
         try{
             if( selectedValue ){
                 this.doctorData.serviceList = selectedValue.value;
             }
             if(this.doctorData.serviceList){
                 let serviceLen = this.doctorData.serviceList.length;
                 if ( serviceLen == 0 ) {
                     this.selectService = false;
                 }
                 
                 this.servicesString = "";
                 let selectedServiceLen = this.doctorData.serviceList.length;
                 for ( let i = 0; i < selectedServiceLen; i++ ) {
                     if ( i < selectedServiceLen - 1 ) {
                         this.servicesString = this.servicesString + this.doctorData.serviceList[i].name + ", ";
                     } else {
                         this.servicesString = this.servicesString + this.doctorData.serviceList[i].name;
                     }
                 }
             }
         }catch(err){
             console.log("err......", err);
         }
     }
     
     /**
      * Function to getEducation
      */
     
     getEducation() {
         this.manageHospitalServiceProvider.getDoctorEducation().subscribe( res => {
             if ( res.status == "success" ) {
                 this.doctorEducation = res.data.list;
                 if(this.doctorEducation){
                     let educationLen = this.doctorEducation.length;
                     for( let n=0;n<educationLen;n++ ){
                         this.doctorEducation[n].label = this.doctorEducation[n].name;
                         this.doctorEducation[n].value = this.doctorEducation[n];
                         if( this.doctorData.educationList ){
                             for( let m=0;m<this.doctorData.educationList.length;m++ ){
                                 if( this.doctorEducation[n].id == this.doctorData.educationList[m].id ){
                                     this.doctorData.educationList.splice( m,1 );
                                     this.doctorData.educationList.splice( m,0,this.doctorEducation[n].value );
                                 }
                             }
                         }
                     }
                 }
                 //this.getDoctorEdu();
             }
         } )
     }
     /*
     * on change doctor edu
     */
                     
     getDoctorEdu( selectedValue?: any ){
         if( selectedValue ){
             this.doctorData.educationList = selectedValue.value;
         }
         this.selectEdu = false;
         if(this.doctorData.educationList){
             let eduLen = this.doctorData.educationList.length;
             if( this.commonService.checkIsWeb() ){
                 for ( let j = 0; j < eduLen; j++ ) {
                     this.doctorData.educationList[j].value = this.doctorData.educationList[j];
                     this.doctorData.educationList[j].label = this.doctorData.educationList[j].name;
                 }
             }
         }
     }
                     
     formatEducation = () => {
         return this.doctorData.educationList.map(education => education.name).join(", ");
     }
             
     formatCategory = () => {
         return this.category.map(selectedCategory => selectedCategory.name).join(", ");
     }

     formatSpecialization = () => {
         return this.doctorData.specializationList.map(specialization => specialization.name).join(", ");
     }

     formatServices = () => {
         return this.doctorData.serviceList.map(service => service.name).join(", ");
     }

     /**
      * Function to get doctor profile details
      */
     viewDoctorProfile(){
       this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
       this.doctorRoleServiceProvider.getDoctorProfile(this.navParams.get('id')).subscribe(
           res => {
             this.commonService.hideLoading();
             if(res.status == 'success'){
               this.doctorData = res.data;
               this.address = this.doctorData.address;
               this.imageUpload.file = this.doctorData.imageBase64;
               this.cloneDoctorData = _.cloneDeep( this.doctorData , false);
               this.getImageBase();
              // console.log("doctorData****************",this.doctorData);
                if( this.userData.userId == this.doctorData.doctorId){
                     this.isMyProfile = true;
                }
                this.getSpecializationServices();
                this.getEducation();
             }
           }, error => {
               this.commonService.hideLoading();
           });
     }

     
     
     /* function to get images in async */ 
     public getImageBase =()=>{
         //let startIndex = this.pageNo * this.constants.PAGE_SIZE;
         
         console.log("on get image base");
             let imageData = {
                     'userType': "USER_PROFILE_IMAGE",
                     'id': this.doctorData.doctorId,
                     'rank': 1,
              }   
         this.commonService.downloadImage( imageData ).subscribe(
         res => {
             if ( res.status == "success" ) {
                 console.log("image base 64=======>", res);
                 this.doctorData.imageBase64 = res.data.file;
             }
         },
         error => {
             this.commonService.hideLoading();
           /*  let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
             this.commonService.presentToast(errorMsg);*/
     });
     }
     
     
     /**
      * Function to create doctor profile
      */
     createDoctor( form: NgForm ){
          this.formSubmitted = true;
          if ( form.valid && this.doctorData.educationList && this.doctorData.educationList.length > 0 && this.doctorData.daysForAdvanceBooking <= 30) {
             var educationIdArr = []; 
             this.formSubmitted = false;
             let drFirstName = this.doctorData.firstName;
             let replacedFirstName = drFirstName.replace("Dr. ", "");
             this.doctorData.firstName = replacedFirstName;
             if(this.doctorData.specializationList){
                 for( let m=0;m< this.doctorData.specializationList.length;m++ ){
                     if(this.doctorData.specializationList[m] && this.doctorData.specializationList[m].label){    
                         delete this.doctorData.specializationList[m].label;
                     }
                     if(this.doctorData.specializationList[m] && this.doctorData.specializationList[m].value){
                         delete this.doctorData.specializationList[m].value;
                     }
                 }
             }
             if(this.doctorData.serviceList){
                 for( let n=0;n< this.doctorData.serviceList.length;n++ ){
                     if(this.doctorData.serviceList[n] && this.doctorData.serviceList[n].label){
                        delete this.doctorData.serviceList[n].label;
                     }
                     if(this.doctorData.serviceList[n] && this.doctorData.serviceList[n].value){
                        delete this.doctorData.serviceList[n].value;
                     }
                 }
             }
             if(this.doctorData.educationList){
                 var tempEducationList = this.doctorData.educationList;
                 for( let e=0; e< this.doctorData.educationList.length; e++ ){
                     educationIdArr.push(this.doctorData.educationList[e].id);
                 }
             }
             
             this.doctorData.educationList = educationIdArr; 
             this.doctorData.address = this.address;
             this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
             if(this.editFlag){
                 this.doctorRoleServiceProvider.updateDoctorProfile( this.doctorData ).subscribe(
                 res => {
                     this.commonService.hideLoading();
                     if(res.status == 'success'){
                         this.commonService.presentToast(res.message);
                         this.doctorData = res.data;
                        // console.log("this.userData in edit ======",this.doctorData);
                         if( this.doctorData && this.doctorData.firstName ){
                             this.doctorData.firstName = this.doctorData.firstName.replace("Dr. ", "");
                         }
                         if( this.doctorData && this.doctorData.imageBase64 ){
                             this.imageUpload.file = this.doctorData.imageBase64;
                         } 
                         
                         if(this.doctorData && this.doctorData.fileId && this.isUpload){
                             this.editImage(this.doctorData.fileId);
                         }else if(this.doctorData.doctorId && !this.doctorData.fileId && this.isUpload){
                             this.uploadImage(this.doctorData.doctorId);
                         }
                         
                         setTimeout(()=>{
                             this.navCtrl.setRoot("SystemAdminDoctorListPage");
                         }, 3000);
                     }
                 }, error => {
                     this.commonService.hideLoading();
                     this.doctorData.educationList = tempEducationList;
                     this.commonService.presentToast(error.message);
                     console.log("error....", error);
                 });
             }else{
                 this.manageHospitalServiceProvider.createDoctor( this.doctorData ).subscribe(
                 res => {
                     this.commonService.hideLoading();
                     if(res.status == 'success'){
                         this.doctorData = res.data;
                         if( this.doctorData && this.doctorData.firstName ){
                             this.doctorData.firstName = this.doctorData.firstName.replace("Dr. ", "");
                         }
                         
                         if(this.doctorData && this.doctorData.fileId && this.isUpload){
                             this.editImage(this.doctorData.fileId);
                         }else if(this.doctorData.doctorId && !this.doctorData.fileId && this.isUpload){
                             this.uploadImage(this.doctorData.doctorId);
                         }
                         
                         setTimeout(()=>{
                             this.navCtrl.setRoot("SystemAdminDoctorListPage");
                         }, 3000);
                     }
                 }, error => {
                     this.commonService.hideLoading();
                     this.commonService.presentToast(error.message);
                     this.doctorData.educationList = tempEducationList;
                 });
             }
         }
     }
     
     /**
      * Function to upload image 
      **/
      uploadImage = ( userId: number ) => {
         let imageData = {
             "userType": "USER_PROFILE_IMAGE",
             "mediaType": "IMAGE",
             "id": userId,
             "file": this.imageUpload.file,
             "fileName": this.imageUpload.fileName,
             "isDefault": this.isDefault,
             "rank" : this.rank
         }
         this.commonService.uploadImage(imageData).subscribe(
             res => {
                 if ( res.status == "success" ) {
                 }
             },
             error => {
                 this.commonService.hideLoading();
                 let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                 this.commonService.presentToast(errorMsg);
         });
      }                       
              
      /**
       * Function to upload image 
       **/
       editImage = ( fileId: number ) => {
          let imageData = {
              "userType": "USER_PROFILE_IMAGE",
              "mediaType": "IMAGE",
              "id": fileId, //file Id
              "file": this.imageUpload.file,
              "fileName": this.imageUpload.fileName,
              "isDefault": this.isDefault,
              "rank" : this.rank
          }
          this.commonService.editImage(imageData).subscribe(
              res => {
                  if ( res.status == "success" ) {
                  }
              },
              error => {
                  this.commonService.hideLoading();
                  this.commonService.showAlert( "Error", error.message );
          });
      }
                 
                 
         /**
          * Function to browse image 
          **/
     fileChangeListener($event, isDefault?: boolean, isUpload?: boolean) {
         this.rank++;
         this.isDefault = isDefault;
         var image:any = new Image();
         var file:File = $event.target.files[0];
         var myReader:FileReader = new FileReader();
         var that = this;
         this.imageUpload = {
             "file":"",
             "fileName":""
         }
         myReader.onloadend = (loadEvent:any) => {
             image.src = loadEvent.target.result;
             this.commonService.convertImgToBase64(image.src, (callback)=>{
                 this.imageUpload.file = callback;
                 this.imageUpload.fileName = file.name;
                 if( callback ){
                     this.isUpload = isUpload;
                 }
             })
             //console.log("image.src change listener=====",  this.imageUpload.file);
         };
              myReader.readAsDataURL(file);
              //console.log("this.imageUpload.file change listener=====", this.imageUpload.file);
       }                 
                 
     editProfile(){
         this.isProfileEdit = true;
         this.editFlag = true;
         this.viewFlag = false;
         this.imageUpload.file = this.doctorData.imageBase64;
     }

     cancelEdit(){
        
         this.viewDoctorProfile();
         this.isProfileEdit = false;
         this.editFlag = false;
         this.viewFlag = true;
         this.imageUpload.file = '';
         //this.viewDoctorProfile();
         this.doctorData =  _.cloneDeep(this.cloneDoctorData , false);
        // console.log('this.doctorData',this.cloneDoctorData, this.doctorData);
     }
     
     /***
      * Function to open camera/photo gallery
      */
      protected onCamera = () => {
          try{
             // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
              this.cameraService.loadImage(this.successCallback,this.errorCallback);
          }catch(e){
              
          }
      }

      /**
       * Function to handle camera success callback
       * @param success
       */
      private successCallback = ( base64Image: any ) => {
          this.isUpload = true;
          this.rank++;
          this.isDefault = true;
        //  this.imageUpload.file = base64Image;
          this.commonService.convertImgToBase64(base64Image, (callback)=>{
              this.imageUpload.file = callback;
          })
        console.log('success callback',base64Image);
      }
      
      /**
       * Function to handle camera error callback
       * @param error
       */
      private errorCallback = ( error: any ) => {
         console.log( 'Unable to load profile picture.' ,error);
      }
     
     
     /**
      * Function to show coming soon popup
      */
     comingSoonPopup = () => {
         this.commonService.presentToast();
     }

     /*
     * Function to viewMore
     **/
    viewMore(){
        this.isViewMore = true;
    }

    viewLess(){
        this.isViewMore = false;
    }
}

