import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MultiSelectModule } from "primeng/multiselect";

import { SystemAdminCreateDoctorPage } from './system-admin-create-doctor';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    SystemAdminCreateDoctorPage,
  ],
  imports: [
    IonicPageModule.forChild(SystemAdminCreateDoctorPage),
    TranslateModule.forChild(),
    SharedModule,
    SelectSearchableModule,
    MultiSelectModule
  ],
  exports: [
     SystemAdminCreateDoctorPage    
   ]
})
export class SystemAdminCreateDoctorPageModule {}
