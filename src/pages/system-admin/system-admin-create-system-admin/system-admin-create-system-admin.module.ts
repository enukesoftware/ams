import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SystemAdminCreateSystemAdminPage } from './system-admin-create-system-admin';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    SystemAdminCreateSystemAdminPage,
  ],
  imports: [
        IonicPageModule.forChild(SystemAdminCreateSystemAdminPage),
        SharedModule,
        TranslateModule.forChild()
  ],
  exports: [
    SystemAdminCreateSystemAdminPage
  ]
})
export class SystemAdminCreateSystemAdminPageModule {}
