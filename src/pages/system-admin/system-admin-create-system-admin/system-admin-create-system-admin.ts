import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonicPage, NavController, Content, NavParams, MenuController, Platform } from 'ionic-angular';

import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO, SystemAdminsListDto } from "../../../interfaces/user-data-dto";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { LocalStorageService } from '../../../providers/localStorage-service/localStorage.service';
import { CameraService } from '../../../providers/camera-service/camera.service';


@IonicPage({
    name: 'SystemAdminCreateSystemAdminPage',
    segment: 'system-admin-create-system-admin/:mode/:id'
})
@Component({
  selector: 'page-system-admin-create-system-admin',
  templateUrl: 'system-admin-create-system-admin.html',
})
export class SystemAdminCreateSystemAdminPage {
    @ViewChild( Content ) content: Content;

    isEditSysAdminMode: any;
    selectedAdminDetails: SystemAdminsListDto = {};
    showDetails =false;
    userData: UserDataDTO = {};
    adminData:any;
    formSubmitted: boolean;
    isManageSysAdmin: boolean = false;
    isMyProfile: boolean = false;
    disableEmail: boolean = false;
    isCreate: boolean = false;
    userId: any;
    mode: string;
    rank: number;
    isDefault: boolean;
    imageUpload: any = {};
    isView: boolean = false;
    isUpload: boolean = false;
    
      constructor( private platform: Platform, public cameraService : CameraService,public navCtrl: NavController, private constants: Constants, public navParams: NavParams, private menu: MenuController, public commonService: CommonService,  private manageSysAdminServiceProvider:ManageSysAdminServiceProvider) {

          console.log('in constructor');
      }
      
      ionViewWillEnter(){
         this.menu.enable(true);
         this.rank = 0;
         //check if user is authorized or not to access the screen if not redirect him to login screen
         this.isAuthorized();
      }
      
      /*
       * Function to check if user is authorized or not to access the screen
       * */
      isAuthorized = () => {
          this.commonService.isLoggedIn((result) => {
              if( !result ){
                  this.navCtrl.setRoot( 'LoginPage' );
              }else{
                  this.userData = result;
                  this.userId = result.userId;
                  this.mode = this.navParams.get('mode');
                  if(this.navParams.get('mode') == "create"){
                      this.isEditSysAdminMode = false;
                      this.isCreate = true;
                      this.isView = false;
                      this.disableEmail = false;
                  } else if( this.navParams.get('mode') == "view"){
                      this.isView = true;
                      this.isEditSysAdminMode = false;
                      this.isCreate = false;
                      this.disableEmail = false;
                      if(this.navParams.get('id') == this.userId || this.navParams.get('id') == "0"){
                          this.isMyProfile = true;
                          this.getAdminData(this.userId);
                      }else{
                          this.userId = this.navParams.get('id');
                          this.isMyProfile = false;
                          this.getAdminData(this.userId);
                      }
                  }else{
                      this.isEditSysAdminMode = true;
                      this.isView = false;
                      this.isCreate = false;
                      this.disableEmail = true;
                      if(this.navParams.get('id') == this.userId || this.navParams.get('id') == "0"){
                          this.isMyProfile = true;
                          this.getAdminData(this.userId);
                      }else{
                          this.userId = this.navParams.get('id');
                          this.isMyProfile = false;
                          this.getAdminData(this.userId);
                      }
                  }
                  if( this.isMyProfile ){
                      this.commonService.fireSelectedEvent('#/system-admin-create-system-admin/view/0');
                  }else{
                      this.commonService.fireSelectedEvent('#/system-admin-manage-sysadmins');
                  }
                  this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                  
              }
          });
      }
      
      cancelEdit = () => {
          this.getAdminData(this.userId);
          this.eidtProfile(true);
      }
      
           
    /**
     * Function to activate or deactivate doctor
     */

    public activateDeactivateSysAdmin =() => {
          let activationStatus:any;
          this.commonService.getActivationStatus(this.selectedAdminDetails,(cb)=>{
              activationStatus =cb;
              console.log("activationStatus",activationStatus);
          });
          
          let sysAdminObj ={
                  userId:this.selectedAdminDetails.userId,
                  action:activationStatus.Action
        }
          
          let activationPopUpMsg = this.commonService.getTranslate('CONFIRM_AD_MSG1',{})+  this.commonService.getTranslate(activationStatus.text,{})
          let warningTitle =  this.commonService.getTranslate('WARNING',{});
           
          let alert = this.commonService.confirmAlert(warningTitle, activationPopUpMsg+'?');
          alert.setMode("ios");
          alert.present();
          alert.onDidDismiss((data) => {
            if(data == true){  
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.manageSysAdminServiceProvider.activateDeactivateSysAdmin(sysAdminObj).subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.commonService.hideLoading();
                    if(this.selectedAdminDetails.blockStatus =="UNBLOCK"){
                        console.log("in if",this.selectedAdminDetails.blockStatus);
                        this.selectedAdminDetails.blockStatus ="BLOCK";
                    } 
                    else if(this.selectedAdminDetails.blockStatus =="BLOCK"){
                        console.log("in else",this.selectedAdminDetails.blockStatus);
                        this.selectedAdminDetails.blockStatus ="UNBLOCK";
                    }
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
                else{
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
            }
        },
        error => {
            this.commonService.hideLoading();
            this.commonService.presentToast(error.message);
        });
             }else{}
         });
     }
      
      /*
       * Function to to get admin data
       * */
      getAdminData(id){
          this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
          this.manageSysAdminServiceProvider.getSysAdminFromId(id).subscribe(
             res=>{
                 this.commonService.hideLoading();
                 if(res.status == "success"){
                     this.selectedAdminDetails = res.data;
                    if( this.selectedAdminDetails.imageBase64 ){
                        this.imageUpload.file = this.selectedAdminDetails.imageBase64;
                    } 
                    this.getImageBase();
                 }
                 this.commonService.setPaddingTopScroll( this.content );
             },
             error=>{
               this.commonService.hideLoading();
               console.log("error.......", error);
               this.commonService.setPaddingTopScroll( this.content );
             });
      }
      
      
      /* function to get images in async */ 
      public getImageBase =()=>{
          //let startIndex = this.pageNo * this.constants.PAGE_SIZE;
          
          console.log("on get image base");
              let imageData = {
                      'userType': "USER_PROFILE_IMAGE",
                      'id': this.selectedAdminDetails.userId,
                      'rank': 1,
               }   
          this.commonService.downloadImage( imageData ).subscribe(
          res => {
              if ( res.status == "success" ) {
                  console.log("image base 64=======>", res);
                  this.selectedAdminDetails.imageBase64 = res.data.file;
              }
          },
          error => {
              this.commonService.hideLoading();
            /*  let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
              this.commonService.presentToast(errorMsg);*/
      });
      }
      
 
      /*
       * Function save edit profile
       * */
      
      eidtProfile = (isCancel?: boolean) => {
          this.isEditSysAdminMode = !this.isEditSysAdminMode;
          this.isCreate = false;
          if( isCancel ){
              this.isView = true;
              this.imageUpload.file = '';
          }else{
              this.isView = false;
          }
          
          if( this.isMyProfile ){
              if( isCancel ){
                  this.disableEmail = true;
                  this.imageUpload.file = '';
              }else{
                  this.disableEmail = false;
              }
              this.isMyProfile = true;
          }else{
              this.disableEmail = true;
              this.isMyProfile = false;
              if(isCancel){
                  this.isView = true;
                  this.imageUpload.file = '';
              }else{
                  this.isView = false;  
              }
          }
      }
      
      
      /*
       * Function save system admin details
       * */
      
      saveSystemAdminDetails( formData: NgForm ){
          console.log("in save",formData.value);
          this.formSubmitted = true;
          if(formData.valid){    
              this.formSubmitted = false;
              this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
              this.manageSysAdminServiceProvider.createEditSysAdmin(this.selectedAdminDetails, this.mode).subscribe(res=>{
                 if( res.status = 'success'){
                    this.selectedAdminDetails = res.data;
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                     if( !this.selectedAdminDetails.fileId && this.isUpload ){
                        this.uploadImage(this.selectedAdminDetails.userId);
                    }else if( this.isUpload && this.selectedAdminDetails.fileId){
                        this.editImage(this.selectedAdminDetails.fileId);
                    }
                    if( this.isMyProfile ){
                        this.userData = res.data;
                        this.commonService.setInStorage( 'userData',res.data );
                    }

                     if( this.mode == 'create' ){
                        this.navCtrl.setRoot( 'SystemAdminManageSysadminsPage');
                    }else{
                        setTimeout(()=>{
                            this.navCtrl.setRoot( 'SystemAdminCreateSystemAdminPage',{mode:'view',id:this.selectedAdminDetails.userId});
                           }, 3000);
                    }
                 }
              }, err => {
                  this.commonService.hideLoading();
                  this.commonService.presentToast(err.message);
              } );
              
          }
      }

    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean) {
        this.rank++;
        this.isDefault = isDefault;
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        this.imageUpload = {
            "file":"",
            "fileName":""
        }
        myReader.onloadend = (loadEvent:any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback)=>{
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if( callback ){
                    this.isUpload = isUpload;
                }
            })
        };
         myReader.readAsDataURL(file);
    }

     /**
     * Function to upload image 
     **/
     uploadImage = ( userId: any ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                    console.log("upload image sys admin=================", res);
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
        });
    }

    /**
     * Function to upload image 
     **/
     editImage = ( fileId: number ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": fileId, //file Id
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.editImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                    console.log("upload image sys admin=================", res);
                }
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.showAlert( "Error", error.message );
        });
    }

    /**
     * Cominsoon popup
     */
    showComingSoon = () => {
        this.commonService.presentToast();        
    }
      
    
    /***
     * Function to open camera/photo gallery
     */
     protected onCamera = () => {
         try{
            // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
             this.cameraService.loadImage(this.successCallback,this.errorCallback);
         }catch(e){
             
         }
     }

     /**
      * Function to handle camera success callback
      * @param success
      */
     private successCallback = ( base64Image: any ) => {
         this.isUpload = true;
         this.rank++;
         this.isDefault = true;
         //this.imageUpload.file = base64Image;
         this.commonService.convertImgToBase64(base64Image, (callback)=>{
             this.imageUpload.file = callback;
         })
         console.log('success callback',base64Image);
         
     }
     
     /**
      * Function to handle camera error callback
      * @param error
      */
     private errorCallback = ( error: any ) => {
        console.log( 'Unable to load profile picture.' ,error);
     }
    }