import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams } from 'ionic-angular';


//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { UserDataDTO, PatientDataDTO,AddressDto } from '../../../interfaces/user-data-dto';
import { LocalStorageService } from '../../../providers/localStorage-service/localStorage.service';
import { CameraService } from '../../../providers/camera-service/camera.service';

@IonicPage( {
    name: 'SystemAdminCreatePatientsPage',
    segment: 'system-admin-create-patients'
} )
@Component( {
    selector: 'page-system-admin-create-patients',
    templateUrl: 'system-admin-create-patients.html',
} )
export class SystemAdminCreatePatientsPage {
    @ViewChild( Content ) content: Content;
    address: AddressDto = {};
    patientData: PatientDataDTO = {};
    formSubmitted = false;
    userData: UserDataDTO = {};
    inputType: any = "text";
    isDefault: boolean;
    imageUpload: any = {};
    rank: number;
    isUpload: boolean = false;
    
    constructor( public navCtrl: NavController, public cameraService : CameraService, public navParams: NavParams, public commonService: CommonService,
        private constants: Constants, private manageSysAdminServiceProvider: ManageSysAdminServiceProvider,
        private locstr: LocalStorageService ) {
    }
  
    ionViewDidLoad() {
        console.log( 'ionViewDidLoad SystemAdminCreatePatientsPage' );
    }
    
    ionViewWillEnter = () => {
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.rank = 1;
        this.isAuthorized();
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                this.navCtrl.setRoot( 'LoginPage' );
            }else{
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.fireSelectedEvent('#/system-admin-manage-patients');
                this.commonService.setPaddingTopScroll( this.content );
            }
        });
    }

    public createPatient = ( form: any ) => {
        this.formSubmitted = true;
        if ( form.valid ) {
            this.formSubmitted = false;
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.patientData.address=this.address;
            this.manageSysAdminServiceProvider.createPatient( this.patientData ).subscribe(
                res => {
                    if ( res.status == "success" ) {
                        this.commonService.hideLoading();
                        this.commonService.presentToast( res.message );
                        this.address=this.patientData.address;
                        console.log("res",res);
                        if( this.isUpload && res.data && res.data.userId){
                            this.uploadImage(res.data.userId);
                        }
                        this.navCtrl.setRoot( 'SystemAdminManagePatientsPage' );
                    }
                    else {
                        this.commonService.hideLoading();
                        this.commonService.presentToast( res.message );
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);

                }
            );
        }
        else {
            console.log( "invalid form", form );
        }
    }

    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean) {
        this.rank++;
        this.isDefault = isDefault;
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        var that = this;
        this.imageUpload = {
            "file":"",
            "fileName":""
        }
        myReader.onloadend = (loadEvent:any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback)=>{
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if( callback ){
                    this.isUpload = isUpload;
                }
            });
        };
         myReader.readAsDataURL(file);
    }

     /**
     * Function to upload image 
     **/
     uploadImage = ( userId: number ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                    console.log("upload image sys admin=================", res);
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
        });
    }
    
     /***
     * Function to open camera/photo gallery
     */
     protected onCamera = () => {
         try{
            // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
             this.cameraService.loadImage(this.successCallback,this.errorCallback);
         }catch(e){
             
         }
     }

     /**
      * Function to handle camera success callback
      * @param success
      */
     private successCallback = ( base64Image: any ) => {
         this.isUpload = true;
         this.rank++;
         this.isDefault = true;
         //this.imageUpload.file = base64Image;
         this.commonService.convertImgToBase64(base64Image, (callback)=>{
             this.imageUpload.file = callback;
         })
       console.log('success callback',base64Image);
     }
     
     /**
      * Function to handle camera error callback
      * @param error
      */
     private errorCallback = ( error: any ) => {
        console.log( 'Unable to load profile picture.' ,error);
     }
    
    
    /*
     * Function to handle click event on profile photo
     * */
    public comingsoonPopup = () => {
        this.commonService.presentToast();
    }

}
