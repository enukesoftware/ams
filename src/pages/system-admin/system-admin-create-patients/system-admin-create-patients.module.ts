import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";

import { SystemAdminCreatePatientsPage } from './system-admin-create-patients';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    SystemAdminCreatePatientsPage,
  ],
  imports: [
    IonicPageModule.forChild(SystemAdminCreatePatientsPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    SystemAdminCreatePatientsPage
  ]
})
export class SystemAdminCreatePatientsPageModule {}
