import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, Platform, MenuController, NavParams } from 'ionic-angular';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { UserDataDTO,SystemAdminsListDto } from "../../../interfaces/user-data-dto";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
//import { HospitalListComponent } from "../../components/hospital-list-component/hospital-list-component";
import { AuthService } from "../../../providers/authService/authService";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";

@IonicPage( {
    name: 'SystemAdminManageSysadminsPage',
    segment: 'system-admin-manage-sysadmins'
} )
@Component( {
    selector: 'page-system-admin-manage-sysadmins',
    templateUrl: 'system-admin-manage-sysadmins.html',
} )
export class SystemAdminManageSysadminsPage {
    @ViewChild(Content) content: Content;
    
    isManageSysAdmin: boolean = false;
    userData: UserDataDTO = {};
    pageNo: number = 0;
    fromSystemAdmin: string = "systemAdmin";
    //assign sys admins dto
    systemAdmins: SystemAdminsListDto[];
    loadFinished: boolean = false;
    
    constructor( private platform: Platform, public authService: AuthService, private constants: Constants, private menu: MenuController, public navCtrl: NavController, public navParams: NavParams, public manageHospitalService: ManageHospitalServiceProvider, public commonService: CommonService,
                 private manageSysAdminServiceProvider:ManageSysAdminServiceProvider) {}

    ionViewDidLoad() { }

    ionViewWillEnter = () => {
        this.menu.enable(true);
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                this.navCtrl.setRoot( 'LoginPage' );
            }else{
                this.userData = result;
                this.getSysAdminsList(true);
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.fireSelectedEvent('#/system-admin-manage-sysadmins');
                
            }
        });
    }
    
    /* function to get images in async */ 
    public getImageBase =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.systemAdmins.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.systemAdmins[i].userId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.systemAdmins.length; j++ ){
                    if(this.systemAdmins[j].userId == res.data.userId){
                        this.systemAdmins[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.systemAdmins);
            }
        },
        error => {
            this.commonService.hideLoading();
            /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }
    
    //To do : get system admin list on web service response
    public getSysAdminsList = (firstPageCall?: boolean) =>{
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.manageSysAdminServiceProvider.getSysAdminsList(this.pageNo).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == "success" ) {
                    //this.systemAdmins = res.data.list;
                    this.commonService.hideLoading();
                    if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                        this.systemAdmins = res.data.list;
                        this.getImageBase();
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else if( res.data.list && res.data.list.length > 0 ){
                        for( let j=0;j<res.data.list.length;j++ ){
                            this.systemAdmins.push(res.data.list[j]);
                        }
                        this.getImageBase();
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else{
                        this.loadFinished = true;
                    }
                }
                else{
                    this.commonService.presentToast(res.message);
                }
                this.commonService.setPaddingTopScroll( this.content );
            },
            error => {
                this.commonService.hideLoading();
                console.log('errorMsg==================outer====>');
                this.commonService.presentToast(error.message);
                this.commonService.setPaddingTopScroll( this.content );
            }
        );
    }
        /*
         * Function to load more data when clicked on load more button
         * */
       public loadMoreData = () => {
            this.pageNo = this.pageNo + 1;
            this.getSysAdminsList();
        }  
    
}