import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SystemAdminManageSysadminsPage } from './system-admin-manage-sysadmins';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    SystemAdminManageSysadminsPage
  ],
  imports: [
    IonicPageModule.forChild( SystemAdminManageSysadminsPage),
       SharedModule,
       TranslateModule.forChild()
  ],
})
export class SystemAdminManageSysadminsPageModule {}
