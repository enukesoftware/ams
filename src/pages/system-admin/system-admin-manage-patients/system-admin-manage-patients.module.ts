import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";

import { SystemAdminManagePatientsPage } from './system-admin-manage-patients';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    SystemAdminManagePatientsPage,
  ],
  imports: [
    IonicPageModule.forChild(SystemAdminManagePatientsPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
        SystemAdminManagePatientsPage,
      ]
})
export class SystemAdminManagePatientsPageModule {}
