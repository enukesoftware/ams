import { Component, ViewChild, NgZone,HostListener } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Platform, Events } from 'ionic-angular';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { AuthService } from "../../../providers/authService/authService";
import { CommonService } from "../../../providers/common-service/common.service";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { UserDataDTO } from "../../../interfaces/user-data-dto";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { MapServiceProvider } from "../../../providers/map-integration-service/map-integration-service";

@IonicPage( {
    name: 'SystemAdminManagePatientsPage',
    segment: 'system-admin-manage-patients'
} )
@Component( {
    selector: 'page-system-admin-manage-patients',
    templateUrl: 'system-admin-manage-patients.html'
} )
export class SystemAdminManagePatientsPage {
    @ViewChild( Content ) content: Content;

    cities: any;
    cityId: number;
    userData: UserDataDTO = {};
    patientList: any;
    loadFinished: boolean = false;
    pageNo: number = 0;
    protected address:any;
    protected matchCity:any;
    searchText: string = "";
    showSearchResultContainer: boolean = false;
    seeAllSearchClicked: boolean = false;
    
    constructor( private platform: Platform, public navCtrl: NavController, public authService: AuthService,private _ngZone: NgZone,public mapService : MapServiceProvider,
        private manageSysAdminServiceProvider: ManageSysAdminServiceProvider, public navParams: NavParams, private commonService: CommonService, private constants: Constants,
        private evts: Events) {

    }

    ionViewDidLoad() {
        console.log( 'ionViewDidLoad SystemAdminManagePatientsPage' );
    }

    ionViewWillEnter = () => {
        //this.getListOfCities();
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
               this.getListPatient(true);
                this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.setActiveUserRole( this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.fireSelectedEvent( '#/system-admin-manage-patients' );
                this.evts.publish('fire-after-login-event');
            }
        } );
    }

    /**
     * Function To get List of Cities
     **/
    getListPatient( firstPageCall?: boolean ) {
       this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.manageSysAdminServiceProvider.getPatientList(this.pageNo ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                    this.patientList = res.data.list;
                    console.log("PAT_LIST",this.patientList);
                    this.getImageBase();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                } else if ( res.data.list && res.data.list.length > 0 ) {
                    for ( let j = 0; j < res.data.list.length; j++ ) {
                        this.patientList.push( res.data.list[j] );
                    }
                    this.getImageBase();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                } else {
                    this.loadFinished = true;
                    if(res.data.list.length ==0){
                        this.patientList = [];
                    }
                }
                this.commonService.setPaddingTopScroll( this.content );
            }, error => {
                this.commonService.presentToast( error.message );
                this.commonService.hideLoading();
                this.commonService.setPaddingTopScroll( this.content );
            } );
    }


    /* function to get images in async */ 
    public getImageBase =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.patientList.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.patientList[i].userId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.patientList.length; j++ ){
                    if(this.patientList[j].userId == res.data.userId){
                        this.patientList[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.patientList);
            }
        },
        error => {
            this.commonService.hideLoading();
        /*    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }
    
    
    
    
    /*
     * Function to load more data when clicked on load more button
     * */
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getListPatient();
    }

    showComingSoon = () => {
        this.commonService.presentToast();
    }
    
    hideSearchContainer(event: Event) {
        if (event && event.stopPropagation) {
            event.stopPropagation();
        }
    }
    
    @HostListener('document:click', ['$event'])
    andClickEvent(event) { 
        this.showSearchResultContainer = false;
    }
    
    /*
     * Function for search
     * 
     * */
    onChangeSearchText(){
        if( this.searchText.length > 1 ){
            this._ngZone.run(() => {  
            this.showSearchResultContainer = true;
            console.log("searchText--",this.searchText);
            let searchObj = {
                searchText: this.searchText,
                currentDocIndex: 0,
                currentPage: 0,
                sortField:"",
                type: "patient",
                filters:[{
                         filterName: "",
                         rangeFrom: "",
                         rangeTo: "",
                         selectedValue: [{}]
                    }]
            }
            
            this.evts.publish( 'system-admin-search-patients',{searchObj: searchObj});
            });
        }else{
            this.showSearchResultContainer = false;
            if(this.seeAllSearchClicked){
                this.getListPatient(true);
            }
        }
    }
    
}
