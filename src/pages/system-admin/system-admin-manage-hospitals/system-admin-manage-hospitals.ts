import { Component, ViewChild, NgZone, HostListener } from '@angular/core';
import { NavController, Content, IonicPage, NavParams, Events, Platform, ModalController } from 'ionic-angular';
import { MapServiceProvider } from "../../../providers/map-integration-service/map-integration-service";

//providers services
import { CommonService } from "../../../providers/common-service/common.service";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { AuthService } from "../../../providers/authService/authService";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { UserDataDTO, HospitalListDTO } from "../../../interfaces/user-data-dto";
import { SearchFilterComponent } from "../../components/search-filter/search-filter";

@IonicPage({
    name: 'SAManageHospitalsPage',
    segment: 'system-admin-manage-hospitals'
})
@Component({
    selector: 'page-system-admin-manage-hospitals',
    templateUrl: 'system-admin-manage-hospitals.html'
})
export class SAManageHospitalsPage {
    @ViewChild(Content) content: Content;
    
    hospitals: HospitalListDTO[] = [];
    userData: UserDataDTO = {};
    cities: any;
    cityId: any;
    city:any;
    pageNo: number = 0;
    loadFinished: boolean = false;
    searchText: string = "";
    showSearchResultContainer: boolean = false;
    fromString: string = "systemAdminManageHospitals";
    seeAllClicked: boolean = false;
    protected address:any;
    setHospitalFilter: any;
    protected matchCity:any;
    
    constructor( private platform: Platform, public modalCtrl: ModalController, private _ngZone: NgZone,public mapService : MapServiceProvider,public evts: Events, public navCtrl: NavController, public manageHospitalService: ManageHospitalServiceProvider, public authService: AuthService,public navParams: NavParams, private commonService: CommonService, private constants: Constants ) {
        this.evts.subscribe( 'system-admin-search-more-hospitals', (data)=>{
            this.showSearchResultContainer = false;
            this.hospitals = data.searchObj;
            this.seeAllClicked = true;
            this.formatAvailability();
        });
    }
    
    showComingSoon = () => {
        this.commonService.presentToast();        
    }

    ionViewWillEnter = () => {
        this.getListOfCities();
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }
    
    @HostListener('document:click', ['$event'])
    andClickEvent(event) { 
        this.showSearchResultContainer = false;
    }
    
    hideSearchContainer(event: Event) {
        if (event && event.stopPropagation) {
            event.stopPropagation();
        }
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                this.navCtrl.setRoot( 'LoginPage' );
            }else{
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.fireSelectedEvent('#/system-admin-manage-hospitals');
            }
        });
    }
    
    /*
     * To handle body click when search box is open
     * */
    
    bodyClicked(){
        if(this.showSearchResultContainer){
            this.showSearchResultContainer = false;
        }
    }
    
    /*
     * Function to do calculations for time related 
     * */
    formatAvailability = () => {
        var currentDate = new Date();
        var today = currentDate.getDay();
        let hospitalLength = this.hospitals.length;
        for(let i=0; i< hospitalLength; i++){
            var todayAvailability;
            let availabilities = this.hospitals[i].timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime(availabilities);
            this.hospitals[i].formattedAvailabilities = this.commonService.calculateTime(availabilities);
            let availabilityLength = this.hospitals[i].formattedAvailabilities.length;
            this.hospitals[i].todayAvailability = this.hospitals[i].todaysTimeAvailabilityList;
            if(this.hospitals[i].todaysTimeAvailabilityList && this.hospitals[i].todaysTimeAvailabilityList.length > 0){
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = this.hospitals[i].todayAvailability.length;
                for(let j=0; j<todayTimeLen; j++){
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].toTime), true);
                    if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                        timeSlotFound = true;
                        this.hospitals[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if(!timeSlotFound){
                    this.hospitals[i].todaysAvailabilityString = "Closed";
                }
            } else{
                this.hospitals[i].todaysAvailabilityString = "Closed";
            }
            if(this.hospitals[i].todaysAvailabilityString == "00:00AM - 11:59PM"){
                this.hospitals[i].todaysAvailabilityString = "24 Hrs Open";
            }
        }
    }


/* function to get images in async */ 
public getImageBase =()=>{
    let startIndex = this.pageNo * this.constants.PAGE_SIZE;
    for (let i = startIndex; i <this.hospitals.length; i++) {
        let imageData = {
                'userType': "HOSPITAL_IMAGE",
                'id': this.hospitals[i].hospitalId,
                'rank': 1,
                'isDefault': true
         }   
    console.log("image base imageData=======>", imageData);
    this.commonService.downloadImage( imageData ).subscribe(
    res => {
        if ( res.status == "success" ) {
            console.log("image base 64=======>", res);
            for( let j=startIndex; j<this.hospitals.length; j++ ){
                if(this.hospitals[j].hospitalId == res.data.userId){
                    this.hospitals[j].imageBase64 = res.data.file;
                }
            }
            console.log("this.listOfDoctors=======>", this.hospitals);
        }
    },
    error => {
        this.commonService.hideLoading();
       /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
        this.commonService.presentToast(errorMsg);*/
});

    }
}
    
    /**
     * Function To get List of Hospitals
     **/
        
    getHospitalList = ( cityId:any,firstPageCall?: boolean ) => {
     //  this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        console.log('  this.loadFinished==>',this.loadFinished);
        this.manageHospitalService.getHospitalList( cityId,this.pageNo ).subscribe(res => {
            this.commonService.hideLoading();
            let tempPageNo = this.pageNo;
            if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                this.hospitals = res.data.list;
                this.getImageBase();
                if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                    this.loadFinished = true;
                }
                this.formatAvailability();
            }else if( res.data.list && res.data.list.length > 0 ){
                for( let j=0;j< res.data.list.length;j++ ){
                    this.hospitals.push(res.data.list[j]);
                }
                this.getImageBase();
                if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                    this.loadFinished = true;
                }
                this.formatAvailability();
            }else{
                this.loadFinished = true;
                if(res.data.list.length ==0){
                    this.hospitals = [];
                }
            }
            this.commonService.setPaddingTopScroll( this.content );
        }, error => {
            this.commonService.hideLoading();
            this.commonService.setPaddingTopScroll( this.content );
        });
    }
        
    /*
     * Function to load more data when clicked on load more button
     * */
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getHospitalList(this.cityId);
    }
    
    /**
     * Function To get List of Cities
     **/
    getListOfCities = () => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.commonService.getListOfCities().subscribe(
            res => {
                this.cities = res.data.list;
                if(this.cities && this.cities.length > 0 ){
                    this.city = {id:this.cities[0].id,name:this.cities[0].name};
                    this.cityId = this.cities[0].id;
                }else{
                    this.cityId="";
                }
                this.commonService.getFromStorage("SelectedHospCity").then((value)=>{
                    if(value){
                        this.address = value;  
                        // this.city.id = this.address.id;
                        // this.city.name = this.address.name;
                        this.city = {id:this.address.id,name:this.address.name};
                        console.log("address local storage..",  this.address);
                        this.searchCity(this.address.name,this.address.id); 
                        this.commonService.setInStorage("SelectedHospCity", this.address );
                    }
                    else{
                        this.getCurrentLocationAddress();
                    }
                 });
                //this.getCurrentLocationAddress();
            }, error => {
                console.log("error....", error);
            });
    }
    
    /**
     * Function for get Current Location Address
     * */
    protected getCurrentLocationAddress = () => {
        this.mapService.getCurrentLocation( response => {
            if ( response && response.coords && response.coords.latitude && response.coords.longitude ) {
                this.mapService.reverseGeocoding( response.coords.latitude, response.coords.longitude, ( status, result ) => {
                    if ( status ) {
                        this._ngZone.run(() => {
                            this.address = result.address;
                            this.commonService.setInStorage("SelectedHospCity", this.address );
                            this.searchCity(this.address,this.cityId);
//                            this.getHospitalList(this.cityId,true);
                        });
                    } else {
                        this.getHospitalList(this.cityId,true);
                        //not fetching address from current location   
                    }
                });
            } else {
                this.getHospitalList(this.cityId,true);
            }
        }, error => {
            this.getHospitalList(this.cityId,true);
        });
       
    }


    /*
     * function to find city 
     */
    
    public searchCity = (city:any,cityId:any) => {
        if(this.address!=undefined){
            this.matchCity = this.cities.find( item => item.name === this.address );
            if(this.matchCity){
                this.cityId = this.matchCity.id;  
            }else{
                this.cityId = cityId;
            }
        }else{
            this.cityId = cityId;
        }
        this.getHospitalList(this.cityId,true);
    }
    
    /*
     * set data on city change
     */
    
    onCityChange = (cityId:any) => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.cityId = cityId;
        
        for(let i=0; i<this.cities.length; i++){
            if(this.cities[i].id == cityId){
                this.matchCity = this.cities[i];
                this.commonService.setInStorage("SelectedHospCity",this.matchCity );
            }
        }
        
        this.loadFinished = false;
        this.getHospitalList(this.cityId,true);
    }

    onCityChangeMobile = (event:any) => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.cityId = event.value.id;
        for(let i=0; i<this.cities.length; i++){
            if(this.cities[i].id == event.value.id){
                
                this.matchCity = this.cities[i];
                console.log("MATCH:"+this.matchCity.name);
                this.commonService.setInStorage("SelectedHospCity",this.matchCity );
            }
        }
        this.loadFinished = false;
        this.getHospitalList(event.value.id, true );
      }

    // portChange(event: {
    //     component: IonicSelectableComponent,
    //     value: any
    //   }) {
    //     console.log('port:', event.value);
    //   }
    
    // onCityChangeMobile = (cityId:any) => {
    //     this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
    //     this.cityId = cityId;
        
    //     for(let i=0; i<this.cities.length; i++){
    //         if(this.cities[i].id == cityId){
    //             this.matchCity = this.cities[i];
    //             this.commonService.setInStorage("SelectedHospCity",this.matchCity );
    //         }
    //     }
        
    //     this.loadFinished = false;
    //     this.getHospitalList(this.cityId,true);
    // }

    /*
     * Function related to search functionality
     * */
    onChangeSearchText = () => {
        if( this.searchText.length > 2 ){
            if(!this.matchCity){
                this.matchCity = this.cities[0];
            }
            this.showSearchResultContainer = true;
            
            let searchObj = {};
            
            let filtersForHospital = this.commonService.getHospitalFilters(this.matchCity.name);
            
            
            if(this.setHospitalFilter && this.setHospitalFilter.allDayOpen){
                filtersForHospital.allDayOpen = this.setHospitalFilter.allDayOpen;
                searchObj = {
                        searchText: this.searchText,
                        city:  this.matchCity.name,
                        currentDocIndex: 0,
                        currentPage: 0,
                        sortField:"",
                        type: "hospital",
                        filters:[{
                                 filterName: "is24HrOpen",
                                 rangeFrom: "",
                                 rangeTo: "",
                                 selectedValue: [true]
                            }]
                    }
            } else {
                filtersForHospital.allDayOpen = false;
            }
            
            if(this.setHospitalFilter && this.setHospitalFilter.multiSpeciality){
                filtersForHospital.multiSpeciality = this.setHospitalFilter.multiSpeciality;
                searchObj = {
                        searchText: this.searchText,
                        city:  this.matchCity.name,
                        currentDocIndex: 0,
                        currentPage: 0,
                        sortField:"",
                        type: "hospital",
                        filters:[{
                                 filterName: "isAMultispeciality",
                                 rangeFrom: "",
                                 rangeTo: "",
                                 selectedValue: [true]
                            }]
                    }
            } else{
                filtersForHospital.multiSpeciality = false;
            }
            
            if(!this.setHospitalFilter || (this.setHospitalFilter && !this.setHospitalFilter.allDayOpen && !this.setHospitalFilter.multiSpeciality)){
                searchObj = {
                        searchText: this.searchText,
                        city:  this.matchCity.name,
                        currentDocIndex: 0,
                        currentPage: 0,
                        sortField:"",
                        type: "hospital",
                        filters:[{
                                 filterName: "",
                                 rangeFrom: "",
                                 rangeTo: "",
                                 selectedValue: [{}]
                            }]
                    }
            }
            this.evts.publish('system-admin-search-hospitals',{searchObj: searchObj});
        }else{
            this.showSearchResultContainer = false;
            if(this.seeAllClicked){
                this.getHospitalList(this.cityId, true);
            }
            this.evts.publish( 'unsub-system-admin-search-hospitals' );
        }
    }
    
    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup(){
        this.commonService.presentToast();
    }
  
    /*
     *Function to get filters 
     * */
    getFilters(){
        
        if(!this.matchCity){
            this.matchCity = this.cities[0];
        }
        
        let filtersForHospital = this.commonService.getHospitalFilters(this.matchCity.name);
        
        if(this.setHospitalFilter && this.setHospitalFilter.allDayOpen){
            filtersForHospital.allDayOpen = this.setHospitalFilter.allDayOpen
        } else {
            filtersForHospital.allDayOpen = false;
        }
        
        if(this.setHospitalFilter && this.setHospitalFilter.multiSpeciality){
            filtersForHospital.multiSpeciality = this.setHospitalFilter.multiSpeciality;
        } else{
            filtersForHospital.multiSpeciality = false;
        }
                
        let searchFilterComponent = this.modalCtrl.create( SearchFilterComponent, filtersForHospital, {showBackdrop:true, enableBackdropDismiss: true});
        searchFilterComponent.present();
        
        searchFilterComponent.onDidDismiss((data) => {
            if(data){
                this.hospitals = data.hospitals;
                this.setHospitalFilter = data.searchParams;
                this.formatAvailability();
            }
        });
    }
}

