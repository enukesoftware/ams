import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { IonicSelectableModule } from 'ionic-selectable';
import { SAManageHospitalsPage } from './system-admin-manage-hospitals';
import { SharedModule } from "../../components/shared.module";
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
    declarations: [
       SAManageHospitalsPage
    ],
    imports: [
       IonicPageModule.forChild(SAManageHospitalsPage),
       SharedModule,
       IonicSelectableModule,
       DropdownModule,
       TranslateModule.forChild()
    ],
    exports: [
      SAManageHospitalsPage,
    ]
})
export class SAManageHospitalsPageModule {}