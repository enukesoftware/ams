import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SystemAdminDoctorListPage } from './system-admin-doctor-list';
import { SharedModule } from "../../components/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { IonicSelectableModule } from 'ionic-selectable';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    SystemAdminDoctorListPage,
  ],
  imports: [
    IonicPageModule.forChild(SystemAdminDoctorListPage),
    SharedModule,
    IonicSelectableModule,
    DropdownModule,
    TranslateModule.forChild()
  ],
  exports: [
      SystemAdminDoctorListPage
     ]
})
export class SystemAdminDoctorListPageModule {}
