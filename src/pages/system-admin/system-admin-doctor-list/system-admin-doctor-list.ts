import { Component, ViewChild,NgZone, HostListener } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Events, MenuController, ModalController } from 'ionic-angular';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CommonService } from "../../../providers/common-service/common.service";
import { UserDataDTO } from "../../../interfaces/user-data-dto";
import { MapServiceProvider } from "../../../providers/map-integration-service/map-integration-service";
import { SearchFilterComponent } from "../../components/search-filter/search-filter";

@IonicPage( {
    name: 'SystemAdminDoctorListPage',
    segment: 'system-admin-doctor-list'
} )
@Component( {
    selector: 'page-system-admin-doctor-list',
    templateUrl: 'system-admin-doctor-list.html',
} )
export class SystemAdminDoctorListPage {
    @ViewChild( Content ) content: Content;

    listOfDoctors: any;
    cityId: any;
    cities: any;
    city:any;
    selectedHospital: any;
    userData: UserDataDTO = {};
    from: string;
    pageNo: number = 0;
    loadFinished: boolean = false;
    isPatientDoctorList: any;
    isAssociateDoctorlist: any;
    isSADoctorlist: any;
    protected address:any;
    protected matchCity:any;
    searchText: string = "";
    showSearchResultContainer: boolean = false;
    fromString: string;
    seeAllSearchClicked: boolean = false;
    searchedData: any;
    
    constructor( private evts: Events, public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
        private constants: Constants, private _ngZone: NgZone, private manageHospitalService: ManageHospitalServiceProvider,public mapService : MapServiceProvider ) {
        this.evts.subscribe( 'system-admin-search-more-hospitals', (data)=>{
            this.showSearchResultContainer = false;
            this.listOfDoctors = data.searchObj;
            this.seeAllSearchClicked = true;
        });
    }
        //this.searchCity(this.address);
    
    ionViewDidLoad() {
        this.menu.enable( true );
        this.from = "sysAdmin";
        this.fromString = "sysAdminDoctorList";    //for search
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    @HostListener('document:click', ['$event'])
    andClickEvent(event) { 
        this.showSearchResultContainer = false;
    }
    
    hideSearchContainer(event: Event) {
        if (event && event.stopPropagation) {
            event.stopPropagation();
        }
    }
    
    /**
     * Function for get Current Location Address
     * */
    protected getCurrentLocationAddress = () => {
        this.mapService.getCurrentLocation( response => {
            if ( response && response.coords && response.coords.latitude && response.coords.longitude ) {
                this.mapService.reverseGeocoding( response.coords.latitude, response.coords.longitude, ( status, result ) => {
                    if ( status ) {
                        this._ngZone.run(() => {
                            this.address = result.address;
                            this.commonService.setInStorage("SelectedDoctorCity", this.address );
                            this.searchCity(this.address,this.cityId);
                        });
                    } else {
                        this.getSysDoctorList(this.cityId,true);
                    }
                });
            } else {
                this.getSysDoctorList(this.cityId,true);
            }
        }, error => {
            this.getSysDoctorList(this.cityId,true);
        });
       
    }

    /* To hide search result popup on widow click
     **/
    public hideSerchContainer = () => {
        this.showSearchResultContainer = false;
    }

    /*
     * function to find city 
     */
    
    public searchCity = (city:any,cityId:any) => {
        if(this.address!=undefined){
            this.matchCity = this.cities.find( item => item.name === this.address );
            if(this.matchCity){
                this.cityId = this.matchCity.id;  
            }else{
                this.cityId = cityId;
            }
        }else{
            this.cityId = cityId;
        }
        
        this.getSysDoctorList( this.cityId, true );
    }
    
    onCityChange = (cityId:any) => {

        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.cityId = cityId;
        for(let i=0; i<this.cities.length; i++){
            if(this.cities[i].id == cityId){
                this.matchCity = this.cities[i];
                this.commonService.setInStorage("SelectedDoctorCity",this.matchCity );
            }
        }
        this.loadFinished = false;
        this.getSysDoctorList(cityId, true );
    }

    onCityChangeMobile= (event:any) => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.cityId = event.value.id;
        for(let i=0; i<this.cities.length; i++){
            if(this.cities[i].id == event.value.id){
                this.matchCity = this.cities[i];
                this.commonService.setInStorage("SelectedDoctorCity",this.matchCity );
            }
        }
        this.loadFinished = false;
        this.getSysDoctorList(event.value.id, true );
      }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                this.commonService.fireSelectedEvent( '#/system-admin-doctor-list' );
                this.getListOfCities();
            }
        } );
    }

    /**
     * Function To get List of Cities
     **/
    getListOfCities = () => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.commonService.getListOfCities().subscribe(
            res => {
                this.cities = res.data.list;
                console.log(' this.cities==>', this.cities);
                if(this.cities && this.cities.length > 0 ){
                    this.city = {id:this.cities[0].id,name:this.cities[0].name};
                    this.cityId = this.cities[0].id;
                }else{
                    this.cityId="";
                }
                
                this.commonService.getFromStorage("SelectedDoctorCity").then((value)=>{
                    if(value){
                        this.address = value;  
                        this.city = {id:this.address.id,name:this.address.name};
                        this.searchCity(this.address.name,this.address.id); 
                        this.commonService.setInStorage("SelectedDoctorCity", this.address );
                    }
                    else{
                        this.getCurrentLocationAddress();
                    }
                 });
                
                
                //this.getCurrentLocationAddress();   //old line of code before search retain
            }, error => {
                console.log( "error....", error );
            } );
    }
    
    /**
     * Function to get sysDoctorList
     * 
     * */
    getSysDoctorList( cityId?: any, firstPageCall?: boolean ) {

        this.manageHospitalService.getSysAdmDoctorsList( this.pageNo, cityId ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == "success" ) {
                    //this.listOfDoctors = res.data.list;
                    this.listOfDoctors = res.data.list;
                    if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                        this.listOfDoctors = res.data.list;
                        this.getImageBase();
                     /*   this.commonService.getImageListBase( this.listOfDoctors, 'Doctor' ,(index,cb) => {
                            console.log("calback-----",cb)
                           // this.listOfDoctors[j].imageBase64 = res.data.file;
                            this.listOfDoctors[index].imageBase64 = cb;
                        }
                        );*/
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else if ( res.data.list && res.data.list.length > 0 ) {
                       
                        
                        for ( let j = 0; j < res.data.list.length; j++ ) {
                            this.listOfDoctors.push( res.data.list[j] );
                        }
                        this.getImageBase();
                      /*  this.commonService.getImageListBase( this.listOfDoctors,'Doctor' ,(index,cb) => {
                            console.log("calback-----",cb)
                           // this.listOfDoctors[j].imageBase64 = res.data.file;
                            this.listOfDoctors[index].imageBase64 = cb;
                        });*/
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else {
                        this.loadFinished = true;
                        if(res.data.list.length ==0){
                            this.listOfDoctors = [];
                        }
                    }
                    this.commonService.setPaddingTopScroll( this.content );
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast( errorMsg );
                this.commonService.setPaddingTopScroll( this.content );
            }
        );
    }
    
    
 
   /* function to get images in async */ 
    public getImageBase =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i =startIndex; i <this.listOfDoctors.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.listOfDoctors[i].doctorId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.listOfDoctors.length; j++ ){
                    if(this.listOfDoctors[j].doctorId == res.data.userId){
                        this.listOfDoctors[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.listOfDoctors);
            }
        },
        error => {
            this.commonService.hideLoading();
            /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }
    
    
    /*
     * Function for search
     * 
     * */
    onChangeSearchText(){
        if( this.searchText.length > 2 ){
            this.showSearchResultContainer = true;
            
            if(!this.matchCity){
                this.matchCity = this.cities[0];
            }
            
            let searchObj = {
                searchText: this.searchText,
                city:  this.matchCity.name,
                currentDocIndex: 0,
                currentPage: 0,
                sortField:"",
                type: "doctor",
                filters:[{
                         filterName: "",
                         rangeFrom: "",
                         rangeTo: "",
                         selectedValue: [{}]
                    }]
            }
            
            this.commonService.getFromStorage("doctorFiltersArray").then((value)=>{
                if(value){
                    searchObj.filters = value;  
                }
                this.evts.publish( 'system-admin-search-hospitals',{searchObj: searchObj} );
             });
        }else{
            this.showSearchResultContainer = false;
            this.evts.publish( 'unsub-system-admin-search-hospitals' );
            if(this.seeAllSearchClicked){
                this.getSysDoctorList(this.cityId, true );
            }
        }
    }

    /*
     * Function to load more data when clicked on load more button
     * */
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getSysDoctorList( this.cityId );
    }
    
    
    getFilters(){
        
        if(!this.matchCity){
            this.matchCity = this.cities[0];
        }  
        
      let filtersForDoctor = this.commonService.getDoctorFilters(this.matchCity.name);
      
      if(this.searchedData){
          if(this.searchedData.feesSlots){
              filtersForDoctor.feesSlots = this.searchedData.feesSlots; 
          }
          if(this.searchedData.doctorExp){
              filtersForDoctor.doctorExp = this.searchedData.doctorExp; 
          }
          if(this.searchedData.availability){
              filtersForDoctor.availability = this.searchedData.availability;
          }
      }
      
      let searchFilterComponent = this.modalCtrl.create( SearchFilterComponent, filtersForDoctor, {showBackdrop:true, enableBackdropDismiss: true});
      searchFilterComponent.present();
      
      searchFilterComponent.onDidDismiss((data) => {
          if(data){
              this.listOfDoctors = data.doctors;
              this.searchedData = data.searchParams;
          }
      });
  }

    gotoCreateDoctor = () => {
        this.navCtrl.setRoot( 'SystemAdminCreateDoctorPage' );
    }

    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
        this.commonService.presentToast();
    }


}


