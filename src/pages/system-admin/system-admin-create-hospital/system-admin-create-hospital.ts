import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, MenuController, Platform } from 'ionic-angular';
import { NgForm } from '@angular/forms';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { HospitalDataDTO, UserDataDTO } from '../../../interfaces/user-data-dto';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';

@IonicPage( {
    name: 'SACreateHospitalPage',
    segment: 'system-admin-create-hospital'
} )
@Component( {
    selector: 'page-create-hospital',
    templateUrl: 'system-admin-create-hospital.html',
} )
export class SASystemAdminCreateHospitalPage {
    @ViewChild(Content) content: Content;
    
    formSubmitted: boolean;
    isEditSysAdminMode:any;
    selectedAdminDetails:any;
    showDetails =false;
    adminData:any;  

    userData: UserDataDTO = {};
    hospitalData: HospitalDataDTO = {};
    cityList: any;
    cityId: number;
    inputType: any = "text";
    constructor( private platform: Platform, public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private commonService: CommonService, private constants: Constants,
        private manageHospitalService: ManageHospitalServiceProvider ) {
        
        this.isEditSysAdminMode = this.navParams.get('isManageSysAdmin');
        this.isAdminDataAvailable();
    }

    ionViewDidLoad() {
        this.menu.enable( true );
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        this.isAdminDataAvailable();
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }
    
    isAdminDataAvailable(){ 
        this.adminData = this.navParams.get('selectedAdmin');
        if(this.adminData){
            console.log('in true');
            this.selectedAdminDetails = this.adminData;
        }else{
            console.log('in false');
            this.selectedAdminDetails = [];
        }
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_SYSTEM_ADMIN );
                this.listOfCities();
                this.commonService.fireSelectedEvent( '#/system-admin-manage-hospitals' );
                this.commonService.setPaddingTopScroll( this.content );
            }
        } );
    }

    /**
     * Function to create New hospital
     * @param hospitalData
     */
    createHospital = ( form: NgForm ) => {
        this.formSubmitted = true;
        if ( form.valid ) {
            this.formSubmitted = false;
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.manageHospitalService.createHospital( this.hospitalData ).subscribe(
                res => {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                    setTimeout(()=>{
                        this.navCtrl.setRoot( "SAManageHospitalsPage" );
                    }, 3000);
                    /*let alert = this.commonService.showAlert( "Success", "Hospital profile has been created successfully" );
                    alert.onDidDismiss(() => {
                        this.navCtrl.setRoot( "SAManageHospitalsPage" );
                    } );
                    this.commonService.hideLoading();*/
                },
                error => {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(error.message);
                }
            );
        }
    }

    /**
     * Function to Get list of cities
     * @param data
     */
    listOfCities() {
        this.commonService.getListOfCities().subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.cityList = res.data.list;
                    this.hospitalData.cityId = 0;
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }

}