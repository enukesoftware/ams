import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SASystemAdminCreateHospitalPage } from './system-admin-create-hospital';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    SASystemAdminCreateHospitalPage,
  ],
  imports: [
        IonicPageModule.forChild(SASystemAdminCreateHospitalPage),
        SharedModule,
        TranslateModule.forChild()
    ],
    exports: [
        SASystemAdminCreateHospitalPage
    ]
})
export class CreateHospitalPageModule {}
