import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { AppointmentList } from './appointment-list';
import { SharedModule } from "../components/shared.module";

@NgModule({
  declarations: [
    AppointmentList,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentList),
    TranslateModule.forChild(),
    SharedModule
  ],
  exports: [
    AppointmentList
  ]
})
export class AppointmentListModule {}
