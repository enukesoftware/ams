import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Content, Events ,Platform} from 'ionic-angular';
import { Location } from '@angular/common';

//providers
import { CommonService } from '../../providers/common-service/common.service';
import { Constants } from '../../providers/appSettings/constant-settings';
import { UserDataDTO } from '../../interfaces/user-data-dto';
import { PatientRoleServiceProvider } from "../../providers/patient-role-service/patient-role-service";
import { ConfirmBookingStep2Page } from "../booking-flow/confirm-booking-step2/confirm-booking-step2";

import * as moment from 'moment';

@IonicPage( {
    name: 'AppointmentList',
    segment: 'appointment-list/:fromPage'
} )
@Component( {
    selector: 'appointment-list',
    templateUrl: 'appointment-list.html',
} )
export class AppointmentList {
    @ViewChild( Content ) content: Content;
    userData: UserDataDTO = {};
    appointmentList: any = [];
    loadFinished: boolean = false;
    pageNo: number = 0;
    pageName: any;
    formattedDate: any;
    formattedTime: any;
    isUpcominAppointments: boolean = false;
    protected patientId: any;    
    isSkipAndExplore:any;
    isPatientDependents: boolean = false;
    bookingUrl: any;


    constructor( private commonService: CommonService, private evts: Events, private patientRoleService: PatientRoleServiceProvider, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private constants: Constants,public platform : Platform, public location: Location ) {
        this.menu.enable( true );
        this.platform.registerBackButtonAction(() => {
            this.location.back();
        });
    }

    ionViewWillEnter = () => {
        this.appointmentList = [];
        this.menu.enable( true );
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        this.pageName = this.navParams.get( 'fromPage' );
        this.commonService.getFromStorage('isSkipAndExplore').then((value)=>{
            this.isSkipAndExplore =value;
        })
        this.commonService.getFromStorage( "patientId" ).then(( value ) => {
            if ( value ) {
                this.patientId = value;
                console.log( ' this.patientId --->', this.patientId );
                if ( this.pageName != undefined && (this.pageName == 'patientUpcoming' || this.pageName == 'apppointmentConfirmed') ) {
                    this.pageNo = 0;
                    this.getPatientUpcomingAppointments( true );
                    this.isUpcominAppointments = true;
                } else if(this.pageName == 'patientHistory'){
                    this.pageNo = 0;
                    this.getPatientAppointmentHistory( true );
                    this.isUpcominAppointments = false;
                }
                
            } else {
                this.patientId = "";
                if(this.isSkipAndExplore  && (this.pageName == 'patientUpcoming')){
                    this.pageNo = 0;
                    this.isUpcominAppointments = true;
                }
                else if(this.isSkipAndExplore  && (this.pageName == 'patientHistory')){
                    this.pageNo = 0;
                    this.isUpcominAppointments = false;
                }
            }
        } );

    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                if ( this.pageName != undefined &&  (this.pageName == 'patientUpcoming' || this.pageName == 'apppointmentConfirmed') ) {
                    // console.log("on 55...");
                    this.commonService.fireSelectedEvent( '#/appointment-list/patientUpcoming' );
                } else {
                    //  console.log("on 58...");
                    this.commonService.fireSelectedEvent( '#/appointment-list/patientHistory' );
                }
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_PATIENT, true );
                this.commonService.setPaddingTopScroll( this.content );
                
                //after updating appointment remove appointment details from storage
                this.commonService.removeFromStorage("appointmentDetails");
                this.commonService.removeFromStorage("isConfirmed");
                this.commonService.removeFromStorage("isUpdateOnView");
                this.commonService.removeFromStorage("hospitalWithAppointmentTimeSlot");

                this.getPatientDependent();
            }
        } );
    }

    /**
     * Function To get upcoming Appointments
     **/
    getPatientUpcomingAppointments( firstPageCall?: boolean ) {

        console.log( 'this.patientId======', this.patientId );
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.patientRoleService.getPatientUpcomingAppointments( this.patientId, this.pageNo ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                    this.appointmentList = res.data.list;
                    this.getImageBase();
                    for(let i=0; i<this.appointmentList.length; i++){
                        var newDate = moment(this.appointmentList[i].appointmentDetails.fromTime).toDate();
                        console.log("time to IE",newDate)
                        this.appointmentList[i].fromTimeString = this.commonService.convertTo12hrsFormat(new Date(newDate), false, true);
                    }
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                    // console.log("upcoming appointmentList--->",this.appointmentList);
                    // this.formattedTime='1535411707';
                    /*                    for(let i=0; i< this.appointmentList.length;i++){
                                            this.commonService.convertTimestampToDate(this.appointmentList[i].appointmentDetails.appointmentDate,((cb)=>{
                                                console.log("formatted date callback",cb);
                                                this.formattedDate = cb;
                                            }));
                                            this.commonService.convertTimestampToTime(this.appointmentList[i].appointmentDetails.fromTime,((cb)=>{
                                                console.log("formatted date callback",cb);
                                                this.formattedTime = cb;
                                            }));
                                           }*/
                }else if ( res.data.list && res.data.list.length > 0 ) {
                    for ( let j = 0; j < res.data.list.length; j++ ) {
                        this.appointmentList.push( res.data.list[j] );
                    }
                    this.getImageBase();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                }else {
                    this.loadFinished = true;
                }
            }, error => {
                this.commonService.presentToast( error.message );
                this.commonService.hideLoading();
            } );
    }


    /* function to get images in async */ 
    public getImageBase =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.appointmentList.length; i++) {
            let hospitalImageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.appointmentList[i].doctorsDetails.doctorId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( hospitalImageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.appointmentList.length; j++ ){
                    if(this.appointmentList[j].doctorsDetails.doctorId == res.data.userId){
                        this.appointmentList[j].doctorsDetails.imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.appointmentList);
            }
        },
        error => {
            this.commonService.hideLoading();
            /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
        });
                
                let doctorImageData = {
                        'userType': "HOSPITAL_IMAGE",
                        'id': this.appointmentList[i].hospitalDetails.hospitalId,
                        'rank': 1,
                 }
                this.commonService.downloadImage( doctorImageData ).subscribe(
                        res => {
                            if ( res.status == "success" ) {
                                console.log("image base 64=======>", res);
                                for( let j=startIndex; j<this.appointmentList.length; j++ ){
                                    if(this.appointmentList[j].hospitalDetails.hospitalId == res.data.userId){
                                        this.appointmentList[j].hospitalDetails.imageBase64 = res.data.file;
                                    }
                                }
                                console.log("this.listOfDoctors=======>", this.appointmentList);
                            }
                        },
                        error => {
                            this.commonService.hideLoading();
                           /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                            this.commonService.presentToast(errorMsg);*/
                    });

        }
    }
    
    
    
    /**
     * Function To get Appointment history
     **/
    getPatientAppointmentHistory( firstPageCall?: boolean ) {
        this.commonService.getFromStorage( "patientId" ).then(( value ) => {
            if ( value ) {
                this.patientId = value;
            } else {
                this.patientId = "";
            }
        } );
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.patientRoleService.getPatientAppointmentHistory( this.patientId, this.pageNo ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                    this.appointmentList = res.data.list;
                    this.getImageBase();
                    for(let i=0; i<this.appointmentList.length; i++){
                        var newAppHistDate = moment(this.appointmentList[i].appointmentDetails.fromTime).toDate();
                        console.log("time to IE",newAppHistDate)
                        this.appointmentList[i].fromTimeString = this.commonService.convertTo12hrsFormat(new Date(newAppHistDate), false, true);
                    }
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                    // console.log("appointment history List--->",this.appointmentList);
                }else if ( res.data.list && res.data.list.length > 0 ) {
                    for ( let j = 0; j < res.data.list.length; j++ ) {
                        this.appointmentList.push( res.data.list[j] );
                    }
                    this.getImageBase();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                }else {
                    this.loadFinished = true;

                }
            }, error => {
                this.commonService.presentToast( error.message );
                this.commonService.hideLoading();
            } );
    }
    
    /*
     * On book Again btn click get appointment details to send bookingstep1 page for appointment booking
     **/
    getBookingDetails( appointment:any,fromPage: any, redirectPage: any, redirectParam: any, doctorId: any ){
        this.commonService.setInStorage("hospitalId", appointment.hospitalDetails.hospitalId );
        this.onClickBookAppointment(fromPage,redirectPage,redirectParam,doctorId);
    }
    
    /*
     * redirect with navCtrl for mobile
     * 
     * */
    geToConfirmBookingPage(appointment:any){
        this.commonService.setInStorage("hospitalId", appointment.hospitalDetails.hospitalId );
        this.navCtrl.setRoot(ConfirmBookingStep2Page, {
            'from':'patientUpcomingAppointmentViewDetails',
            'redirectParam':appointment.appointmentDetails.appointmentId
        });
    }
    
    /*
     * Function to load more data when clicked on load more button
     **/
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getPatientUpcomingAppointments();
        this.getPatientAppointmentHistory();
    }

    /*
    * Function to redirect to native map app on mobile device and on web open google map in new tab
    **/
    public plotAddressOnMap = ( hospitalName:any, address: any ) => {
        let currentHospitalAddress = hospitalName+ "+" +address.street + "+" + address.cityName + "+" +
            address.state + "+" + address.zipCode;
        this.commonService.setAddressOnMap( currentHospitalAddress );
    }

    comingSoonPopup = () => {
        this.commonService.presentToast();
    }
    
    public goToRateAndFeedback =(data) => {
        console.log(" go to update  ",data);
        this.navCtrl.setRoot('RateAndFeedbackPage',{mode:'createFeedback',doctorId: data.doctorsDetails.doctorId,'hospitalId':data.hospitalDetails.hospitalId,'appointmentId':data.appointmentDetails.appointmentId});
    }
    
    getPatientDependent = () => {
        if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            this.commonService.getFromStorage('HospitalDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });
        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            this.commonService.getFromStorage('DoctorDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            this.commonService.getFromStorage('PatientDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });

        }

    }

    onClickBookAppointment(fromPage: any, redirectPage: any, redirectParam: any, doctorId: any) {
        console.log("DATA:","FROM:"+fromPage +" REDIRECT_PAGE:" +redirectPage +" PARAM:" +redirectParam + " DOC:"+doctorId);
        if (this.isPatientDependents) {
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
            console.log("BOOKING_URL1",this.bookingUrl);
        } else {
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            console.log("BOOKING_URL2",this.bookingUrl);
        }
    }
    
}
