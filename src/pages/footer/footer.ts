import { Component, HostListener } from '@angular/core';
import { Platform } from 'ionic-angular';

//providers services
import { CommonService } from '../../providers/common-service/common.service';

@Component({
    selector: 'footer',
    templateUrl: 'footer.html'
})
export class FooterPage {
    @HostListener('window:resize', ['$event']) footerWindowResize: any;
    
    isWeb: boolean = false;
    currentYear: any;
    arrowStatusName: string = "ios-arrow-up";
    subFooterBottom: any;
    
    constructor( public commonService: CommonService, public platform: Platform ){
        this.isWeb = this.commonService.checkIsWeb();
        let dateObj = new Date();
        this.currentYear = dateObj.getFullYear();
        setTimeout(()=>{
            this.subFooterBottomInitialCalc();
        },500);
        this.footerWindowResize = (event) => {
            this.subFooterBottomInitialCalc();
        }
    }
    
    subFooterBottomInitialCalc = () => {
        if( this.isWeb ){
            this.calculationToggleSubFooter();
        }
    }
    
    /*
     * Function to do calculation related to toggling the sub footer
     * */
    calculationToggleSubFooter = () => {
        let subFooter = document.getElementsByClassName('sub-footer');
        for( let i=0;i<subFooter.length;i++ ){
            if( subFooter[i] ){
                if( (<HTMLElement>subFooter[i]).offsetHeight > 0 ){
                    this.subFooterBottom = (<HTMLElement>subFooter[i]).offsetHeight - 36;
                    this.subFooterBottom = "-" + this.subFooterBottom;
                }
            }
        }
    }
    
    /*
     * Function to toggle sub footer menu
     * */
    toggleSubFooter = () => {
        if( this.arrowStatusName == "ios-arrow-down" ){
            this.arrowStatusName = "ios-arrow-up";
            this.calculationToggleSubFooter();
        }else if( this.arrowStatusName == "ios-arrow-up" ){
            this.arrowStatusName = "ios-arrow-down";
            this.subFooterBottom = 36;
        }
    }
    
    comingSoon = () => {
        this.commonService.presentToast();
    }
}
