import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Content, Events,Platform} from 'ionic-angular';
import { Location } from '@angular/common';

//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO } from '../../../interfaces/user-data-dto';

@IonicPage( {
    name: 'DoctorRoleAppointmentHistoryVisitDetailsPage',
    segment: 'doctor-role-appointment-history-visit-details'
} )
@Component({
  selector: 'page-doctor-role-appointment-history-visit-details',
  templateUrl: 'doctor-role-appointment-history-visit-details.html',
})
export class DoctorRoleAppointmentHistoryVisitDetailsPage {
  @ViewChild( Content ) content: Content;
  userData: UserDataDTO = {};
  fromAppointmentHistory: any;
  fromUpcomingAppointment: any;
  fromHospitalAdminAppointmentVisitDetails: any;
  
  constructor( private commonService: CommonService, private evts: Events, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private constants: Constants,
               public platform : Platform, public location: Location) {
        this.menu.enable( true );
        this.platform.registerBackButtonAction(() => {
            this.location.back();
          });
    }
  
    ionViewWillEnter = () => {
        this.menu.enable( true );
        
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR, true );
                this.commonService.fireSelectedEvent( '#/doctor-role-appointments-history' );
                this.commonService.setPaddingTopScroll( this.content );
            }
        } );
    }

   comingSoonPopup = () => {
        this.commonService.presentToast();
    }

}
