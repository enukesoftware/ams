import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from "../../components/shared.module";
import { DoctorRoleAppointmentHistoryVisitDetailsPage } from './doctor-role-appointment-history-visit-details';

@NgModule({
  declarations: [
    DoctorRoleAppointmentHistoryVisitDetailsPage,
  ],
  imports: [
      IonicPageModule.forChild(DoctorRoleAppointmentHistoryVisitDetailsPage),
       TranslateModule.forChild(),
       SharedModule
    ],
    exports: [
       DoctorRoleAppointmentHistoryVisitDetailsPage
    ]
})
export class DoctorRoleAppointmentHistoryVisitDetailsPageModule {}
