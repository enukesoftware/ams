import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorRoleHolidayCalendarPage } from './doctor-role-holiday-calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from "../../components/shared.module";
import { CalendarModule } from 'primeng/calendar';
import { PipesModule } from '../../../pipes/pipes.module';
import { HeaderForRoleCreationPage } from '../../header-for-role-creation-form/header-for-role-creation-form';

@NgModule({
  declarations: [
    DoctorRoleHolidayCalendarPage
    ],
  imports: [
    IonicPageModule.forChild(DoctorRoleHolidayCalendarPage),
    TranslateModule.forChild(),
    MultiSelectModule,
    SharedModule,
    CalendarModule,
    PipesModule
  ],
  exports: [
    DoctorRoleHolidayCalendarPage
  ]
})
export class DoctorRoleHolidayCalendarPageModule {}
