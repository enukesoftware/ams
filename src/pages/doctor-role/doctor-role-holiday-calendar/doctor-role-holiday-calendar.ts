import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, MenuController, Platform, Events } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import * as moment from 'moment';


import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO, DoctorDataDto, AddressDto, serviceAndSpecializationDataDto, HospitalListDTO } from "../../../interfaces/user-data-dto";
import { Constants } from '../../../providers/appSettings/constant-settings';
import { DoctorRoleServiceProvider } from '../../../providers/doctor-role-service/doctor-role-service';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CalendarModule } from 'primeng/calendar';
//import { groupByDatePipe } from '../../../pipes/groupByDate.pipe';

//3rd party npm module
import * as _ from 'lodash';
import { from } from 'rxjs/observable/from';
import { leaveView } from '@angular/core/src/render3/instructions';
/**
 * Generated class for the DoctorRoleHolidayCalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
    name: 'DoctorRoleHolidayCalendarPage',
    segment: 'doctor-role-holiday-calendar/:mode/:doctorId/:fromPage'
})
@Component({
    selector: 'page-doctor-role-holiday-calendar',
    templateUrl: 'doctor-role-holiday-calendar.html',
})
export class DoctorRoleHolidayCalendarPage {
    @ViewChild(Content) content: Content;
    loadFinishedHospital: boolean;
    hospitals: any;
    currentSegment: string = 'hospitalInfo';
    hospitalPageNo: number = 0;
    userData: UserDataDTO = {};
    doctorData: DoctorDataDto = {};
    doctorId: any;
    pageNo: number = 0;
    pageName: any;
    feedbackData = [];
    doctorServices: any[];
    doctorSpecialities: any[];
    viewMoreSpFlag: boolean = false;
    lessThen3Sp: boolean = true;
    initialSpecialities: any[];
    windowWidth: number;
    today: any;
    todayForWeb: any;
    todayDateStr: any;

    viewMoreServiceFlag: boolean = false;
    lessThen3Service: boolean = true;
    initialServices: any[];
    isSkipAndExplore: boolean = false;
    redirectParam: any;
    popover: any;
    fromPage: any;
    mode: any;
    viewFlag: boolean = false;
    editFlag: boolean = false;
    createFlag: boolean = false;
    formSubmitted: boolean = false;
    applyLeaveData: any = {};
    selectedHospitals: any[] = [];
    startDateValue: any;
    endDateValue: any;
    startTimeValue: any;
    endTimeValue: any;
    viewLeaves: any;
    startDateValueClone: any;
    endDateValueClone: any;
    startTimeValueClone: any;
    endTimeValueClone: any;
    hospitalsClone: any;
    leaveDescriptionClone: any;
    leaveDescription: any;
    leaveIds: any[] = [];
    selectedHospitalsClone: any[] = [];
    fromDateView: any;
    fromTimeView: any;
    toDateView: any;
    toTimeView: any;
    backPageUrl: any;
    homePageUrl: any;

    constructor(private evts: Events, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public platform: Platform, public location: Location, private _ngZone: NgZone, public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
        private constants: Constants, private doctorRoleService: DoctorRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider) {
        this.platform.registerBackButtonAction(() => {
            this.location.back();
        });
        this.menu.enable(true);
    }

    ionViewDidLoad() {
        this.menu.enable(true);
    }




    //   ionViewWillEnter = () => {
    // this.menu.enable( true );
    // this.today = this.convertDate();
    // this.todayForWeb = new Date();
    // console.log("today--->",this.today);
    // this.todayDateStr = this.commonService.formatDateString(new Date());
    // //check if user is authorized or not to access the screen if not redirect him to login screen
    // this.isAuthorized();
    // // this.pageName = this.navParams.get('fromPage');
    // }

    ionViewWillEnter = () => {
        this.menu.enable(true);

        this.today = this.convertDate();
        this.todayForWeb = new Date();
        console.log("today--->", this.today);
        this.todayDateStr = this.commonService.formatDateString(new Date());

        this.doctorId = this.navParams.get('doctorId');
        this.fromPage = this.navParams.get('fromPage');
        this.mode = this.navParams.get('mode');

        this.changeMode(this.mode);




        // if(this.fromPage && this.fromPage == "DRDoctorDashboard"){
        //     this.menu.enable(true);
        // }else{
        //     this.menu.enable(false);
        // }

        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        this.windowWidth = this.commonService.getWindowWidth();
        this.redirectParam = this.navParams.get('param');

        if (this.redirectParam == 'PRHospitalProfileDLProfile') {
            this.currentSegment = 'otherInfo';
        } else {
            this.currentSegment = 'hospitalInfo';
        }

        console.log("param....................", this.navParams.get('doctorId'));
        console.log("param....................", this.navParams.get('fromPage'));



        //    this.commonService.getFromStorage('isSkipAndExplore').then((value)=>{
        //         this.isSkipAndExplore =value;
        //     })
    }

    convertDate = () => {
        var d = new Date(moment().format("DD-MM-YYYY hh:mm:ss"));

        return [d.getFullYear(), this.pad(d.getMonth() + 1), this.pad(d.getDate())].join('-')[this.pad(d.getHours()), this.pad(d.getMinutes()), this.pad(d.getSeconds())];
    }

    pad = (s) => {
        return (s < 10) ? '0' + s : s;
    }


    /*
    * Function to check if user is authorized or not to access the screen
    * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if (!result) {
                this.navCtrl.setRoot('LoginPage');
            } else {
                this.userData = result;

                // this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR );
                if (this.fromPage && this.fromPage == "HADoctorHolidayCalendar") {
                    this.commonService.fireSelectedEvent('#/hospital-admin-doctor-holiday-calendar');
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN);
                    if (this.userData.role == this.constants.ROLE_HOSPITAL_STAFF) {
                        this.backPageUrl = "#/hospital-admin-doctor-holiday-calendar";
                        this.homePageUrl = "#/staff-role-dashboard/0"
                    } else {
                        this.backPageUrl = "#/hospital-admin-doctor-holiday-calendar";
                        this.homePageUrl = "#/hospital-admin-dashboard/0"
                    }
                } else if (this.fromPage && this.fromPage == "HSRoleADoctorHolidayCalendar") {
                    this.commonService.fireSelectedEvent('#/hospital-staff-associated-doctor-list/HSRoleDashboard');
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_STAFF);
                    this.backPageUrl = "#/hospital-staff-associated-doctor-list/HSRoleDashboard";
                    this.homePageUrl = "#/staff-role-dashboard/0"

                }
                else {
                    this.commonService.fireSelectedEvent('#/doctor-role-holiday-calendar/create/0/DRHolidayCalendar');
                    this.backPageUrl = "#/doctor-dashboard/0";
                    this.homePageUrl = "#/doctor-dashboard/0"
                }
                // this.commonService.setPaddingTopScroll( this.content );
                this.commonService.getFromStorage("userLoginRole").then(value => {
                    // if(value){
                    //     if(value == this.constants.ROLE_DOCTOR){
                    //         this.showDrPrefix = true;
                    //     }else{
                    //         this.showDrPrefix = false;
                    //     }
                    // } else{
                    //     this.showDrPrefix = false;
                    // } 
                });

                if (this.doctorId && this.doctorId != 0) {
                    this.getDoctorInfo(this.doctorId);
                    this.getAssociatedHospitals(this.doctorId, true);
                } else {
                    this.commonService.getFromStorage("doctorId").then((value) => {
                        if (value) {
                            this.doctorId = value;
                            this.getDoctorInfo(this.doctorId);
                            this.getAssociatedHospitals(this.doctorId, true);

                        } else {
                            this.doctorId = "";
                        }
                    });
                }
                // this.getDoctorProfile(this.doctorId);


            }
        });



    }

    /*
     * getDoctor Info
     * */

    getDoctorInfo(id) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.doctorRoleServiceProvider.getDoctorProfile(id).subscribe(
            res => {
                this.commonService.hideLoading();
                if (res.status == 'success') {
                    this.doctorData = res.data;
                    this.getImageBase();
                    // this.commonService.setInStorage("patientRoleDoctorId", this.doctorData.doctorId);
                    this.doctorSpecialities = this.doctorData.specializationList;
                    this.doctorServices = this.doctorData.serviceList;
                    console.log("doctorSpecialities", this.doctorSpecialities);
                    if (this.windowWidth <= 768) {
                        let spLen = this.doctorSpecialities.length;

                        console.log("spLen", spLen);
                        if (spLen > 3) {
                            this.doctorSpecialities = [];
                            this.viewMoreSpFlag = true;
                            this.lessThen3Sp = false;
                            for (let i = 0; i < spLen; i++) {
                                if (i < 3) {
                                    this.doctorSpecialities.push(this.doctorData.specializationList[i]);
                                }
                            }
                        } else {
                            this.lessThen3Sp = true;
                        }
                        this.initialSpecialities = this.doctorSpecialities;

                        let serviceLen = this.doctorServices.length;
                        if (serviceLen > 3) {
                            this.doctorServices = [];
                            this.viewMoreServiceFlag = true;
                            this.lessThen3Service = false;
                            for (let i = 0; i < serviceLen; i++) {
                                if (i < 3) {
                                    this.doctorServices.push(this.doctorData.serviceList[i]);
                                }
                            }
                        } else {
                            this.lessThen3Service = true;
                        }
                        this.initialServices = this.doctorServices;
                    }
                }
            }, error => {
                this.commonService.hideLoading();
                if (!this.isSkipAndExplore) {
                    this.commonService.presentToast(error.message);
                }
                console.log("error....", error);
            });
    }


    /* function to get profile image in async */
    public getImageBase = () => {
        console.log("on get image base");
        let imageData = {
            'userType': "USER_PROFILE_IMAGE",
            'id': this.doctorData.doctorId,
            'rank': 1,
        }
        this.commonService.downloadImage(imageData).subscribe(
            res => {
                if (res.status == "success") {
                    console.log("image base 64=======>", res);
                    this.doctorData.imageBase64 = res.data.file;
                }
            },
            error => {
                this.commonService.hideLoading();
                /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                 this.commonService.presentToast(errorMsg);*/
            });
    }






    /*
     * getDoctor Assosciated Hospitals
     * */

    getAssociatedHospitals(doctorId: any, firstHospitalPageCall?: boolean) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.manageHospitalServiceProvider.getAssociatedHospitalList(doctorId, this.hospitalPageNo).subscribe(res => {

            let tempHospitalPageNo = this.hospitalPageNo;
            if (res.data.list && res.data.list.length > 0 && firstHospitalPageCall) {
                this.commonService.hideLoading();
                this.hospitals = res.data.list;
                console.log("Hospitals---->", this.hospitals);
                // for(let i=0;i<10;i++){
                //     this.hospitals.push(this.hospitals[0]);
                // }
                this.getImageBaseHospitalList();

                if ((tempHospitalPageNo + 1) >= res.data.totalPages) {
                    this.loadFinishedHospital = true;
                }
                this.formatAvailability();
            } else if (res.data.list && res.data.list.length > 0) {
                for (let j = 0; j < res.data.list.length; j++) {
                    this.hospitals.push(res.data.list[j]);
                }
                this.getImageBaseHospitalList();
                if ((tempHospitalPageNo + 1) >= res.data.totalPages) {
                    this.loadFinishedHospital = true;
                }
                this.formatAvailability();
            } else {
                this.loadFinishedHospital = true;
            }
        }, error => {
            this.commonService.hideLoading();
            if (!this.isSkipAndExplore) {
                this.commonService.presentToast(error);
            }
        });
    }



    /* function to get images in async */
    public getImageBaseHospitalList = () => {
        let startIndex = this.hospitalPageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i < this.hospitals.length; i++) {
            let imageData = {
                'userType': "HOSPITAL_IMAGE",
                'id': this.hospitals[i].hospitalDetails.hospitalId,
                'rank': 1,
                'isDefault': true
            }
            this.commonService.downloadImage(imageData).subscribe(
                res => {
                    if (res.status == "success") {
                        console.log("image base ho 64=======>", res);
                        for (let j = startIndex; j < this.hospitals.length; j++) {
                            if (this.hospitals[j].hospitalDetails.hospitalId == res.data.userId) {
                                this.hospitals[j].hospitalDetails.imageBase64 = res.data.file;
                            }
                        }
                        console.log("this.hospitals=======>", this.hospitals);
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                     this.commonService.presentToast(errorMsg);*/
                });
        }
    }

    /*
       * Function to do calculations for time related 
       * */
    formatAvailability = () => {
        var currentDate = new Date();
        var today = currentDate.getDay();
        let hospitalLength = this.hospitals.length;
        for (let i = 0; i < hospitalLength; i++) {
            var todayAvailability;
            let availabilities = this.hospitals[i].hospitalDetails.timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime(availabilities);
            this.hospitals[i].formattedAvailabilities = this.commonService.calculateTime(availabilities);
            let availabilityLength = this.hospitals[i].formattedAvailabilities.length;

            this.hospitals[i].todayAvailability = this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList;
            if (this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList && this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList.length > 0) {
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = this.hospitals[i].todayAvailability.length;
                for (let j = 0; j < todayTimeLen; j++) {
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].toTime), true);

                    if (currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes) {
                        timeSlotFound = true;
                        this.hospitals[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if (!timeSlotFound) {
                    this.hospitals[i].todaysAvailabilityString = "Closed";
                }
            } else {
                this.hospitals[i].todaysAvailabilityString = "Closed";
            }
            if (this.hospitals[i].todaysAvailabilityString == "00:00AM - 11:59PM") {
                this.hospitals[i].todaysAvailabilityString = "24 Hrs Open";
            }
        }
    }

    /*
       * Function to do calculations for time related 
       * */
    formatAvailabilityForHospital = (hospitals) => {
        // var currentDate = new Date();
        // var today = currentDate.getDay();
        let hospitalLength = hospitals.length;
        for (let i = 0; i < hospitalLength; i++) {
            var todayAvailability;
            let availabilities = hospitals[i].hospitalDetails.timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime(availabilities);
            hospitals[i].formattedAvailabilities = this.commonService.calculateTime(availabilities);
            let availabilityLength = hospitals[i].formattedAvailabilities.length;

            hospitals[i].todayAvailability = hospitals[i].hospitalDetails.todaysTimeAvailabilityList;
            if (hospitals[i].hospitalDetails.todaysTimeAvailabilityList && hospitals[i].hospitalDetails.todaysTimeAvailabilityList.length > 0) {
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = hospitals[i].todayAvailability.length;
                for (let j = 0; j < todayTimeLen; j++) {
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].toTime), true);

                    if (currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes) {
                        timeSlotFound = true;
                        hospitals[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(hospitals[i].hospitalDetails.todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if (!timeSlotFound) {
                    hospitals[i].todaysAvailabilityString = "Closed";
                }
            } else {
                hospitals[i].todaysAvailabilityString = "Closed";
            }
            if (hospitals[i].todaysAvailabilityString == "00:00AM - 11:59PM") {
                hospitals[i].todaysAvailabilityString = "24 Hrs Open";
            }
        }
    }

    /*
       * Function to redirect to native map app on mobile device and on web open google map in new tab
       * */
    public plotAddressOnMap = (hospital: any) => {
        let currentHospitalAddress = hospital.name + "+" + hospital.address.street + "+" + hospital.address.cityName + "+" +
            hospital.address.state + "+" + hospital.address.zipCode;
        this.commonService.setAddressOnMap(currentHospitalAddress);
    }

    openTimingDetails = (hospital) => {
        let title = hospital.hospitalDetails.name;
        let subTitle = hospital.hospitalDetails.address.street + ", " + hospital.hospitalDetails.address.cityName;
        this.popover = this.commonService.showTimingList(title, subTitle, hospital.formattedAvailabilities);
        this.commonService.closePopupOnBackBtn(this.popover);
    }

    /*
     * more Specialities List and service list
     * */

    hideMore(type) {
        if (type == "speciality") {
            console.log("speciality 185.......");
            this.doctorSpecialities = this.doctorData.specializationList;
            this.viewMoreSpFlag = false;
        } else {
            console.log("services 189.......");
            this.doctorServices = this.doctorData.serviceList;
            this.viewMoreServiceFlag = false;
        }
        this.content.resize();
    }

    /*
     * less Specialities List and service list
     * */
    hideLess(type) {
        if (type == "speciality") {
            this.doctorSpecialities = this.initialSpecialities;
            this.viewMoreSpFlag = true;
        } else {
            this.doctorServices = this.initialServices;
            this.viewMoreServiceFlag = true;
        }
        this.content.resize();
    }

    hospitalListClick() {
        this._ngZone.run(() => {
            this.currentSegment = 'hospitalInfo';
        });
    }

    otherInfoClick() {
        this._ngZone.run(() => {
            this.currentSegment = 'otherInfo';
        });
    }

    showGoToLoginPopup = () => {
        this.commonService.showGoTOLoginPopup(this.navCtrl);
    }

    /*
        * function to apply date filter
        * */

    applyDateFilter = () => {
        // if(!this.startValueStr){
        //     this.goBtnClicked =true;
        //     this.errorMsgEndDate='';
        //     this.errorMSG = this.commonService.getTranslate('SELECTSTARTDATE',{});
        //     this.commonService.presentToast(this.errorMSG );
        // }
        // else if(!this.endValueStr && this.startValueStr){
        //     this.goBtnClicked =true;
        //     this.errorMSG = '';
        //     this.errorMsgEndDate = this.commonService.getTranslate('SELECTENDDATE',{});
        //     this.commonService.presentToast(this.errorMsgEndDate );
        // }
        // else{
        //     this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        //     this.getDoctorUpcomingAppointments(true);
        // }
    }

    changeMode = (mode: any) => {
        if (!mode || mode == "undefined") {
            this.mode = "create";
        }
        this.mode = mode;
        console.log("MODE", mode);
        if (this.mode == "view") {
            this.viewFlag = true;
            this.createFlag = false;
            this.editFlag = false;
            this.setViewMode();
        } else if (this.mode == "create") {
            this.viewFlag = false;
            this.createFlag = true;
            this.editFlag = false;
        } else if (this.mode == "edit") {
            this.viewFlag = false;
            this.createFlag = false;
            this.editFlag = true;
        }
    }

    setViewMode = () => {
        this.getViewLeaves(this.doctorData.doctorId);
    }

    setEditMode = () => {

    }

    setCreateMode = () => {

    }

    cancelClick = () => {
        if (this.mode == "view") {
            this.viewFlag = false;
            this.createFlag = true;
            this.editFlag = false;
            this.mode = "create";
        } else if (this.mode == "edit") {
            this.revertClone();
            this.viewFlag = true;
            this.createFlag = false;
            this.editFlag = false;
            this.mode = "view";

        }
    }

    hospitalSelect = (hospital: any) => {
        console.log("HOS:", hospital);
        hospital.hospitalDetails.isChecked = !hospital.hospitalDetails.isChecked
        if (hospital.hospitalDetails.isChecked) {
            this.selectedHospitals.push(hospital);
        } else {

            this.selectedHospitals.splice(this.hospitals.indexOf(hospital), 1);
        }

    }

    markMyLeave = (form: NgForm) => {
        console.log(form);
        // console.log(this.hospitals[]);

        this.formSubmitted = true;
        if (form.valid) {

            if (this.selectedHospitals.length == 0) {
                this.commonService.presentToast('Please select any hospital');
                return;
            }

            this.formSubmitted = false;
            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.applyLeaveData = {};
            this.applyLeaveData.description = this.leaveDescription;
            this.applyLeaveData.profileId = this.doctorData.doctorId;
            this.applyLeaveData.roleType = this.constants.ROLE_DOCTOR;//this.userData.role;


            //convert date time to utc format
            let fromDateTimeISO = new Date(moment(form.value.fromdate).format('YYYY-MM-DD') + " " + moment(form.value.startTime, "HH:mm").format('hh:mm A')).toISOString();
            this.applyLeaveData.fromDateTime = moment.utc(fromDateTimeISO).format('YYYY-MM-DDThh:mm A');

            //convert date time to utc format
            let toDateTimeISO = new Date(moment(form.value.todate).format('YYYY-MM-DD') + " " + moment(form.value.endTime, "HH:mm").format('hh:mm A')).toISOString();
            this.applyLeaveData.toDateTime = moment.utc(toDateTimeISO).format('YYYY-MM-DDThh:mm A');

            this.applyLeaveData.hospitalIds = [];
            this.hospitals.forEach(element => {
                if (element.hospitalDetails.isChecked) {
                    this.applyLeaveData.hospitalIds.push(element.hospitalDetails.hospitalId);
                }
            });



            // this.applyLeaveData = {
            //     "profileId": "659de03c-2bca-407d-b44d-fe0281c20a2a",
            //     "roleType": "DOCTOR",
            //     "description": "Feeling Sick",
            //     "hospitalIds": ["d971aeef-9e79-4df5-bb92-944edb910dea"],
            //     "fromDateTime": "2019-02-16T10:30 AM",
            //     "toDateTime": "2019-02-18T10:30 AM"
            //     }




            if (this.mode == "edit") {
                this.applyLeaveData.leaveIds = this.leaveIds;
                console.log("REQUEST", this.applyLeaveData);
                this.doctorRoleServiceProvider.updateLeave(this.applyLeaveData).subscribe(
                    res => {
                        if (res.message) {
                            this.commonService.presentToast(res.message);
                        }
                        if (res.status == "success") {
                            console.log("DATA", res.data);
                            this.commonService.hideLoading();
                            this.cancelClick();
                            this.getViewLeaves(this.doctorData.doctorId);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                        this.commonService.presentToast(errorMsg);
                    }
                );
            } else {
                this.doctorRoleServiceProvider.applyLeave(this.applyLeaveData).subscribe(
                    res => {
                        if (res.message) {
                            this.commonService.presentToast(res.message);
                        }
                        if (res.status == "success") {
                            console.log("DATA", res.data);
                            this.commonService.hideLoading();


                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                        this.commonService.presentToast(errorMsg);
                    }
                );
            }

        }

    }


    /*
     * get View Leaves
     * */

    getViewLeaves(doctorId: any) {
        console.log("DOCTOR_ID", doctorId);
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.doctorRoleServiceProvider.viewLeaves(doctorId).subscribe(res => {

            // let tempHospitalPageNo = this.hospitalPageNo;
            if (res.data && res.data.leaveDTOs && res.data.leaveDTOs.length > 0) {
                this.commonService.hideLoading();
                this.viewLeaves = res.data.leaveDTOs;

                // if(!this.commonService.checkIsWeb()){
                //     this.sTimeChanged = true;
                //     this.timeSlot.startTime = new Date(sTime);
                // } else{
                //     let tempDate = new Date(sTime);
                //     let hrMinStrin = tempDate.getHours() + ":" + tempDate.getMinutes();
                //     this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes( hrMinStrin );
                // }

                this.viewLeaves.forEach(element => {
                    element.fromDateView = new Date(element.fromDate);
                    element.toDateView = new Date(element.toDate);


                    // var newDate = moment(this.appointmentList[i].appointmentDetails.fromTime).toDate();
                    // console.log("time to IE",newDate)
                    // element.fromTimeView = this.commonService.convertTo12hrsFormat(new Date(element.fromDate), false, true);

                    let startTime = new Date(element.fromDate);
                    let endTime = new Date(element.toDate);
                    element.fromTimeView = new Date(startTime.getTime() + (startTime.getTimezoneOffset() * 60000));
                    element.toTimeView = new Date(endTime.getTime() + (endTime.getTimezoneOffset() * 60000));

                    element.hospitals = [];
                    console.log('element hospital', element.hospitals);
                    element.hospitalDTO.forEach(elementHospital => {
                        elementHospital.isChecked = true;
                        let hospitalDetails = { "hospitalDetails": elementHospital };
                        element.hospitals.push(hospitalDetails);


                    });
                    if (element.hospitals.length > 0) {
                        this.formatAvailabilityForHospital(element.hospitals);
                    }

                });



                // for(let i=0;i<10;i++){
                //     this.hospitals.push(this.hospitals[0]);
                // }
                // this.getImageBaseHospitalList();
                // console.log("hospitals---->", this.hospitals);
                // if ((tempHospitalPageNo + 1) >= res.data.totalPages) {
                //     this.loadFinishedHospital = true;
                // }
                // this.formatAvailability();
            } else {
                // this.loadFinishedHospital = true;
            }
        }, error => {
            this.commonService.hideLoading();
            this.commonService.presentToast(error);

        });
    }

    editLeave = (viewLeave: any) => {
        this.cloneData();

        this.leaveDescription = viewLeave.description;

        if (this.commonService.checkIsWeb()) {
            this.startDateValue = new Date(viewLeave.fromDate);//new Date(moment(viewLeave.fromDate).format("DD/MM/YY")).toISOString();
            this.endDateValue = new Date(viewLeave.toDate);
            let startTime = new Date(viewLeave.fromDate);
            let endTime = new Date(viewLeave.toDate);
            // this.startTimeValue = new Date(startTime.getTime() + (startTime.getTimezoneOffset()*60000));
            // this.endTimeValue = new Date(endTime.getTime() + (endTime.getTimezoneOffset()*60000));
            this.startTimeValue = new Date(startTime.getTime());
            this.endTimeValue = new Date(endTime.getTime());
        } else {

            this.startDateValue = new Date(viewLeave.fromDate).toISOString();
            this.endDateValue = new Date(viewLeave.toDate).toISOString();

            let startTime = new Date(viewLeave.fromDate);
            let endTime = new Date(viewLeave.toDate);
            this.startTimeValue = new Date(startTime.getTime() - startTime.getTimezoneOffset() * 60000).toISOString();
            this.endTimeValue = new Date(endTime.getTime() - endTime.getTimezoneOffset() * 60000).toISOString();
        }



        // this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes(tempLocalStartTime);
        // this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes(tempLocalEndTime);  

        // this.timeSlot.startTime = tempLocalStartTime;
        // this.timeSlot.endTime = tempLocalEndTime;

        // this.startTimeValue = moment(viewLeave.fromDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format('hh:mm A');
        // this.endTimeValue = moment(viewLeave.toDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format('hh:mm A');
        // this.hospitals = _.copyDeep(viewLeave.hospitalDTO);
        this.hospitals = [];
        this.selectedHospitals = [];

        viewLeave.hospitalDTO.forEach(element => {
            element.isChecked = true;
            let hospitalDetails = { "hospitalDetails": element };
            this.selectedHospitals.push(element.hospitalId);
            this.hospitals.push(hospitalDetails);

        });

        this.getImageBaseHospitalList();

        this.formatAvailability();

        this.leaveIds = _.cloneDeep(viewLeave.leaveIds);
        this.changeMode("edit");
    }

    cloneData = () => {
        this.startDateValueClone = _.cloneDeep(this.startDateValue);
        this.endDateValueClone = _.cloneDeep(this.endDateValue);
        this.startTimeValueClone = _.cloneDeep(this.startTimeValue);
        this.endTimeValueClone = _.cloneDeep(this.endTimeValue);
        this.hospitalsClone = _.cloneDeep(this.hospitals);
        this.selectedHospitalsClone = _.cloneDeep(this.selectedHospitals);
        this.leaveDescriptionClone = _.cloneDeep(this.leaveDescription);
    }

    revertClone = () => {
        this.startDateValue = _.cloneDeep(this.startDateValueClone);
        this.endDateValue = _.cloneDeep(this.endDateValueClone);
        this.startTimeValue = _.cloneDeep(this.startTimeValueClone);
        this.endTimeValue = _.cloneDeep(this.endTimeValueClone);
        this.hospitals = _.cloneDeep(this.hospitalsClone);
        this.selectedHospitals = _.cloneDeep(this.selectedHospitalsClone);
        this.leaveDescription = _.cloneDeep(this.leaveDescriptionClone);
    }

}

