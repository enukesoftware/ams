import { Component, ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, MenuController, Content, NavParams, Events, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorDataDto } from '../../../interfaces/user-data-dto';
import { DoctorRoleServiceProvider } from "../../../providers/doctor-role-service/doctor-role-service";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { DoctorRoleProfilePage } from "../doctor-role-profile/doctor-role-profile";

@IonicPage( {
    name: 'DoctorDashboardPage',
    segment: 'doctor-dashboard/:id'
} )
@Component({
  selector: 'page-doctor-dashboard',
  templateUrl: 'doctor-dashboard.html',
})
export class DoctorDashboardPage {
    userLoginRole: string;
    @ViewChild( Content ) content: Content;
    @ViewChild( 'DoctorDashboardGrid' ) DoctorDashboardGrid: ElementRef;
    userData: UserDataDTO = {};
    doctorData: DoctorDataDto = {};
    scrollHTFlag: boolean = false;
    doctorId: any;
    userActiveRole: string;
    showMyProfile: boolean = false;
    doctorEducation: any[];
    reportsRedirectionRole:any;
    
    constructor( private commonService: CommonService, private evts: Events, private location: Location, public platform: Platform, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private constants: Constants, public manageHospitalServiceProvider: ManageHospitalServiceProvider ) {
        this.menu.enable( true );
        /**
         *handle device back button
         */
        this.platform.registerBackButtonAction(() => {
//             this.location.back();
          });
        
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );

//        this.commonService.setInStorage('userPrevRole', this.commonService.getActiveUserRole());
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    ionViewDidLoad = () => {
        //dashboard Wrapper height dynamically set based on scroll content height
        setTimeout(() => {
            this.scrollHTFlag = this.commonService.contentHeight( this.content,this.DoctorDashboardGrid );
        }, 100 );
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
                this.userData = result;
                this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_DOCTOR );
                this.commonService.setActiveUserRole( this.constants.ROLE_DOCTOR );
                this.evts.publish('fire-after-login-event');
                
                this.userLoginRole = this.commonService.getUserLoginRole();
                this.userActiveRole = this.commonService.getActiveUserRole();
               
                    if(this.userLoginRole == 'DOCTOR'){
                        this.reportsRedirectionRole ='doctor';
                    }
                    if(this.userLoginRole == 'SYSTEM_ADMIN'){
                        this.reportsRedirectionRole ='sysAdmDoctor';
                    }
                
                    if(this.userLoginRole == this.userActiveRole){
                        this.showMyProfile = true;
                    }
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR );
                this.commonService.fireSelectedEvent( '#/doctor-dashboard/0' );
                this.doctorId = this.navParams.get('id');
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR, true );
                this.commonService.setPaddingTopScroll( this.content );
                this.getDoctorProfile();
                
                //booking details clear from local storage
                this.commonService.removeFromStorage("appointmentDetails");
                this.commonService.removeFromStorage("isConfirmed");
                this.commonService.removeFromStorage("isUpdateOnView");
            }
        } );
    }
    
    /*
     * Function to get doctor details based on id
     * */
    getDoctorProfile = () => {
        if( this.navParams.get('id') && this.navParams.get('id') != ":id" && this.navParams.get('id') != "0" ){
            this.doctorId = this.navParams.get('id');
            this.commonService.setInStorage("doctorId", this.doctorId );
            this.doctorRoleServiceProvider.getDoctorProfile(this.doctorId).subscribe(res=>{
                this.doctorData = res.data;
                this.commonService.hideLoading();
                this.commonService.setInStorage("doctorId", res.data.doctorId);
                this.commonService.setInStorage("doctorProfileId", res.data.doctorProfileId);
                if(this.userData.userId == this.doctorData.doctorId || this.userData.doctorId == this.doctorData.doctorId){
                    this.showMyProfile = true;
                } else{
                    this.showMyProfile = false;
                }
            }, error => {
                //this.commonService.showAlert("Error", err.message);
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            });
        }else{
            this.commonService.getFromStorage("doctorId").then(( value ) => {
                if( value ) {
                    this.doctorId = value;
                }else{
                    this.doctorId = "";
                }
                this.doctorRoleServiceProvider.getDoctorProfile(this.doctorId).subscribe(res=>{
                    this.doctorData = res.data;
                    this.commonService.hideLoading();
                    this.commonService.setInStorage("doctorId", res.data.doctorId);
                    this.commonService.setInStorage("doctorProfileId", res.data.doctorProfileId);
                    if(this.userData.userId == this.doctorData.doctorId || this.userData.doctorId == this.doctorData.doctorId){
                        this.showMyProfile = true;
                    } else{
                        this.showMyProfile = false;
                    }
                }, error=>{
                    //this.commonService.showAlert("Error", err.message);
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);
                });
            });
        }
    }
    
    goToProfile(){
        this.navCtrl.setRoot('DoctorRoleProfilePage');
    }

    showComingSoon = () => {
        this.commonService.presentToast();
    }
}