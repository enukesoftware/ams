import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from "../../components/shared.module";
import { DoctorDashboardPage } from './doctor-dashboard';

@NgModule({
  declarations: [
    DoctorDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorDashboardPage),
    TranslateModule.forChild(),
    SharedModule
  ],
  exports: [
    DoctorDashboardPage
  ]
})
export class DoctorDashboardPageModule {}
