import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";
import { DoctorRoleAppointmentsHistoryPage } from './doctor-role-appointments-history';
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";
import { SharedModule } from "../../components/shared.module";
import { CalendarModule } from 'primeng/calendar';
import { PipesModule } from '../../../pipes/pipes.module'

@NgModule({
  declarations: [
    DoctorRoleAppointmentsHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorRoleAppointmentsHistoryPage),
        TranslateModule.forChild(),
        MultiSelectModule,
        SharedModule,
        CalendarModule,
        PipesModule
  ],
  exports: [
    DoctorRoleAppointmentsHistoryPage
   ]
})
export class DoctorRoleUpcomingAppointmentsPageModule {}
