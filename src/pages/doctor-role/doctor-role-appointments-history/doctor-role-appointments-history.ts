import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, MenuController, Platform} from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO, DoctorDataDto, AddressDto, serviceAndSpecializationDataDto, HospitalListDTO } from "../../../interfaces/user-data-dto";
import { Constants } from '../../../providers/appSettings/constant-settings';
import { DoctorRoleServiceProvider } from '../../../providers/doctor-role-service/doctor-role-service';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";

//3rd party npm module
import * as _ from 'lodash';
import * as moment from 'moment';

@IonicPage( {
    name: 'DoctorRoleAppointmentsHistoryPage',
    segment: 'doctor-role-appointments-history'
} )
@Component( {
    selector: 'page-doctor-role-appointments-history',
    templateUrl: 'doctor-role-appointments-history.html',
} )
export class DoctorRoleAppointmentsHistoryPage {
    @ViewChild( Content ) content: Content;
    fromDoctorAppointmentHistory: any;
    fromDoctorUpcomingAppointment: any;
    appointmentList: any;

    selectedCityId( arg0: any, arg1: any ): any {
        throw new Error( "Method not implemented." );
    }

    userData: UserDataDTO = {};
    doctorData: DoctorDataDto = {};
    address: AddressDto = {};
    isProfileEdit: boolean;
    isMyProfile: boolean;
    formSubmitted: boolean;
    servicesString: any;
    specializationString: any;
    doctorEducation: any[];
    hospitals: HospitalListDTO[] = [];
    loadFinished: boolean = false;
    pageNo: number = 0;
    pageName: any;
    startValue: any;
    startValueStr: string;
    endValue: any;
    endValueStr: string;
    protected doctorDetails: any;
    protected doctorProfileId: any;
    protected doctorId: any;
    today: any;
    todayDate: any;
    showDrPrefix: boolean = false;
    goBtnClicked = false;
    errorMSG:any;
    errorMsgEndDate:any;
    isPatientDependents:any;
    bookingUrl:any;
    
    constructor( public navCtrl: NavController, private constants: Constants, private menu: MenuController, public navParams: NavParams, public commonService: CommonService, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider, public _ngZone: NgZone,
            public platform : Platform, public location: Location) {
        this.todayDate = new Date();
        this.platform.registerBackButtonAction(() => {
            this.location.back();
          });
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        this.todayDate = new Date();
        this.today =  this.convertDate();
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.getPatientDependent();
        this.isAuthorized();
    }

        
    convertDate = () =>{
        var d = new Date();
        
        return [d.getFullYear(), this.pad(d.getMonth()+1), this.pad(d.getDate())].join('-');
    }
    
    pad = (s) => {
        return (s < 10) ? '0' + s : s;
    }

    /*function to show coming soon popup
     * */
    comingSoonPopup = () => {
        this.commonService.presentToast();
    }

    /**
     * Function to get doctor profile details
     */
    getDoctorProfile( doctorId ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.doctorRoleServiceProvider.getDoctorProfile( doctorId ).subscribe(
            res => {
                this.commonService.hideLoading();
                if ( res.status == 'success' ) {
                    this.doctorDetails = res.data;
                    this.doctorProfileId = this.doctorDetails.doctorProfileId;
                    this.getDoctorAppointmentHistory( true );
                }
            }, error => {
                this.commonService.hideLoading();
                this.commonService.presentToast( error.message );
                console.log( "error....", error );
            } );
    }


    /**
     * Function to check if user is authorized or not to access the screen
     */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                //this.getDoctorProfile(this.doctorId);
                this.commonService.getFromStorage("userLoginRole").then(value =>{
                    if(value){
                        if(value == this.constants.ROLE_DOCTOR){
                            this.showDrPrefix = true;
                        }else{
                            this.showDrPrefix = false;
                        }
                    } else{
                        this.showDrPrefix = false;
                    } 
                 });
                
                
                this.commonService.getFromStorage( "doctorId" ).then(( value ) => {
                    if ( value ) {
                        this.doctorId = value;
                        this.getDoctorProfile( this.doctorId );
                    } else {
                        this.doctorId = "";
                    }
                } );
                // this.getDoctorAppointmentHistory(true);
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR );
                this.commonService.fireSelectedEvent( '#/doctor-role-appointments-history' );
            }
        } );
    }
    
    /* function to get images in async */ 
    public getImageBaseHospitalList =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.appointmentList.length; i++) {
            let imageData = {
                    'userType': "HOSPITAL_IMAGE",
                    'id': this.appointmentList[i].hospitalDetails.hospitalId,
                    'rank': 1,
                    'isDefault': true
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.appointmentList.length; j++ ){
                    if(this.appointmentList[j].hospitalDetails.hospitalId == res.data.userId){
                        this.appointmentList[j].hospitalDetails.imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.appointmentList);
            }
        },
        error => {
            this.commonService.hideLoading();
            /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }

    /**
     * Function To get Appointment history
     **/
    getDoctorAppointmentHistory( firstPageCall?: boolean ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        if(firstPageCall){
            this.appointmentList = "";
        }
        let startDate = "", endDate = "";
        if( this.startValueStr){
           if(!this.commonService.checkIsWeb()){
               var now = new Date(this.startValueStr);
               var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
               startDate = utc_now.getTime().toString();
           }
           else{
               new Date(this.startValueStr).setHours(11, 59, 59);
               startDate = new Date(this.startValueStr).getTime().toString();
           }
        } 
        if(this.endValueStr){
            if(!this.commonService.checkIsWeb()){
                var now = new Date(this.endValueStr);
                var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                endDate = utc_now.getTime().toString();
            }
            else{
                new Date(this.endValueStr).setHours(11, 59, 59);
                endDate = new Date(this.endValueStr).getTime().toString();
            }
        }
        if( this.startValueStr && this.endValueStr ){
            this.goBtnClicked=false;
        }
        this.doctorRoleServiceProvider.getDoctorAppointmentHistory( this.doctorProfileId, this.pageNo ,startDate,  endDate).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                    this.appointmentList = res.data.list;
                    this.getImageBaseHospitalList();
                    console.log("appointmentList..",this.appointmentList);
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                }else if ( res.data.list && res.data.list.length > 0 ) {
                    for ( let j = 0; j < res.data.list.length; j++ ) {
                        this.appointmentList.push( res.data.list[j] );
                    }
                    this.getImageBaseHospitalList();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                }else {
                    this.loadFinished = true;
                }
                
                if(this.appointmentList && this.appointmentList.length > 0){
                    for(let i=0; i<this.appointmentList.length; i++){
                        if(this.appointmentList[i].timings && this.appointmentList[i].timings.length > 0){
                            var newAppHistDate = moment(this.appointmentList[i].timings[0].fromTime).toDate();
                            var newAppHistToDate = moment(this.appointmentList[i].timings[this.appointmentList[i].timings.length - 1].toTime).toDate();
                            this.appointmentList[i].currentAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(newAppHistDate), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(newAppHistToDate), false, true);
                        } else{
                            this.appointmentList[i].currentAvailabilityString = "Not available";
                        }
                        this.appointmentList[i].showAppointmentDate = new Date(this.appointmentList[i].appointmentDate);
                    }
                this.commonService.setPaddingTopScroll( this.content );
                }
                
                
            }, error => {
                this.commonService.presentToast( error.message );
                this.commonService.hideLoading();
                this.commonService.setPaddingTopScroll( this.content );
            } );
    }
    
    /*
     * apply date filter
     * 
     * */
    applyDateFilter = () => {
        if(!this.startValueStr){
            this.goBtnClicked =true;
            this.errorMsgEndDate='';
            this.errorMSG = this.commonService.getTranslate('SELECTSTARTDATE',{});
            this.commonService.presentToast(this.errorMSG );
        }
        else if(!this.endValueStr && this.startValueStr){
            this.goBtnClicked =true;
            this.errorMSG = '';
            this.errorMsgEndDate = this.commonService.getTranslate('SELECTENDDATE',{});
            this.commonService.presentToast(this.errorMsgEndDate );
        }
        else{
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.getDoctorAppointmentHistory(true);
        }
       
    }
    /*
     * Function to load more data when clicked on load more button
     * */
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getDoctorAppointmentHistory();
    }


    getPatientDependent = () => {
        if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            this.commonService.getFromStorage('HospitalDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });
        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            this.commonService.getFromStorage('DoctorDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            this.commonService.getFromStorage('PatientDependents').then((value) => {
                if (value) {
                    console.log("DEP:",value);
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });

        }

    }

    onClickBookAppointment(fromPage: any, redirectPage: any, redirectParam: any, doctorId: any) {
        if (this.isPatientDependents) {
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
            console.log("BOOKING_URL1",this.bookingUrl);
        } else {
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            console.log("BOOKING_URL2",this.bookingUrl);
        }
    }

}