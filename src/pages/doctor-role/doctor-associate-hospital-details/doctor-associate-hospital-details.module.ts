import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { DoctorAssociateHospitalDetailsPage } from './doctor-associate-hospital-details';

import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    DoctorAssociateHospitalDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorAssociateHospitalDetailsPage),
    TranslateModule.forChild(),
    SharedModule
    ],
    exports: [
       DoctorAssociateHospitalDetailsPage
    ]
})
export class DoctorAssociateHospitalDetailsPageModule {}
