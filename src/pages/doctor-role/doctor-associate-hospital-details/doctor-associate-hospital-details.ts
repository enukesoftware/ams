import { Component, ViewChild, Renderer2 } from '@angular/core';
import { IonicPage, PopoverController, NavController, NavParams, MenuController, Content, Slides, ModalController,Platform } from 'ionic-angular';
import { Location } from '@angular/common';

import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorListDTO, HospitalDTO, HospitalDataDTO, HospitalListDTO } from '../../../interfaces/user-data-dto';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';

import { GalleryModal } from 'ionic-gallery-modal';
import { PopoverSliderComponent } from "../../components/popover-slider/popover-slider";
import { CallNumber } from "@ionic-native/call-number";

@IonicPage( {
    name: 'DoctorAssociateHospitalDetailsPage',
    segment: 'doctor-associate-hospital-details/:hospitalId'
} )
@Component( {
    selector: 'page-doctor-associate-hospital-details',
    templateUrl: 'doctor-associate-hospital-details.html',
} )
export class DoctorAssociateHospitalDetailsPage {
    @ViewChild( Content ) content: Content;
    @ViewChild( Slides ) slides: Slides;
    fromDoctorRoleAssociatedHospitalDetails: any;
    userData: UserDataDTO = {};
    hospitalData: HospitalDTO = { address: {} };
    selectedHospital: HospitalListDTO = {};
    isSliding: boolean = false;
    clickedImgIndex: number;
    overlayHidden: boolean;
    hospitalId: string;
    imgUrl: any = [];
    rank: number;
    imgCounter: number;
    pageNo: number = 0;
    loadFinished: boolean = false;
    //assign sys admins dto
    systemAdmins: DoctorListDTO[];
    showDrPrefix: boolean = false;
    hospitalServices: any[];
    hospitalSpecialities: any[];
    viewMoreSpFlag: boolean = false;
    viewMoreServiceFlag: boolean = false;
    windowWidth: number;
    lessThen3Sp: boolean = true;
    initialSpecialities: any[];
    lessThen3Service: boolean = true;
    initialServices: any[];
    popOver: any;
    noDefaultImg: boolean = false;
    tempImgArr: any = [];
    
    constructor( public popoverCtrl: PopoverController, public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
        private constants: Constants, private manageHospitalService: ManageHospitalServiceProvider, private renderer: Renderer2, private callNumber: CallNumber,public platform : Platform, public location: Location ) {

    }

    ionViewDidLoad = () => {
        this.menu.enable( true );
        //Wrapper height dynamically set based on scroll content height
        this.isAuthorized();
        this.imgCounter = 1;
    }
    
    
    ionViewWillEnter = () => {
        this.windowWidth = this.commonService.getWindowWidth();
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                
                this.commonService.getFromStorage("userLoginRole").then(value =>{
                    if(value){
                        if(value == this.constants.ROLE_DOCTOR){
                            this.showDrPrefix = true;
                        }else{
                            this.showDrPrefix = false;
                        }
                    } else{
                        this.showDrPrefix = false;
                    } 
                 });
                
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR );
                this.commonService.fireSelectedEvent( '#/doctor-associate-hospitals' );
                this.hospitalId = this.navParams.get('hospitalId');
                this.commonService.setInStorage("hospitalId", this.hospitalId);
                this.viewHospitalDetails( this.hospitalId );
                this.getHospitalAdminList( this.hospitalId, true );
                /*this.commonService.getFromStorage( "hospitalId" ).then(( value ) => {
                    if ( value ) {
                        this.hospitalId = value;
                        
                    }
                } );*/
            }
        } );
    }

    /**
     * function to get hospital details
     */
    viewHospitalDetails = ( hospitalId: any ) => {
        this.manageHospitalService.getHospitalDetails( this.hospitalId ).subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.hospitalData = res.data;
                  //download image
                    if( this.hospitalData ){
                        this.rank = this.hospitalData.imagesCount;
                    
                        if( this.hospitalData.imagesCount ){
                            for( let i=0; i<this.imgUrl.length; i++ ){
                                if( this.imgUrl[i].file != null ){
                                    this.imgUrl[i].file = "assets/imgs/edit_defauld_profile_pic.png";
                                }
                            }
                        }
                    
                        //download image function call
                        this.downloadImage();
                    }
                            
                            
                    this.hospitalSpecialities = this.hospitalData.specializationList;
                    this.hospitalServices = this.hospitalData.serviceList;
                    
                    if(this.windowWidth <= 768){
                        let spLen = this.hospitalSpecialities.length;
                        if(spLen > 3){
                            this.hospitalSpecialities = [];
                            this.viewMoreSpFlag = true;
                            this.lessThen3Sp = false;
                            for(let i=0; i<spLen; i++){
                                if(i < 3){
                                    this.hospitalSpecialities.push(this.hospitalData.specializationList[i]);
                                }
                            }
                        } else{
                            this.lessThen3Sp = true;
                        }
                        this.initialSpecialities = this.hospitalSpecialities;
                        
                        let serviceLen = this.hospitalServices.length;
                        if(serviceLen > 3){
                            this.hospitalServices = [];
                            this.viewMoreServiceFlag = true;
                            this.lessThen3Service = false;
                            for(let i=0; i<serviceLen; i++){
                                if(i < 3){
                                    this.hospitalServices.push(this.hospitalData.serviceList[i]);
                                }
                            }
                        } else{
                            this.lessThen3Service = true;
                        }
                        
                        this.initialServices = this.hospitalServices;
                    }

                    var currentDate = new Date();
                    var today = currentDate.getDay();
                    var todayAvailability;
                    let availabilities = this.hospitalData.timeAvailabilityList;
                    let calculatedTime = this.commonService.calculateTime( availabilities );
                    this.hospitalData.formattedAvailabilities = this.commonService.calculateTime( availabilities );
                    let availabilityLength = this.hospitalData.formattedAvailabilities.length;
                    for ( let j = 0; j < availabilityLength; j++ ) {
                        if ( today == this.hospitalData.formattedAvailabilities[j].dayCount ) {
//                            todayAvailability = this.hospitalData.formattedAvailabilities[j];
                        }
                    }
                    this.hospitalData.todayAvailability = this.hospitalData.todaysTimeAvailabilityList;
                    if(this.hospitalData.todaysTimeAvailabilityList && this.hospitalData.todaysTimeAvailabilityList.length > 0){
                        let timeSlotFound = false;
                        let currentTime = new Date();
                        let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                        let todayTimeLen = this.hospitalData.todayAvailability.length;
                        for(let j=0; j<todayTimeLen; j++){
                            let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitalData.todaysTimeAvailabilityList[j].fromTime), true);
                            let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.hospitalData.todaysTimeAvailabilityList[j].toTime), true);
                            
                            if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                                timeSlotFound = true;
                                this.hospitalData.todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitalData.todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitalData.todaysTimeAvailabilityList[j].toTime), false, true);
                            }
                        }
                        if(!timeSlotFound){
                            this.hospitalData.todaysAvailabilityString = "Closed";
                        }
//                        this.hospitalData.todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitalData.todaysTimeAvailabilityList[0].fromTime)) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitalData.todaysTimeAvailabilityList[0].toTime));
                    } else{
                        this.hospitalData.todaysAvailabilityString = "Closed";
                    }
                }
                    this.commonService.setPaddingTopScroll( this.content );
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast( errorMsg );
                this.commonService.setPaddingTopScroll( this.content );
            }
        );
    }

    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public plotAddressOnMap = ( hospital: any ) => {
        let currentHospitalAddress = hospital.name+ "+" +hospital.address.street + "+" + hospital.address.cityName + "+" +
            hospital.address.state + "+" + hospital.address.zipCode;
        this.commonService.setAddressOnMap( currentHospitalAddress );
    }

    /**
     * Array function for count
     */
    counter = ( i: number ) => {
        return new Array( i );
    }

    /**
     * Function to show image on popover
     * @param index 
     */
    presentPopover( index ) {
        this.isSliding = true;
        this.popOver = this.popoverCtrl.create( PopoverSliderComponent, { data: this.imgUrl, index: index }, {
            'enableBackdropDismiss': true
        } );
        this.popOver.present();
        this.commonService.closePopupOnBackBtn(this.popOver);
    }


    //To do : get hospital admin list on web service response
    public getHospitalAdminList( id, firstPageCall?: boolean ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.manageHospitalService.getHospitalAdminsList( id, this.pageNo ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == "success" ) {
                    if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                        this.systemAdmins = res.data.list;
                        this.getImageBase();
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    } else if ( res.data.list && res.data.list.length > 0 ) {
                        for ( let j = 0; j < res.data.list.length; j++ ) {
                            this.systemAdmins.push( res.data.list[j] );
                        }
                        this.getImageBase();
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    } else {
                        this.loadFinished = true;
                    }
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast( errorMsg );
            }
        );
    }

    
    /* function to get images in async */ 
    public getImageBase =()=>{
        console.log("image base 64 getImageBase =======>");
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.systemAdmins.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.systemAdmins[i].userId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64 eeeeee=======>", res);
                for( let j=startIndex; j<this.systemAdmins.length; j++ ){
                    if(this.systemAdmins[j].userId == res.data.userId){
                        this.systemAdmins[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.systemAdmins);
            }
        },
        error => {
            this.commonService.hideLoading();
      /*      let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }
    
    
    /**
     * DownLoad hospital images
     * @param imageData 
     */
    downloadImage(){
         let imageData = {
            'userType': "HOSPITAL_IMAGE",
            'id': this.hospitalData.hospitalId,
            'rank': this.imgCounter,
        }   
        if( this.imgCounter <= this.hospitalData.imagesCount){
            this.commonService.downloadImage( imageData ).subscribe(
                res => {
                    if ( res.status == "success" ) {
                        this.imgUrl[this.imgCounter-1] = res.data;
                        this.imgCounter ++;
                        this.downloadImage(); 
                        let isProfileImgFound = false;
                        let profileImgArray = [];
                        for( let i=0; i<this.imgUrl.length; i++ ){
                        console.log("this.imgUrl", this.imgUrl);
                            if( this.imgUrl[i].isDefault && !this.imgUrl[i].file ){
                                   this.tempImgArr.push(this.imgUrl[i]);
                                   isProfileImgFound = true;
                            }else if( this.imgUrl[i].isDefault && this.imgUrl[i].file){
                                profileImgArray.push(this.imgUrl[i]);
                            }     
                        }
                        
                        if( !isProfileImgFound || (this.tempImgArr && this.tempImgArr.length > 0)){
                            this.noDefaultImg = true;
                        }
            
                        if( profileImgArray && profileImgArray.length > 0){
                            this.noDefaultImg = false;
                        }
                    }
                },
                error => {
                    this.commonService.hideLoading();
                   /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);*/
            });
       }
       
    }
    
    
    /**
     * function to make a phone call
     **/
    makeCall(number){
        this.callNumber.callNumber(number, true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
    }
    
    /*
     * Function to load more data when clicked on load more button
     * */
    public loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getHospitalAdminList( this.hospitalId );
    }
    
    /*
     * Function to open timing details
     * */
    openTimingDetails = (hospital) => {
        let title = hospital.name;
        let subTitle= hospital.address.street +", "+ hospital.address.cityName;
        let popover = this.commonService.showTimingList(title, subTitle, hospital.formattedAvailabilities);
        this.commonService.closePopupOnBackBtn(popover);
    }

    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
        this.commonService.presentToast();
    }
    
    /*
     * more Specialities List
     * */
                    
     hideMore(type){
         if(type == "speciality"){
             console.log("speciality 185.......");
             this.hospitalSpecialities = this.hospitalData.specializationList;
             this.viewMoreSpFlag = false;
         } else{
             console.log("services 189.......");
             this.hospitalServices = this.hospitalData.serviceList;
             this.viewMoreServiceFlag = false;
         }
     }
     
     /*
      * less Specialities List
      * */
     hideLess(type){
         if(type == "speciality"){
             this.hospitalSpecialities = this.initialSpecialities;
             this.viewMoreSpFlag = true;   
         } else{
             this.hospitalServices = this.initialServices;
             this.viewMoreServiceFlag = true;
         }
     }
    
    
}