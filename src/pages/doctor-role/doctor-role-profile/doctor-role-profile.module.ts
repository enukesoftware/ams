import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";
import { SelectSearchableModule } from 'ionic-select-searchable';
import { SharedModule } from "../../components/shared.module";
import { DoctorRoleProfilePage } from './doctor-role-profile';

@NgModule({
  declarations: [
    DoctorRoleProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorRoleProfilePage),
        TranslateModule.forChild(),
        MultiSelectModule,
        SelectSearchableModule,
        SharedModule
  ],
  exports: [
        DoctorRoleProfilePage
   ]
})
export class DoctorRoleProfilePageModule {}
