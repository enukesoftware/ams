import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from "../../components/shared.module";
import { DoctorRoleAppointmentVisitDetailsPage } from './doctor-role-appointment-visit-details';

@NgModule({
  declarations: [
    DoctorRoleAppointmentVisitDetailsPage,
  ],
  imports: [
      IonicPageModule.forChild(DoctorRoleAppointmentVisitDetailsPage),
       TranslateModule.forChild(),
       SharedModule
    ],
    exports: [
       DoctorRoleAppointmentVisitDetailsPage
    ]
})
export class DoctorRoleAppointmentVisitDetailsPageModule {}
