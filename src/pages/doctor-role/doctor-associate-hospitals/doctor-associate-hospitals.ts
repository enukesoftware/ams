import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, Events, NavParams, Content, MenuController,Platform } from 'ionic-angular';
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { DoctorRoleServiceProvider } from '../../../providers/doctor-role-service/doctor-role-service';
import * as moment from 'moment';
import { UserDataDTO,HospitalListDTO } from '../../../interfaces/user-data-dto';
import { Location } from '@angular/common';

@IonicPage( {
  name: 'DoctorAssociateHospitalsPage',
  segment: 'doctor-associate-hospitals'
} )

@Component({
  selector: 'page-doctor-associate-hospitals',
  templateUrl: 'doctor-associate-hospitals.html',
})
export class DoctorAssociateHospitalsPage {
  @ViewChild( Content ) content: Content;
  userData: UserDataDTO = {};
  protected doctorId:any;
  protected loadFinished: boolean = false;
  protected pageNo: number = 0;
  hospitals: HospitalListDTO = {};
  listOfHospitals: any = [];
  showDrPrefix: boolean = false;
  
  constructor(public navCtrl: NavController, private evts: Events, private menu: MenuController, public navParams: NavParams, private commonService: CommonService, private constants: Constants,
              public doctorRoleServiceProvider:DoctorRoleServiceProvider,public platform : Platform, public location: Location) {
    this.listOfHospitals = [];  
    this.menu.enable( true );
  }

  ionViewDidLoad = () => {
    this.menu.enable( true );
    //Wrapper height dynamically set based on scroll content height
    this.isAuthorized();
}

/*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
      this.commonService.isLoggedIn(( result ) => {
          if ( !result ) {
              this.navCtrl.setRoot( 'LoginPage' );
          } else {
              this.userData = result;
             // console.log("this.userData ",this.userData);
              this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_DOCTOR );
              this.commonService.setActiveUserRole( this.constants.ROLE_DOCTOR );
//              this.evts.publish('fire-after-login-event');
              
              this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR );
              this.commonService.fireSelectedEvent( '#/doctor-associate-hospitals' );
          
              this.commonService.getFromStorage("userLoginRole").then(value =>{
                 if(value){
                     if(value == this.constants.ROLE_DOCTOR){
                         this.showDrPrefix = true;
                     }else{
                         this.showDrPrefix = false;
                     }
                 } else{
                     this.showDrPrefix = false;
                 } 
              });
              
              this.commonService.getFromStorage("doctorId").then(( value ) => {
                  if ( value ) {
                      this.doctorId = value;
                      this.getListOfAssociatedHospitals(this.doctorId,true);
                  }
              });
          }
      } );
  }
    
    /* function to get images in async */ 
    public getImageBase =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.listOfHospitals.length; i++) {
            let imageData = {
                    'userType': "HOSPITAL_IMAGE",
                    'id': this.listOfHospitals[i].hospitalDetails.hospitalId,
                    'rank': 1,
                    'isDefault': true
            
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.listOfHospitals.length; j++ ){
                    if(this.listOfHospitals[j].hospitalDetails.hospitalId == res.data.userId){
                        this.listOfHospitals[j].hospitalDetails.imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.listOfHospitals);
            }
        },
        error => {
            this.commonService.hideLoading();
       /*     let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }

    getListOfAssociatedHospitals( doctorId: any ,firstPageCall?: boolean ) {
        //this.manageHospitalService.getAssociatedDoctorsList( hospitalId ).subscribe(
        //console.log("getListOfAssociatedDoctors ==================================>",doctorId);
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.doctorRoleServiceProvider.getAssociatedHospitalsList( doctorId, this.pageNo).subscribe(
            res => {
                //this.commonService.presentToast("List of cities success.......");
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == "success" ) {
                    //this.listOfHospitals = [];
                    if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                        this.listOfHospitals = res.data.list;
                        this.getImageBase();
                        console.log("listOfDoctors",this.listOfHospitals);
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                        this.formatAvailability();
                    }else if( res.data.list && res.data.list.length > 0 ){
                        for( let j=0;j<res.data.list.length;j++ ){
                            this.listOfHospitals.push(res.data.list[j]);
                            console.log("listOfDoctors",this.listOfHospitals);
                        }
                        this.getImageBase();
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                        this.formatAvailability();
                    }else{
                        this.loadFinished = true;
                    }
                }
                this.commonService.setPaddingTopScroll( this.content );
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                //this.commonService.showAlert( "Error", error.message );
                this.commonService.setPaddingTopScroll( this.content );
            }
        );
    }
    
    
    /*
     * Function to do calculations for time related 
     * */
    formatAvailability = () => {
        var currentDate = new Date();
        var today = currentDate.getDay();
        let hospitalLength = this.listOfHospitals.length;
        for(let i=0; i< hospitalLength; i++){
            var todayAvailability;
            this.listOfHospitals[i].hospitals = this.listOfHospitals[i].hospitalDetails;
            let availabilities = this.listOfHospitals[i].hospitals.timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime(availabilities);
            this.listOfHospitals[i].hospitals.formattedAvailabilities = this.commonService.calculateTime(availabilities);
            let availabilityLength = this.listOfHospitals[i].hospitals.formattedAvailabilities.length;
            this.listOfHospitals[i].hospitals.todayAvailability = this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList;
            
            if(this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList && this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList.length > 0){
                let todayTimeLen = this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList.length;
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                for(let j=0; j<todayTimeLen; j++){
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList[j].toTime), true);
                    if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                        timeSlotFound = true;
                        this.listOfHospitals[i].hospitals.todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.listOfHospitals[i].hospitals.todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if(!timeSlotFound){
                    this.listOfHospitals[i].hospitals.todaysAvailabilityString = "Closed";
                }
            } else{
                this.listOfHospitals[i].hospitals.todaysAvailabilityString = "Closed";
            }
        }
    }
    
    openTimingDetails = (hospital) => {
        let title = hospital.hospitalDetails.name;
        let subTitle= hospital.hospitalDetails.address.street +", "+ hospital.hospitalDetails.address.cityName;
        let popover = this.commonService.showTimingList(title, subTitle, hospital.hospitalDetails.formattedAvailabilities);
        this.commonService.closePopupOnBackBtn(popover);
    }
    
    
    /*
     * Function to load more data when clicked on load more button
     * */
   public loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getListOfAssociatedHospitals(this.doctorId);
    }  
    
    
  /**
   * Function for coming soon popup
   */
   comingSoonPopup = () => {
        this.commonService.presentToast();
    }

}
