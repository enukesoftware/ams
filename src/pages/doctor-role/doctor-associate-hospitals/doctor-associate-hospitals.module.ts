import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { DoctorAssociateHospitalsPage } from './doctor-associate-hospitals';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    DoctorAssociateHospitalsPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorAssociateHospitalsPage),
    TranslateModule.forChild(),
    SharedModule
    ],
    exports: [
       DoctorAssociateHospitalsPage
    ]
})
export class DoctorAssociateHospitalsPageModule {}
