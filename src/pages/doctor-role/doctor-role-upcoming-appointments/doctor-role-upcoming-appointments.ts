import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, MenuController,Platform } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO, DoctorDataDto, AddressDto, serviceAndSpecializationDataDto, HospitalListDTO } from "../../../interfaces/user-data-dto";
import { Constants } from '../../../providers/appSettings/constant-settings';
import { DoctorRoleServiceProvider } from '../../../providers/doctor-role-service/doctor-role-service';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CalendarModule } from 'primeng/calendar';
//import { groupByDatePipe } from '../../../pipes/groupByDate.pipe';

//3rd party npm module
import * as _ from 'lodash';

@IonicPage( {
    name: 'DoctorRoleUpcomingAppointmentsPage',
    segment: 'doctor-role-upcoming-appointments'
} )
@Component( {
    selector: 'page-doctor-role-upcoming-appointments',
    templateUrl: 'doctor-role-upcoming-appointments.html',


} )
export class DoctorRoleUpcomingAppointmentsPage {
    @ViewChild( Content ) content: Content;
    fromDoctorAppointmentHistory: any;
    fromDoctorUpcomingAppointment: any;

    selectedCityId( arg0: any, arg1: any ): any {
        throw new Error( "Method not implemented." );
    }
    startValue: any;
    endValue: any;
    userData: UserDataDTO = {};
    doctorData: DoctorDataDto = {};
    address: AddressDto = {};
    isProfileEdit: boolean;
    isMyProfile: boolean;
    formSubmitted: boolean;
    servicesString: any;
    specializationString: any;
    doctorEducation: any[];
    hospitals: HospitalListDTO[] = [];
    appointmentList: any;
    loadFinished: boolean = false;
    pageNo: number = 0;
    pageName: any;
    today: any;
    protected doctorDetails: any;
    protected doctorProfileId: any;
    protected doctorId: any;
    todayDateStr: any;
    showDrPrefix: boolean = false;
    goBtnClicked = false;
    errorMSG:any;
    errorMsgEndDate:any;
    startValueStr: string;
    endValueStr: string;
    todayForWeb: any;
    isPatientDependents:boolean =false;
    bookingUrl:any;

    constructor( public navCtrl: NavController, private constants: Constants, private menu: MenuController, public navParams: NavParams, public commonService: CommonService, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider,
            private location: Location, public platform: Platform) {
        this.platform.registerBackButtonAction(() => {
            this.location.back();
          });
    }

   

    ionViewWillEnter = () => {
        this.menu.enable( true );
        this.today = this.convertDate();
        this.todayForWeb = new Date();
        console.log("today--->",this.today);
        this.todayDateStr = this.commonService.formatDateString(new Date());
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.getPatientDependent();
        this.isAuthorized();
        // this.pageName = this.navParams.get('fromPage');
    }
    
    convertDate = () =>{
        var d = new Date();
        
        return [d.getFullYear(), this.pad(d.getMonth()+1), this.pad(d.getDate())].join('-');
    }
    
    pad = (s) => {
        return (s < 10) ? '0' + s : s;
    }


    /**
     * Function to check if user is authorized or not to access the screen
     */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.getFromStorage("userLoginRole").then(value =>{
                    if(value){
                        if(value == this.constants.ROLE_DOCTOR){
                            this.showDrPrefix = true;
                        }else{
                            this.showDrPrefix = false;
                        }
                    } else{
                        this.showDrPrefix = false;
                    } 
                 });
                // this.getDoctorProfile(this.doctorId);
                this.commonService.getFromStorage( "doctorId" ).then(( value ) => {
                    if ( value ) {
                        this.doctorId = value;
                        this.getDoctorProfile( this.doctorId );
                    } else {
                        this.doctorId = "";
                    }
                } );
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR );
                this.commonService.fireSelectedEvent( '#/doctor-role-upcoming-appointments' );
            }
        } );
    }

    /**
     * Function to get doctor profile details
     */
    getDoctorProfile( doctorId ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.doctorRoleServiceProvider.getDoctorProfile( doctorId ).subscribe(
            res => {
                this.commonService.hideLoading();
                if ( res.status == 'success' ) {
                    this.doctorDetails = res.data;
                    this.doctorProfileId = this.doctorDetails.doctorProfileId;
                    this.getDoctorUpcomingAppointments( true );
                }
                this.commonService.setPaddingTopScroll( this.content );
            }, error => {
                this.commonService.hideLoading();
                this.commonService.presentToast( error.message );
                console.log( "error....", error );
                this.commonService.setPaddingTopScroll( this.content );
            } );
    }
    
    /* function to get images in async */ 
    public getImageBaseHospitalList =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.appointmentList.length; i++) {
            let imageData = {
                    'userType': "HOSPITAL_IMAGE",
                    'id': this.appointmentList[i].hospitalDetails.hospitalId,
                    'rank': 1,
                    'isDefault': true   
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.appointmentList.length; j++ ){
                    if(this.appointmentList[j].hospitalDetails.hospitalId == res.data.userId){
                        this.appointmentList[j].hospitalDetails.imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.appointmentList);
            }
        },
        error => {
            this.commonService.hideLoading();
           /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }

    /**
     * Function To get upcoming Appointments
     **/
    getDoctorUpcomingAppointments( firstPageCall?: boolean ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        if(firstPageCall){
            this.appointmentList = "";
        }
        let startDate = "", endDate = "";
        if( this.startValueStr){
            if(!this.commonService.checkIsWeb()){
                var now = new Date(this.startValueStr);
                var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                startDate = utc_now.getTime().toString();
            }
            else{
                new Date(this.startValueStr).setHours(11, 59, 59);
                startDate = new Date(this.startValueStr).getTime().toString();
            }
        } 
        if(this.endValueStr){
            if(!this.commonService.checkIsWeb()){
                var now = new Date(this.endValueStr);
                var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                endDate = utc_now.getTime().toString();
            }
            else{
                new Date(this.endValueStr).setHours(11, 59, 59);
                endDate = new Date(this.endValueStr).getTime().toString();
            }
        }
        if( this.startValueStr && this.endValueStr ){
            this.goBtnClicked=false;
        }
        this.doctorRoleServiceProvider.getDoctorUpcomingAppointments( this.doctorProfileId, this.pageNo, startDate, endDate).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                    this.appointmentList = res.data.list;
                    this.getImageBaseHospitalList();
                    console.log("appointmentList..",this.appointmentList);
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
//                    this.getSortedAppointmentsList( this.appointmentList );
                } else if ( res.data.list && res.data.list.length > 0 ) {
                    for ( let j = 0; j < res.data.list.length; j++ ) {
                        this.appointmentList.push( res.data.list[j] );
                    }
                    this.getImageBaseHospitalList();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                } else {
                    this.loadFinished = true;
                    if(res.data.list.length ==0){
                        this.appointmentList = [];
                    }
                }
                
                if(this.appointmentList && this.appointmentList.length > 0){
                    for(let i=0; i<this.appointmentList.length; i++){
                        if(this.appointmentList[i].timings && this.appointmentList[i].timings.length > 0){
                            this.appointmentList[i].currentAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.appointmentList[i].timings[0].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.appointmentList[i].timings[this.appointmentList[i].timings.length - 1].toTime), false, true);
                        } else{
                            this.appointmentList[i].currentAvailabilityString = "Not available";
                        }
                        this.appointmentList[i].showAppointmentDate = new Date(this.appointmentList[i].appointmentDate);
                    }
                }
            }, error => {
                this.commonService.presentToast( error.message );
                this.commonService.hideLoading();
            } );
    }

    /*
     * Function to load more data when clicked on load more button
     * */
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getDoctorUpcomingAppointments();
    }

    /*function to show coming soon popup
     * */
    comingSoonPopup = () => {
        this.commonService.presentToast();
    }
    
    /*
     * function to apply date filter
     * */

    applyDateFilter = () => {
        if(!this.startValueStr){
            this.goBtnClicked =true;
            this.errorMsgEndDate='';
            this.errorMSG = this.commonService.getTranslate('SELECTSTARTDATE',{});
            this.commonService.presentToast(this.errorMSG );
        }
        else if(!this.endValueStr && this.startValueStr){
            this.goBtnClicked =true;
            this.errorMSG = '';
            this.errorMsgEndDate = this.commonService.getTranslate('SELECTENDDATE',{});
            this.commonService.presentToast(this.errorMsgEndDate );
        }
        else{
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.getDoctorUpcomingAppointments(true);
        }
    }
    
    /*
     * function to get upcoming appointments 
     * */
    getSortedAppointmentsList = ( appointmentList ) => {
        for ( let i = 0; i < appointmentList.length; i++ ) {
            //               for(let j = i; j <= this.appointmentList[i].length; j++) {
            //                   if(i != j && this.appointmentList[i] == this.appointmentList[j]) {
            //                   }
            //               }    
        }
    }

    getPatientDependent = () => {
        if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            this.commonService.getFromStorage('HospitalDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });
        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            this.commonService.getFromStorage('DoctorDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            this.commonService.getFromStorage('PatientDependents').then((value) => {
                if (value) {
                    console.log("DEP:",value);
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });

        }

    }

    onClickBookAppointment(fromPage: any, redirectPage: any, redirectParam: any, doctorId: any) {
        console.log("DATA:","FROM:"+fromPage +" REDIRECT_PAGE:" +redirectPage +" PARAM:" +redirectParam + " DOC:"+doctorId);
        if (this.isPatientDependents) {
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
            console.log("BOOKING_URL1",this.bookingUrl);
        } else {
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            console.log("BOOKING_URL2",this.bookingUrl);
        }
    }

   }