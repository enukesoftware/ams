import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";
import { DoctorRoleUpcomingAppointmentsPage } from './doctor-role-upcoming-appointments';
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";
import { SharedModule } from "../../components/shared.module";
import { CalendarModule } from 'primeng/calendar';
import { PipesModule } from '../../../pipes/pipes.module'

@NgModule({
  declarations: [
    DoctorRoleUpcomingAppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorRoleUpcomingAppointmentsPage),
        TranslateModule.forChild(),
        MultiSelectModule,
        SharedModule,
        CalendarModule,
        PipesModule
  ],
  exports: [
DoctorRoleUpcomingAppointmentsPage
   ]
})
export class DoctorRoleUpcomingAppointmentsPageModule {}
