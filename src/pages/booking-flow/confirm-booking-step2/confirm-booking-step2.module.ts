import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from "../../components/shared.module";
import { ConfirmBookingStep2Page } from './confirm-booking-step2';
import { DropdownModule } from 'primeng/dropdown';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    ConfirmBookingStep2Page,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmBookingStep2Page),
       TranslateModule.forChild(),
       SharedModule,
       DropdownModule,
       IonicSelectableModule
    ],
    exports: [
       ConfirmBookingStep2Page
    ]
})
export class ConfirmBookingStep2PageModule {}
