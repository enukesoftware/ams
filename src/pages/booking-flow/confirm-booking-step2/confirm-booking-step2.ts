import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ModalController, MenuController, Platform } from 'ionic-angular';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

import { UserDataDTO, HospitalDTO, AddressDto, DoctorDataDto } from '../../../interfaces/user-data-dto';

//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { CameraService } from '../../../providers/camera-service/camera.service';

import { CancelAppointmentConfirmationComponent } from "../../components/cancel-appointment-confirmation/cancel-appointment-confirmation";
import { PatientRoleHomePage } from "../../patient-role/patient-role-home/patient-role-home";

@IonicPage({
    name: 'ConfirmBookingStep2',
    segment: 'confirm-booking-step2/:from/:redirectParam'
})
@Component({
    selector: 'page-confirm-booking-step2',
    templateUrl: 'confirm-booking-step2.html'
})
export class ConfirmBookingStep2Page {
    @ViewChild(Content) content: Content;
    userData: UserDataDTO = {};
    date: any;
    imgUrl: any = [];
    rank: number = 0;
    imgCounter: number = 0;
    profileImg: any = {};
    uploadImgArr: any = [];
    uploadImgCount: number;
    formSubmitted: boolean;
    subImages: any;
    hospitalWithTimeSlotData: any;
    formattedDate: any;
    selectedHospitalSlot: any;
    selectedTimeSlot: any;
    doctorDetails: DoctorDataDto = {};
    hospitalDetails: HospitalDTO = {};
    address: AddressDto = {};
    bookingUserName: any;
    docArr: any = [];
    appointmentData: any;
    isConfirm = false;
    fromPage: any;
    appointmentDetails: any;
    bookingUserContactNo: any;
    appointmentStartTime: any;
    patientId: any;
    redirectParam: any;
    isUpdateOnView: boolean = false;
    okBtnUrl: any;
    editBtnUrlParam: any;
    changeDetailswithoutConfirm: any;
    changeDetailsWithConfirm: any;
    isUpdateBooking: boolean = false;
    fees: any;
    imagesCount: any = 0;
    uploadCount: any = 1;
    appointmentDataWithImageCount: any;
    downloadFilesCount: any;
    downloadFileAppointmentId: any;
    isCreate: boolean = false;
    appointmentType: any;
    agreeTermsTickImgUrl: any;
    agree: boolean = false;
    backBtnUrl: any;
    mobileUploadedFileType: any;
    mobileIsUploadFile: boolean = false;
    mobileIsDefault: boolean;
    userActiveRole: any;
    tokenNumber: any;
    popover: any;

    patientDependents: any = [];
    patient: any = {};
    dependentStorageKey: any;

    constructor(private commonService: CommonService, public cameraService: CameraService, private _ngZone: NgZone, public navCtrl: NavController, public platform: Platform, public location: Location, public modalCtrl: ModalController, public navParams: NavParams, private menu: MenuController, private constants: Constants) {
        this.menu.enable(true);
        this.commonService.closePopupOnBackBtn(this.popover);
    }

    ionViewDidLoad() {
        this.menu.enable(true);

        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();

        this.uploadImgCount = 0;

        if (this.navParams.get('from')) {
            this.fromPage = this.navParams.get('from');
        }
        if (this.navParams.get('redirectParam')) {
            this.redirectParam = this.navParams.get('redirectParam');
        }
    }

    ionViewWillEnter() {
        //If isView mode
        this.commonService.getFromStorage("isConfirmed").then((value) => {
            if (value) {
                this.isConfirm = value;
            }
        });

        //   this.getPatientDependent();
        this.agreeTermsTickImgUrl = "assets/imgs/01_multispacility_uncheck.png";
    }

    /*
     * Function to check if user is authorized or not to access the screen
     **/
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if (!result) {
                this.navCtrl.setRoot('LoginPage');
            } else {
                this.userData = result;
                console.log("this.userData", this.userData)
                // this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_DOCTOR, true);
                this.commonService.setPaddingTopScroll(this.content);

                /* To get User Active role and User login role from storage
                 * To pre-populate patient name and contact no. only for patient role
                 **/
                this.commonService.getFromStorage("userActiveRole").then((value) => {
                    if (value) {
                        this.userActiveRole = value;
                        if (this.userActiveRole == this.constants.ROLE_HOSPITAL_ADMIN) {
                            this.dependentStorageKey = "HospitalDependents"
                        } else if (this.userActiveRole == this.constants.ROLE_DOCTOR) {
                            this.dependentStorageKey = "DoctorDependents"
                        } else if (this.userActiveRole == this.constants.ROLE_PATIENT) {
                            this.dependentStorageKey = "PatientDependents"
                        } else if (this.userActiveRole == this.constants.ROLE_HOSPITAL_STAFF) {
                            this.dependentStorageKey = "HospitalStaffDependents"
                        }

                        this.getPatientDependent();
                    }
                });

                //Get patient Id from storage
                this.commonService.getFromStorage("patientId").then((value) => {
                    if (value) {
                        this.patientId = value;
                    }
                });

                //Get patient Id from storage
                this.commonService.getFromStorage("isUpdateOnView").then((value) => {
                    if (value) {
                        this.isUpdateOnView = value;
                    }
                });

                //Get appointment data from storage( locally stored on bookingStep1)
                this.commonService.getFromStorage("hospitalWithAppointmentTimeSlot").then((value) => {
                    if (value) {
                        this.hospitalWithTimeSlotData = value;
                        this.fees = this.hospitalWithTimeSlotData.fees;
                        if (this.hospitalWithTimeSlotData) {
                            if (this.hospitalWithTimeSlotData.selectedTimeSlot) {
                                this.selectedTimeSlot = this.hospitalWithTimeSlotData.selectedTimeSlot;
                            }

                            if (this.hospitalWithTimeSlotData.selectedHospitalSlot) {
                                this.hospitalDetails = this.hospitalWithTimeSlotData.selectedHospitalSlot;
                            }

                            if (this.hospitalWithTimeSlotData.doctorDetails) {
                                this.doctorDetails = this.hospitalWithTimeSlotData.doctorDetails;
                            }

                            if (this.hospitalWithTimeSlotData.selectedHospitalSlot && this.hospitalWithTimeSlotData.selectedHospitalSlot.address) {
                                this.address = this.hospitalWithTimeSlotData.selectedHospitalSlot.address;
                                console.log("ADDRESS:", this.address);
                            }
                        }

                        //Based on redirection change in bindings
                        if (this.fromPage == 'HSRoleAssociatedDoctorList' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/hospital-staff-associated-doctor-list/HSRoleDashboard');
                            this.okBtnUrl = '#/hospital-staff-associated-doctor-list/HSRoleDashboard';
                            this.editBtnUrlParam = 'HSRoleAssociatedDoctorList';
                            this.backBtnUrl = "#/booking-step1/0/0/HSRoleAssociatedDoctorList";
                            this.changeDetailswithoutConfirm = 'HSRoleAssociatedDoctorList';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'HSRoleOnboardPatient' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/onboard-patient/HSRoleDashboard/HSRoleAssociatedDoctorList/0/0');
                            this.okBtnUrl = '#/onboard-patient/HSRoleDashboard/HSRoleAssociatedDoctorList/0/0';
                            this.editBtnUrlParam = 'HSRoleOnboardPatient';
                            this.backBtnUrl = "#/booking-step1/0/0/HSRoleOnboardPatient";
                            this.changeDetailswithoutConfirm = 'HSRoleOnboardPatient';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'patientHome' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/patient-role-home/0');
                            this.okBtnUrl = '#/appointment-list/apppointmentConfirmed';
                            this.editBtnUrlParam = 'patientHome';
                            this.backBtnUrl = "#/booking-step1/0/0/patientHome";
                            this.changeDetailswithoutConfirm = 'patientHome';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'HAAssociatedDoctorList' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/hospital-admin-associated-doctor-list');
                            this.okBtnUrl = '#/hospital-admin-manage-appointment';
                            this.editBtnUrlParam = 'HAAssociatedDoctorList';
                            this.backBtnUrl = "#/booking-step1/0/0/HAAssociatedDoctorList";
                            this.changeDetailswithoutConfirm = 'HAAssociatedDoctorList';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'HAManageAppointment' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/hospital-admin-manage-appointment');
                            this.changeDetailswithoutConfirm = 'HAManageAppointment';
                            this.okBtnUrl = '#/hospital-admin-manage-appointment';
                            this.backBtnUrl = "#/booking-step1/0/0/HAManageAppointment";
                            this.editBtnUrlParam = 'HAManageAppointment';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'HAManageAppointmentVDBook' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/hospital-admin-manage-appointment');
                            this.changeDetailswithoutConfirm = 'HAManageAppointmentVDBook';
                            this.okBtnUrl = '#/hospital-admin-manage-appointment';
                            this.backBtnUrl = "#/booking-step1/0/0/HAManageAppointmentVDBook";
                            this.editBtnUrlParam = 'HAManageAppointmentVDBook';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'doctorRoleUpcomingAppointment' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/doctor-role-upcoming-appointments');
                            this.changeDetailswithoutConfirm = 'doctorRoleUpcomingAppointment';
                            this.okBtnUrl = '#/doctor-role-upcoming-appointments';
                            this.backBtnUrl = "#/booking-step1/0/0/doctorRoleUpcomingAppointment";
                            this.editBtnUrlParam = 'doctorRoleUpcomingAppointment';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'doctorRoleUpcomingAppointmentVDBook' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/doctor-role-upcoming-appointments');
                            this.changeDetailswithoutConfirm = 'doctorRoleUpcomingAppointmentVDBook';
                            this.okBtnUrl = '#/doctor-role-upcoming-appointments';
                            this.backBtnUrl = "#/booking-step1/0/0/doctorRoleUpcomingAppointmentVDBook";
                            this.editBtnUrlParam = 'doctorRoleUpcomingAppointmentVDBook';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'doctorRoleAppointmentHistory' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/doctor-role-appointments-history');
                            this.changeDetailswithoutConfirm = 'doctorRoleAppointmentHistory';
                            this.okBtnUrl = '#/doctor-role-appointments-history';
                            this.backBtnUrl = "#/booking-step1/0/0/doctorRoleAppointmentHistory";
                            this.editBtnUrlParam = 'doctorRoleAppointmentHistory';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'patientUpcomingAppointmentBookAgain' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                            this.changeDetailswithoutConfirm = 'patientUpcomingAppointmentBookAgain';
                            this.okBtnUrl = '#/appointment-list/patientUpcoming';
                            this.backBtnUrl = "#/booking-step1/0/0/patientUpcomingAppointmentBookAgain";
                            this.editBtnUrlParam = 'patientUpcomingAppointmentBookAgain';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                            this.changeDetailswithoutConfirm = 'patientUpcomingAppointmentViewDetailsBookAgain';
                            this.okBtnUrl = '#/appointment-list/patientUpcoming';
                            this.backBtnUrl = "#/booking-step1/0/0/patientUpcomingAppointmentViewDetailsBookAgain";
                            this.editBtnUrlParam = 'patientUpcomingAppointmentViewDetailsBookAgain';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'patientAppointmentHistory' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/appointment-list/patientHistory');
                            this.changeDetailswithoutConfirm = 'patientAppointmentHistory';
                            this.okBtnUrl = '#/appointment-list/patientUpcoming';
                            this.backBtnUrl = "#/booking-step1/0/0/patientAppointmentHistory";
                            this.editBtnUrlParam = 'patientAppointmentHistory';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/doctor-role-appointments-history');
                            this.changeDetailswithoutConfirm = 'doctorRoleAppointmentHistoryVisitDetailsBookAgain';
                            this.okBtnUrl = '#/doctor-role-appointments-history';
                            this.backBtnUrl = "#/booking-step1/0/0/doctorRoleAppointmentHistoryVisitDetailsBookAgain";
                            this.editBtnUrlParam = 'doctorRoleAppointmentHistoryVisitDetailsBookAgain';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'patientDoctorProfile' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/patient-role-home/0');
                            this.okBtnUrl = '#/appointment-list/apppointmentConfirmed';
                            this.backBtnUrl = "#/booking-step1/0/0/patientDoctorProfile";
                            this.changeDetailswithoutConfirm = 'patientDoctorProfile';
                            this.editBtnUrlParam = 'patientDoctorProfile';
                            this.OnNewBooking();
                        } else if (this.fromPage == 'patientHospitalProfile' && this.redirectParam == 'bookFlow') {
                            this.commonService.fireSelectedEvent('#/patient-role-home/0');
                            this.okBtnUrl = '#/appointment-list/apppointmentConfirmed';
                            this.changeDetailswithoutConfirm = 'patientHospitalProfile';
                            this.backBtnUrl = "#/booking-step1/0/0/patientHospitalProfile";
                            this.editBtnUrlParam = 'patientHospitalProfile';
                            this.OnNewBooking();
                        }

                        //Based on redirection change in bindings
                        if (this.fromPage == 'patientHome' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/patient-role-home/0');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'patientHome';
                        } else if (this.fromPage == 'HAAssociatedDoctorList' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/hospital-admin-associated-doctor-list');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'HAAssociatedDoctorList';
                        } else if (this.fromPage == 'HAManageAppointment' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/hospital-admin-manage-appointment');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'HAManageAppointment';
                        } else if (this.fromPage == 'doctorRoleUpcomingAppointment' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/doctor-role-upcoming-appointments');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'doctorRoleUpcomingAppointment';
                        } else if (this.fromPage == 'doctorRoleAppointmentHistory' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/doctor-role-appointments-history');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'doctorRoleAppointmentHistory';
                        } else if (this.fromPage == 'patientUpcomingAppointmentBookAgain' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'patientUpcomingAppointmentBookAgain';
                        } else if (this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'patientUpcomingAppointmentViewDetailsBookAgain';
                        } else if (this.fromPage == 'patientAppointmentHistory' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/appointment-list/patientHistory');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'patientAppointmentHistory';
                        } else if (this.fromPage == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('doctor-role-appointments-history');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'doctorRoleAppointmentHistoryVisitDetailsBookAgain';
                        } else if (this.fromPage == 'patientUpcomingAppointment' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/patient-role-home/0');
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'patientUpcomingAppointment';
                        } else if (this.fromPage == 'DoctorRoleUpdateVisitDetails' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/doctor-role-upcoming-appointments');
                            this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'DoctorRoleUpdateVisitDetails';
                        } else if (this.fromPage == 'DoctorRoleUpdateVisitDetails' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/doctor-role-upcoming-appointments');
                            this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
                            this.OnUpdateAppointment();
                            this.changeDetailsWithConfirm = 'DoctorRoleUpdateVisitDetails';
                        } else if (this.fromPage == 'HAUpdateVisitDetails' && this.redirectParam == 'updatedAppointmentSlot') {
                            this.commonService.fireSelectedEvent('#/hospital-admin-manage-appointment');
                            this.OnUpdateAppointment();
                            this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
                            this.changeDetailsWithConfirm = 'HAUpdateVisitDetails';
                        }
                    }
                });


                if (this.fromPage == 'patientUpcomingAppointment' ||
                    this.fromPage == 'patientUpcomingAppointmentViewDetails' ||
                    this.fromPage == 'HAUpdateVisitDetails' ||
                    this.fromPage == 'DoctorRoleUpdateVisitDetails') {
                    //From patient role upcoming appointments( update appointment )
                    if ((this.fromPage == 'patientUpcomingAppointment' || this.fromPage == 'HAUpdateVisitDetails' || this.fromPage == 'patientUpcomingAppointmentViewDetails') && this.redirectParam == 'updatedAppointmentSlot') {
                        //if redirect param is updatedAppointmentSlot
                        this.commonService.getFromStorage("PatientUpcomingAppointmentid").then((value) => {
                            if (value) {
                                this.getAppointmentDetails(value, this.selectedTimeSlot);
                            }
                        });

                        this.changeDetailsWithConfirm = 'patientUpcomingAppointment';
                        this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                    } else if (this.fromPage == 'patientUpcomingAppointment' && (this.redirectParam != 'updatedAppointmentSlot' || this.redirectParam != 'bookFlow')) {
                        this.isConfirm = false;
                        //if redirect param is appointmentId
                        this.getAppointmentDetails(this.redirectParam);
                        this.changeDetailsWithConfirm = 'patientUpcomingAppointment';
                        this.backBtnUrl = "#/appointment-list/patientUpcoming";
                        this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                    } else if (this.fromPage == 'HAUpdateVisitDetails' && this.redirectParam != 'updatedAppointmentSlot') {
                        this.isConfirm = false;
                        //if redirect param is appointmentId
                        this.changeDetailsWithConfirm = 'HAUpdateVisitDetails';
                        this.getAppointmentDetails(this.redirectParam);
                        this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
                        this.commonService.fireSelectedEvent('#/hospital-admin-manage-appointment');
                    } else if (this.fromPage == 'DoctorRoleUpdateVisitDetails' && this.redirectParam != 'updatedAppointmentSlot') {
                        this.isConfirm = false;
                        //if redirect param is appointmentId
                        this.changeDetailsWithConfirm = 'DoctorRoleUpdateVisitDetails';
                        this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
                        this.getAppointmentDetails(this.redirectParam);
                        this.commonService.fireSelectedEvent('#/doctor-role-upcoming-appointments');
                    } else if (this.fromPage == 'patientUpcomingAppointmentViewDetails') {
                        this.isConfirm = true;
                        this.backBtnUrl = "#/appointment-list/patientUpcoming";
                        //if redirect param is appointmentId
                        this.changeDetailsWithConfirm = 'patientUpcomingAppointmentViewDetails';
                        this.getAppointmentDetails(this.redirectParam);
                        this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                    }

                }
            }
        });
    }

    /*
     * On update appointment patient name and date changes;
     **/
    OnUpdateAppointment() {
        let startTime = this.selectedTimeSlot.fromTime;
        console.log("selected time slot", this.selectedTimeSlot);
        this.appointmentStartTime = this.commonService.convertTo12hrsFormat(new Date(startTime), false, true);
        console.log("this.hospitalWithTimeSlotData.appointmentDetails.............", this.hospitalWithTimeSlotData.appointmentDetails);
        if (this.hospitalWithTimeSlotData.appointmentDetails) {
            this.appointmentData = this.hospitalWithTimeSlotData.appointmentDetails;
            this.bookingUserName = this.hospitalWithTimeSlotData.appointmentDetails.patientName;
            this.bookingUserContactNo = this.hospitalWithTimeSlotData.appointmentDetails.patientContactNumber;
            this.formattedDate = this.selectedTimeSlot.currentDate;
            this.appointmentType = this.hospitalWithTimeSlotData.appointmentDetails.visitType;
            console.log("formattedDate --- -- >>>", this.hospitalWithTimeSlotData.appointmentDetails);
            if (this.appointmentType == 'TOKEN') {
                this.tokenNumber = this.hospitalWithTimeSlotData.appointmentDetails.tokenNumber;
            }
        } else {
            this.hospitalWithTimeSlotData.appointmentDetails = this.appointmentData;
            console.log("this.hospitalWithTimeSlotData.appointmentDetails222.............", this.hospitalWithTimeSlotData.appointmentDetails);
        }

        if (this.hospitalWithTimeSlotData.selectedHospitalSlot) {
            this.hospitalDetails = this.hospitalWithTimeSlotData.selectedHospitalSlot;
            console.log("this.hospital", this.hospitalDetails);
        }

        if (this.hospitalWithTimeSlotData.selectedHospitalSlot) {
            this.appointmentType = this.selectedTimeSlot.visitType;
            if (this.appointmentType == 'TOKEN') {
                this.tokenNumber = this.hospitalWithTimeSlotData.appointmentDetails.tokenNumber;
            }
        }
    }

    OnNewBooking() {
        //pre-populate loggedin user name and contact no. only for patient role
        if (this.userActiveRole == 'PATIENT') {
            //This code will run if normal booking flow executed
            let lastName;
            if (this.userData.lastName && this.userData.lastName != "" && this.userData.lastName != null) {
                lastName = " " + this.userData.lastName;
            } else {
                lastName = "";
            }
            this.bookingUserName = this.userData.firstName + lastName;
            this.bookingUserContactNo = this.userData.contactNo;
        } else {
            this.bookingUserName = "";
            this.bookingUserContactNo = "";
        }

        let startTime = this.selectedTimeSlot.fromTime;
        this.selectedTimeSlot.startTime = this.commonService.convertTo12hrsFormat(new Date(startTime), false, true);
        this.formattedDate = this.selectedTimeSlot.currentDate;
        this.appointmentType = this.selectedTimeSlot.visitType;
        if (this.appointmentType == 'TOKEN') {
            this.tokenNumber = this.selectedTimeSlot.tokenNumber;
        }

    }
    /*
     * Function to show comingSoon popUp
     **/
    comingSoonPopup = () => {
        this.commonService.presentToast();
    }

    /*
     * Function to create an appointment
     **/
    createAppointment(form: NgForm) {
        this.formSubmitted = true;
        if (form.valid) {
            this.formSubmitted = false;

            // Create appointment API obj formation
            let appointmentDate = this.selectedTimeSlot.currentDate;
            let fromTime = new Date(this.selectedTimeSlot.fromTime).toISOString();
            let toTime = new Date(this.selectedTimeSlot.toTime).toISOString();
            let appointmentData = {
                "appointmentDate": this.selectedTimeSlot.currentDate,
                "doctorProfileId": this.doctorDetails.doctorProfileId,
                "fees": this.fees,
                "fromDateTime": fromTime,
                "hospitalId": this.hospitalDetails.hospitalId,
                "patientContactNumber": this.bookingUserContactNo,
                "patientName": this.bookingUserName,
                "toDateTime": toTime,
                "appointmentType": this.selectedTimeSlot.visitType,
                "patientId": this.patientId
            }

            //create appointment API call
            this.popover = this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.commonService.createAppointment(appointmentData).subscribe(
                res => {
                    if (res.status == 'success') {
                        this.commonService.hideLoading();
                        console.log("res..............", res);
                        this.appointmentData = res.data.appointmentDetails;
                        this.getAppointmentDetails(this.appointmentData.appointmentId);
                        this.commonService.presentToast(res.message);

                        //Image upload function call
                        if (this.appointmentData) {
                            this.bookingUserName = this.appointmentData.patientName;
                            this.bookingUserContactNo = this.appointmentData.patientContactNumber;
                            this.uploadCount = 0;
                            if (this.uploadImgArr.length > 0) {
                                this.isCreate = true;
                                for (let i = 0; i < this.uploadImgArr.length; i++) {
                                    this.uploadImgCount++;
                                    this.uploadImgArr[i].rank = this.uploadImgCount;
                                    this.uploadImgArr[i].id = this.appointmentData.appointmentId;
                                }
                                this.uploadImage(this.uploadImgArr[0]);
                            } else {
                                this.isConfirm = true;
                                this.commonService.setInStorage('isConfirmed', this.isConfirm);
                            }
                        }
                    }
                    this.commonService.setPaddingTopScroll(this.content);
                },
                error => {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(error.message);
                    this.commonService.setPaddingTopScroll(this.content);
                });
        }
    }

    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     **/
    public plotAddressOnMap = (hospitalName: any, address: any) => {
        let currentHospitalAddress = hospitalName + "+" + address.street + "+" + address.cityName + "+" + address.state + "+" + address.zipCode;
        this.commonService.setAddressOnMap(currentHospitalAddress);
    }

    /*
     * Function to show confirmation popup on cancel appointment
     **/
    openCancelAppointmentConfirmPopup() {
        this.popover = this.modalCtrl.create(CancelAppointmentConfirmationComponent, '', { cssClass: 'cancelAppointmentPopover', showBackdrop: true, enableBackdropDismiss: false });
        this.popover.onDidDismiss((data) => {
            if (data) {
                this.cancelAppointment();
            }
        });
        this.popover.present();

        this.commonService.closePopupOnBackBtn(this.popover);
    }

    /*
     * Get Appointment details
     **/
    getAppointmentDetails(appointmentId, selectedTimeSlot?: any) {
        this.popover = this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.commonService.getAppointmentDetails(appointmentId).subscribe(
            res => {
                console.log("res", res);
                if (res.status == 'success') {
                    this.commonService.hideLoading();
                    if (this.fromPage == 'patientUpcomingAppointment' ||
                        this.fromPage == 'patientUpcomingAppointmentViewDetails' ||
                        this.fromPage == 'HAUpdateVisitDetails' ||
                        this.fromPage == 'DoctorRoleUpdateVisitDetails') {

                        if (this.fromPage == 'patientUpcomingAppointmentViewDetails' && this.redirectParam != 'updatedAppointmentSlot') {
                            this.isConfirm = true;
                        } else {
                            this.isConfirm = false;
                        }

                        this.uploadImgArr = [];
                        this.imgCounter = 1;
                        this.downloadFilesCount = res.data.appointmentDetails.totalFilesCount;
                        this.downloadFileAppointmentId = res.data.appointmentDetails.appointmentId;
                        this.downloadImage();

                    }

                    if ((this.fromPage == 'patientUpcomingAppointment' ||
                        this.fromPage == 'patientUpcomingAppointmentViewDetails' ||
                        this.fromPage == 'HAUpdateVisitDetails' ||
                        this.fromPage == 'DoctorRoleUpdateVisitDetails') && this.redirectParam == 'updatedAppointmentSlot') {

                        if (this.hospitalWithTimeSlotData && this.hospitalWithTimeSlotData.selectedHospitalSlot) {
                            this.hospitalDetails = this.hospitalWithTimeSlotData.selectedHospitalSlot;
                        }
                        if (this.hospitalWithTimeSlotData && this.hospitalWithTimeSlotData.selectedHospitalSlot && this.hospitalWithTimeSlotData.selectedHospitalSlot.address) {
                            this.address = this.hospitalWithTimeSlotData.selectedHospitalSlot.address;
                        }
                        if (this.hospitalWithTimeSlotData) {
                            this.appointmentType = this.hospitalWithTimeSlotData.visitType;
                            console.log("this.appointmentType", this.appointmentType)
                        }
                    } else {
                        this.hospitalWithTimeSlotData = res.data;
                        this.fees = res.data.appointmentDetails.fees;
                        if (this.hospitalWithTimeSlotData.doctorsDetails) {
                            this.doctorDetails = this.hospitalWithTimeSlotData.doctorsDetails;
                        }
                        if (this.hospitalWithTimeSlotData.hospitalDetails) {
                            this.hospitalDetails = this.hospitalWithTimeSlotData.hospitalDetails;
                        }
                        if (this.hospitalWithTimeSlotData.hospitalDetails.address) {
                            this.address = this.hospitalWithTimeSlotData.hospitalDetails.address;
                        }


                        /*if( this.hospitalWithTimeSlotData.appointmentDetails ){
                            console.log("111111111111", this.hospitalWithTimeSlotData.appointmentDetails);
                            this.appointmentData = this.hospitalWithTimeSlotData.appointmentDetails;
                            this.bookingUserName =  this.appointmentData.patientName;
                            this.bookingUserContactNo = this.appointmentData.patientContactNumber;
                            let appointmentDate;
                            
                            //Used for html binding
                            if(selectedTimeSlot && this.selectedTimeSlot.currentDate ){
                                let startTime = new Date(  this.selectedTimeSlot.fromTime );
                                this.appointmentStartTime = this.commonService.convertTo12hrsFormat( startTime, false, true );
                                this.formattedDate = this.selectedTimeSlot.currentDate;
                                this.appointmentType = this.selectedTimeSlot.visitType;
                                if(this.appointmentType == 'TOKEN'){
                                    this.tokenNumber =  this.selectedTimeSlot.tokenNumber;
                                }
                                
                            }else{
                                let startTime = new Date(  this.appointmentData.fromTime );
                                this.appointmentStartTime = this.commonService.convertTo12hrsFormat( startTime, false, true );
                                appointmentDate = new Date( this.appointmentData.appointmentDate ).toDateString();
                                this.formattedDate =  moment(appointmentDate).format('D MMM, YYYY');
                                this.appointmentType = this.appointmentData.appointmentType;     
                                console.log("3333333333333", this.appointmentData);
                                if(this.appointmentType == 'TOKEN'){
                                    this.tokenNumber =  this.appointmentData.tokenNumber;
                                }
                                
                            }   
                           
                            this.commonService.setInStorage("appointmentDetails", this.appointmentData);*/
                    }
                    console.log("111111111111", this.hospitalWithTimeSlotData.appointmentDetails);
                    this.appointmentData = this.hospitalWithTimeSlotData.appointmentDetails;
                    this.bookingUserName = this.appointmentData.patientName;
                    this.bookingUserContactNo = this.appointmentData.patientContactNumber;
                    let appointmentDate;
                    //Used for html binding
                    if (selectedTimeSlot && this.selectedTimeSlot.currentDate) {
                        let startTime = new Date(this.selectedTimeSlot.fromTime);
                        this.appointmentStartTime = this.commonService.convertTo12hrsFormat(startTime, false, true);
                        this.formattedDate = this.selectedTimeSlot.currentDate;
                        this.appointmentType = this.selectedTimeSlot.visitType;
                        if (this.appointmentType == 'TOKEN') {
                            this.tokenNumber = this.selectedTimeSlot.tokenNumber;
                        }

                    } else {
                        let startTime = new Date(this.appointmentData.fromTime);
                        this.appointmentStartTime = this.commonService.convertTo12hrsFormat(startTime, false, true);
                        appointmentDate = new Date(this.appointmentData.appointmentDate).toDateString();
                        this.formattedDate = moment(appointmentDate).format('D MMM, YYYY');
                        this.appointmentType = this.appointmentData.appointmentType;
                        console.log("3333333333333", this.appointmentData);
                        if (this.appointmentType == 'TOKEN') {
                            this.tokenNumber = this.appointmentData.tokenNumber;
                        }

                    }

                    this.commonService.setInStorage("appointmentDetails", this.appointmentData);
                }
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.presentToast(error.message);
                console.log("error....", error);
            });
    }

    /*
     * Function to edit appointment details
     **/
    editAppointment(form: NgForm) {
        this.formSubmitted = true;
        if (form.valid) {
            this.formSubmitted = false;
            let fromTime;
            let appointmentDate;
            /*if not selecting appointment details on update appointment(booking step1 page) then
             *use from storage otherwise take it from selected slot
             */
            if (this.selectedTimeSlot && this.selectedTimeSlot.currentDate) {
                appointmentDate = this.selectedTimeSlot.currentDate;
                fromTime = new Date(this.selectedTimeSlot.fromTime).toISOString();
                this.appointmentType = this.selectedTimeSlot.visitType;
            } else {
                console.log("update data................", this.hospitalWithTimeSlotData.appointmentDetails);
                appointmentDate = this.hospitalWithTimeSlotData.appointmentDetails.appointmentDate;
                fromTime = new Date(this.hospitalWithTimeSlotData.appointmentDetails.fromTime).toISOString();
                this.appointmentType = this.hospitalWithTimeSlotData.appointmentDetails.appointmentType;
            }

            let toTime = new Date(this.hospitalWithTimeSlotData.appointmentDetails.toTime).toISOString();
            let updateAppointmentData = {
                "appointmentDate": appointmentDate,
                "appointmentId": this.hospitalWithTimeSlotData.appointmentDetails.appointmentId,
                "appointmentType": this.appointmentType,
                "doctorProfileId": this.doctorDetails.doctorProfileId,
                "fees": this.fees,
                "fromDateTime": fromTime,
                "hospitalId": this.hospitalDetails.hospitalId,
                "patientContactNumber": this.bookingUserContactNo,
                "patientId": this.patientId,
                "patientName": this.bookingUserName,
                "toDateTime": toTime
            }

            this.popover = this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.commonService.editAppointmentDetails(updateAppointmentData).subscribe(
                res => {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                    if (this.uploadImgArr.length > 0) {

                        if (res.data.appointmentDetails.totalFilesCount > 0) {
                            this.deleteImage('PATIENT_APPOINTMENT_FILE', res.data.appointmentDetails.appointmentId, this.uploadImgArr);
                        } else {

                            for (let i = 0; i < this.uploadImgArr.length; i++) {
                                this.uploadImgCount++;
                                this.uploadImgArr[i].rank = this.uploadImgCount;
                                this.uploadImgArr[i].id = res.data.appointmentDetails.appointmentId;
                            }

                            this.uploadImage(this.uploadImgArr[0]);
                        }
                    }
                    setTimeout(() => {
                        this.OnCancelAndEditAppointmentRedirection();
                    }, 3000);
                },
                error => {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(error.message);
                    console.log("error....", error);
                });
        }
    }

    /*
     * Function to cancel appointment
     **/
    cancelAppointment() {
        this.popover = this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.commonService.cancelAppointment(this.appointmentData.appointmentId).subscribe(
            res => {
                this.commonService.hideLoading();
                this.commonService.presentToast(res.message);
                setTimeout(() => {
                    this.OnCancelAndEditAppointmentRedirection();
                }, 3000);
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.presentToast(error.message);
                console.log("error....", error);
            });
    }

    updateDetailsOnView() {
        this.isConfirm = false;
        this.isUpdateOnView = true;
        this.backBtnUrl = "#/appointment-list/patientUpcoming";
        this.commonService.setInStorage('isUpdateOnView', this.isUpdateOnView);
    }

    editUAViewDetails() {
        this._ngZone.run(() => {
            this.isConfirm = true;
            this.isUpdateOnView = false;
            this.commonService.setInStorage('isUpdateOnView', this.isUpdateOnView);
        });

    }

    /*
     * On cancel
     **/
    OnCancelAndEditAppointmentRedirection() {
        if (this.fromPage == 'patientHome') {
            this.navCtrl.setRoot("AppointmentList", {
                fromPage: 'patientUpcoming'
            });
        } else if (this.fromPage == 'HAAssociatedDoctorList') {
            this.navCtrl.setRoot("HospitalAdminManageAppointmentPage");
        } else if (this.fromPage == 'doctorRoleUpcomingAppointment') {
            this.navCtrl.setRoot("DoctorRoleUpcomingAppointmentsPage");
        } else if (this.fromPage == 'doctorRoleAppointmentHistory') {
            this.navCtrl.setRoot("DoctorRoleAppointmentsHistoryPage");
        } else if ((this.fromPage == 'patientUpcomingAppointment' && this.redirectParam == 'updatedAppointmentSlot') ||
            (this.fromPage == 'patientUpcomingAppointment') ||
            (this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain' && this.redirectParam == 'updatedAppointmentSlot') ||
            (this.fromPage == 'patientAppointmentHistory' && this.redirectParam == 'updatedAppointmentSlot')) {
            this.navCtrl.setRoot("AppointmentList", {
                fromPage: 'patientUpcoming'
            });
        } else if (this.fromPage == 'HAUpdateVisitDetails') {
            this.navCtrl.setRoot("HospitalAdminManageAppointmentPage");
        } else if (this.fromPage == 'DoctorRoleUpdateVisitDetails') {
            this.navCtrl.setRoot("DoctorRoleUpcomingAppointmentsPage");
        } else if (this.fromPage == 'patientUpcomingAppointmentViewDetails') {
            this.navCtrl.setRoot("AppointmentList", {
                fromPage: 'patientUpcoming'
            });
        } else if (this.fromPage == 'patientUpcomingAppointmentBookAgain') {
            this.navCtrl.setRoot("AppointmentList", {
                fromPage: 'patientUpcoming'
            });
        } else if (this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain') {
            this.navCtrl.setRoot("AppointmentList", {
                fromPage: 'patientUpcoming'
            });
        }
    }

    /**
     * Function to upload image 
     **/
    uploadImage = (uploadImgData) => {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        if (this.uploadCount < this.uploadImgArr.length) {
            this.commonService.uploadImage(uploadImgData).subscribe(
                res => {
                    if (res.status == "success") {
                        this.uploadCount++;
                        this.uploadImage(this.uploadImgArr[this.uploadCount]);
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    this.commonService.showAlert("Error", error.message);
                });
        } else {
            this.commonService.hideLoading();
            if (this.isCreate) {
                this.isConfirm = true;
                this.uploadImgArr = [];
                this.imgCounter = 1;
                this.imgUrl = [];
                this.docArr = [];
                this.commonService.setInStorage('isConfirmed', this.isConfirm);
                this.commonService.getAppointmentDetails(this.appointmentData.appointmentId).subscribe(
                    res => {
                        if (res.status == 'success') {
                            this.appointmentDataWithImageCount = res.data.appointmentDetails;
                            this.downloadFilesCount = this.appointmentDataWithImageCount.totalFilesCount;
                            this.downloadFileAppointmentId = this.appointmentDataWithImageCount.appointmentId;
                            this.downloadImage();
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        this.commonService.presentToast(error.message);
                        console.log("error....", error);
                    });
            }
        }
    }

    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean, uploadedFileType?: any) {
        if ($event.target.files) {
            this.rank++;
            this.imgCounter++;
            let imageData;
            let mediaType;
            if (uploadedFileType == 'img') {
                mediaType = 'IMAGE';
            } else if (uploadedFileType == 'doc') {
                mediaType = 'DOCS';
            }
            imageData = {
                "userType": "PATIENT_APPOINTMENT_FILE",
                "id": '',
                "mediaType": mediaType,
                "file": "",
                "fileName": "",
                "isDefault": isDefault,
                "rank": this.rank
            }
            var image: any = new Image();
            var file: File = $event.target.files[0];
            var myReader: FileReader = new FileReader();
            var that = this;
            myReader.onloadend = (loadEvent: any) => {
                image.src = loadEvent.target.result;
                this.commonService.convertImgToBase64(image.src, (callback) => {
                    imageData.file = callback;
                    imageData.fileName = file.name;
                    if (uploadedFileType == 'img' && imageData.file) {
                        this.imgUrl.push(imageData);
                    } else if (uploadedFileType == 'doc' && imageData.file) {
                        this.docArr.push(imageData);
                    }

                    if (this.imagesCount < 8) {
                        this.imagesCount++;
                    }
                })
            };
            myReader.readAsDataURL(file);
            this.uploadImgArr.push(imageData);
        }
    }

    /**
     *  
     */
    deleteImage(userType: any, appointmentId: any, imgArr: any) {
        this.commonService.deleteImage(userType, appointmentId).subscribe(
            res => {
                if (res.status == 'success') {
                    for (let i = 0; i < this.uploadImgArr.length; i++) {
                        this.uploadImgCount++;
                        this.uploadImgArr[i].rank = this.uploadImgCount;
                        this.uploadImgArr[i].id = appointmentId;
                        this.uploadImgArr[i].userType = userType;
                    }

                    this.uploadImage(this.uploadImgArr[0]);
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }

    /**
     * DownLoad hospital images
     * @param imageData 
     */
    downloadImage() {
        let imageData = {
            'userType': "PATIENT_APPOINTMENT_FILE",
            'id': this.downloadFileAppointmentId,
            'rank': this.imgCounter
        }
        if (this.imgCounter <= this.downloadFilesCount) {
            this.commonService.downloadImage(imageData).subscribe(
                res => {
                    if (res.status == "success") {
                        if (res.data.mediaType == 'IMAGE') {
                            this.imgUrl.push(res.data);
                        } else {
                            this.docArr.push(res.data);
                        }
                        this.uploadImgArr.push(res.data);
                        this.imgCounter++;
                        this.downloadImage();
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    // this.commonService.showAlert( "Error", error.message );
                });
        }
        this.uploadCount = 0;
    }

    /**
     * Function to cancel image upload
     */
    cancelImage(img?: any) {
        let confirmPopUpMsg = this.commonService.getTranslate('EDIT_HOSPITAL_IMG_CANCEL_MSG', {});
        let warningTitle = this.commonService.getTranslate('WARNING', {});
        let alert = this.commonService.confirmAlert(warningTitle, confirmPopUpMsg + '?');
        alert.present();
        alert.onDidDismiss((data) => {
            if (data == true) {
                if (img.mediaType == 'IMAGE') {
                    for (let i = 0; i < this.imgUrl.length; i++) {
                        if (img.rank == this.imgUrl[i].rank) {
                            let index = this.imgUrl.indexOf(this.imgUrl[i]);
                            this.imgUrl.splice(index, 1);
                        }
                    }
                } else {
                    for (let i = 0; i < this.docArr.length; i++) {
                        if (img.rank == this.docArr[i].rank) {
                            let index = this.docArr.indexOf(this.docArr[i]);
                            this.docArr.splice(index, 1);
                        }
                    }
                }

                for (let i = 0; i < this.uploadImgArr.length; i++) {
                    if (img.rank == this.uploadImgArr[i].rank) {
                        let index = this.uploadImgArr.indexOf(this.uploadImgArr[i]);
                        this.uploadImgArr.splice(index, 1);
                    }
                }
            }
        });
    }

    /**
     * Array function for count
     */
    counter = (i: number) => {
        return new Array(i);
    }

    /***
     * Function to open camera/photo gallery
     */
    protected onCamera = (isDefault?: boolean, isUpload?: boolean, uploadedFileType?: any) => {
        this.mobileIsDefault = isDefault;
        this.mobileUploadedFileType = uploadedFileType;
        this.mobileIsUploadFile = isUpload;
        try {
            // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
            this.cameraService.loadImage(this.successCallback, this.errorCallback);
        } catch (e) {

        }
    }

    /**
     * Function to handle camera success callback
     * @param success
     */
    private successCallback = (base64Image: any) => {
        this.rank++;
        this.imgCounter++;
        let imageData;
        let mediaType;
        if (this.mobileUploadedFileType == 'img') {
            mediaType = 'IMAGE';
        } else if (this.mobileUploadedFileType == 'doc') {
            mediaType = 'DOCS';
        }
        imageData = {
            "userType": "PATIENT_APPOINTMENT_FILE",
            "id": '',
            "mediaType": mediaType,
            "file": "",
            "fileName": "",
            "isDefault": this.mobileIsDefault,
            "rank": this.rank
        }

        //imageData.file = base64Image;

        this.commonService.convertImgToBase64(base64Image, (callback) => {
            imageData.file = callback;
            if (this.mobileUploadedFileType == 'img' && imageData.file) {
                this.imgUrl.push(imageData);
            } else if (this.mobileUploadedFileType == 'doc' && imageData.file) {
                this.docArr.push(imageData);
            }

            if (this.imagesCount < 8) {
                this.imagesCount++;
            }

            this.uploadImgArr.push(imageData);

        })
    }

    /**
     * Function to handle camera error callback
     * @param error
     */
    private errorCallback = (error: any) => {
        console.log('Unable to load profile picture.', error);
    }

    /*
     * Function to show privacy policy checkbox
     **/
    OnPrivacyCheckboxClick() {
        this._ngZone.run(() => {
            this.agree = !this.agree;
            if (this.agree) {
                this.agreeTermsTickImgUrl = "assets/imgs/01_multispacility_checked.svg";
            } else {
                this.agreeTermsTickImgUrl = "assets/imgs/01_multispacility_uncheck.png";
            }
        });
    }

    /*
     * Function to show privacy policy
     **/
    openPrivacyPolicy(title: string) {
        this.popover = this.commonService.showPrivacyPolicyAlert(title, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
        this.commonService.closePopupOnBackBtn(this.popover);
    }

    getPatientDependent = () => {
        console.log("ADMIN:", this.commonService.getActiveUserRole());
        console.log("STORAGE_KEY", this.dependentStorageKey);

        this.commonService.getFromStorage(this.dependentStorageKey).then((value) => {
            if (value) {
                this.patientDependents = value;
                console.log("DEP:", this.patientDependents);
                let index = this.patientDependents.findIndex(patient => patient.isSelected == true);
                this.patient = this.patientDependents[index];
                if (this.patient) {
                    if (this.patient.lastName) {
                        this.bookingUserName = this.patient.firstName + " " + this.patient.lastName;
                    } else {
                        this.bookingUserName = this.patient.firstName;
                    }

                    if (this.patient.contactNo) {
                        this.bookingUserContactNo = this.patient.contactNo;
                    }
                }
            }
        });



    }



    /*
     * set data on city change
     */

    onDependentSelect = (patient: any) => {

        this.patientDependents.forEach(element => {
            element.isSelected = false;
            patient.isSelected = true;
        });

        if (this.patient.lastName) {
            this.bookingUserName = this.patient.firstName + " " + this.patient.lastName;
        } else {
            this.bookingUserName = this.patient.firstName;
        }

        if (this.patient.contactNo) {
            this.bookingUserContactNo = this.patient.contactNo;
        }

        this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);

    }

}