import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarModule } from 'primeng/calendar';
import { SharedModule } from "../../components/shared.module";
import { BookingStep1Page } from './booking-step1';

@NgModule({
  declarations: [
    BookingStep1Page,
  ],
  imports: [
    IonicPageModule.forChild(BookingStep1Page),
       TranslateModule.forChild(),
       CalendarModule,
       SharedModule
    ],
    exports: [
       BookingStep1Page
    ]
})
export class BookingStep1PageModule {}
