import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, MenuController, Content, Events, Platform } from 'ionic-angular';
import * as moment from 'moment';
import { Location } from '@angular/common';

//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, HospitalDTO, AddressDto, DoctorDataDto, DoctorListDTO } from '../../../interfaces/user-data-dto';

import { DoctorRoleServiceProvider } from "../../../providers/doctor-role-service/doctor-role-service";
import { CancelAppointmentConfirmationComponent } from "../../components/cancel-appointment-confirmation/cancel-appointment-confirmation";

@IonicPage( {
    name: 'BookingStep1Page',
    segment: 'booking-step1/:doctorId/:from/:editParam'
} )
@Component( {
    selector: 'page-booking-step1',
    templateUrl: 'booking-step1.html',
} )
export class BookingStep1Page {
    @ViewChild( Content ) content: Content;
    userData: UserDataDTO = {};
    hospitalData: any = [];
    hospitalDetails: HospitalDTO = {};
    date: any;
    doctorId: any;
    doctorProfileId: any;
    minDate: any;
    pageNo: number = 0;
    address: AddressDto = {};
    hospitalAvailableStartTime: any;
    hospitalAvailableEndTime: any;
    timeSlotStartTime: any;
    timeSlotEndTime: any;
    isSlotSelected: boolean = false;
    hospitalWithTimeSlotArr: any;
    hospitalWithTokenTimeSlotArr: any;
    isData: boolean = false;
    doctorDetails: DoctorDataDto = {};
    isTimeslotSelected: boolean = false;
    maxDate: any;
    loadFinished: boolean = false;
    milliseconds: any;
    selectedDate: any;
    morningAppointmentSlot = [];
    morningTokenSlot = [];
    afternoonAppointmentSlot = [];
    afternoonTokenSlot = [];
    eveningAppointmentSlot = [];
    eveningTokenSlot = [];
    nightAppointmentSlot = [];
    nightTokenSlot = [];
    isTokenSelected: boolean = false;
    fromPage: any;
    appointmentDetails: any = {};
    redirectionUrl: any;
    editParam:any;
    bookAgainHospitalId: any;
    hospitalSlotDetail:any = {};
    hospitalId:any;
    backBtnUrl: any; 
    homeBtnUrl:any;
    popover: any;
    doctorListDto:DoctorListDTO={};

    
    constructor( private commonService: CommonService, public modalCtrl: ModalController, public platform: Platform, public location: Location, private evts: Events, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private doctorRoleServiceProvider: DoctorRoleServiceProvider, private constants: Constants ) {
        this.menu.enable( true );
        this.platform.registerBackButtonAction(() => {
            this.location.back();
            this.commonService.closePopupOnBackBtn(this.popover);
        });
        
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        
        this.fromPage = this.navParams.get('from');
        this.editParam = this.navParams.get('editParam');
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        this.minDate = new Date();
        this.maxDate = new Date();
        this.appointmentDetails = {
            'appointmentDate': ''
        }
        this.commonService.removeFromStorage('isConfirmed');
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR, true );
                this.commonService.setPaddingTopScroll( this.content );
                
                /* On patientRole upcoming appointment and appointment history book again btn click, 
                 * storing hospitalId in local storage
                 * get from local storage and pass as param for slot list API
                 */
                if( this.fromPage == 'patientUpcomingAppointmentBookAgain' || 
                        this.editParam == 'patientUpcomingAppointmentBookAgain' || 
                        this.fromPage == 'patientAppointmentHistory' || 
                        this.editParam == 'patientAppointmentHistory' ||
                        this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain' ||
                        this.editParam == 'patientUpcomingAppointmentViewDetailsBookAgain' ||
                        this.fromPage == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain' ||
                        this.editParam == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain'){
                    this.commonService.getFromStorage( "hospitalId" ).then(( value ) => {
                        if ( value ) {
                            this.bookAgainHospitalId = value;
                            console.log("bookAgainHospitalId", this.bookAgainHospitalId);
                        }
                    } );
                }
                
                
                if( this.navParams.get( 'from' ) == 'patientHome' || 
                    this.navParams.get( 'from' ) == 'patientHospitalProfile' ||
                    this.navParams.get( 'from' ) == 'patientDoctorProfile' || 
                    this.editParam == 'patientHome' ){
                    
                    this.commonService.getFromStorage( "hospitalId" ).then(( value ) => {
                        if ( value ) {
                            this.hospitalId = value;
                            console.log("this.hospitalId", this.hospitalId);
                        }
                        if(this.navParams.get( 'editParam' ) != 0){
                            this.hospitalId = this.editParam;
                        }
                    });
                    
                    this.commonService.fireSelectedEvent( '#/patient-role-home/0' );
                }else if( this.fromPage == 'HAAssociatedDoctorList' || this.editParam == 'HAAssociatedDoctorList' ){
                    this.commonService.fireSelectedEvent( '#/hospital-admin-associated-doctor-list' );
                }else if( this.fromPage == 'HAManageAppointment' || this.editParam == 'HAManageAppointment'){
                    this.commonService.fireSelectedEvent( '#/hospital-admin-manage-appointment' );
                }else if( (this.fromPage == 'doctorRoleUpcomingAppointment' || this.fromPage == 'doctorRoleUpcomingAppointmentVDBook') || (this.editParam == 'doctorRoleUpcomingAppointment' || this.editParam == 'doctorRoleUpcomingAppointmentVDBook')){
                    this.commonService.fireSelectedEvent( '#/doctor-role-upcoming-appointments' );
                }else if( this.fromPage == 'doctorRoleAppointmentHistory' || this.editParam == 'doctorRoleAppointmentHistory'){
                    this.commonService.fireSelectedEvent( '#/doctor-role-appointments-history' );
                }else if( this.fromPage == 'patientAppointmentHistory' || this.editParam == 'patientAppointmentHistory'){
                   this.commonService.fireSelectedEvent('#/appointment-list/patientHistory');
                }else if( (this.fromPage == 'patientUpcomingAppointmentBookAgain' || this.fromPage == 'patientUpcomingAppointment' || this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain') || 
                        (this.editParam == 'patientUpcomingAppointmentBookAgain' || this.editParam == 'patientUpcomingAppointment') || this.editParam == 'patientUpcomingAppointmentViewDetailsBookAgain'){
                   this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                }else if( this.fromPage == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain' || this.editParam == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain' ){
                    this.commonService.fireSelectedEvent('#/doctor-role-appointments-history');
                }else if( this.fromPage == 'updateAppointment'){
                    this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                }else if(this.fromPage == 'HSRoleAssociatedDoctorList' || this.editParam=='HSRoleAssociatedDoctorList'){
                    this.commonService.fireSelectedEvent('#/hospital-staff-associated-doctor-list/HSRoleDashboard');
                }else if(this.fromPage == 'HSRoleOnboardPatient' || this.editParam == 'HSRoleOnboardPatient'){
                    this.commonService.fireSelectedEvent('#/onboard-patient/HSRoleDashboard/HSRoleAssociatedDoctorList/0/0');
                }
                
                if ( this.navParams.get( 'doctorId' ) && this.navParams.get( 'doctorId' ) != 0 ) {
                    this.doctorId = this.navParams.get( 'doctorId' ); 
                    this.commonService.setInStorage("doctorId", this.doctorId);
                    this.getDoctorProfile( this.doctorId );
                }else{
                    this.commonService.getFromStorage( "doctorId" ).then(( value ) => {
                        if ( value ) {
                            this.doctorId = value;
                            this.getDoctorProfile( this.doctorId );
                        } else {
                            this.doctorId = "";
                        }
                    } );
                }
               
            }
        } );
    }

    /**
      * Function to get doctor profile details
      */
    getDoctorProfile( doctorId ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.doctorRoleServiceProvider.getDoctorProfile( doctorId ).subscribe(
            res => {
                this.commonService.hideLoading();
                if ( res.status == 'success' ) {
                    this.doctorDetails = res.data;
                    this.doctorListDto = res.data;
                    this.formatAvailability();
                    this.doctorProfileId = this.doctorDetails.doctorProfileId;
                    console.log("get doctor details..................",this.doctorListDto);
                    this.maxDate = new Date( moment( this.maxDate, "DD-MM-YYYY" ).add( this.doctorDetails.advanceBookingDays, 'days' ).toString() );
                    this.selectedDate = new Date (moment(this.date).format('D MMM, YYYY')); // some mock date
                    if( this.navParams.get( 'from' ) != 'updateAppointment'){
                        this.onDateSelect(this.selectedDate);
                    }
                    //redirection function call
                    this.redirectionDetails();
                }
            }, error => {
                this.commonService.hideLoading();
                this.commonService.presentToast( error.message );
                console.log( "error....", error );
            });
    }
    
    /*
     * Redirections
     **/
    redirectionDetails(){
        //In normal booking flow
        if( this.fromPage == 'HAAssociatedDoctorList'|| this.editParam == 'HAAssociatedDoctorList'){
            this.redirectionUrl = "#/confirm-booking-step2/HAAssociatedDoctorList/bookFlow";
            this.backBtnUrl = "#/hospital-admin-associated-doctor-list";
            this.homeBtnUrl= "#/hospital-admin-dashboard/0";
        }else if( this.fromPage == 'HAManageAppointment' || this.editParam == 'HAManageAppointment'){
            this.redirectionUrl = "#/confirm-booking-step2/HAManageAppointment/bookFlow";
            this.backBtnUrl = "#/hospital-admin-manage-appointment";
            this.homeBtnUrl= "#/hospital-admin-dashboard/0";
        }else if( this.fromPage == 'HAManageAppointmentVDBook' || this.editParam == 'HAManageAppointmentVDBook'){
            this.redirectionUrl = "#/confirm-booking-step2/HAManageAppointmentVDBook/bookFlow";
            this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
            this.homeBtnUrl= "#/hospital-admin-dashboard/0";
        }else if( this.fromPage == 'doctorRoleUpcomingAppointment' || this.editParam == 'doctorRoleUpcomingAppointment'){
            this.redirectionUrl = "#/confirm-booking-step2/doctorRoleUpcomingAppointment/bookFlow";
            this.backBtnUrl = "#/doctor-role-upcoming-appointments";
            this.homeBtnUrl= "#/doctor-dashboard/0";
        }else if( this.fromPage == 'doctorRoleUpcomingAppointmentVDBook' || this.editParam == 'doctorRoleUpcomingAppointmentVDBook'){
            this.redirectionUrl = "#/confirm-booking-step2/doctorRoleUpcomingAppointmentVDBook/bookFlow";
            this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
            this.homeBtnUrl= "#/doctor-dashboard/0";
        }else if( this.fromPage == 'doctorRoleAppointmentHistory' || this.editParam == 'doctorRoleAppointmentHistory'){
            this.redirectionUrl = "#/confirm-booking-step2/doctorRoleAppointmentHistory/bookFlow";
            this.backBtnUrl = "#/doctor-role-appointments-history";
            this.homeBtnUrl= "#/doctor-dashboard/0";
        }else if( this.fromPage == 'patientUpcomingAppointment' || this.editParam == 'patientUpcomingAppointment'){
            this.redirectionUrl = "#/confirm-booking-step2/patientUpcomingAppointment/bookFlow";
            this.backBtnUrl = "#/appointment-list/patientUpcoming";
            this.homeBtnUrl= "#/patient-role-home/0";
        }else if( this.fromPage == 'patientUpcomingAppointmentBookAgain' || this.editParam == 'patientUpcomingAppointmentBookAgain' ){
            this.redirectionUrl = "#/confirm-booking-step2/patientUpcomingAppointmentBookAgain/bookFlow";
            this.backBtnUrl = "#/appointment-list/patientUpcoming";
            this.homeBtnUrl= "#/patient-role-home/0";
        }else if( this.fromPage == 'patientAppointmentHistory' || this.editParam == 'patientAppointmentHistory'){
            this.redirectionUrl = "#/confirm-booking-step2/patientAppointmentHistory/bookFlow";
            this.backBtnUrl = "#/appointment-list/patientHistory";
            this.homeBtnUrl= "#/patient-role-home/0";
        }else if( this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain' || this.editParam == 'patientUpcomingAppointmentViewDetailsBookAgain' ){
            this.redirectionUrl = "#/confirm-booking-step2/patientUpcomingAppointmentViewDetailsBookAgain/bookFlow";
            this.backBtnUrl = "#/appointment-list/patientUpcoming";
            this.homeBtnUrl= "#/patient-role-home/0";
        }else if( this.fromPage == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain' || this.editParam == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain' ){
            this.redirectionUrl = "#/confirm-booking-step2/doctorRoleAppointmentHistoryVisitDetailsBookAgain/bookFlow";
            this.backBtnUrl = "#/hospital-admin-appointment-visit-details/0/0/0/0";
            this.homeBtnUrl= "#/doctor-dashboard/0";
        }else if( this.fromPage == 'patientDoctorProfile' || this.editParam == 'patientDoctorProfile'){
            this.redirectionUrl = "#/confirm-booking-step2/patientDoctorProfile/bookFlow"
            this.backBtnUrl = "#/patient-role-doctor-profile/"+this.doctorDetails.doctorId+"/PRHomeDoctorProfile";
            this.homeBtnUrl= "#/patient-role-home/0";
        }else if( this.fromPage == 'patientHospitalProfile' || this.editParam == 'patientHospitalProfile'){
            this.redirectionUrl = "#/confirm-booking-step2/patientHospitalProfile/bookFlow"
            this.backBtnUrl = "#/patient-role-hospital-profile/0/0";
            this.homeBtnUrl= "#/patient-role-home/0";
            console.log("#/patient-role-hospital-profile");
        }else if( this.fromPage == 'patientHome'|| this.editParam == 'patientHome'){
            this.redirectionUrl = "#/confirm-booking-step2/patientHome/bookFlow";
            this.backBtnUrl = "#/patient-role-home/0";
            this.homeBtnUrl= "#/patient-role-home/0";
        }else if(this.fromPage == 'HSRoleAssociatedDoctorList' || this.editParam == 'HSRoleAssociatedDoctorList'){
            this.redirectionUrl = "#/confirm-booking-step2/HSRoleAssociatedDoctorList/bookFlow";
            this.backBtnUrl = '#/hospital-staff-associated-doctor-list/HSRoleOnboardPatient';
            this.homeBtnUrl= '#/staff-role-dashboard/0';
        }else if(this.fromPage == 'HSRoleOnboardPatient' || this.editParam == 'HSRoleOnboardPatient'){
            this.redirectionUrl = "#/confirm-booking-step2/HSRoleOnboardPatient/bookFlow";
            this.backBtnUrl = '#/onboard-patient/HSRoleAssociatedDoctorList/BookingStep1Page/0/' + this.doctorId;
            this.homeBtnUrl= '#/staff-role-dashboard/0';
        }
        
        //On update appointment
            if( this.fromPage == 'updateAppointment' && this.editParam == 'patientHome'){
                this.redirectionUrl = "#/confirm-booking-step2/patientHome/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/patientHome/updatedAppointmentSlot";
                this.homeBtnUrl= "#/patient-role-home/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'HAAssociatedDoctorList'){
                this.redirectionUrl = "#/confirm-booking-step2/HAAssociatedDoctorList/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/HAAssociatedDoctorList/updatedAppointmentSlot";
                this.homeBtnUrl= "#/hospital-admin-dashboard/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'HAManageAppointment'){
                this.redirectionUrl = "#/confirm-booking-step2/HAManageAppointment/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/HAManageAppointment/updatedAppointmentSlot";
                this.homeBtnUrl= "#/hospital-admin-dashboard/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'HAManageAppointmentVDBook'){
                this.redirectionUrl = "#/confirm-booking-step2/HAManageAppointmentVDBook/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/HAManageAppointmentVDBook/updatedAppointmentSlot";
                this.homeBtnUrl= "#/hospital-admin-dashboard/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'doctorRoleUpcomingAppointment'){
                this.redirectionUrl = "#/confirm-booking-step2/doctorRoleUpcomingAppointment/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/doctorRoleUpcomingAppointment/updatedAppointmentSlot";
                this.homeBtnUrl= "#/doctor-dashboard/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'doctorRoleUpcomingAppointmentVDBook'){
                this.redirectionUrl = "#/confirm-booking-step2/doctorRoleUpcomingAppointmentVDBook/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/doctorRoleUpcomingAppointmentVDBook/updatedAppointmentSlot";
                this.homeBtnUrl= "#/doctor-dashboard/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'doctorRoleAppointmentHistory'){
                this.redirectionUrl = "#/confirm-booking-step2/doctorRoleAppointmentHistory/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/doctorRoleAppointmentHistory/updatedAppointmentSlot";
                this.homeBtnUrl= "#/doctor-dashboard/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'patientUpcomingAppointment' ){
                this.redirectionUrl = "#/confirm-booking-step2/patientUpcomingAppointment/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/patientUpcomingAppointment/updatedAppointmentSlot";
                this.homeBtnUrl= "#/patient-role-home/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'patientUpcomingAppointmentBookAgain'){
                this.redirectionUrl = "#/confirm-booking-step2/patientUpcomingAppointmentBookAgain/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/patientUpcomingAppointmentBookAgain/updatedAppointmentSlot";
                this.homeBtnUrl= "#/patient-role-home/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'patientUpcomingAppointmentViewDetailsBookAgain'){
                this.redirectionUrl = "#/confirm-booking-step2/patientUpcomingAppointmentViewDetailsBookAgain/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/patientUpcomingAppointmentViewDetailsBookAgain/updatedAppointmentSlot";
                this.homeBtnUrl= "#/patient-role-home/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'patientAppointmentHistory'){
                this.redirectionUrl = "#/confirm-booking-step2/patientAppointmentHistory/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/patientAppointmentHistory/updatedAppointmentSlot";
                this.homeBtnUrl= "#/patient-role-home/0";
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'HAUpdateVisitDetails' ){
                this.redirectionUrl = "#/confirm-booking-step2/HAUpdateVisitDetails/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/HAUpdateVisitDetails/updatedAppointmentSlot";
                this.homeBtnUrl= "#/hospital-admin-dashboard/0";
                this.commonService.fireSelectedEvent('#/hospital-admin-manage-appointment');
                this.updateAppointment();
            }else if( this.fromPage == 'updateAppointment' && this.editParam == 'DoctorRoleUpdateVisitDetails' ){
                this.redirectionUrl = "#/confirm-booking-step2/DoctorRoleUpdateVisitDetails/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/DoctorRoleUpdateVisitDetails/updatedAppointmentSlot";
                this.homeBtnUrl= "#/doctor-dashboard/0";
                this.commonService.fireSelectedEvent('#/doctor-role-upcoming-appointments');
                this.updateAppointment();
            }else if(  this.fromPage == 'updateAppointment' && this.editParam == 'patientUpcomingAppointmentViewDetails'){
                this.redirectionUrl = "#/confirm-booking-step2/patientUpcomingAppointmentViewDetails/updatedAppointmentSlot";
                this.backBtnUrl = "#/confirm-booking-step2/patientUpcomingAppointmentViewDetails/updatedAppointmentSlot";
                this.homeBtnUrl= "#/patient-role-home/0";
                this.commonService.fireSelectedEvent('#/appointment-list/patientUpcoming');
                this.updateAppointment();
            }
    }
    
    /*
     * Update appointment
     **/
    updateAppointment(){
        this.commonService.getFromStorage( "appointmentDetails" ).then(( value ) => {
            if ( value ) {
                this.appointmentDetails = value;
                this.selectedDate = new Date(this.appointmentDetails.appointmentDate);
                this.date = new Date(this.appointmentDetails.appointmentDate);
                this.commonService.setInStorage("PatientUpcomingAppointmentid", this.appointmentDetails.appointmentId);
                if( this.editParam == 'patientAppointmentHistory' || 
                    this.editParam == 'patientUpcomingAppointmentBookAgain' ||
                    this.editParam == 'patientUpcomingAppointmentViewDetailsBookAgain' ||
                    this.editParam == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain'){
                    if(this.bookAgainHospitalId){
                        console.log("bookAgainHospitalId", this.bookAgainHospitalId);
                        this.getHospitalListWithTimeslots( this.doctorProfileId, this.appointmentDetails.appointmentId, this.selectedDate, this.bookAgainHospitalId, true );
                    }
                }else{
                    console.log("noooooooooooooo bookAgainHospitalId");
                    this.getHospitalListWithTimeslots( this.doctorProfileId, this.appointmentDetails.appointmentId, this.appointmentDetails.appointmentDate, '', true );
                }
            }
            
        } );
    }
    /*
     * Function to get Hospital List With Timeslots
     **/
    getHospitalListWithTimeslots( doctorProfileId: any, appointmentId: any, date: any, hospitalId: any, firstPageCall?: boolean ) {
        let payload = {
            "doctorProfileId": doctorProfileId,
            "date": date,
            "appointmentId": appointmentId,
            "hospitalId": hospitalId,
        }
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.commonService.getHospitalListWithTimeslots( payload, this.pageNo ).subscribe(
            res => {
                this.commonService.hideLoading();
                if ( res.status = "success" ) {
                    if ( res.data.list ) {
                        let tempPageNo = this.pageNo;
                        if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                            this.hospitalData = res.data.list;
                            if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                                this.loadFinished = true;
                            }
                        } else if ( res.data.list && res.data.list.length > 0 ) {
                            for ( let j = 0; j < res.data.list.length; j++ ) {
                                this.hospitalData.push( res.data.list[j] );
                            }
                            if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                                this.loadFinished = true;
                            }
                        } else {
                            this.loadFinished = true;
                        }
                        for ( let i = 0; i < this.hospitalData.length; i++ ) {
                            if(this.hospitalData[i].doctorsAvailableTimeSlots && this.hospitalData[i].doctorsAvailableTimeSlots.length > 0){
                                this.hospitalData[i].isData = false;
                                //To show available time slots
                               this.getAvailableTimeSlot( this.hospitalData[i].doctorsAvailableTimeSlots, this.hospitalData[i] );
                                if(this.hospitalSlotDetail){
                                    this.hospitalData[i].morningSlot = this.hospitalSlotDetail.hospitalMorningSlot,
                                    this.hospitalData[i].afternoonSlot = this.hospitalSlotDetail.hospitalAfternoonSlot,
                                    this.hospitalData[i].eveningSlot = this.hospitalSlotDetail.hospitalEveningSlot,
                                    this.hospitalData[i].nightSlot = this.hospitalSlotDetail.hospitalNightSlot
                                }
                                
                                if ( this.hospitalData[i].morningSlot ) {
                                    if( this.hospitalData[i].morningSlot.appointmentSlots && this.hospitalData[i].morningSlot.appointmentSlots.length > 0 ){
                                            let last = this.hospitalData[i].morningSlot.appointmentSlots[this.hospitalData[i].morningSlot.appointmentSlots.length - 1].endTime;
                                            this.hospitalData[i].morningSlot.appointmentSlots.appointmentSlotFromTime = this.hospitalData[i].morningSlot.appointmentSlots[0].startTime;
                                            this.hospitalData[i].morningSlot.appointmentSlots.appointmentSlotToTime = last;
                                            this.hospitalData[i].morningSlot.appointmentSlots.visitType = 'APPOINTMENT';
                                      }
                                    
                                    if( this.hospitalData[i].morningSlot.tokenSlots && this.hospitalData[i].morningSlot.tokenSlots.length > 0 ){
                                        let last = this.hospitalData[i].morningSlot.tokenSlots[this.hospitalData[i].morningSlot.tokenSlots.length - 1].endTime;
                                        this.hospitalData[i].morningSlot.tokenSlots.tokenSlotFromTime = this.hospitalData[i].morningSlot.tokenSlots[0].startTime;
                                        this.hospitalData[i].morningSlot.tokenSlots.tokenSlotToTime = last;
                                        this.hospitalData[i].morningSlot.tokenSlots.visitType = 'TOKEN';
                                        console.log("this.hospitalData[i].morningSlot.tokenSlots", this.hospitalData[i].morningSlot.tokenSlots);
                                    }
                                    
                                }
                                
                                if ( this.hospitalData[i].afternoonSlot ) {
                                    if( this.hospitalData[i].afternoonSlot.appointmentSlots && this.hospitalData[i].afternoonSlot.appointmentSlots.length > 0 ){
                                        let last = this.hospitalData[i].afternoonSlot.appointmentSlots[this.hospitalData[i].afternoonSlot.appointmentSlots.length - 1].endTime;
                                        this.hospitalData[i].afternoonSlot.appointmentSlots.appointmentSlotFromTime = this.hospitalData[i].afternoonSlot.appointmentSlots[0].startTime;
                                        this.hospitalData[i].afternoonSlot.appointmentSlots.appointmentSlotToTime = last;
                                        this.hospitalData[i].afternoonSlot.appointmentSlots.visitType = 'APPOINTMENT';
                                    }
                                    
                                    if( this.hospitalData[i].afternoonSlot.tokenSlots && this.hospitalData[i].afternoonSlot.tokenSlots.length > 0 ){
                                        let last = this.hospitalData[i].afternoonSlot.tokenSlots[this.hospitalData[i].afternoonSlot.tokenSlots.length - 1].endTime;
                                        this.hospitalData[i].afternoonSlot.tokenSlots.tokenSlotFromTime = this.hospitalData[i].afternoonSlot.tokenSlots[0].startTime;
                                        this.hospitalData[i].afternoonSlot.tokenSlots.tokenSlotToTime = last;
                                        this.hospitalData[i].afternoonSlot.tokenSlots.visitType = 'TOKEN';
                                    }
                                }
                                
                                if ( this.hospitalData[i].eveningSlot ) {
                                    if( this.hospitalData[i].eveningSlot.appointmentSlots && this.hospitalData[i].eveningSlot.appointmentSlots.length > 0 ){
                                        let last = this.hospitalData[i].eveningSlot.appointmentSlots[this.hospitalData[i].eveningSlot.appointmentSlots.length - 1].endTime;
                                        this.hospitalData[i].eveningSlot.appointmentSlots.appointmentSlotFromTime = this.hospitalData[i].eveningSlot.appointmentSlots[0].startTime;
                                        this.hospitalData[i].eveningSlot.appointmentSlots.appointmentSlotToTime = last;
                                        this.hospitalData[i].eveningSlot.appointmentSlots.visitType = 'APPOINTMENT';
                                    }
                                    
                                    if( this.hospitalData[i].eveningSlot.tokenSlots && this.hospitalData[i].eveningSlot.tokenSlots.length > 0 ){
                                        let last = this.hospitalData[i].eveningSlot.tokenSlots[this.hospitalData[i].eveningSlot.tokenSlots.length - 1].endTime;
                                        this.hospitalData[i].eveningSlot.tokenSlots.tokenSlotFromTime = this.hospitalData[i].eveningSlot.tokenSlots[0].startTime;
                                        this.hospitalData[i].eveningSlot.tokenSlots.tokenSlotToTime = last;
                                        this.hospitalData[i].eveningSlot.tokenSlots.visitType = 'TOKEN';
                                    }
                                }
                                
                                if ( this.hospitalData[i].nightSlot ) {
                                    if( this.hospitalData[i].nightSlot.appointmentSlots && this.hospitalData[i].nightSlot.appointmentSlots.length > 0 ){
                                            let last = this.hospitalData[i].nightSlot.appointmentSlots[this.hospitalData[i].nightSlot.appointmentSlots.length - 1].endTime;
                                            this.hospitalData[i].nightSlot.appointmentSlots.appointmentSlotFromTime = this.hospitalData[i].nightSlot.appointmentSlots[0].startTime;
                                            this.hospitalData[i].nightSlot.appointmentSlots.appointmentSlotToTime = last;
                                            this.hospitalData[i].nightSlot.appointmentSlots.visitType = 'APPOINTMENT';
                                        console.log("appointmentSlotToTime.....", this.hospitalData[i].nightSlot.appointmentSlots)
                                                                        
                                        }
                                    
                                    if( this.hospitalData[i].nightSlot.tokenSlots && this.hospitalData[i].nightSlot.tokenSlots.length > 0 ){
                                        let last = this.hospitalData[i].nightSlot.tokenSlots[this.hospitalData[i].nightSlot.tokenSlots.length - 1].endTime;
                                        this.hospitalData[i].nightSlot.tokenSlots.tokenSlotFromTime = this.hospitalData[i].nightSlot.tokenSlots[0].startTime;
                                        this.hospitalData[i].nightSlot.tokenSlots.tokenSlotToTime = last;
                                        this.hospitalData[i].nightSlot.tokenSlots.visitType = 'TOKEN';
                                    }
                                }
                               
                            }else{
                                this.hospitalData[i].isData = true;
                            }

                        }
                    }
                }
            }, error => {
                this.commonService.presentToast( error.message );
                this.commonService.hideLoading();
            } );
    }
    
     /*
      *  Function to convert available time(only fromTime) slot from millisecond to date format
     **/
    getAvailableTimeSlot( doctorAvailableSlotArr, hospital ) {
        console.log("in getAvailableTimeSlot  ...")
        /* On slot selection, get fromTime and endTime
         * get hours of fromTime and endTime(24 hours format)
         * divide slots as per daySession
         * convert them to 12 hours format (for html binding)
         */      
        let morningAppointmentSlot = [];
        let morningTokenSlot = [];
        let afternoonAppointmentSlot = [];
        let afternoonTokenSlot = [];
        let eveningAppointmentSlot = [];
        let eveningTokenSlot = [];
        let nightAppointmentSlot = [];
        let nightTokenSlot = [];
        let tokenNumber: any;
        
        let doctorAvailableTimeslotAtMorning: any = {
                        'appointmentSlots': '',
                        'tokenSlots':''
                    };
        let doctorAvailableTimeslotAtAfternoon: any = {
                           'appointmentSlots': '',
                           'tokenSlots':''
                      };
        let doctorAvailableTimeslotAtEvening: any = {
                        'appointmentSlots': '',
                        'tokenSlots':''
                    };
        let doctorAvailableTimeslotAtNight: any = {
                        'appointmentSlots': '',
                        'tokenSlots':''
                  };
        for ( let k = 0; k < doctorAvailableSlotArr.length; k++ ) {
            let daySession;
           
            let startTime = new Date( doctorAvailableSlotArr[k].fromTime );
            let endTime = new Date( doctorAvailableSlotArr[k].toTime );
            let hrs = startTime.getHours();
           
            if ( hrs < 12 ) {
                daySession = 'morning';
            } else if ( hrs >= 12 && hrs < 17 ) {
                daySession = 'afternoon';
            } else if ( hrs >= 17 && hrs < 20 ) {
                daySession = 'evening';
            } else{
                daySession = 'night';
            }
            this.timeSlotStartTime = this.commonService.convertTo12hrsFormat( startTime, false, true );
            this.timeSlotEndTime = this.commonService.convertTo12hrsFormat( endTime, false, true );
            
            //When updating slot then this code will execute( show preselect previously selected time slot)
            if( doctorAvailableSlotArr[k].isMySlot == true && doctorAvailableSlotArr[k].visitType == 'APPOINTMENT' ){
                this.isSlotSelected = true;
                this.isTimeslotSelected = true;
                this.setAppointmentData( doctorAvailableSlotArr[k], hospital);
            }else if( doctorAvailableSlotArr[k].isMySlot == true && doctorAvailableSlotArr[k].visitType == 'TOKEN'){
                this.isSlotSelected = true;
                this.isTimeslotSelected = true;
                this.setAppointmentData( doctorAvailableSlotArr[k], hospital);
            }else{
                this.isSlotSelected = false;
                if(doctorAvailableSlotArr[k].visitType == 'TOKEN'){
                    tokenNumber = doctorAvailableSlotArr[k].tokenRemaining + 1;
                    console.log("this.tokenNumber", tokenNumber);
                }else{
                    tokenNumber = 0;
                }
            }
            
            //slot Obj formation
            let slotData = {
                'startTime': this.timeSlotStartTime,
                'endTime': this.timeSlotEndTime,
                'fromTime': doctorAvailableSlotArr[k].fromTime,
                'toTime': doctorAvailableSlotArr[k].toTime,
                'isSelected': this.isSlotSelected,
                'currentDate': this.selectedDate,
                'daySession': daySession,
                'visitType': doctorAvailableSlotArr[k].visitType,
                'tokenNumber': tokenNumber
            }
            
            /*Based on daysSession every slot categorized in morning, afternoon, evening, night session
            * for both Appointment and Token slots
            */
            if ( slotData.daySession == 'morning' ) {
                if ( slotData.visitType == 'APPOINTMENT' ) {
                    morningAppointmentSlot.push( slotData );
                    doctorAvailableTimeslotAtMorning.appointmentSlots = morningAppointmentSlot;
                } else if ( slotData.visitType == 'TOKEN' ) {
                    morningTokenSlot.push( slotData );
                    doctorAvailableTimeslotAtMorning.tokenSlots = morningTokenSlot;
                    console.log(" doctorAvailableTimeslotAtMorning.tokenSlots",  doctorAvailableTimeslotAtMorning.tokenSlots);
                }
    
            } else if ( slotData.daySession == 'afternoon' ) {
                if ( slotData.visitType == 'APPOINTMENT' ) {
                    afternoonAppointmentSlot.push( slotData );
                    doctorAvailableTimeslotAtAfternoon.appointmentSlots = afternoonAppointmentSlot;
                } else if ( slotData.visitType == 'TOKEN' ) {
                    afternoonTokenSlot.push( slotData );
                    doctorAvailableTimeslotAtAfternoon.tokenSlots = afternoonTokenSlot;
                }
            } else if ( slotData.daySession == 'evening' ) {
                if ( slotData.visitType == 'APPOINTMENT' ) {
                    eveningAppointmentSlot.push( slotData );
                    doctorAvailableTimeslotAtEvening.appointmentSlots = eveningAppointmentSlot;
                } else if ( slotData.visitType == 'TOKEN' ) {
                    eveningTokenSlot.push( slotData );
                    doctorAvailableTimeslotAtEvening.tokenSlots = eveningTokenSlot;
                }
            } else if ( slotData.daySession == 'night' ) {
                if ( slotData.visitType == 'APPOINTMENT' ) {
                    nightAppointmentSlot.push( slotData );
                    doctorAvailableTimeslotAtNight.appointmentSlots = nightAppointmentSlot;
                } else if ( slotData.visitType == 'TOKEN' ) {
                    nightTokenSlot.push( slotData );
                    doctorAvailableTimeslotAtNight.tokenSlots = nightTokenSlot;
                }
            }
            this.hospitalSlotDetail ={
                  'hospitalMorningSlot':  doctorAvailableTimeslotAtMorning,
                  'hospitalAfternoonSlot':  doctorAvailableTimeslotAtAfternoon,
                  'hospitalEveningSlot':  doctorAvailableTimeslotAtEvening,
                  'hospitalNightSlot':  doctorAvailableTimeslotAtNight
            }
           
        }//end of for
        return this.hospitalSlotDetail;
    }

    /*
     * Function to get user selected timeSlot of type appointment booking
     * this flow works in normal booking flow (when clicks on slot)
     **/
    getSelectedTimeslot( selectedTimeslot, hospital ) {
        this.deselectionOfSlot();
        console.log("selectedTimeslot appointmentttttttttttttttt",selectedTimeslot);
        selectedTimeslot.isSelected = true;
        this.isTokenSelected = false;
        this.isTimeslotSelected = true;
        this.setAppointmentData( selectedTimeslot, hospital );
    }
    
    deselectionOfSlot(){
        if( this.hospitalData && this.hospitalData.length > 0){
            for ( let i = 0; i < this.hospitalData.length; i++ ) {
                if(this.hospitalData[i].morningSlot){
                    if( this.hospitalData[i].morningSlot.appointmentSlots){
                        for ( let j = 0; j < this.hospitalData[i].morningSlot.appointmentSlots.length; j++ ) {
                            this.hospitalData[i].morningSlot.appointmentSlots[j].isSelected = false;
                        }
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].morningSlot.tokenSlots){
                        for ( let j = 0; j < this.hospitalData[i].morningSlot.tokenSlots.length; j++ ) {
                            this.hospitalData[i].morningSlot.tokenSlots[j].isSelected = false;
                        }
                    } //end of token slot
                } //end of morningSlot
                
                if(this.hospitalData[i].afternoonSlot){
                    if( this.hospitalData[i].afternoonSlot.appointmentSlots){
                        for ( let j = 0; j < this.hospitalData[i].afternoonSlot.appointmentSlots.length; j++ ) {
                            this.hospitalData[i].afternoonSlot.appointmentSlots[j].isSelected = false;
                        }
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].afternoonSlot.tokenSlots){
                        for ( let j = 0; j < this.hospitalData[i].afternoonSlot.tokenSlots.length; j++ ) {
                            this.hospitalData[i].afternoonSlot.tokenSlots[j].isSelected = false;
                        }
                    } //end of token slot
                } //end of afternoonSlot
                
                if(this.hospitalData[i].eveningSlot){
                    if( this.hospitalData[i].eveningSlot.appointmentSlots){
                        for ( let j = 0; j < this.hospitalData[i].eveningSlot.appointmentSlots.length; j++ ) {
                            this.hospitalData[i].eveningSlot.appointmentSlots[j].isSelected = false;
                        }
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].eveningSlot.tokenSlots){
                        for ( let j = 0; j < this.hospitalData[i].eveningSlot.tokenSlots.length; j++ ) {
                            this.hospitalData[i].eveningSlot.tokenSlots[j].isSelected = false;
                        }
                    } //end of token slot
                } //end of eveningSlot 
                
                
                if(this.hospitalData[i].morningSlot){
                    if( this.hospitalData[i].nightSlot.appointmentSlots){
                        for ( let j = 0; j < this.hospitalData[i].nightSlot.appointmentSlots.length; j++ ) {
                            this.hospitalData[i].nightSlot.appointmentSlots[j].isSelected = false;
                        }
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].nightSlot.tokenSlots){
                        for ( let j = 0; j < this.hospitalData[i].nightSlot.tokenSlots.length; j++ ) {
                            this.hospitalData[i].nightSlot.tokenSlots[j].isSelected = false;
                        }
                    } //end of token slot
                } //end of nightSlot
                
            }
        }
    }
    
    /*
     *  Function to set selected appointmentdata in storage
     **/
    setAppointmentData( slot, hospital){
        //for appointment slot
         if( slot.visitType == 'APPOINTMENT'){
             this.hospitalWithTimeSlotArr = {
                 'selectedHospitalSlot': hospital.hospitalDetails,
                 'selectedTimeSlot': slot,
                 'doctorDetails': this.doctorDetails,
                 'appointmentDetails': this.appointmentDetails,
                 'fees':hospital.fees,
                 'visitType': "APPOINTMENT'",
             }
             this.commonService.setInStorage( 'hospitalWithAppointmentTimeSlot', this.hospitalWithTimeSlotArr );
         }else if( slot.visitType == 'TOKEN'){
             console.log("tokennnnnnnnnnnn store.........", slot);
             //for token slot
             this.hospitalWithTokenTimeSlotArr = {
                 'selectedHospitalSlot': hospital.hospitalDetails,
                 'selectedTimeSlot': slot,
                 'doctorDetails': this.doctorDetails,
                 'appointmentDetails': this.appointmentDetails,
                 'visitType': "TOKEN",
                 'fees':hospital.fees
             }
             this.commonService.setInStorage( 'hospitalWithAppointmentTimeSlot', this.hospitalWithTokenTimeSlotArr );
         }
                
        //If appointment slot selected, set slot in storage
      /*  if( this.hospitalWithTimeSlotArr && this.hospitalWithTimeSlotArr.selectedTimeSlot && this.hospitalWithTimeSlotArr.selectedTimeSlot.visitType == 'APPOINTMENT' ){
             this.commonService.setInStorage( 'hospitalWithAppointmentTimeSlot', this.hospitalWithTimeSlotArr );
        }else if( this.hospitalWithTokenTimeSlotArr  && this.hospitalWithTimeSlotArr.selectedTimeSlot && this.hospitalWithTokenTimeSlotArr.visitType == 'TOKEN'){
            console.log("tokennnnnnnnnnnn store.........2", this.hospitalWithTokenTimeSlotArr);
            //If token slot selected, set slot in storage
            this.commonService.setInStorage( 'hospitalWithAppointmentTimeSlot', this.hospitalWithTokenTimeSlotArr );
        }*/
    }

    /*
     * Function to get user selected timeslot of type appointment booking
     **/
    getSelectedTokenTimeslot( selectedTimeslot, hospital ) {
        console.log("selectedTimeslot tokennnnnnnnnnnnnnnnnnnn",selectedTimeslot);
        this.deselectionOfSlot(); 
        selectedTimeslot.isSelected = true;
        this.isTimeslotSelected = true;
        selectedTimeslot.visitType = "TOKEN";
        this.setAppointmentData( selectedTimeslot, hospital );
    }

    /*
     * Function to show coming soon popup
     **/
    comingSoonPopup = () => {
        this.commonService.presentToast();
    }
    
    /*
     *  Function for date selection
     **/
    onDateSelect( selectedDate ) {
        console.log("onDateSelect..................................");
        //For appointment slot (On date select clear array)
        if( this.hospitalData && this.hospitalData.length > 0){
            for ( let i = 0; i < this.hospitalData.length; i++ ) {
                if(this.hospitalData[i].morningSlot){
                    if( this.hospitalData[i].morningSlot.appointmentSlots){
                        this.hospitalData[i].morningSlot.appointmentSlots.splice(0, this.hospitalData[i].morningSlot.appointmentSlots.length);
                        this.hospitalData[i].morningSlot.appointmentSlots.appointmentSlotFromTime = '';
                        this.hospitalData[i].morningSlot.appointmentSlots.appointmentSlotToTime = '';
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].morningSlot.tokenSlots){
                        this.hospitalData[i].morningSlot.tokenSlots.splice(0, this.hospitalData[i].morningSlot.tokenSlots.length);
                        this.hospitalData[i].morningSlot.tokenSlots.tokenSlotFromTime = '';
                        this.hospitalData[i].morningSlot.tokenSlots.tokenSlotToTime = '';
                    } //end of token slot
                } //end of morningSlot
                
                if(this.hospitalData[i].afternoonSlot){
                    if( this.hospitalData[i].afternoonSlot.appointmentSlots){
                        this.hospitalData[i].afternoonSlot.appointmentSlots.splice(0, this.hospitalData[i].afternoonSlot.appointmentSlots.length);
                        this.hospitalData[i].afternoonSlot.appointmentSlots.appointmentSlotFromTime = '';
                        this.hospitalData[i].afternoonSlot.appointmentSlots.appointmentSlotToTime = '';
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].afternoonSlot.tokenSlots){
                        this.hospitalData[i].afternoonSlot.tokenSlots.splice(0, this.hospitalData[i].afternoonSlot.tokenSlots.length);
                        this.hospitalData[i].afternoonSlot.tokenSlots.tokenSlotFromTime = '';
                        this.hospitalData[i].afternoonSlot.tokenSlots.tokenSlotToTime = '';
                    } //end of token slot
                } //end of afternoonSlot
                
                if(this.hospitalData[i].eveningSlot){
                    if( this.hospitalData[i].eveningSlot.appointmentSlots){
                        this.hospitalData[i].eveningSlot.appointmentSlots.splice(0, this.hospitalData[i].eveningSlot.appointmentSlots.length);
                        this.hospitalData[i].eveningSlot.appointmentSlots.appointmentSlotFromTime = '';
                        this.hospitalData[i].eveningSlot.appointmentSlots.appointmentSlotToTime = '';
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].eveningSlot.tokenSlots){
                        this.hospitalData[i].eveningSlot.tokenSlots.splice(0, this.hospitalData[i].eveningSlot.tokenSlots.length);
                        this.hospitalData[i].eveningSlot.tokenSlots.tokenSlotFromTime = '';
                        this.hospitalData[i].eveningSlot.tokenSlots.tokenSlotToTime = '';
                    } //end of token slot
                } //end of eveningSlot 
                
                
                if(this.hospitalData[i].nightSlot){
                    if( this.hospitalData[i].nightSlot.appointmentSlots){
                        this.hospitalData[i].nightSlot.appointmentSlots.splice(0, this.hospitalData[i].nightSlot.appointmentSlots.length);
                        this.hospitalData[i].nightSlot.appointmentSlots.appointmentSlotFromTime = '';
                        this.hospitalData[i].nightSlot.appointmentSlots.appointmentSlotToTime = '';
                    }//end of appointment slot
                    
                    if( this.hospitalData[i].nightSlot.tokenSlots){
                        this.hospitalData[i].nightSlot.tokenSlots.splice(0, this.hospitalData[i].nightSlot.tokenSlots.length);
                        this.hospitalData[i].nightSlot.tokenSlots.tokenSlotFromTime = '';
                        this.hospitalData[i].nightSlot.tokenSlots.tokenSlotToTime = '';
                    } //end of token slot
                } //end of nightSlot
                
            }
        }
        this.isTimeslotSelected = false;
        this.pageNo = 0;
        this.selectedDate = new Date (moment(selectedDate).format('D MMM, YYYY')); // some mock date
        if( this.fromPage == 'updateAppointment' && this.editParam == 'patientUpcomingAppointment' && this.appointmentDetails){
            if(this.appointmentDetails.appointmentDate){
                this.appointmentDetails.appointmentDate = this.selectedDate;
            }
        }
        
        //On date select pass hospital Id, If we are coming from patient role upcoming appointment and appointment history page on book again Btn click
        if( this.fromPage == 'patientAppointmentHistory' || this.fromPage == 'patientUpcomingAppointmentBookAgain' || this.fromPage == 'patientUpcomingAppointmentViewDetailsBookAgain' || this.fromPage == 'doctorRoleAppointmentHistoryVisitDetailsBookAgain'){
            if(this.bookAgainHospitalId){
                this.getHospitalListWithTimeslots( this.doctorProfileId, '', this.selectedDate, this.bookAgainHospitalId, true );
            }
        }else if(this.fromPage == 'patientHospitalProfile'){
                console.log("home hospitalId.............", this.hospitalId);
            this.getHospitalListWithTimeslots( this.doctorProfileId, '', this.selectedDate, this.hospitalId, true );
        }else{
            this.getHospitalListWithTimeslots( this.doctorProfileId, '', this.selectedDate, '', true );
        }
        
    }

    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public plotAddressOnMap = ( hospitalName:any, address: any ) => {
        let currentHospitalAddress = hospitalName+ "+" +address.street + "+" + address.cityName + "+" +
            address.state + "+" + address.zipCode;
        this.commonService.setAddressOnMap( currentHospitalAddress );
    }


    /*
     * Function to load more data when clicked on load more button
     * */
    public loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getHospitalListWithTimeslots( this.doctorProfileId, '',  this.selectedDate, '', false );
    }

     /*
     * Function to do calculations for time related 
     * */
    formatAvailability = () => {
        var currentDate = new Date();
        var today = currentDate.getDay();
            // console.log("this.listOfDoctors", this.listOfDoctors[i]);
            let availabilities = this.doctorListDto.timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime(availabilities);
            this.doctorListDto.formattedAvailabilities = calculatedTime;
            let availabilityLength = this.doctorListDto.formattedAvailabilities.length;
            for (let j = 0; j < availabilityLength; j++) {
                if (today == this.doctorListDto.formattedAvailabilities[j].dayCount) {
                    //console.log( "todayAvailability=========================>", todayAvailability );
                }
            }
            this.doctorListDto.todayAvailability = this.doctorListDto.todaysTimeAvailabilityList;
            if (this.doctorListDto.todaysTimeAvailabilityList && this.doctorListDto.todaysTimeAvailabilityList.length > 0) {
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = this.doctorListDto.todayAvailability.length;
                for (let j = 0; j < todayTimeLen; j++) {
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.doctorListDto.todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.doctorListDto.todaysTimeAvailabilityList[j].toTime), true);

                    if (currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes) {
                        timeSlotFound = true;
                        this.doctorListDto.todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.doctorListDto.todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.doctorListDto.todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if (!timeSlotFound) {
                    this.doctorListDto.todaysAvailabilityString = "Unavailable";
                }
            } else {
                this.doctorListDto.todaysAvailabilityString = "Unavailable";
            }
        
    }

    /**
     * Function to show Timing popup
     **/
    openTimingDetails = () => {
        if(this.doctorListDto){
            console.log("doctor obj...", this.doctorListDto);
            let title = this.doctorListDto.firstName + " " + this.doctorListDto.lastName;
            let subTitle = this.doctorListDto.address.street + ", " + this.doctorListDto.address.cityName;
            this.popover = this.commonService.showTimingList(title, subTitle, this.doctorListDto.formattedAvailabilities);
            this.commonService.closePopupOnBackBtn(this.popover);
        }
       
    }
    
}