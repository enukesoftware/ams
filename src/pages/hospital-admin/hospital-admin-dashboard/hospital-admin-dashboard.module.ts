import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from "../../components/shared.module";
import { HADashboardPage } from './hospital-admin-dashboard';
 
@NgModule({
    declarations: [
       HADashboardPage
    ],
    imports: [
       IonicPageModule.forChild(HADashboardPage),
       TranslateModule.forChild(),
       SharedModule
    ],
    exports: [
       HADashboardPage
    ]
})
export class HADashboardPageModule {}
