import { Component, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { IonicPage, NavController, MenuController, Content, NavParams, Events, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, HospitalListDTO } from '../../../interfaces/user-data-dto';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";

@IonicPage( {
    name: 'HADashboardPage',
    segment: 'hospital-admin-dashboard/:hospitalId'
} )
@Component( {
    selector: 'page-hospital-admin-dashboard',
    templateUrl: 'hospital-admin-dashboard.html'
} )
export class HADashboardPage {
    @ViewChild( Content ) content: Content;
    @ViewChild( 'HAlistGrid' ) HAlistGrid: ElementRef;
    userData: UserDataDTO = {};
    selectedHospital: HospitalListDTO[];
    scrollHTFlag: boolean = false;
    hospitalId: string = "";
    userLoginRole: string;
    userActiveRole: string;
    showMyProfile: boolean = false;
    reportsRedirectionRole:any;

    constructor( private commonService: CommonService, private evts: Events, private location: Location, public platform: Platform, public manageHospitalServiceProvider: ManageHospitalServiceProvider, public navParams: NavParams, private renderer: Renderer2, private navCtrl: NavController, private menu: MenuController, private constants: Constants ) {
        this.menu.enable( true );
        /**
         *handle device back button
         */
         this.platform.registerBackButtonAction(() => {
//             this.location.back();
           });
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    ionViewDidLoad = () => {
        //dashboard Wrapper height dynamically set based on scroll content height
        setTimeout(() => {
            this.scrollHTFlag = this.commonService.contentHeight( this.content,this.HAlistGrid );
        }, 100 );
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
                this.userData = result;
                this.commonService.getFromStorage("userLoginRole").then((value)=>{
                   if(value) {
                       if(value == this.constants.ROLE_SYSTEM_ADMIN || value == this.constants.ROLE_HOSPITAL_ADMIN){
                           this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_HOSPITAL_ADMIN );
                       }
                   }
                });
                if(result.role == this.constants.ROLE_SYSTEM_ADMIN || result.role == this.constants.ROLE_HOSPITAL_ADMIN){
                    this.commonService.setActiveUserRole( this.constants.ROLE_HOSPITAL_ADMIN );
                }
                this.evts.publish('fire-after-login-event');
                
                this.userLoginRole = this.commonService.getUserLoginRole();
                this.userActiveRole = this.commonService.getActiveUserRole();
                
                if(this.userLoginRole == 'HOSPITAL_ADMIN'){
                    this.reportsRedirectionRole ='hospitalAdmin';
                }
                if(this.userLoginRole == 'SYSTEM_ADMIN'){
                    this.reportsRedirectionRole ='sysAdmHospitalAdmin';
                }
                
                if(this.userLoginRole == this.userActiveRole){
                    this.showMyProfile = true;
                }
                                
                this.commonService.fireSelectedEvent( '#/hospital-admin-dashboard/0');
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN, true );
                
                if( this.navParams.get('hospitalId') && this.navParams.get('hospitalId') != ":hospitalId" && this.navParams.get('hospitalId') != "0" ){
                    this.hospitalId = this.navParams.get('hospitalId');
                    this.commonService.setInStorage("hospitalId", this.hospitalId );
                    console.log("HOS_ID1:",this.hospitalId);
                    this.manageHospitalServiceProvider.getHospitalDetails(this.hospitalId).subscribe(res=>{
                        this.selectedHospital = res.data;
                        this.commonService.hideLoading();
                        this.commonService.setInStorage("hospitalId", res.data.hospitalId);
                    }, error=>{
                        this.commonService.hideLoading();
                        let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                        this.commonService.presentToast(errorMsg);
                    });
                }else{
                    this.commonService.getFromStorage("hospitalId").then(( value ) => {
                        if( value ) {
                            this.hospitalId = value;
                        }else{
                            this.hospitalId = "";
                        }
                        console.log("HOS_ID2:",this.hospitalId);
                        this.manageHospitalServiceProvider.getHospitalDetails(this.hospitalId).subscribe(res=>{
                            this.selectedHospital = res.data;
                            this.commonService.hideLoading();
                            this.commonService.setInStorage("hospitalId", res.data.hospitalId);
                        }, error=>{
                            this.commonService.hideLoading();
                            let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                            this.commonService.presentToast(errorMsg);
                        });
                    });
                }
                
                this.commonService.setPaddingTopScroll( this.content );
                

                //booking details clear from local storage
                this.commonService.removeFromStorage("appointmentDetails");
                this.commonService.removeFromStorage("isConfirmed");
                this.commonService.removeFromStorage("isUpdateOnView");
            }
        } );
    }

    showComingSoon = () => {
        this.commonService.presentToast();
    }
}