/**
 * hospital admin manage appointment controller module
 * manage appointment related functionality
 * Created By : #1166
 * Date :28 August. 2018
 */

/*------------------------ angular component ----------------------------*/
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content , Platform} from 'ionic-angular';
import { CalendarModule } from 'primeng/calendar';
import { Location } from '@angular/common';

/*------------------------ Providers ----------------------------*/
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { UserDataDTO, AppointmentDTO } from '../../../interfaces/user-data-dto';

@IonicPage({
    name: 'HospitalAdminManageAppointmentPage',
    segment: 'hospital-admin-manage-appointment'
  })
@Component( {
    selector: 'page-hospital-admin-manage-appointment',
    templateUrl: 'hospital-admin-manage-appointment.html',
} )
export class HospitalAdminManageAppointmentPage {
    @ViewChild( Content ) content: Content;
    userData: UserDataDTO = {};
    protected appointmentList: any;
    protected loadFinished: boolean = false;
    protected pageNo: number = 0;
    protected hospitalId: any;
    protected hospitalData: any;
    startValue: any;
    startValueStr: string;
    endValue: any;
    endValueStr: string;
    imgUrl: any = [];
    rank: number;
    imgCounter: number;
    today: any;
    todayDateStr: any;
    goBtnClicked = false;
    errorMSG:any;
    errorMsgEndDate:any;
    todayForWeb: any;
    fromHospitalStaffManageAppointment:boolean;
    fromHospitalAdminManageAppointment:boolean;
    
    
    constructor( public navCtrl: NavController, public navParams: NavParams, private commonService: CommonService, public manageHospitalService: ManageHospitalServiceProvider,
        private constants: Constants,private location: Location, public platform: Platform, ) { 
        /**
         *handle device back button
         */
         this.platform.registerBackButtonAction(() => {
             this.location.back();
         });
         
         this.startValueStr='';
         this.endValueStr='';
    }

    ionViewWillEnter = () => {
        this.isAuthorized(); 
        this.imgCounter = 1;
        
        this.today =  this.convertDate();
        this.todayForWeb = new Date().setHours(0,0,0);
        
        this.todayDateStr = this.commonService.formatDateString(new Date());
    }
    
    convertDate = () =>{
        var d = new Date();
        
        return [d.getFullYear(), this.pad(d.getMonth()+1), this.pad(d.getDate())].join('-');
    }
    
    pad = (s) => {
        return (s < 10) ? '0' + s : s;
    }

    ionViewDidLoad() { }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.getFromStorage("userActiveRole").then(( value ) => {
                    if( value ) {
                        if(value == this.constants.ROLE_HOSPITAL_STAFF){
                            this.fromHospitalStaffManageAppointment = true;
                            this.fromHospitalAdminManageAppointment = false;
                            this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_STAFF );

                        }else{
                            this.fromHospitalStaffManageAppointment = false;
                            this.fromHospitalAdminManageAppointment = true;
                            this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );

                        }
        
                        // this.getPatientDependent();
                    }
                }); 
                this.commonService.fireSelectedEvent( '#/hospital-admin-manage-appointment' );
                if ( this.commonService.checkIsWeb() ) {
                    this.commonService.setPaddingTopScroll( this.content );
                }
                this.commonService.getFromStorage( "hospitalId" ).then(( value ) => {
                    if ( value ) {
                        this.hospitalId = value;
                        this.viewHospitalDetails( this.hospitalId );
                    }
                } );
            }
        } );
    }
    
    /**
     * Function To get Hospital details
     **/

    viewHospitalDetails = ( hospitalId: any ) => {
        this.manageHospitalService.getHospitalDetails( this.hospitalId ).subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.hospitalData = res.data;
                    this.getHospitalAppoinTments( true );
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast( errorMsg );
            }
        );
    }
    
    /* function to get images in async */ 
    public getImageBaseDoctorList =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.appointmentList.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.appointmentList[i].doctorsDetails.doctorId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.appointmentList.length; j++ ){
                    if(this.appointmentList[j].doctorsDetails.doctorId == res.data.userId){
                        this.appointmentList[j].doctorsDetails.imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.appointmentList);
            }
        },
        error => {
            this.commonService.hideLoading();
           /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }

    /**
     * Function To get Appointments List
     **/
    getHospitalAppoinTments( firstPageCall?: boolean ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        if(firstPageCall){
            this.appointmentList = "";
        }
        let startDate = "", endDate = "";
        if( this.startValueStr){
            console.log("startDate any...", this.startValueStr);
            if(!this.commonService.checkIsWeb()){
                var now = new Date(this.startValueStr);
                var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                startDate = utc_now.getTime().toString();
                console.log("startDate mob...", startDate);
            }
            else{
                if(this.commonService.checkIsWeb()){
                    new Date(this.startValueStr).setHours(11, 59, 59);
                    startDate = new Date(this.startValueStr).getTime().toString();
                    console.log("startDate web...", startDate);
                }
            }
            /*
             * common function
             * startDate = this.commonService.getFormattedStartDate(this.startValueStr);
             * */
        } 
        if(this.endValueStr){
            if(!this.commonService.checkIsWeb()){
                var now = new Date(this.endValueStr);
                var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                endDate = utc_now.getTime().toString();
                console.log("endDate...", endDate);
            }
            else{
                if(this.commonService.checkIsWeb()){
                new Date(this.endValueStr).setHours(11, 59, 59);
                endDate = new Date(this.endValueStr).getTime().toString();
                console.log("endDate...", endDate);
                }
            }
        }
        if( this.startValueStr && this.endValueStr ){
            this.goBtnClicked=false;
        }
        this.manageHospitalService.getAppointmentList( this.hospitalId, this.pageNo, startDate, endDate ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                    this.appointmentList = res.data.list;
                    this.getImageBaseDoctorList();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                }else if ( res.data.list && res.data.list.length > 0 ) {
                    for ( let j = 0; j < res.data.list.length; j++ ) {
                        this.appointmentList.push( res.data.list[j] );
                    }
                    this.getImageBaseDoctorList();
                    if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                        this.loadFinished = true;
                    }
                }else {
                    this.loadFinished = true;
                }
                
                if(this.appointmentList && this.appointmentList.length > 0){
                   for(let i=0; i<this.appointmentList.length; i++){
                       if(this.appointmentList[i].timings && this.appointmentList[i].timings.length > 0){
                           this.appointmentList[i].currentAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.appointmentList[i].timings[0].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.appointmentList[i].timings[0].toTime), false, true);
                        }
                       this.appointmentList[i].showAppointmentDate = new Date(this.appointmentList[i].appointmentDate);   
                    }
                }
                console.log("appointmentList at 147......", this.appointmentList);
            
            }, error => {
                this.commonService.presentToast( error.message );
                this.commonService.hideLoading();
            } );
    }


    
    /*
     * Function to apply date filter
     * */
    applyDateFilter = () => {
        if(!this.startValueStr){
            this.goBtnClicked =true;
            this.errorMsgEndDate='';
            this.errorMSG = this.commonService.getTranslate('SELECTSTARTDATE',{});
            this.commonService.presentToast(this.errorMSG );
        }
        else if(!this.endValueStr && this.startValueStr){
            this.goBtnClicked =true;
            this.errorMSG = '';
            this.errorMsgEndDate = this.commonService.getTranslate('SELECTENDDATE',{});
            this.commonService.presentToast(this.errorMsgEndDate );
        }
        else{
            this.getHospitalAppoinTments(true);
        }
       
    }
    
    /*
     * Function to load more data when clicked on load more button
     * */
    loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getHospitalAppoinTments();
    }


    /*function to show coming soon popup
     * */
    comingSoonPopup = () => {
        this.commonService.presentToast();
    }
}