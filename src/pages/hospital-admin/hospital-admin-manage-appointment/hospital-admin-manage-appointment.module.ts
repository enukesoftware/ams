import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HospitalAdminManageAppointmentPage } from './hospital-admin-manage-appointment';
import { SharedModule } from "../../components/shared.module";
import { TranslateModule } from '@ngx-translate/core';
import { CalendarModule } from 'primeng/calendar';
import { PipesModule } from "../../../pipes/pipes.module";

@NgModule({
  declarations: [
    HospitalAdminManageAppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminManageAppointmentPage),
    SharedModule,
    TranslateModule.forChild(),
    CalendarModule,
    PipesModule
  ],
  exports: [
            HospitalAdminManageAppointmentPage,
          ]
})
export class HospitalAdminManageAppointmentPageModule {}
