import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Content, NavParams, MenuController , Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorListDTO, HospitalListDTO, HospitalDataDTO } from '../../../interfaces/user-data-dto';

@IonicPage( {
    name: 'HospitalAdminCreateContractPage',
    segment: 'hospital-admin-create-contract'
} )
@Component( {
    selector: 'page-hospital-admin-create-contract',
    templateUrl: 'hospital-admin-create-contract.html',
} )
export class HospitalAdminCreateContractPage {
    @ViewChild(Content) content: Content;
    userData: UserDataDTO = {};
    hospitalData: HospitalListDTO = {};
    selectedHospital: HospitalListDTO = {};

    timeSlot: any;
    weekdays: Array<{ id: number, name: string }>;
    visitTypes: Array<{ id: number, name: string }>;
    timeSlotArray: any[];
    contractData: any;
    doctor: DoctorListDTO = {};

    constructor( public navCtrl: NavController, private menu: MenuController, public navParams: NavParams, private commonService: CommonService, private constants: Constants,
            private location: Location, public platform: Platform) {
        this.platform.registerBackButtonAction(() => {
            this.location.back();
          });
        this.timeSlotArray = [];
        this.weekdays = [
            { id: 1, name: 'Monday' },
            { id: 2, name: 'Tuesday' },
            { id: 3, name: 'Wednesday' },
            { id: 4, name: 'Thursday' },
            { id: 5, name: 'Friday' },
            { id: 6, name: 'Saturday' },
            { id: 7, name: 'Sunday' }
        ]
        this.visitTypes = [
            { id: 1, name: 'Appointment' },
            { id: 2, name: 'Tokens' }
        ]
        this.timeSlot = {
            day: 'Monday',
            startTime: '',
            startTimeMinutes: '',
            endTime: '',
            endTimeMinutes: '',
            visitType: 'Appointment',
            visitValue: ''
        }
        this.contractData = {
            fees: '12',
            applicableDate: ''
        }
    }

    ionViewDidLoad() {
        this.menu.enable( true );
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        
    }

    /**
     * Function to check if user is authorized or not to access the screen
     */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );
                this.commonService.fireSelectedEvent('#/hospital-admin-associated-doctor-list');
                this.commonService.getFromStorage( 'selectedHospital' ).then(( value ) => {
                    if ( value ) {
                        this.selectedHospital = value;
                    }
                } );
                this.commonService.getFromStorage( 'selectedDoctor' ).then(( value ) => {
                    if ( value ) {
                        this.doctor = value;
                    }
                } );
                this.commonService.setPaddingTopScroll( this.content );
            }
        } );
    }

     /**
      * Function to add time slot to time array
      **/
    addTimeSlotClick = () => {
        if ( this.timeSlot.day != '' ) {
            if ( this.timeSlot.startTime != '' ) {
                if ( this.timeSlot.endTime != '' ) {
                    if ( this.timeSlot.visitValue != '' ) {
                        if ( this.isTimeRepeat( this.timeSlot ) ) {
                            this.commonService.presentToast( "Please select valid time slot." );
                        } else {
                            this.timeSlotArray.push( JSON.parse( JSON.stringify( this.timeSlot ) ) );
                            this.timeSlot = {
                                day: 'Monday',
                                startTime: '',
                                startTimeMinutes: '',
                                endTime: '',
                                endTimeMinutes: '',
                                visitType: 'Appointment',
                                visitValue: ''
                            }
                        }
                    } else {
                        this.commonService.presentToast( "Please enter valid value for Appointment/Tokens." );
                    }
                } else {
                    this.commonService.presentToast( "Please select the end time." )
                }
            } else {
                this.commonService.presentToast( "Please select the start time." )
            }
        } else {
            this.commonService.presentToast( "Please select a day." )
        }

    }

    /**
     * Function to handle on change event of time picker 
     **/
    onChangeStartTime = ( sTime ) => {
        this.timeSlot.startTimeMinutes = this.getTimeInMinutes( sTime );
    }

    /**
     * Function to handle on change event of time picker 
     **/
    onChangeEndTime = ( eTime ) => {
        if ( this.getTimeInMinutes( eTime ) > this.timeSlot.startTimeMinutes ) {
            this.timeSlot.endTimeMinutes = this.getTimeInMinutes( eTime );
        } else {
            this.commonService.presentToast( "Please select valid end time." )
            this.timeSlot.endTime = '';
        }
    }

    /**
     * Function to validate time slots 
     **/
    isTimeRepeat = ( tempSlot ) => {
        if ( this.timeSlotArray.length == 0 ) { return false }
        for ( let i = 0; i < this.timeSlotArray.length; i++ ) {
            if ( tempSlot.day == this.timeSlotArray[i].day && tempSlot.startTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.startTimeMinutes <= this.timeSlotArray[i].endTimeMinutes ) {
                return true;
            }
            if ( tempSlot.day == this.timeSlotArray[i].day && tempSlot.endTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes <= this.timeSlotArray[i].endTimeMinutes ) {
                return true;
            }
            if ( tempSlot.day == this.timeSlotArray[i].day && tempSlot.startTimeMinutes < this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes > this.timeSlotArray[i].endTimeMinutes ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Function to return time in minutes
     **/
    getTimeInMinutes = ( timeInStr ) => {
        let hr = 0;
        let min = 0;

        let temp = timeInStr.split( ':' );
        hr = parseInt( temp[0] );
        let minArray = temp[1].split( ' ' );
        min = parseInt( minArray[0] );

        return ( hr * 60 ) + min;
    }

    /**
     * Function to remove time slot from array
     * @param index of time slot
     */
    removeTimeSlot = ( index ) => {
        this.timeSlotArray.splice( index, 1 );
    }

    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
        this.commonService.presentToast();
    }

}
