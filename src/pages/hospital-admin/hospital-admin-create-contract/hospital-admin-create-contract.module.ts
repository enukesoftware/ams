import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";
import { CalendarModule } from 'primeng/calendar';
import { SharedModule } from "../../components/shared.module";
import { HospitalAdminCreateContractPage } from './hospital-admin-create-contract';

@NgModule({
  declarations: [
    HospitalAdminCreateContractPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminCreateContractPage),
    TranslateModule.forChild(),
    CalendarModule,
    SharedModule
  ],
  exports: [
    HospitalAdminCreateContractPage
  ],
})
export class HospitalAdminCreateContractPageModule {}
