import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform, MenuController } from 'ionic-angular';
import { UserDataDTO, DoctorListDTO } from "../../../interfaces/user-data-dto";
import { AuthService } from "../../../providers/authService/authService";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CommonService } from "../../../providers/common-service/common.service";
import { Location } from '@angular/common';

@IonicPage( {
    name: 'HospitalAdminManageHospitalAdminPage',
    segment: 'hospital-admin-manage-hospital-admin'
} )
@Component( {
    selector: 'page-hospital-admin-manage-hospital-admin',
    templateUrl: 'hospital-admin-manage-hospital-admin.html',
} )
export class HospitalAdminManageHospitalAdminPage {
    @ViewChild( Content ) content: Content;

    isManageSysAdmin: boolean = false;
    userData: UserDataDTO = {};
    hospitalId: any;
    fromHospitalAdmin: string = "hospitalAdmin";
    pageNo: number = 0;
    loadFinished: boolean = false;
    //assign sys admins dto
    systemAdmins: DoctorListDTO[];
    constructor( private platform: Platform, public authService: AuthService, private location: Location, private constants: Constants, private menu: MenuController, public navCtrl: NavController, public navParams: NavParams, public manageHospitalService: ManageHospitalServiceProvider, public commonService: CommonService ) {
        /**
         *handle device back button
         */
         this.platform.registerBackButtonAction(() => {
             this.location.back();
           });
    }

    ionViewDidLoad() {
        console.log( 'ionViewDidLoad HospitalAdminManageHospitalAdminPage' );
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                this.commonService.getFromStorage( "hospitalId" ).then(( value ) => {
                    if ( value ) {
                        this.hospitalId = value;
                        this.getHospitalAdminList( this.hospitalId,true );
                    }
                } );
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );
                this.commonService.fireSelectedEvent( '#/hospital-admin-manage-hospital-admin' );
            }
        } );
    }
    
    /* function to get images in async */ 
    public getImageBase =()=>{
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i <this.systemAdmins.length; i++) {
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.systemAdmins[i].userId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                for( let j=startIndex; j<this.systemAdmins.length; j++ ){
                    if(this.systemAdmins[j].userId == res.data.userId){
                        this.systemAdmins[j].imageBase64 = res.data.file;
                    }
                }
                console.log("this.listOfDoctors=======>", this.systemAdmins);
            }
        },
        error => {
            this.commonService.hideLoading();
      /*      let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });

        }
    }

    //To do : get hospital admin list on web service response
    public getHospitalAdminList( id,firstPageCall?: boolean ) {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.manageHospitalService.getHospitalAdminsList( id,this.pageNo).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == "success" ) {
                    if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                        this.systemAdmins = res.data.list;
                        this.getImageBase();
                        console.log("this.systemAdmins", this.systemAdmins);
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else if( res.data.list && res.data.list.length > 0 ){
                        for( let j=0;j<res.data.list.length;j++ ){
                            this.systemAdmins.push(res.data.list[j]);
                        }
                        this.getImageBase();
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else{
                        this.loadFinished = true;
                    }
                }
                this.commonService.setPaddingTopScroll( this.content );
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                this.commonService.setPaddingTopScroll( this.content );
            }
        );
    }
    
    
    /*
     * Function to load more data when clicked on load more button
     * */
   public loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getHospitalAdminList(this.hospitalId);
    }   

}