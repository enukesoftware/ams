import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";

import { HospitalAdminManageHospitalAdminPage } from './hospital-admin-manage-hospital-admin';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    HospitalAdminManageHospitalAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminManageHospitalAdminPage),
    SharedModule,
    TranslateModule.forChild()
  ],
})
export class HospitalAdminManageHospitalAdminPageModule {}
