import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, MenuController, Platform } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

//3rd party npm module
import * as _ from 'lodash';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorListDTO, HospitalListDTO, HospitalDataDTO, HospitalDTO } from '../../../interfaces/user-data-dto';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';
import { HospitalAdminViewHospitalPage } from "../hospital-admin-view-hospital/hospital-admin-view-hospital";
import { CameraService } from '../../../providers/camera-service/camera.service';

@IonicPage( {
    name: 'HospitalAdminEditHospitalPage',
    segment: 'hospital-admin-edit-hospital'
} )
@Component( {
    selector: 'page-hospital-admin-edit-hospital',
    templateUrl: 'hospital-admin-edit-hospital.html',
} )
export class HospitalAdminEditHospitalPage {
    
    tempTimeAvailability: { 'day': any; 'dayId': any; 'fromTime': string; 'toTime': string; 'startTime': any; 'endTime': any; };
    @ViewChild(Content) content: Content;
    
    formSubmitted: boolean;
    userData: UserDataDTO = {};
    hospitalData: HospitalDTO = { address: {} };
    isWeb: boolean = false;
    isMobileWeb:boolean = false;

    weekdays: Array<{ id: number, name: string }>;
    timeSlot: any;
    timeSlotArray: any[];
    cityList: any;

    servicesString: string;
    selectService: boolean = true;
    doctorData: any;
    specializationString: string;
    selectSpec: boolean = true;
    services: any;
    specialization: any;
    selectedCategories: any;
    selectCat: boolean = true;
    servicesArr: any[];
    specializationArr: any[];
    category: any;
    categories: any[];
    inputType: any = "text";
    imgUrl: any = [];
    rank: number = 0;
    imgCounter: number;
    profileImg: any = {};
    uploadImgArr: any = [];
    uploadImgCount: number;
    subImages: any;
    selectedWeekDay: any;
    timeAvailability: any = [];
    effectiveFrom:any;
    editImageArr:any = [];
    tempTimeSlotObj: any = {};
    tempTimeSlotArray: any = [];
    webTimeSlotObj: any = {};
    webTimeSlotArray: any = [];
    open24Hours: boolean = false;
    open24TickImgUrl: string;
    multiTickImgUrl: string;
    tempTimeSlotStringArr:any = []; 
    fullDaySlot: boolean = false;
    downloadProfileImg: any;
    isDeletedImg: boolean = false;
    deleteImgArr:any = [];
    eTimeChanged: boolean = false;
    sTimeChanged: boolean = false;
    defaultImg: boolean = false;
    
    constructor( public navCtrl: NavController, private _ngZone: NgZone,  public cameraService : CameraService, public navParams: NavParams, private location: Location, public platform: Platform, private menu: MenuController, private commonService: CommonService,
        private constants: Constants, private manageHospitalService: ManageHospitalServiceProvider ) {
        this.timeSlotArray = [];
        this.tempTimeSlotArray = [];
        this.tempTimeSlotStringArr = [];
        this.timeSlot = {
            day: '',
            startTime: '',
            startTimeMinutes: '',
            endTime: '',
            endTimeMinutes: '',
            visitType: 'Appointment',
            visitValue: ''
        }
        
        /**
         *handle device back button
         */
         this.platform.registerBackButtonAction(() => {
             this.location.back();
         });
    }

    ionViewDidLoad() {
        if( this.commonService.checkIsWeb() ){
            this.menu.enable( true );
        }else{
            this.menu.enable( false );
        }
        
        //set default time
        
        this.timeSlot.fromTime = new Date();
        this.timeSlot.toTime = new Date();
        this.timeSlot.startTime = new Date();
        this.timeSlot.endTime = new Date();
        
        if(!this.commonService.checkIsWeb()){
            this.timeSlot.startTime = new Date(this.timeSlot.startTime.getTime() - this.timeSlot.startTime.getTimezoneOffset()*60000).toISOString();
            this.timeSlot.endTime = new Date((this.timeSlot.endTime.getTime()+60000) - this.timeSlot.endTime.getTimezoneOffset()*60000).toISOString();
        }
    }

    ionViewWillEnter = () => {
        if( this.commonService.checkIsWeb() ){
            this.menu.enable( true );
        }else{
            this.menu.enable( false );
        }        
        this.uploadImgCount = 0;
        this.imgCounter = 1;
        if( !this.profileImg.file ){
            this.profileImg.file = "assets/imgs/hospital_profile_image_default.png";
        }
        this.isAuthorized();
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.isWeb = this.commonService.checkIsWeb();
                this.isMobileWeb = this.commonService.checkMobileweb();
                this.userData = result;
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );
                this.commonService.fireSelectedEvent( '#/hospital-admin-view-hospital' );
                this.listOfCities();
                this.getDays();
                this.commonService.clearTimeSlotArr();
                
                this.commonService.getFromStorage("hospitalId").then(( value ) => {
                    if ( value ) {
                        this.manageHospitalService.getHospitalDetails(value).subscribe(res=>{
                            this.hospitalData = res.data;
                            let timeAvailabilityList = res.data.timeAvailabilityList;
                            
                            if(timeAvailabilityList && timeAvailabilityList.length > 0){
                                for(let i=0; i<timeAvailabilityList.length; i++){
                                    let tempTimeSlotStringObj = {'day': '','slotString': '', 'fromTime': ''};
                                    let tempTimeSlotObj = {'day': '', 'daysId': '', 'fromTime': '', 'toTime': '', 'startTime': '', 'endTime': ''};
                                    tempTimeSlotObj.day =  timeAvailabilityList[i].day;
                                    tempTimeSlotObj.daysId = timeAvailabilityList[i].daysId;
                                    tempTimeSlotObj.fromTime = new Date(timeAvailabilityList[i].fromTime).toISOString();
                                    tempTimeSlotObj.toTime = new Date(timeAvailabilityList[i].toTime).toISOString();
                                    tempTimeSlotObj.startTime = new Date(timeAvailabilityList[i].fromTime).toString();
                                    tempTimeSlotObj.endTime = new Date(timeAvailabilityList[i].toTime).toString();
                                    
                                    this.tempTimeSlotArray.push(tempTimeSlotObj);
                                    
                                    tempTimeSlotStringObj.day = timeAvailabilityList[i].day;
                                    tempTimeSlotStringObj.fromTime = new Date(timeAvailabilityList[i].fromTime).toISOString();
                                    let fromTime = this.commonService.convertTo12hrsFormat(new Date(timeAvailabilityList[i].fromTime), false, true);
                                    let toTime = this.commonService.convertTo12hrsFormat(new Date(timeAvailabilityList[i].toTime), false, true);
                                    tempTimeSlotStringObj.slotString = fromTime + " - " + toTime;
                                    this.tempTimeSlotStringArr.push(tempTimeSlotStringObj); 
                                }
                            }
                            
                            
                            this.hospitalData.address.cityId = res.data.address.cityId;
                            if(this.hospitalData.isAMultispeciality == true){
                                this.multiTickImgUrl = "assets/imgs/01_multispacility_checked.png";
                            } else{
                                this.multiTickImgUrl = "assets/imgs/01_multispacility_uncheck.png";
                            }
                            this.open24TickImgUrl = "assets/imgs/01_multispacility_uncheck.png";

                            this.downloadImage();
                            this.getSpecializationServices();
                        }, err=>{
                            console.log("err.....", err.message);
                        });
                    }
                    
                });
            }
        } );
    }
            
    /*
     * to get city on change
     * */
    
    cityChange(){
        if(this.cityList){
            let cityLen = this.cityList.length;
            for(let i=0; i<cityLen; i++){
                if(this.hospitalData.address.cityId == this.cityList[i].id){
                    this.hospitalData.address.cityName = this.cityList[i].name;
                }
            }   
        }
    }

    /**
     * Function to update Hospital
     **/
    updateHospital = ( form: NgForm ) => {
        this.formSubmitted = true;
        if ( form.valid ) {
            this.formSubmitted = false;
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            for( let m=0;m<this.hospitalData.specializationList.length;m++ ){
                delete this.hospitalData.specializationList[m].label;
                delete this.hospitalData.specializationList[m].value;
            }
            for( let n=0;n<this.hospitalData.serviceList.length;n++ ){
                delete this.hospitalData.serviceList[n].label;
                delete this.hospitalData.serviceList[n].value;
            }
            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.manageHospitalService.updateHospital( this.hospitalData ).subscribe(
                res => {
                    if ( res.status == "success" ) {
                        this.hospitalData = res.data;
                        this.commonService.hideLoading();
                        this.commonService.presentToast(res.message);
                        console.log("manageHospitalService =====================> this.uploadImgArr", this.uploadImgArr, "this.hospitalData", this.hospitalData);
                            if( this.uploadImgArr.length > 0 || this.hospitalData.imagesCount > 0 ){
                                console.log("this.uploadImgArr", this.uploadImgArr, "this.hospitalData", this.hospitalData);
                                if(this.hospitalData.imagesCount > 0){
                                     this.deleteImage( 'HOSPITAL_IMAGE', this.hospitalData.hospitalId, this.uploadImgArr );
                                }else{
                                    for( let i=0; i<this.uploadImgArr.length; i++ ){
                                        this.uploadImgCount++;
                                        this.uploadImgArr[i].rank = this.uploadImgCount;
                                        this.uploadImage( this.uploadImgArr[i] );
                                    }
                                }
                                this.uploadImgArr =[];
                           }
                       setTimeout(()=>{
                                        try{
                                            this.navCtrl.setRoot(HospitalAdminViewHospitalPage);
                                        }
                                        catch(mobile){
                                           console.log(mobile);
                                        }
                             
                        }, 3000);
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);
                }
            );
        }
    }


    /*
     * Function to get category , specialization and services
     * */
    getSpecializationServices = () => {
        this.manageHospitalService.getDoctorSpecializationServices().subscribe( res => {
            if ( res.status == "success" ) {
                this.categories = res.data;
                try{
                    var catIdArr = [];    
                    if( this.categories ){
                        if( this.commonService.checkIsWeb() ){
                            for( let n=0;n<this.categories.length;n++ ){
                                this.categories[n].value = this.categories[n];
                                this.categories[n].label = this.categories[n].name;
                            }
                        }
                        
                        if(this.hospitalData.serviceList){
                        var serviceLen = this.hospitalData.serviceList.length;
                        if(serviceLen > 0){
                            for(let i=0; i<serviceLen; i++){
                                if(catIdArr.indexOf( this.hospitalData.serviceList[i].categoryId ) == -1 ){
                                   catIdArr.push(this.hospitalData.serviceList[i].categoryId);
                                }
                            }
                        }
                    }
                    if(this.hospitalData.specializationList){
                        if(specLen > 0){
                            for(let i=0; i<specLen; i++){
                                if(catIdArr.indexOf( this.hospitalData.specializationList[i].categoryId ) == -1 ){
                                    catIdArr.push(this.hospitalData.specializationList[i].categoryId);
                                }
                            }
                        }
                     }
                    
                    var specLen = this.hospitalData.specializationList.length;
                    
                    var catLen = this.categories.length; 
                    var catIdLen = catIdArr.length; 
                    var selectedCats = [];  
                    if(catLen > 0){
                       for(let i=0; i<catLen; i++){
                           for(let j=0; j<catIdLen; j++){
                              if(this.categories[i].id == catIdArr[j]){
                                   selectedCats.push(this.categories[i]);
                               }
                           }
                       }
                       this.category = selectedCats;
                   }
                 }
            }catch(err){
               console.log("err......", err);
            }
            this.onChangeCategory();
           }
        }, error => {
            console.log( "specialization error.....", error );
        } );
    }
                   
   /**
    * Based on category, select specialization and services
    **/
   onChangeCategory = ( dropDownChange?: boolean, values?: any ) => {
//   onChangeCategory = ( values?: any ) => {
       console.log("values--->",values," ----- this.category:: ",this.category);
       if( values ){
           this.category = values.value;
           console.log("values--->",values);
       }
       this.specializationArr = [];
       this.servicesArr = [];
       try{
           if( this.category ){
               for( let h=0;h<this.category.length;h++ ){
                   if( this.category[h].specializationList ){
                       for( let t=0;t<this.category[h].specializationList.length;t++ ){
                           this.category[h].specializationList[t].categoryId = this.category[h].id;
                           if( this.commonService.checkIsWeb() ){
                               console.log("in caegory web check");
                               this.category[h].specializationList[t].label = this.category[h].specializationList[t].name;
                               this.category[h].specializationList[t].value = this.category[h].specializationList[t];
                           }
                           this.specializationArr.push( this.category[h].specializationList[t] );
                           for( let r=0;r<this.hospitalData.specializationList.length;r++ ){
                               if( this.hospitalData.specializationList[r].id == this.category[h].specializationList[t].id && this.hospitalData.specializationList[r].categoryId == this.category[h].id ){
                                   this.hospitalData.specializationList.splice( r,1 );
                                   if( this.commonService.checkIsWeb() ){
                                       this.hospitalData.specializationList.splice( r,0,this.category[h].specializationList[t].value );
                                   }else{
                                       this.hospitalData.specializationList.splice( r,0,this.category[h].specializationList[t] );
                                   }
                               }
                           }
                       }
                   }
                   if ( this.category[h].serviceList ) {
                       for ( let d=0;d<this.category[h].serviceList.length;d++ ){
                           this.category[h].serviceList[d].categoryId = this.category[h].id;
                           if( this.commonService.checkIsWeb() ){
                               this.category[h].serviceList[d].label = this.category[h].serviceList[d].name;
                               this.category[h].serviceList[d].value = this.category[h].serviceList[d];
                           }
                           this.servicesArr.push( this.category[h].serviceList[d] );
                           for( let q=0;q<this.hospitalData.serviceList.length;q++ ){
                               if( this.hospitalData.serviceList[q].id == this.category[h].serviceList[d].id && this.hospitalData.serviceList[q].categoryId == this.category[h].id ){
                                   this.hospitalData.serviceList.splice( q,1 );
                                   if( this.commonService.checkIsWeb() ){
                                       this.hospitalData.serviceList.splice( q,0,this.category[h].serviceList[d].value );
                                   }else{
                                       this.hospitalData.serviceList.splice( q,0,this.category[h].serviceList[d] );
                                   }
                               }
                           }
                       }
                   }
               }
               if( dropDownChange ){
                   if( this.specializationArr.length <= 0 ){
                       this.hospitalData.specializationList = [];
                   }
               
                   if( this.servicesArr.length <= 0 ){
                       this.hospitalData.serviceList = [];
                   }
                   for( let b=0;b<this.hospitalData.specializationList.length;b++ ){
                       let foundSpecializationCat = _.findIndex(this.specializationArr,(o) => { return o.categoryId == this.hospitalData.specializationList[b].categoryId; });
                       if( foundSpecializationCat == -1 ){
                           this.hospitalData.specializationList.splice( b,1 );
                       }
                   }
           
                   for( let z=0;z<this.hospitalData.serviceList.length;z++ ){
                       let foundServiceCat = _.findIndex(this.servicesArr,(o) => { return o.categoryId == this.hospitalData.serviceList[z].categoryId; });
                       if( foundServiceCat == -1 ){
                           this.hospitalData.serviceList.splice( z,1 );
                       }
                   }
               }
           }
        }catch(err){
            console.log("err on 279...", err)
        }
       this.getSpecialization();
       this.getServices();
   }

     
                        
    foo = (demo) =>{
        console.log("demo",demo);
    }                          
                        
                        
    /**
     * On specialization change display selected specialization in block
     **/
    getSpecialization = ( selectedValue?: any ) => {
        if( selectedValue ){
            this.hospitalData.specializationList = selectedValue.value;
        }
        if(this.hospitalData.specializationList){
            let specLen = this.hospitalData.specializationList.length;
            if ( specLen == 0 ) {
                this.selectSpec = false
            }
            
            this.specializationString = "";
            let selectedSpecLen = this.hospitalData.specializationList.length;
            for ( let i = 0; i < selectedSpecLen; i++ ) {
                if ( i < selectedSpecLen - 1 ) {
                    this.specializationString = this.specializationString + this.hospitalData.specializationList[i].name + ", ";
                } else {
                    this.specializationString = this.specializationString + this.hospitalData.specializationList[i].name;
                }
            }
        }
    }

    /**
    * On service change, display selected services in block
    **/
    getServices = ( selectedValue?: any ) => {
        if( selectedValue ){
            this.hospitalData.serviceList = selectedValue.value;
        }
        if(this.hospitalData.serviceList){
            let serviceLen = this.hospitalData.serviceList.length;
            if ( serviceLen == 0 ) {
                //this.hospitalData.serviceList = "";
                this.selectService = false;
            }
            this.servicesString = "";
            let selectedServiceLen = this.hospitalData.serviceList.length;
            for ( let i = 0; i < selectedServiceLen; i++ ) {
                if ( i < selectedServiceLen - 1 ) {
                    this.servicesString = this.servicesString + this.hospitalData.serviceList[i].name + ", ";
                } else {
                    this.servicesString = this.servicesString + this.hospitalData.serviceList[i].name;
                }
            }
        }
    }
    
    formatCategory = () => {
        return this.category.map(selectedCategory => selectedCategory.name).join(", ");
    }
    
    formatSpecialization = () => {
        return this.hospitalData.specializationList.map(specialization => specialization.name).join(", ");
    }
    
    formatServices = () => {
        return this.hospitalData.serviceList.map(service => service.name).join(", ");
    }

    /**
     * Function to add time slot to time array
     **/
    addTimeSlotClick = () => {
        console.log('addTimeSlotClick===============>');
        let storedSlots = [];
        if(this.tempTimeSlotArray.length > 0){
            storedSlots = this.tempTimeSlotArray;
            for(let i=0; i< this.tempTimeSlotArray.length; i++){
                storedSlots[i].startTimeMinutes = this.commonService.getTimeInMinutes(this.tempTimeSlotArray[i].startTime);
                storedSlots[i].endTimeMinutes = this.commonService.getTimeInMinutes(this.tempTimeSlotArray[i].endTime);
            }
        }
            
        
        if(!this.commonService.checkIsWeb()){
            if(this.timeSlot.day != ""){
                let startTime =  new Date(this.timeSlot.startTime);
                let endTime = new Date(this.timeSlot.endTime);
                let tempLocalStartTime = new Date(startTime.getTime() + (startTime.getTimezoneOffset()*60000));
                let tempLocalEndTime = new Date(endTime.getTime() + (endTime.getTimezoneOffset()*60000));
                
                this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes(tempLocalStartTime);
                this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes(tempLocalEndTime);  
                
                this.timeSlot.startTime = tempLocalStartTime;
                this.timeSlot.endTime = tempLocalEndTime;
            
            }
        } else{
            this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes(this.timeSlot.startTime);
            this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes(this.timeSlot.endTime);
            console.log('empty day');
        }
        
        let tempStr = _.cloneDeep(this.timeSlot);
        if(tempStr.day.name){
            tempStr.day = tempStr.day.name; 
        }
        let timeSlot = this.commonService.addTimeSlotClick(tempStr, false, true, "", "", storedSlots);
            
        let tempTimeSlotObj = {
            day:'',
            daysId: '',
            fromTime: '',
            toTime: '',
            startTime: '',
            endTime: ''
        }
        let tempTimeSlotStringObj = {'day': '','slotString': '', 'fromTime': ''};
        console.log('timeSlot.timeValidater=============>',timeSlot.timeValidate);
        if(timeSlot.timeValidate){
            console.log('timeSlot.timeValidate --if',timeSlot.timeValidate);
            tempTimeSlotObj.day = this.timeSlot.day.name ? this.timeSlot.day.name : this.timeSlot.day;
            tempTimeSlotObj.daysId = this.timeSlot.day.id;
            tempTimeSlotObj.fromTime = new Date(timeSlot.startTime).toISOString();
            tempTimeSlotObj.toTime = new Date(timeSlot.endTime).toISOString();
            tempTimeSlotObj.startTime = timeSlot.startTime;
            tempTimeSlotObj.endTime = timeSlot.endTime;
            if(timeSlot.day.name){
                tempTimeSlotStringObj.day = timeSlot.day.name;
            } else{
                tempTimeSlotStringObj.day = timeSlot.day;
            }
            
            tempTimeSlotStringObj.fromTime = new Date(timeSlot.startTime).toISOString();
            let fromTime = this.commonService.convertTo12hrsFormat(timeSlot.startTime);
            let toTime = this.commonService.convertTo12hrsFormat(timeSlot.endTime);
            tempTimeSlotStringObj.slotString = fromTime + " - " + toTime;
            this.timeSlot.slotString = fromTime + " - " + toTime;
            
            this.timeSlotArray.push(timeSlot);
            this._ngZone.run(() => {
                this.tempTimeSlotStringArr.push(tempTimeSlotStringObj);
            });
            console.log('this.tempTimeSlotStringArr=============>',this.tempTimeSlotStringArr);
            this.timeAvailability.push(tempTimeSlotObj);
            this.hospitalData.timeAvailabilityList.push(tempTimeSlotObj);// = tempTimeSlotObj;//this.tempTimeSlotArray;
            
            this.timeSlot = {
                day: '',
                startTime: '',
                startTimeMinutes: '',
                endTime: '',
                endTimeMinutes: '',
                visitType: 'Appointment',
                visitValue: ''
            }
            this.timeSlot.fromTime = new Date();
            this.timeSlot.toTime = new Date();
            this.timeSlot.startTime = new Date();
            this.timeSlot.endTime = new Date();
            
            if(!this.commonService.checkIsWeb()){
                this.timeSlot.startTime = new Date(new Date(tempTimeSlotObj.startTime).getTime() - new Date(tempTimeSlotObj.startTime).getTimezoneOffset()*60000).toISOString();
                this.timeSlot.endTime = new Date(new Date(tempTimeSlotObj.endTime).getTime() - new Date(tempTimeSlotObj.endTime).getTimezoneOffset()*60000).toISOString();
            }
            
            this.fullDaySlot = false;
            this.open24TickImgUrl = "assets/imgs/01_multispacility_uncheck.png";

//            this.timeSlot = {};
//            this.timeSlot.startTime = "";
//            this.timeSlot.endTime = "";
        }
        
        /*for ( let i = 0; i < this.timeSlotArray.length; i++ ) {
            let tempStartTime = new Date(this.timeSlotArray[i].startTime);
            this.timeSlotArray[i].startTime = this.commonService.convertTo12hrsFormat( tempStartTime );
            let tempEndTime = new Date(this.timeSlotArray[i].endTime);
            this.timeSlotArray[i].endTime = this.commonService.convertTo12hrsFormat( tempEndTime );
            this.tempTimeAvailability = {
                'day': this.timeSlotArray[i].dayName,
                'dayId': this.timeSlotArray[i].dayId,
                'fromTime':new Date(this.timeSlot.startTime).toISOString(),
                'toTime': new Date(this.timeSlot.endTime).toISOString(),
                'startTime':this.timeSlotArray[i].startTime,
                'endTime': this.timeSlotArray[i].endTime 
            }
        }
        this.hospitalData.effectiveFrom = new Date(this.effectiveFrom).toISOString();
        this.timeAvailability.push(this.tempTimeAvailability);
        for(let j=0; j<this.timeAvailability.length; j++){
            this.hospitalData.timeAvailabilityList.push(this.timeAvailability[j]);
        }
        
       if( this.effectiveFrom != null ) {
            if ( this.timeSlot.day != '' ) {
                if ( this.timeSlot.startTime != '' ) {
                    if ( this.timeSlot.endTime != '' ) {
                        if ( this.isTimeRepeat( this.timeSlot ) ) {
                            this.commonService.presentToast( "Please select valid time slot." );
                            
                        } else {
                            if ( this.timeSlot.endTimeMinutes <= this.timeSlot.startTimeMinutes ) {
                                this.commonService.presentToast( "Please select valid end time." )
                                this.timeSlot.endTime = '';
                            }else{
                                let tempTimeAvailability;
                                this.timeSlot.dayName = this.timeSlot.day.name;
                                this.timeSlot.dayId = this.timeSlot.day.id;
                                this.timeSlotArray.push( JSON.parse( JSON.stringify( this.timeSlot ) ) );
                                
                                for ( let i = 0; i < this.timeSlotArray.length; i++ ) {
                                    let tempStartTime = new Date(this.timeSlotArray[i].startTime);
                                    this.timeSlotArray[i].startTime = this.commonService.convertTo12hrsFormat( tempStartTime );
                                    let tempEndTime = new Date(this.timeSlotArray[i].endTime);
                                    this.timeSlotArray[i].endTime = this.commonService.convertTo12hrsFormat( tempEndTime );
                                    tempTimeAvailability = {
                                        'day': this.timeSlotArray[i].dayName,
                                        'dayId': this.timeSlotArray[i].dayId,
                                        'fromTime':new Date(this.timeSlot.startTime).toISOString(),
                                        'toTime': new Date(this.timeSlot.endTime).toISOString(),
                                        'startTime':this.timeSlotArray[i].startTime,
                                        'endTime': this.timeSlotArray[i].endTime 
                                    }
                                }
                                this.hospitalData.effectiveFrom = new Date(this.effectiveFrom).toISOString();
                                this.timeAvailability.push(tempTimeAvailability);
                                for(let j=0; j<this.timeAvailability.length; j++){
                                    this.hospitalData.timeAvailabilityList.push(this.timeAvailability[j]);
                                }
                            }
                        }
                    } else {
                        //alert("select End time")
                        this.commonService.presentToast( "Please select the end time." )
                    }
                } else {
                    this.commonService.presentToast( "Please select the start time." )
                }
            } else {
                this.commonService.presentToast( "Please select a day." )
            }
        }else{
            this.commonService.presentToast( "Please select Effective from date." )
        }*/
        
    }

    /**
     * Function to handle on change event of time picker 
     **/
    onChangeStartTime = ( sTime ) => {
        if(!this.commonService.checkIsWeb()){
            this.sTimeChanged = true;
            this.timeSlot.startTime = new Date(sTime);
        } else{
            let tempDate = new Date(sTime);
            let hrMinStrin = tempDate.getHours() + ":" + tempDate.getMinutes();
            this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes( hrMinStrin );
        }
    }

    /**
     * Function to handle on change event of time picker 
     **/
    onChangeEndTime = ( eTime ) => {
        if(!this.commonService.checkIsWeb()){ 
            this.eTimeChanged = true;
            this.timeSlot.endTime = new Date(eTime);
        } else{
            let tempDate = new Date(eTime);
            let hrMinStrin = tempDate.getHours() + ":" + tempDate.getMinutes();
            this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes( hrMinStrin );
        }
        
    }

    /**
       * Function to validate time slots 
       **/
    isTimeRepeat = ( tempSlot ) => {
        if ( this.timeSlotArray.length == 0 ) { return false }
        for ( let i = 0; i < this.timeSlotArray.length; i++ ) {

            if ( tempSlot.dayName == this.timeSlotArray[i].dayName && tempSlot.startTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.startTimeMinutes <= this.timeSlotArray[i].endTimeMinutes ) {
                return true;
            }
            if ( tempSlot.dayName == this.timeSlotArray[i].dayName && tempSlot.endTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes <= this.timeSlotArray[i].endTimeMinutes ) {
                return true;
            }
            if ( tempSlot.dayName == this.timeSlotArray[i].dayName && tempSlot.startTimeMinutes < this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes > this.timeSlotArray[i].endTimeMinutes ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Function to return time in minutes
     **/
    getTimeInMinutes = ( timeInStr ) => {
        let hr = 0;
        let min = 0;

        let temp = timeInStr.split( ':' );
        hr = parseInt( temp[0] );
        let minArray = temp[1].split( ' ' );
        min = parseInt( minArray[0] );

        return ( hr * 60 ) + min;
    }

    /*
    * function to set hospital details
    */
    setHospitalDetails(id){
        this.manageHospitalService.getHospitalDetails(id).subscribe(res=>{
            this.hospitalData = res.data;
            this.listOfCities();
            this.getSpecializationServices();
        }, error=>{
            let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);
        })
    }

    /**
     * Function to remove time slot from array
     * @param index of time slot
     */
    removeTimeSlot = ( index, slot ) => {
        var alert = this.commonService.confirmAlert("Warning!", "Are you sure you want to delete this time solt?");
        alert.setMode("ios");
        alert.present();
        alert.onDidDismiss( (data) => {
            if(data == true){
               this.hospitalData.timeAvailabilityList.splice(index, 1);
               if(this.tempTimeSlotArray && this.tempTimeSlotArray.length > 0){
                   this.tempTimeSlotArray.splice(index, 1);
               }
               this.tempTimeSlotStringArr.splice( index, 1 );
               this.commonService.removeTimeSlot(index);
           }
        });
    }

    /**
     * Function to Get list of cities
     * @param data
     */
    listOfCities() {
        this.commonService.getListOfCities().subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.cityList = res.data.list;
                }
                this.commonService.setPaddingTopScroll( this.content );
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                this.commonService.setPaddingTopScroll( this.content );
            }
        );
    }

     /**
     * Function to upload image 
     **/
     uploadImage = ( uploadImgData ) => {
        this.commonService.uploadImage(uploadImgData).subscribe(
            res => {
                if ( res.status == "success" ) {
                }
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.showAlert( "Error", error.message );
        });
    }

    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean) {
        if( $event.target.files ){
            this.rank++;
            this.imgCounter++;
            let imageData = {
                "userType": "HOSPITAL_IMAGE",
                "mediaType": "IMAGE",
                "id": this.hospitalData.hospitalId,
                "file": "",
                "fileName":"",
                "isDefault": isDefault,
                "rank" : this.rank
            }
            this.subImages = {
                "file": "",
                "isDefault": isDefault,
                "rank" : this.rank
            }
            var image:any = new Image();
            var file:File = $event.target.files[0];
            var myReader:FileReader = new FileReader();
            var that = this;
            myReader.onloadend = (loadEvent:any) => {
                image.src = loadEvent.target.result;
                this.commonService.convertImgToBase64(image.src, (callback)=>{
                    this.subImages.file = callback;
                    if(this.subImages.isDefault == true){
                        this.profileImg = Object.assign(this.subImages);
                    }else{
                        this.imgUrl.push(this.subImages);
                    }
                    imageData.file = callback;
                    imageData.fileName = file.name;
                    if( this.hospitalData.imagesCount < 7 ){
                        this.hospitalData.imagesCount++;
                    }
                })
            };
            myReader.readAsDataURL(file);
            if( isDefault ){
                //let myFlag = false;
                let myIndex = -1;
                console.log("this.uploadImgArr",this.uploadImgArr)
                if( this.uploadImgArr.length > 0 ){
                    for(let k=0; k<this.uploadImgArr.length; k++){
                        if(this.uploadImgArr[k].isDefault){
                            //myFlag = true;
                            myIndex = k; 
                            break;
                        }
                    }
                    if( myIndex !== -1 ){
                        this.uploadImgArr[myIndex] = imageData;
                    }else{
                        this.uploadImgArr.push( imageData );
                    }
                }else{
                    this.uploadImgArr.push( imageData );
                }
            }else{
                this.uploadImgArr.push( imageData );
            }
        }
                    console.log("fileChangeListener ===========================>",this.uploadImgArr,isDefault);
    }
                    
    /***
    * Function to open camera/photo gallery
    */
    protected onCamera = (isDefault) => {
        try{
            
            console.log("isDefault 843........", isDefault);
            this.defaultImg = isDefault;
           // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
            this.cameraService.loadImage(this.successCallback,this.errorCallback);
        }catch(e){
            
        }
    }
    
    /**
     * Function to handle camera success callback
     * @param success
     */
    private successCallback = ( base64Image: any ) => {
        this._ngZone.run(() => {
            this.rank++;
            this.imgCounter++;
            let imageData = {
                "userType": "HOSPITAL_IMAGE",
                "mediaType": "IMAGE",
                "id": this.hospitalData.hospitalId,
                "file": "",
                "fileName":"",
                "isDefault": this.defaultImg,
                "rank" : this.rank
            }
            this.subImages = {
                "file": "",
                "isDefault": this.defaultImg,
                "rank" : this.rank
            }
            this.subImages.file = base64Image;
            if(this.subImages.isDefault == true){
                this.profileImg = Object.assign(this.subImages);
            }else{
                this.imgUrl.push(this.subImages);
            }
            
            this.commonService.convertImgToBase64(base64Image, (callback)=>{
                imageData.file = callback;
                if( this.hospitalData.imagesCount < 7 ){
                    this.hospitalData.imagesCount++;
                }
                
                if( this.defaultImg ){
                    if( this.uploadImgArr.length > 0 ){
                        let defaultImgFound = false;
                        for(let k=0; k<this.uploadImgArr.length; k++){
                            if(this.uploadImgArr[k].isDefault){
                                defaultImgFound = true;
                                this.uploadImgArr[k] = imageData;
                                break;
                            }
                        }
                        if(!defaultImgFound){
                            this.uploadImgArr.push( imageData );
                        console.log("this.uploadImgArr 897", this.uploadImgArr);
                        }
                    }else{
                        this.uploadImgArr.push( imageData );
                    }
                }else{
                    this.uploadImgArr.push( imageData );
                }
            });
                
         })
            //imageData.fileName = file.name;

    }
    
    /**
     * Function to handle camera error callback
     * @param error
     */
    private errorCallback = ( error: any ) => {
       console.log( 'Unable to load profile picture.' ,error);
    }

    /**
     *  
     */
    deleteImage( userType:any, hospitalId:any, imgArr:any ){
        console.log("hospitalId", hospitalId);
        this.commonService.deleteImage(userType, hospitalId).subscribe(
            res => {
                if( res.status == 'success'){
                    console.log("imgArr 123", imgArr);
                     for( let i=0; i<imgArr.length; i++ ){
                        this.uploadImgCount++;
                        imgArr[i].rank = this.uploadImgCount;
                         if(!imgArr[i].userType && !imgArr[i].id){
                             imgArr[i].userType = "HOSPITAL_IMAGE";
                             imgArr[i].id = this.hospitalData.hospitalId;
                         }

                     console.log("imgArr[i] 123", imgArr[i]);
                        this.uploadImage( imgArr[i] );
                     }
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }

    /**
     * DownLoad hospital images
     * @param imageData 
     */
    downloadImage(){
         let imageData = {
            'userType': "HOSPITAL_IMAGE",
            'id': this.hospitalData.hospitalId,
            'rank': this.imgCounter,
        }
       if( this.imgCounter <= this.hospitalData.imagesCount ){
            this.commonService.downloadImage( imageData ).subscribe(
                res => {
                    if ( res.status == "success" ) {
                            if(res.data.isDefault && res.data.file ){
                                //console.log("res.data", res.data);
                                this.profileImg = Object.assign(res.data);
                                this.uploadImgArr.push(res.data);
                            }else{
                                this.imgUrl.push(res.data);
                                this.uploadImgArr.push(res.data);
                            }
                        this.imgCounter ++;
                        this.downloadImage();
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    //this.commonService.showAlert( "Error", error.message );
            });
       }
    } 

    /**
     * Function to cancel image upload
     */
    cancelImage( img?: any ){
        let confirmPopUpMsg = this.commonService.getTranslate('EDIT_HOSPITAL_IMG_CANCEL_MSG',{});
        let warningTitle =  this.commonService.getTranslate('WARNING',{});
        let alert = this.commonService.confirmAlert(warningTitle, confirmPopUpMsg+'?');
        alert.present();
        alert.onDidDismiss(( data ) => {
            if( data == true ){
                for(let i=0; i<this.imgUrl.length; i++){
                    if( img.rank == this.imgUrl[i].rank){
                        let index = this.imgUrl.indexOf(this.imgUrl[i]);
                        this.imgUrl.splice(index,1);
                    }
                }
                for(let i=0; i<this.uploadImgArr.length; i++){
                    if( img.rank == this.uploadImgArr[i].rank){
                        let index = this.uploadImgArr.indexOf(this.uploadImgArr[i]);
                        this.uploadImgArr.splice(index,1);
                    }
                }
                console.log("cancelImage =================================>",this.uploadImgArr);
            }
        } );
    }   
    
    /**
     * Array function for count
     */
    counter = ( i: number ) => {        
        return new Array(i);
    }
    
    chk24HoursClick(){
        this._ngZone.run(() => {
            this.hospitalData.is24HrOpen = !this.hospitalData.is24HrOpen;
            if(this.hospitalData.is24HrOpen){
                this.open24TickImgUrl = "assets/imgs/01_multispacility_checked.png";
                this.sTimeChanged = false;
                this.eTimeChanged = false;
                this.timeSlot.fromTime = new Date(new Date().setHours(0, 0, 0));
                this.timeSlot.toTime = new Date(new Date().setHours(23, 59, 0));
                this.timeSlot.startTime = new Date(new Date().setHours(0, 0, 0));
                this.timeSlot.endTime = new Date(new Date().setHours(23, 59, 0));
                
                if(!this.commonService.checkIsWeb()){
                    this.timeSlot.startTime = new Date(this.timeSlot.startTime.getTime() - this.timeSlot.startTime.getTimezoneOffset()*60000).toISOString();
                    this.timeSlot.endTime = new Date(this.timeSlot.endTime.getTime() - this.timeSlot.endTime.getTimezoneOffset()*60000).toISOString();
                }
                this.fullDaySlot = true;
            } else{
                this.fullDaySlot = false;
                this.open24TickImgUrl = "assets/imgs/01_multispacility_uncheck.png";
            }
        });
        
    }
    
    chkMultiSpClick(){
        this._ngZone.run(() => {
            this.hospitalData.isAMultispeciality = !this.hospitalData.isAMultispeciality;
            if(this.hospitalData.isAMultispeciality){
                this.multiTickImgUrl = "assets/imgs/01_multispacility_checked.png";
            } else{
                this.multiTickImgUrl = "assets/imgs/01_multispacility_uncheck.png";
            }
        });
    }

    /**
     * Function to get day's
     */
    getDays(){
        this.commonService.getDay().subscribe(
                res => {
                    if ( res.status == "success" ) {
                        this.weekdays = res.data.list;
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    this.commonService.showAlert( "Error", error.message );
            });
    }

    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
        this.commonService.presentToast();
    }
}