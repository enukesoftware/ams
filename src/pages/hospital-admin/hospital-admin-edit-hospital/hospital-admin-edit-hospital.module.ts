import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from 'primeng/calendar';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { IonicSelectableModule } from 'ionic-selectable';

import { HospitalAdminEditHospitalPage } from './hospital-admin-edit-hospital';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    HospitalAdminEditHospitalPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminEditHospitalPage),
    MultiSelectModule,
    CalendarModule,
    SharedModule,
    SelectSearchableModule,
    IonicSelectableModule,
    TranslateModule.forChild()
  ],
  exports: [
    HospitalAdminEditHospitalPage,
  ]
})
export class HospitalAdminEditHospitalPageModule {}
