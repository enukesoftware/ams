import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HospitalAdminDoctorHolidayCalendarPage } from './hospital-admin-doctor-holiday-calendar';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../components/shared.module';

@NgModule({
  declarations: [
    HospitalAdminDoctorHolidayCalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminDoctorHolidayCalendarPage),
    TranslateModule.forChild(),
    SharedModule
  ],
  exports: [
    HospitalAdminDoctorHolidayCalendarPage
 ]
})
export class HospitalAdminDoctorHolidayCalendarPageModule {}
