import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Content, Events,Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO } from '../../../interfaces/user-data-dto';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';

@IonicPage( {
    name: 'HospitalAdminAppointmentVisitDetailsPage',
    segment: 'hospital-admin-appointment-visit-details/:hospitalId/:doctorProfileId/:date/:detailMode'
} )
@Component( {
    selector: 'page-hospital-admin-appointment-visit-details',
    templateUrl: 'hospital-admin-appointment-visit-details.html',
} )
export class HospitalAdminAppointmentVisitDetailsPage {
    @ViewChild( Content ) content: Content;
    userData: UserDataDTO = {};
    fromAppointmentHistory: any;
    fromUpcomingAppointment: any;
    fromHospitalAdminAppointmentVisitDetails: any;
    //hospitalId: any;
    pageNo: number = 0;
    protected doctorProfileId: any;
    protected hospitalId: any;
    protected date: any;
    protected appointmentInfo: any;
    protected loadFinished: boolean = false;
    protected detailMode: any;
    protected hospitalInfo: any;
    details: any;
    dateInMili: any;
    HAAppointmentDetails: any;
    imgCounter: number = 0;
    uploadImgArr: any = [];
    imgUrl: any = [];
    rank: number = 0;
    backBtnUrl:any;

    isPatientDependents: boolean = false;
    bookingUrl: any;
    fromHospitalStaffManageAppointment:boolean;
    fromHospitalAdminManageAppointment:boolean;
    
    constructor( private commonService: CommonService, private ManageHospitalServiceProvider: ManageHospitalServiceProvider, private evts: Events, public navParams: NavParams, private navCtrl: NavController, private menu: MenuController, private constants: Constants,
            private location: Location, public platform: Platform) {
        this.menu.enable( true );
        this.platform.registerBackButtonAction(() => {
            this.location.back();
          });
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();;
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                this.userData = result;
                if(this.detailMode == "hospitalAppointmentDetails"){
                    this.commonService.fireSelectedEvent( '#/hospital-admin-manage-appointment' );
                } else if(this.detailMode == "appointmentDetails"){
                    this.commonService.fireSelectedEvent( '#/doctor-role-upcoming-appointments' );
                } else if(this.detailMode == "historyDetails"){
                    this.commonService.fireSelectedEvent( '#/doctor-role-appointments-history' );
                }

                this.commonService.getFromStorage("userActiveRole").then(( value ) => {
                    if( value ) {
                        if(value == this.constants.ROLE_HOSPITAL_STAFF){
                            this.fromHospitalStaffManageAppointment = true;
                            this.fromHospitalAdminManageAppointment = false;
                            this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_STAFF );

                        }else{
                            this.fromHospitalStaffManageAppointment = false;
                            this.fromHospitalAdminManageAppointment = true;
                            this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );

                        }
        
                        // this.getPatientDependent();
                    }
                }); 

                console.log("this.navParams detailMode...", this.navParams.get( "detailMode" ));
                if(this.navParams.get( "doctorProfileId" ) && this.navParams.get( "doctorProfileId" ) != 0 &&
                   this.navParams.get( "hospitalId" ) && this.navParams.get( "hospitalId" ) != 0 && 
                   this.navParams.get( "date" ) && this.navParams.get( "date" ) != 0 &&
                   this.navParams.get( "detailMode" ) && this.navParams.get( "detailMode" ) != 0){
                    this.doctorProfileId = this.navParams.get( "doctorProfileId" );
                    this.hospitalId = this.navParams.get( "hospitalId" );
                    this.date = this.navParams.get( "date" );
                    this.detailMode = this.navParams.get( "detailMode" );
                    this.dateInMili = new Date(this.date).getTime();
                    this.commonService.setInStorage("DRVisitDetails", this.navParams);
                    this.commonService.setInStorage("hospitalId", this.navParams.get( "hospitalId" ));
                    this.getVisitDetails( this.hospitalId, true );
                    this.getDetails( this.hospitalId );
                    
                }else{
                    this.commonService.getFromStorage( 'DRVisitDetails' ).then(( value ) => {
                        if ( value ) {
                            this.doctorProfileId = value.data.doctorProfileId;
                            this.hospitalId = value.data.hospitalId;
                            this.date = value.data.date;
                            this.detailMode = value.data.detailMode;   
                            this.dateInMili = new Date(this.date).getTime();
                            this.getVisitDetails( this.hospitalId, true );
                            this.getDetails( this.hospitalId );
                            console.log("storage.......................112", this.detailMode);
                        }
                    } );
                }
                
                if(this.detailMode == "doctorHistoryDetails" ){
                    console.log("doctorHistoryDetails.......................");
                    this.backBtnUrl = '#/doctor-role-appointments-history';
                }else if(this.detailMode == "doctorAppointmentDetails" ){
                    console.log("doctorAppointmentDetails.......................");
                    this.backBtnUrl = '#/doctor-role-upcoming-appointments';
                }else{
                    this.backBtnUrl = '#/hospital-admin-manage-appointment';
                }
            }
        } );

        this.getPatientDependent();
    }

    comingSoonPopup = () => {
        this.commonService.presentToast();
    }

    getVisitDetails = ( hospitalId: any, firstPageCall?: boolean ) => {
        this.ManageHospitalServiceProvider.getHospitalAppointmentVisitDetails( this.hospitalId, this.doctorProfileId, this.dateInMili, this.pageNo, this.detailMode ).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == 'success' ) {
                    if ( res.data.list && res.data.list.length > 0 && firstPageCall ) {
                        this.appointmentInfo = res.data.list;
                        console.log("this.appointmentInfo", this.appointmentInfo);
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else if ( res.data.list && res.data.list.length > 0 ) {
                        for ( let j = 0; j < res.data.list.length; j++ ) {
                            this.appointmentInfo.push( res.data.list[j] );
                        }
                        if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                            this.loadFinished = true;
                        }
                    }else {
                        this.loadFinished = true;
                    }
                    
                    if(this.appointmentInfo && this.appointmentInfo.length > 0){
                        for(let i=0; i<this.appointmentInfo.length; i++){
                            this.appointmentInfo[i].fromTimeStr = this.commonService.convertTo12hrsFormat(new Date(this.appointmentInfo[i].fromTime), false, true);
                            this.downloadImage(this.appointmentInfo[i]);
                        }
                    }
                }
                this.commonService.setPaddingTopScroll( this.content );
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.presentToast( error.message );
                this.commonService.setPaddingTopScroll( this.content );
            }
        )
    }

    protected getDetails = ( hospitalId: any ) => {
        this.ManageHospitalServiceProvider.getHospitalForDetails( hospitalId, this.doctorProfileId, this.dateInMili ).subscribe(
            res => {
                this.hospitalInfo = res.data;
                this.getImageBaseForDoc();
                this.getImageBaseForHosp();
                console.log("this.hospitalInfo", this.hospitalInfo);
                this.hospitalInfo.fromToTimeStr = "";
                if(this.hospitalInfo.timings && this.hospitalInfo.timings.length > 0){
                    this.hospitalInfo.availabilityString = this.commonService.convertTo12hrsFormat(new Date(this.hospitalInfo.timings[0].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.hospitalInfo.timings[0].toTime), false, true);
                } else{
                    this.hospitalInfo.availabilityString = "Not available";
                }
                this.hospitalInfo['date'] = this.date;
                
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.presentToast( error.message );
            }
        )
    }

    /* function to get images in async */ 
    public getImageBaseForDoc =()=>{
            let imageData1 = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.hospitalInfo.doctorsDetails.doctorId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData1 ).subscribe(
        res => {
            console.log("on get image base res")
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                this.hospitalInfo.doctorsDetails.imageBase64 = res.data.file;
            }
        },
        error => {
            this.commonService.hideLoading();
           /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });
    }
    
    
    /* function to get images in async */ 
    public getImageBaseForHosp =()=>{
            let imageData1 = {
                    'userType': "HOSPITAL_IMAGE",
                    'id': this.hospitalInfo.hospitalDetails.hospitalId,
                    'rank': 1,
                    'isDefault':true
             }   
        this.commonService.downloadImage( imageData1 ).subscribe(
        res => {
            console.log("on get image base res")
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                this.hospitalInfo.hospitalDetails.imageBase64 = res.data.file;
            }
        },
        error => {
            this.commonService.hideLoading();
           /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });
    }
    
    
    /*
     * Function to load more data when clicked on load more button
     * */
    public loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getVisitDetails( this.hospitalId );
    }
    
    /**
     * DownLoad hospital images
     * @param imageData 
     */
    downloadImage( appointmentInfo ){
        let imageData;
        let uploadImgArrTemp = [];
        console.log("appointmentInfo", appointmentInfo, "this.imgCounter", this.imgCounter);
       for(let j=1; j<=appointmentInfo.totalFilesCount; j++ ){
           imageData = {
               'userType': "PATIENT_APPOINTMENT_FILE",
               'id': appointmentInfo.appointmentId,
               'rank': j
           }
       console.log( "imageData", imageData);
            this.commonService.downloadImage( imageData ).subscribe(
                res => {
                    if ( res.status == "success" ) {
                            if(res.data.mediaType == 'IMAGE' ){
                                 uploadImgArrTemp.push(res.data);
                            }
                            console.log("this.appointmentInfo.imgArr", this.appointmentInfo.imgArr);
                    }
                },
                error => {
                    /*this.commonService.hideLoading();
                    this.commonService.showAlert( "Error", error.message );*/
            });
       }
        appointmentInfo.imgArr = uploadImgArrTemp;
        return appointmentInfo.imgArr;
    } 

    getPatientDependent = () => {
        if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            this.commonService.getFromStorage('HospitalDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });
        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            this.commonService.getFromStorage('DoctorDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            this.commonService.getFromStorage('PatientDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });

        }

    }

    onClickBookAppointment(fromPage: any, redirectPage: any, redirectParam: any, doctorId: any) {
        console.log("DATA:","FROM:"+fromPage +" REDIRECT_PAGE:" +redirectPage +" PARAM:" +redirectParam + " DOC:"+doctorId);
        if(this.fromHospitalStaffManageAppointment){
            this.bookingUrl = "#/onboard-patient/HSRoleManageAppointment/" + redirectPage + "/" + redirectParam + "/" + doctorId;
        }else if(this.fromHospitalAdminManageAppointment){
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
        }else if (this.isPatientDependents) {
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
            console.log("BOOKING_URL1",this.bookingUrl);
        } else {
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            console.log("BOOKING_URL2",this.bookingUrl);
        }
    }
}
