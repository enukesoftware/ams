import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from "../../components/shared.module";
import { HospitalAdminAppointmentVisitDetailsPage } from './hospital-admin-appointment-visit-details';

@NgModule({
  declarations: [
    HospitalAdminAppointmentVisitDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminAppointmentVisitDetailsPage),
    TranslateModule.forChild(),
       SharedModule
    ],
    exports: [
       HospitalAdminAppointmentVisitDetailsPage
    ]
})
export class HospitalAdminAppointmentVisitDetailsPageModule {}
