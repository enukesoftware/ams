import { IonicPage, NavController, NavParams, Nav, MenuController, Content, Platform } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Component, ElementRef, ViewChild } from "@angular/core";
import { Location } from '@angular/common';

//3rd party npm module
import * as _ from 'lodash';

//providers services
import { CommonService } from "../../../providers/common-service/common.service";
import { Constants } from '../../../providers/appSettings/constant-settings';
import { DoctorDataDto, UserDataDTO, AddressDto, serviceAndSpecializationDataDto } from "../../../interfaces/user-data-dto";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { HospitalAdminDoctorListPage } from "../hospital-admin-doctor-list/hospital-admin-doctor-list";
import { DoctorRoleServiceProvider } from "../../../providers/doctor-role-service/doctor-role-service";
import { CameraService } from "../../../providers/camera-service/camera.service";

@IonicPage( {
    name: 'HACreateDoctorPage',
    segment: 'hospital-admin-create-doctor/:mode/:from/:id'
} )
@Component( {
    selector: 'page-hospital-admin-create-doctor',
    templateUrl: 'hospital-admin-create-doctor.html',
} )
export class HACreateDoctorPage {
    
    @ViewChild(Content) content: Content;
    userData: UserDataDTO = {};
    doctorData: DoctorDataDto = {};
    address: AddressDto = {};
    isProfileEdit: boolean;
    isMyProfile:boolean;
     formSubmitted: boolean;
    servicesString: any;
    specializationString: any;
    doctorEducation: any[];
    selectSpec: boolean = false;
    selectCat: boolean = true;
    selectService: boolean = false;
    selectEdu: boolean = false;
    selectCity: boolean = false;
    isWeb: boolean = false;
    cityList: any;
    doctorEdt: any;
    categories: any;
    category: any;
    selectedCategories: any = [];
    services: any;
    servicesArr: serviceAndSpecializationDataDto[];
    specialization: any;
    specializationArr: serviceAndSpecializationDataDto[];
    viewFlag: boolean = false;
    editFlag: boolean = false;
    createFlag: boolean = false;
    doctorId: any;
    mode: any;
    inputType: any = "text";
    rank: number;
    isDefault: boolean;
    imageUpload: any = {};
    isUpload: boolean = false;
    protected cloneDoctorData:any;
    fromPage:any;
    isMobileWeb:boolean = false;
    isViewMore: boolean = false;
    
    constructor( public navCtrl: NavController,public cameraService : CameraService, private constants: Constants, private location: Location, public platform: Platform, private menu: MenuController, public navParams: NavParams, public commonService: CommonService, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider) {
        /**
         *handle device back button
         */
         this.platform.registerBackButtonAction(() => {
             this.location.back();
           });
         this.rank = 0;
   }

   ionViewWillEnter = () => {
       if( this.commonService.checkIsWeb() ){
           this.menu.enable( true );
       }else{
           this.menu.enable( false );
       }
         //check if user is authorized or not to access the screen if not redirect him to login screen
         this.isAuthorized();
         this.fromPage = this.navParams.get("from") ;
         console.log("fromPage in HA create doc",this.fromPage);
         
         let mode = this.navParams.get("mode") ;
         console.log("fromPage in HA create doc",mode);
         
     }

     /**
      * Function to check if user is authorized or not to access the screen
      */
     isAuthorized = () => {
         this.commonService.isLoggedIn(( result ) => {
             if ( !result ) {
                 this.navCtrl.setRoot( 'LoginPage' );
             } else {
                 this.userData = result;
                 this.isProfileEdit = false;
                 this.isMyProfile = false;
                 this.formSubmitted = false;
                 this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN);
                 this.commonService.fireSelectedEvent( '#/hospital-admin-associated-doctor-list' );
                 this.listOfCities();
                 this.isWeb = this.commonService.checkIsWeb();
                 this.isMobileWeb = this.commonService.checkMobileweb();
                 if(this.navParams.get("mode") == "view"){
                     this.viewFlag = true;
                     this.editFlag = false;
                     this.createFlag = false;
                     this.viewDoctorProfile();
                 }else if(this.navParams.get("mode") == "edit"){
                     this.editFlag = true;
                     this.createFlag = false;
                     this.viewFlag = false;
                     this.viewDoctorProfile();
                 }else{
                     this.createFlag = true;
                     this.viewFlag = false;
                     this.editFlag = false;
                     this.getSpecializationServices();
                 }
                 this.getEducation();
             }
         } );
     }
     
     /***
      * Function to open camera/photo gallery
      */
      protected onCamera = () => {
          try{
             // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
              this.cameraService.loadImage(this.successCallback,this.errorCallback);
          }catch(e){
              
          }
      }
      
      /**
       * Function to handle camera success callback
       * @param success
       */
      private successCallback = ( base64Image: any ) => {
          this.isUpload = true;
          this.rank++;
          this.isDefault = true;
         // this.imageUpload.file = base64Image;
          this.commonService.convertImgToBase64(base64Image, (callback)=>{
              this.imageUpload.file = callback;
          })
      }
      
      /**
       * Function to handle camera error callback
       * @param error
       */
      private errorCallback = ( error: any ) => {
         console.log( 'Unable to load profile picture.' ,error);
      }
     
     getSpecializationServices = () => {
         this.manageHospitalServiceProvider.getDoctorSpecializationServices().subscribe( res => {
             try{
                 if ( res.status == "success" ) {
                     this.categories = res.data;
                     if( this.categories ){
                         if( this.commonService.checkIsWeb() ){
                             let categoryLen = this.categories.length;
                             for( let n=0;n<categoryLen;n++ ){
                                 this.categories[n].value = this.categories[n];
                                 this.categories[n].label = this.categories[n].name;
                             }
                         }
                         var catLen = this.categories.length;
                         var catIdArr = [];
                             
                         if(this.doctorData.serviceList){  
                             var serviceLen = this.doctorData.serviceList.length;
                             if(serviceLen > 0){
                                 for(let i=0; i<serviceLen; i++){
                                     if(catIdArr.indexOf( this.doctorData.serviceList[i].categoryId ) == -1 ){
                                        catIdArr.push(this.doctorData.serviceList[i].categoryId);
                                     }
                                 }
                             }
                         }
                        if(this.doctorData.specializationList){
                            var specLen = this.doctorData.specializationList.length;     
                            if(specLen > 0){
                                for(let i=0; i<specLen; i++){
                                    if(catIdArr.indexOf( this.doctorData.specializationList[i].categoryId ) == -1 ){
                                        catIdArr.push(this.doctorData.specializationList[i].categoryId);
                                    }
                                }
                            }
                        }    
                         var catIdLen = catIdArr.length; 
                         var selectedCats = [];  
                         if(catLen > 0){
                            for(let i=0; i<catLen; i++){
                                for(let j=0; j<catIdLen; j++){
                                   if(this.categories[i].id == catIdArr[j]){
                                        selectedCats.push(this.categories[i]);
                                    }
                                }
                            }
                            this.category = selectedCats;
                        }
                            
                        this.onChangeCategory();
                    }
                }
            }catch(err){
              console.log("err on 166...", err);      
          }
         }, error => {
             console.log( "specialization error.....", error );
         } );
     }
     
     /**
      * Function to get list of cities
      */
     listOfCities = () => {
         this.commonService.getListOfCities().subscribe(
             res => {
                 if ( res.status == "success" ) {
                     this.cityList = res.data.list;
                     this.address.cityId = this.cityList[0].id;
                 }
                 this.commonService.setPaddingTopScroll( this.content );
             },
             error => {
                 this.commonService.hideLoading();
                 let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                 this.commonService.presentToast(errorMsg);
                 //this.commonService.showAlert( "Error", error.message );
                 this.commonService.setPaddingTopScroll( this.content );
             }
         );
     }
     
     
     /**
      * Function onChangeCategory
      */
     onChangeCategory(dropDownChange?: boolean){
         this.specializationArr = [];
         this.servicesArr = [];
         try{
             if( this.category ){
                 for( let h=0;h<this.category.length;h++ ){
                     if( this.category[h].specializationList ){
                         for( let t=0;t<this.category[h].specializationList.length;t++ ){
                             this.category[h].specializationList[t].categoryId = this.category[h].id;
                             if( this.commonService.checkIsWeb() ){
                                 this.category[h].specializationList[t].label = this.category[h].specializationList[t].name;
                                 this.category[h].specializationList[t].value = this.category[h].specializationList[t];
                             }
                             this.specializationArr.push( this.category[h].specializationList[t] );
                             if(this.doctorData.specializationList != null){
                                 for( let r=0;r<this.doctorData.specializationList.length;r++ ){
                                     if( this.doctorData.specializationList[r].id == this.category[h].specializationList[t].id && this.doctorData.specializationList[r].categoryId == this.category[h].id ){
                                         this.doctorData.specializationList.splice( r,1 );
                                         if( this.commonService.checkIsWeb() ){
                                             this.doctorData.specializationList.splice( r,0,this.category[h].specializationList[t].value );
                                         }else{
                                             this.doctorData.specializationList.splice( r,0,this.category[h].specializationList[t] );
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     if ( this.category[h].serviceList ) {
                         for ( let d=0;d<this.category[h].serviceList.length;d++ ){
                             this.category[h].serviceList[d].categoryId = this.category[h].id;
                             if( this.commonService.checkIsWeb() ){
                                 this.category[h].serviceList[d].label = this.category[h].serviceList[d].name;
                                 this.category[h].serviceList[d].value = this.category[h].serviceList[d];
                             }
                             this.servicesArr.push( this.category[h].serviceList[d] );
                             for( let q=0;q<this.doctorData.serviceList.length;q++ ){
                                 if( this.doctorData.serviceList[q].id == this.category[h].serviceList[d].id && this.doctorData.serviceList[q].categoryId == this.category[h].id ){
                                     this.doctorData.serviceList.splice( q,1 );
                                     if( this.commonService.checkIsWeb() ){
                                         this.doctorData.serviceList.splice( q,0,this.category[h].serviceList[d].value );
                                     }else{
                                         this.doctorData.serviceList.splice( q,0,this.category[h].serviceList[d] );
                                     }
                                 }
                             }
                         }
                     }
                 }
                 console.log( "onChangeCategory servicesArr ==========================>",this.servicesArr);
                 console.log( "onChangeCategory serviceList ==========================>",this.doctorData.serviceList);
                 console.log( "onChangeCategory specializationArr ==========================>",this.specializationArr);
                 console.log( "onChangeCategory specializationList ==========================>",this.doctorData.specializationList);
                 if( dropDownChange ){
                     if( this.specializationArr.length <= 0 ){
                         this.doctorData.specializationList = [];                        
                     }
                 
                     if( this.servicesArr.length <= 0 ){
                         this.doctorData.serviceList = [];
                     }
                     for( let b=0;b<this.doctorData.specializationList.length;b++ ){
                         let foundSpecializationCat = _.findIndex(this.specializationArr,(o) => { return o.categoryId == this.doctorData.specializationList[b].categoryId; });
                         if( foundSpecializationCat == -1 ){
                             this.doctorData.specializationList.splice( b,1 );
                         }
                     }
             
                     for( let z=0;z<this.doctorData.serviceList.length;z++ ){
                         let foundServiceCat = _.findIndex(this.servicesArr,(o) => { return o.categoryId == this.doctorData.serviceList[z].categoryId; });
                         if( foundServiceCat == -1 ){
                             this.doctorData.serviceList.splice( z,1 );
                         }
                     }
                 }
             }
         }catch(err){
             console.log("err on category...", err);
         }                
         
         this.getSpecialization();
         this.getServices();
     }
                     
     /**
      * Function to getSpecialization
      */
                     
     getSpecialization = ( selectedValue?: any ) => {
         try{
             if(this.doctorData.specializationList){
                 let specLen = this.doctorData.specializationList.length;
                 if ( specLen == 0 ) {
                     this.selectSpec = false
                 }
                 
                 
                 this.specializationString = "";
                 let selectedSpecLen = this.doctorData.specializationList.length;
                 for ( let i = 0; i < selectedSpecLen; i++ ) {
                     if ( i < selectedSpecLen - 1 ) {
                         this.specializationString = this.specializationString + this.doctorData.specializationList[i].name + ", ";
                     } else {
                         this.specializationString = this.specializationString + this.doctorData.specializationList[i].name;
                     }
                 }
             }
         }catch(err){
             console.log("err on specialization......", err)
         }
     }
     
     
     /**
      * Function to getServices
      */
     getServices = ( selectedValue?: any ) => {
         try{
             if(this.doctorData.serviceList){
                 let serviceLen = this.doctorData.serviceList.length;
                 if ( serviceLen == 0 ) {
                     this.selectService = false;
                 }
                 
                 this.servicesString = "";
                 let selectedServiceLen = this.doctorData.serviceList.length;
                 for ( let i = 0; i < selectedServiceLen; i++ ) {
                     if ( i < selectedServiceLen - 1 ) {
                         this.servicesString = this.servicesString + this.doctorData.serviceList[i].name + ", ";
                     } else {
                         this.servicesString = this.servicesString + this.doctorData.serviceList[i].name;
                     }
                 }
             }
         }catch(err){
             console.log("err......", err);
         }
     }

     /**
      * Function to get doctor profile details
      */
     viewDoctorProfile(){
       this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
       this.doctorRoleServiceProvider.getDoctorProfile(this.navParams.get('id')).subscribe(
           res => {
             this.commonService.hideLoading();
             if(res.status == 'success'){
               this.doctorData = res.data;
            //    this.setSampleInfo();
               this.address = this.doctorData.address;
               this.imageUpload.file = this.doctorData.imageBase64;
               /*if( this.viewFlag ){
                   this.doctorData.firstName = "Dr. "+this.doctorData.firstName;
               }else{
                   this.doctorData.firstName = this.doctorData.firstName.replace("Dr. ", "");
               }*/
                if( this.userData.userId == this.doctorData.doctorId){
                     this.isMyProfile = true;
                }
                this.cloneDoctorData = _.cloneDeep( this.doctorData , false);
                this.getImageBase();
                this.getSpecializationServices();
                this.getEducation();
             }
           }, error => {
               this.commonService.hideLoading();
               this.commonService.presentToast(error.message);
               console.log("error....", error);
           });
     }

     setSampleInfo(){
        //  this.doctorData.profileInfo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"
            
        }
     
     /* function to get images in async */ 
     public getImageBase =()=>{
         //let startIndex = this.pageNo * this.constants.PAGE_SIZE;
         
         console.log("on get image base");
             let imageData = {
                     'userType': "USER_PROFILE_IMAGE",
                     'id': this.doctorData.doctorId,
                     'rank': 1,
              }   
         this.commonService.downloadImage( imageData ).subscribe(
         res => {
             if ( res.status == "success" ) {
                 console.log("image base 64=======>", res);
                 this.doctorData.imageBase64 = res.data.file;
             }
         },
         error => {
             this.commonService.hideLoading();
          /*   let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
             this.commonService.presentToast(errorMsg);*/
     });
     }
     
     /**
      * Function to create doctor profile
      */
     createDoctor( form: NgForm ){
         this.formSubmitted = true;
          if ( form.valid && this.doctorData.educationList && this.doctorData.educationList.length > 0 && this.doctorData.daysForAdvanceBooking <= 30) {
             var educationIdArr = []; 
             this.formSubmitted = false;
             let drFirstName = this.doctorData.firstName;
             let replacedFirstName = drFirstName.replace("Dr. ", "");
             this.doctorData.firstName = replacedFirstName;
             if(this.doctorData.specializationList && this.doctorData.specializationList.length > 0){
                 for( let m=0;m<this.doctorData.specializationList.length;m++ ){
                     if(this.doctorData.specializationList[m].label){
                         delete this.doctorData.specializationList[m].label;
                     }
                     if(this.doctorData.specializationList[m].value){
                         delete this.doctorData.specializationList[m].value;
                     }
                 }
             }
                 
             if(this.doctorData.serviceList && this.doctorData.serviceList.length > 0){    
                 for( let n=0;n<this.doctorData.serviceList.length;n++ ){
                     if(this.doctorData.serviceList[n].label){
                         delete this.doctorData.serviceList[n].label;
                     }
                     if(this.doctorData.serviceList[n].value){
                         delete this.doctorData.serviceList[n].value;
                     }
                 }
             }
             
            if(this.doctorData.educationList && this.doctorData.educationList.length > 0){
                  var tempEducationList = this.doctorData.educationList;
                  console.log("tempEducationList at 413....", this.doctorData.educationList);
                  for( let e=0; e<this.doctorData.educationList.length; e++ ){
//                     if(this.doctorData.educationList[e].label){
//                         delete this.doctorData.educationList[e].label;   
//                     }
//                     if(this.doctorData.educationList[e].value){
//                         delete this.doctorData.educationList[e].value;
//                     }
//                     if(this.doctorData.educationList[e].name){
//                         delete this.doctorData.educationList[e].name;
//                     }
                     educationIdArr.push(this.doctorData.educationList[e].id);
                 }  
             }     
     
             this.doctorData.educationList = educationIdArr; 
     
             this.doctorData.address = this.address;
             this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
             if(this.editFlag){
                 this.doctorRoleServiceProvider.updateDoctorProfile( this.doctorData ).subscribe(
                 res => {
                     this.commonService.hideLoading();
                     if(res.status == 'success'){
                         this.commonService.presentToast(res.message);
                         this.doctorData = res.data;
                         if( this.doctorData && this.doctorData.firstName ){
                             this.doctorData.firstName = this.doctorData.firstName.replace("Dr. ", "");
                         }
                        /* if( this.doctorData && this.doctorData.imageBase64 ){
                             this.imageUpload.file = this.doctorData.imageBase64;
                         } */
                         
                         //Image upload calls
                         if(this.doctorData && this.doctorData.fileId && this.isUpload){
                             this.editImage(this.doctorData.fileId);
                         }else if(this.doctorData.doctorId && !this.doctorData.fileId && this.isUpload){
                             this.uploadImage(this.doctorData.doctorId);
                         }
                         
                         setTimeout(()=>{
                             this.navCtrl.setRoot("HospitalAdminAssociatedDoctorListPage");
                         }, 3000);
                     }
                 }, error => {
                     this.commonService.hideLoading();
                     this.doctorData.educationList = tempEducationList;
                     this.commonService.presentToast(error.message);
                 });
             }else{
                 this.manageHospitalServiceProvider.createDoctor( this.doctorData ).subscribe(
                 res => {
                     this.commonService.hideLoading();
                     if(res.status == 'success'){
                         this.doctorData = res.data;
                         if( this.doctorData && this.doctorData.firstName ){
                             this.doctorData.firstName = this.doctorData.firstName.replace("Dr. ", "");
                         }
                         //Image upload calls
                         if(this.doctorData && this.doctorData.fileId && this.isUpload){
                             this.editImage(this.doctorData.fileId);
                         }else if(this.doctorData.doctorId && !this.doctorData.fileId && this.isUpload){
                             this.uploadImage(this.doctorData.doctorId);
                         }
                         
                         this.commonService.presentToast(res.message);
                         setTimeout(()=>{
                             this.navCtrl.setRoot("HospitalAdminDoctorListPage");
                         }, 3000);
                     }
                 }, error => {
                     this.commonService.hideLoading();
                     this.commonService.presentToast(error.message);
                     this.doctorData.educationList = tempEducationList;
                     console.log("error....", error);
                 });
             }
             
         }
     }
     
     /**
      * Function to getEducation
      */
     
     getEducation() {
         this.manageHospitalServiceProvider.getDoctorEducation().subscribe( res => {
             if ( res.status == "success" ) {
                 this.doctorEducation = res.data.list;
                 console.log("doctorEducation.....", this.doctorEducation);
                 if(this.doctorEducation){
                     let educationLen = this.doctorEducation.length;
                     for( let n=0;n< educationLen;n++ ){
                         if( this.commonService.checkIsWeb() ){
                             this.doctorEducation[n].label = this.doctorEducation[n].name;
                             this.doctorEducation[n].value = this.doctorEducation[n];
                         }
                         if( this.doctorData.educationList ){
                             for( let m=0;m< this.doctorData.educationList.length;m++ ){
                                 if( this.doctorEducation[n].id == this.doctorData.educationList[m].id ){
                                     this.doctorData.educationList.splice( m,1 );
                                     if( this.commonService.checkIsWeb() ){
                                         this.doctorData.educationList.splice( m,0,this.doctorEducation[n].value );
                                     }else{
                                         this.doctorData.educationList.splice( m,0,this.doctorEducation[n] );
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
         } )
     }
                     
     /*
     * on change doctor edu
     */
                     
     getDoctorEdu( selectedValue: any ){
         this.selectEdu = false;
         if(this.doctorData.educationList){
             let eduLen = this.doctorData.educationList.length;
             if( this.commonService.checkIsWeb() ){
                 for ( let j = 0; j < eduLen; j++ ) {
                     this.doctorData.educationList[j].value = this.doctorData.educationList[j];
                     this.doctorData.educationList[j].label = this.doctorData.educationList[j].name;
                 }
             }
         }
     }
                     
     formatEducation(){
         return this.doctorData.educationList.map(education => education.name).join(", ");
     }
                             
     formatCategory (){
         return this.category.map(selectedCategory => selectedCategory.name).join(", ");
     }
                
     formatSpecialization (){
         return this.doctorData.specializationList.map(specialization => specialization.name).join(", ");
     }
                
     formatServices = () => {
         return this.doctorData.serviceList.map(service => service.name).join(", ");
     }
       
     /*
     * on city change
     */
                          
     cityChange() {
         this.selectCity = false;
             if(this.cityList){
             let cityLen = this.cityList.length;
             for(let i=0; i<cityLen; i++){
                 if(this.address.cityId == this.cityList[i].id){
                     this.address.cityName = this.cityList[i].name;
                 }
             }
            }
     }

     editProfile(){
         this.isProfileEdit = true;
         this.viewFlag = false;
         this.editFlag = true;
         this.imageUpload.file = this.doctorData.imageBase64;
     }

     cancelEdit(){
         this.isProfileEdit = false;
         this.viewFlag = true;
         this.editFlag = false;
         this.imageUpload.file = '';
         this.doctorData =  _.cloneDeep(this.cloneDoctorData , false);
         this.viewDoctorProfile();
     }

    /**
     * Function to upload image 
     **/
     uploadImage = ( userId: number ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
        });
     }                       
             
     /**
      * Function to upload image 
      **/
      editImage = ( fileId: number ) => {
         let imageData = {
             "userType": "USER_PROFILE_IMAGE",
             "mediaType": "IMAGE",
             "id": fileId, //file Id
             "file": this.imageUpload.file,
             "fileName": this.imageUpload.fileName,
             "isDefault": this.isDefault,
             "rank" : this.rank
         }
         this.commonService.editImage(imageData).subscribe(
             res => {
                 if ( res.status == "success" ) {
                 }
             },
             error => {
                 this.commonService.hideLoading();
                 this.commonService.showAlert( "Error", error.message );
         });
     }
                
                
        /**
         * Function to browse image 
         **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean) {
        this.rank++;
        this.isDefault = isDefault;
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        var that = this;
        this.imageUpload = {
            "file":"",
            "fileName":""
        }
        myReader.onloadend = (loadEvent:any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback)=>{
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if( callback ){
                    this.isUpload = isUpload;
                }
            });
            //console.log("image.src change listener=====",  this.imageUpload.file);
        };
             myReader.readAsDataURL(file);
             console.log("this.imageUpload.file change listener=====", this.imageUpload.file);
      }                 
            
     /**
      * Function to show coming soon popup
      */
     comingSoonPopup = () => {
         this.commonService.presentToast();
     }

     /*
     * Function to viewMore
     **/
    viewMore(){
        this.isViewMore = true;
    }

    viewLess(){
        this.isViewMore = false;
    }
}