import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from "@ngx-translate/core";
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";
import { SelectSearchableModule } from 'ionic-select-searchable';

import { HACreateDoctorPage } from "./hospital-admin-create-doctor";
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
        HACreateDoctorPage,
  ],
  imports: [
        IonicPageModule.forChild(HACreateDoctorPage),
        SharedModule,
        TranslateModule.forChild(),
        MultiSelectModule,
        SelectSearchableModule
  ],
  exports: [
        HACreateDoctorPage
   ]
})
export class HACreateDoctorPageModule {}
