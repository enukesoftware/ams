import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { HospitalAdminAssociatedDoctorListPage } from './hospital-admin-associated-doctor-list';

import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    HospitalAdminAssociatedDoctorListPage
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminAssociatedDoctorListPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    HospitalAdminAssociatedDoctorListPage
  ]
})
export class AssociatedDoctorListPageModule {}
