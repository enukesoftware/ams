import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Content, NavParams, MenuController, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorListDTO, HospitalListDTO } from '../../../interfaces/user-data-dto';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';

//import { DoctorListPage } from "../../components/doctor-list/doctor-list";

@IonicPage({
    name: 'HospitalAdminAssociatedDoctorListPage',
    segment: 'hospital-admin-associated-doctor-list'
})
@Component({
    selector: 'page-assoicated-doctor-list',
    templateUrl: 'hospital-admin-associated-doctor-list.html',
})
export class HospitalAdminAssociatedDoctorListPage {
    loadFinished: boolean = false;
    pageNo: number = 0;
    @ViewChild(Content) content: Content;
    userData: UserDataDTO = {};
    listOfDoctors: DoctorListDTO[];
    hospitalId: any;
    isPatientDoctorList: any;
    isAssociateDoctorlist: any;
    isSADoctorlist: any;
    noDataFound: boolean = false;
    fromHAOnboardPatient:boolean;
    fromPage:any;
    backPageUrl:any;
    showBackButton:boolean;


    constructor(public navCtrl: NavController, public navParams: NavParams, private location: Location, public platform: Platform, private menu: MenuController, private commonService: CommonService,
        private constants: Constants, private manageHospitalService: ManageHospitalServiceProvider) {
        /**
         *handle device back button
         */
        this.platform.registerBackButtonAction(() => {
            this.location.back();
        });
    }

    ionViewDidLoad = () => {
        this.menu.enable(true);
    }

    ionViewWillEnter = () => {
        this.menu.enable(true);
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        this.commonService.getFromStorage("isConfirmed").then((value) => {
            if (value) {
                this.commonService.removeFromStorage("isConfirmed");
            }
        });
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if (!result) {
                this.navCtrl.setRoot('LoginPage');
            } else {
                this.userData = result;
                this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN);
                this.commonService.fireSelectedEvent('#/hospital-admin-associated-doctor-list');
                if(this.navParams.get('fromPage')){
                    this.fromPage = this.navParams.get('fromPage');
                    
                    if(this.fromPage == 'HAOnboardPatient'){
                        this.fromHAOnboardPatient = true;
                        this.showBackButton = true;
                        this.backPageUrl = '#/onboard-patient/HADashboard/HAAssociatedDoctorList/0/0';
                    }else{
                        this.showBackButton = false;
                        this.fromHAOnboardPatient = false;
                    }
                    console.log("FROM_PAGE:",this.fromPage + " SHOW:"+this.showBackButton);
                }
                if (this.navParams.get('data')) {
                    this.hospitalId = this.navParams.get('data');
                    this.getListOfAssociatedDoctors(this.hospitalId, true);
                } else {
                    this.commonService.getFromStorage('hospitalId').then((value) => {
                        if (value) {
                            this.hospitalId = value;
                            this.getListOfAssociatedDoctors(this.hospitalId, true);
                        }
                    });
                }
            }
        });
    }

    /* function to get images in async */
    public getImageBase = () => {
        let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        for (let i = startIndex; i < this.listOfDoctors.length; i++) {
            let imageData = {
                'userType': "USER_PROFILE_IMAGE",
                'id': this.listOfDoctors[i].doctorId,
                'rank': 1,
            }
            this.commonService.downloadImage(imageData).subscribe(
                res => {
                    if (res.status == "success") {
                        console.log("image base 64=======>", res);
                        for (let j = startIndex; j < this.listOfDoctors.length; j++) {
                            if (this.listOfDoctors[j].doctorId == res.data.userId) {
                                this.listOfDoctors[j].imageBase64 = res.data.file;
                            }
                        }
                        console.log("this.listOfDoctors=======>", this.listOfDoctors);
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);*/
                });

        }
    }
    
    

    getListOfAssociatedDoctors(hospitalId: any, firstPageCall?: boolean) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.manageHospitalService.getAssociatedDoctorsList(hospitalId, this.pageNo).subscribe(
            res => {
                let tempPageNo = this.pageNo;
                if (res.status == "success") {
                    this.commonService.hideLoading();
                    if (res.data.list && res.data.list.length > 0 && firstPageCall) {
                        this.listOfDoctors = res.data.list;
                        this.getImageBase();
                        if ((tempPageNo + 1) >= res.data.totalPages) {
                            this.loadFinished = true;
                        }
                        this.formatAvailability();
                    } else if (res.data.list && res.data.list.length > 0) {
                        for (let j = 0; j < res.data.list.length; j++) {
                            this.listOfDoctors.push(res.data.list[j]);
                        }
                        this.getImageBase();
                        if ((tempPageNo + 1) >= res.data.totalPages) {
                            this.loadFinished = true;
                        }
                        this.formatAvailability();
                    } else {
                        this.loadFinished = true;
                        this.noDataFound = true;
                    }
                }
                this.commonService.setPaddingTopScroll(this.content);
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                //this.commonService.showAlert( "Error", error.message );
                this.commonService.setPaddingTopScroll(this.content);
            }
        );
    }




    /*
     * Function to do calculations for time related 
     * */
    formatAvailability = () => {
        var currentDate = new Date();
        var today = currentDate.getDay();
        for (let i = 0; i < this.listOfDoctors.length; i++) {
            var todayAvailability;
            // console.log("this.listOfDoctors", this.listOfDoctors[i]);
            let availabilities = this.listOfDoctors[i].timeAvailabilityList;
            let calculatedTime = this.commonService.calculateTime(availabilities);
            this.listOfDoctors[i].formattedAvailabilities = calculatedTime;
            let availabilityLength = this.listOfDoctors[i].formattedAvailabilities.length;
            for (let j = 0; j < availabilityLength; j++) {
                if (today == this.listOfDoctors[i].formattedAvailabilities[j].dayCount) {
                    //console.log( "todayAvailability=========================>", todayAvailability );
                }
            }
            this.listOfDoctors[i].todayAvailability = this.listOfDoctors[i].todaysTimeAvailabilityList;
            if (this.listOfDoctors[i].todaysTimeAvailabilityList && this.listOfDoctors[i].todaysTimeAvailabilityList.length > 0) {
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.commonService.getTimeInMinutes(currentTime);
                let todayTimeLen = this.listOfDoctors[i].todayAvailability.length;
                for (let j = 0; j < todayTimeLen; j++) {
                    let startTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.commonService.getTimeInMinutes(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].toTime), true);

                    if (currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes) {
                        timeSlotFound = true;
                        this.listOfDoctors[i].todaysAvailabilityString = this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].fromTime), false, true) + " - " + this.commonService.convertTo12hrsFormat(new Date(this.listOfDoctors[i].todaysTimeAvailabilityList[j].toTime), false, true);
                    }
                }
                if (!timeSlotFound) {
                    this.listOfDoctors[i].todaysAvailabilityString = "Unavailable";
                }
            } else {
                this.listOfDoctors[i].todaysAvailabilityString = "Unavailable";
            }
        }
    }


    /*
     * Function to load more data when clicked on load more button
     * */
    public loadMoreData = () => {
        this.pageNo = this.pageNo + 1;
        this.getListOfAssociatedDoctors(this.hospitalId);
    }

    comingSoonPopup() {
        // console.log("on comingSoonPopup 288.....");
        this.commonService.presentToast();
    }

}