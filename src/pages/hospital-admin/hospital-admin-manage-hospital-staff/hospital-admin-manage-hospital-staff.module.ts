import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HospitalAdminManageHospitalStaffPage } from './hospital-admin-manage-hospital-staff';
import { SharedModule } from '../../components/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    HospitalAdminManageHospitalStaffPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminManageHospitalStaffPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    HospitalAdminManageHospitalStaffPage,
  ]
})
export class HospitalAdminManageHospitalStaffPageModule {}
