import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform, MenuController } from 'ionic-angular';
import { UserDataDTO, StaffDataDTO } from "../../../interfaces/user-data-dto";
import { AuthService } from "../../../providers/authService/authService";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { CommonService } from "../../../providers/common-service/common.service";
import { Location } from '@angular/common';
import { HospitalStaffServiceProvider } from '../../../providers/hospital-staff-service/hospital-staff-service';

/**
 * Generated class for the HospitalAdminManageHospitalStaffPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage( {
  name: 'HospitalAdminManageHospitalStaffPage',
  segment: 'hospital-admin-manage-hospital-staff'
})
@Component({
  selector: 'page-hospital-admin-manage-hospital-staff',
  templateUrl: 'hospital-admin-manage-hospital-staff.html',
})
export class HospitalAdminManageHospitalStaffPage {
  @ViewChild( Content ) content: Content;

  isManageSysAdmin: boolean = false;
  userData: UserDataDTO = {};
  hospitalId: any;
  fromHospitalAdminStaffList: string = "hospitalAdminStaffList";
  pageNo: number = 0;
  loadFinished: boolean = false;
  //assign sys admins dto
  staffList: StaffDataDTO[];
  constructor( private platform: Platform, public authService: AuthService, private location: Location, private constants: Constants, private menu: MenuController, public navCtrl: NavController, public navParams: NavParams, public hospitalStaffServiceProvider: HospitalStaffServiceProvider, public commonService: CommonService ) {
      /**
       *handle device back button
       */
       this.platform.registerBackButtonAction(() => {
           this.location.back();
         });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HospitalAdminManageHospitalStaffPage');
  }

  ionViewWillEnter = () => {
      this.menu.enable( true );
      //check if user is authorized or not to access the screen if not redirect him to login screen
      this.isAuthorized();
  }

  /*
  * Function to check if user is authorized or not to access the screen
  * */
  isAuthorized = () => {
      this.commonService.isLoggedIn(( result ) => {
          if ( !result ) {
              this.navCtrl.setRoot( 'LoginPage' );
          } else {
              this.userData = result;
              this.commonService.getFromStorage( "hospitalId" ).then(( value ) => {
                  if ( value ) {
                      this.hospitalId = value;
                      if(this.userData.role == this.constants.ROLE_SYSTEM_ADMIN){
                        this.getHospitalStaffList( this.hospitalId,true );
                      }else{
                        this.getHospitalStaffList( "",true );
                      }
                      
                  }
              } );
              this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );
              this.commonService.fireSelectedEvent( '#/hospital-admin-manage-hospital-staff' );
          }
      } );
  }

  /* function to get images in async */ 
  public getImageBase =()=>{
      let startIndex = this.pageNo * this.constants.PAGE_SIZE;
      for (let i = startIndex; i <this.staffList.length; i++) {
          let imageData = {
                  'userType': "USER_PROFILE_IMAGE",
                  'id': this.staffList[i].userId,
                  'rank': 1,
          }   
      this.commonService.downloadImage( imageData ).subscribe(
      res => {
          if ( res.status == "success" ) {
              console.log("image base 64=======>", res);
              for( let j=startIndex; j<this.staffList.length; j++ ){
                  if(this.staffList[j].userId == res.data.userId){
                      this.staffList[j].imageBase64 = res.data.file;
                  }
              }
              console.log("this.listOfStaff=======>", this.staffList);
          }
      },
      error => {
          this.commonService.hideLoading();
    /*      let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
          this.commonService.presentToast(errorMsg);*/
  });

      }
  }

  //To do : get hospital staff list on web service response
  public getHospitalStaffList( id,firstPageCall?: boolean ) {
      this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
      this.hospitalStaffServiceProvider.getHospitalStaffList( id,this.pageNo).subscribe(
          res => {
              this.commonService.hideLoading();
              let tempPageNo = this.pageNo;
              if ( res.status == "success" ) {
                  if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                      this.staffList = res.data.list;
                      this.getImageBase();
                      console.log("this.staffList", this.staffList);
                      if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                          this.loadFinished = true;
                      }
                  }else if( res.data.list && res.data.list.length > 0 ){
                      for( let j=0;j<res.data.list.length;j++ ){
                          this.staffList.push(res.data.list[j]);
                      }
                      this.getImageBase();
                      if ( ( tempPageNo + 1 ) >= res.data.totalPages ) {
                          this.loadFinished = true;
                      }
                  }else{
                      this.loadFinished = true;
                  }
              }
              this.commonService.setPaddingTopScroll( this.content );
          },
          error => {
              this.commonService.hideLoading();
              let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
              this.commonService.presentToast(errorMsg);
              this.commonService.setPaddingTopScroll( this.content );
          }
      );
  }


  /*
  * Function to load more data when clicked on load more button
  * */
  public loadMoreData = () => {
      this.pageNo = this.pageNo + 1;
      if(this.userData.role == this.constants.ROLE_SYSTEM_ADMIN){
        this.getHospitalStaffList( this.hospitalId );
      }else{
        this.getHospitalStaffList( "" );
      }
  }  
}
