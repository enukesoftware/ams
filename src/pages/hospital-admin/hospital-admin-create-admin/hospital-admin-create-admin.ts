import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Content, NavParams, MenuController, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO, AddressDto, HospitalAdminDto } from "../../../interfaces/user-data-dto";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CameraService } from '../../../providers/camera-service/camera.service';

import { NgForm } from '@angular/forms';

@IonicPage({
    name: 'HospitalAdminCreateAdminPage',
    segment: 'hospital-admin-create-admin/:mode/:id'
})
@Component({
  selector: 'page-hospital-admin-create-admin',
  templateUrl: 'hospital-admin-create-admin.html',
})
export class HospitalAdminCreateAdminPage {
    viewMode: boolean;
    @ViewChild(Content) content: Content;
    selectCity: boolean;
    cityList: any;
    formSubmitted: boolean;
    isEditSysAdminMode:any;
    selectedAdminDetails:HospitalAdminDto =  {};
    showDetails =false;
    userData: UserDataDTO = {};
    showMobileViewFooter: boolean = false;
    isManageSysAdmin: boolean = false;
    userActiveRole: string;
    cancelUrl: string = "";
    userId: string;
    address: AddressDto = {};
    mode: any;
    isCreate: boolean = false;
    isMyProfile: boolean = false;
    disableEmail: boolean;
    inputType: any = "text";
    rank: number;
    isDefault: boolean;
    imageUpload: any = {};
    isUpload: boolean = false;
    hospitalId: any;
    isMobileWeb:boolean = false;
    
    constructor( public navCtrl: NavController, public cameraService : CameraService, private constants: Constants, private location: Location, public platform: Platform,public navParams: NavParams, private menu: MenuController, public commonService: CommonService, private manageHospitalServiceProvider:ManageHospitalServiceProvider) {
         /**
         *handle device back button
         */
         this.platform.registerBackButtonAction(() => {
             this.location.back();
           });
    }
      
    ionViewWillEnter(){
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        
        this.commonService.getFromStorage("hospitalId").then((value)=>{
            if(value){
                this.hospitalId = value;
            }
        });
        
        this.rank = 0;
    }

   /**
    * Function to check if user is authorized or not to access the screen
    */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                this.navCtrl.setRoot( 'LoginPage' );
            }else{
              //get list of cicties function call
                this.listOfCities();
                this.userData = result;
                this.userId = result.userId;
                this.isMobileWeb = this.commonService.checkMobileweb();
                this.mode = this.navParams.get('mode');
               /* if(this.navParams.get('mode') == "create"){
                    this.disableEmail = false;
                    this.isCreate = true;
                } else{
                    this.isCreate = false;
                    this.isEditSysAdminMode = true;
                    this.disableEmail = true;
                    this.viewMode = true;
                    if(this.navParams.get('id') == this.userId || this.navParams.get('id') == "0"){
                        this.isMyProfile = true;
                        this.getAdminDetails(this.userId);
                    }else{
                        this.userId = this.navParams.get('id');
                        this.isMyProfile = false;
                        this.getAdminDetails(this.userId);
                    }
                }*/
                if(this.navParams.get('mode') == "create"){
                    this.isEditSysAdminMode = false;
                    this.isCreate = true;
                    this.viewMode = false;
                    this.disableEmail = false;
                } else if( this.navParams.get('mode') == "view"){
                    this.viewMode = true;
                    this.isEditSysAdminMode = false;
                    this.isCreate = false;
                    this.disableEmail = false;
                    if(this.navParams.get('id') == this.userId || this.navParams.get('id') == "0"){
                        this.isMyProfile = true;
                        this.getAdminDetails(this.userId);
                    }else{
                        this.userId = this.navParams.get('id');
                        this.isMyProfile = false;
                        this.getAdminDetails(this.userId);
                    }
                }else{
                    this.isEditSysAdminMode = true;
                    this.viewMode = false;
                    this.isCreate = false;
                    this.disableEmail = true;
                    if(this.navParams.get('id') == this.userId || this.navParams.get('id') == "0"){
                        this.isMyProfile = true;
                        this.getAdminDetails(this.userId);
                    }else{
                        this.userId = this.navParams.get('id');
                        this.isMyProfile = false;
                        this.getAdminDetails(this.userId);
                    }
                }
                this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );
                if( this.isMyProfile ){
                    this.commonService.fireSelectedEvent('#/hospital-admin-create-admin/view/0');
                }else{
                    this.commonService.fireSelectedEvent('#/hospital-admin-manage-hospital-admin');
                }
                if( !this.commonService.checkIsWeb() && !this.isMyProfile ){
                    this.menu.enable( false );
                }else{
                    this.menu.enable( true );
                }
                
            }
        });
    }
    
    cancelEdit = () => {
        this.getAdminDetails(this.userId);
        this.eidtProfile(true);
    }
    

   /**
    * Function to get hospital admin details
    */
    getAdminDetails(id){
        console.log("on get details");
        let adminId = id;
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.manageHospitalServiceProvider.getHospitalAdminDetails(adminId).subscribe(
            res=>{
                this.commonService.hideLoading();
                if(res.status == "success"){
                    this.selectedAdminDetails = res.data;
                    //this.AdminDetails =this.selectedAdminDetails;
                    this.address = res.data.address;
                    console.log("address at 168...", this.address);
                    if( this.selectedAdminDetails.imageBase64 ){
                        this.imageUpload.file = this.selectedAdminDetails.imageBase64;
                    } 
                    this.getImageBase();
                    if(res.data.address.cityId != null){
                        this.address.cityId = res.data.address.cityId;
                    }else{
                        this.address.cityId = this.cityList[0].id;
                    }
                    
                }
                this.commonService.setPaddingTopScroll( this.content );
            },
            error=>{
             this.commonService.hideLoading();
             this.commonService.setPaddingTopScroll( this.content );
            })
    }

    /* function to get images in async */ 
    public getImageBase =()=>{
        //let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        
        console.log("on get image base");
            let imageData = {
                    'userType': "USER_PROFILE_IMAGE",
                    'id': this.selectedAdminDetails.userId,
                    'rank': 1,
             }   
        this.commonService.downloadImage( imageData ).subscribe(
        res => {
            if ( res.status == "success" ) {
                console.log("image base 64=======>", res);
                this.selectedAdminDetails.imageBase64 = res.data.file;
            }
        },
        error => {
            this.commonService.hideLoading();
            /*let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);*/
    });
    }
    
    
    
    
   /**
    * get cityList
    */
    listOfCities = () => {
        this.commonService.getListOfCities().subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.cityList = res.data.list;
//                    this.address.cityId = this.cityList[0].id;
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                //this.commonService.showAlert( "Error", error.message );
            }
        );
    }

    /*
    * on change city
    * */
    cityChange() {
        this.selectCity = false;
        if(this.cityList){
            let cityLen = this.cityList.length;
            for(let i=0; i<cityLen; i++){
                if(this.address.cityId == this.cityList[i].id){
                    this.address.cityName = this.cityList[i].name;
                }
            }   
        }
    }


   /**
    * Save hospital details
    */
    saveHospitalAdmin(formData: NgForm){
        this.formSubmitted = true;
        if(formData.valid){    
            this.formSubmitted = false;
            this.commonService.showLoading( "Please wait" );
            this.selectedAdminDetails.addressRequestWrapper = this.address;
            this.commonService.getFromStorage("hospitalId").then((value)=>{
                if(value){
                    if(this.mode == "view"){
                        //                  
                    }else{
                        this.selectedAdminDetails.userId = this.userId;
                    }
                    this.selectedAdminDetails.hospitalId = value;
                    
                    this.manageHospitalServiceProvider.saveHospitalAdminDetails(this.selectedAdminDetails, this.mode).subscribe(
                        res=>{
                        this.commonService.hideLoading();
                        this.commonService.presentToast(res.message);
                        this.selectedAdminDetails = res.data;
                        if( this.isMyProfile ){
                            this.userData = res.data;
                            this.commonService.setInStorage( 'userData',res.data );
                        }
                        console.log("userData",this.userData);
                        if( !this.selectedAdminDetails.fileId && this.isUpload ){
                            this.uploadImage(this.selectedAdminDetails.userId);
                        }else if( this.isUpload && this.selectedAdminDetails.fileId){
                            this.editImage(this.selectedAdminDetails.fileId);
                        }                         

                        /*if( this.mode == 'create' ){
                            this.uploadImage(this.userData.userId);
                        }else{
                            
                            console.log("selectedAdminDetails in edit mode ",this.selectedAdminDetails);
                           this.editImage(this.selectedAdminDetails.fileId);
                        }*/
                        
                        setTimeout(()=>{
                            this.navCtrl.setRoot( "HospitalAdminManageHospitalAdminPage" );
                        }, 3000);
                    }, error=>{
                        this.commonService.hideLoading();
//                        this.commonService.showAlert( "Error", error.message );
                        this.commonService.hideLoading();
                        this.commonService.presentToast(error.message);
                        /*setTimeout(()=>{
                            this.navCtrl.setRoot( "HospitalAdminManageHospitalAdminPage" );
                        }, 3000);*/
                    })
                }
            })
        }
    }

   /**
    * Edit profile button click
    */
    eidtProfile(isCancel?: boolean){
        console.log("in eidtProfile")
        this.isEditSysAdminMode = !this.isEditSysAdminMode;
        this.viewMode = false;
        this.isCreate = false;
        if( isCancel ){
            this.viewMode = true;
            this.imageUpload.file = '';
        }else{
            this.viewMode = false;
        }
        if( this.isMyProfile ){
            if( isCancel ){
                this.disableEmail = true;
                this.viewMode = true;
                this.imageUpload.file = '';
            }else{
                this.disableEmail = false;
            }
            this.isMyProfile = true;
            this.imageUpload.file = this.selectedAdminDetails.imageBase64;
        }else{
            this.disableEmail = true;
            this.isMyProfile = false;
            if(isCancel){
                this.viewMode = true;
                this.imageUpload.file = '';
            }else{
                this.viewMode = false;  
            }
            this.imageUpload.file = this.selectedAdminDetails.imageBase64;
        }
    }
    
    /**
     * Function to activate or deactivate hospital-admin
     */

    public activateDeactivateHospitalAdmin =() => {
          let activationStatus:any;
          this.commonService.getActivationStatus(this.selectedAdminDetails,(cb)=>{
              activationStatus =cb;
          });
          
          let hospitalAdminObj ={
                  userId:this.selectedAdminDetails.userId,
                  action:activationStatus.Action
        }
          
          let activationPopUpMsg = this.commonService.getTranslate('CONFIRM_AD_MSG1',{})+  this.commonService.getTranslate(activationStatus.text,{})
          let warningTitle =  this.commonService.getTranslate('WARNING',{});
          let alert = this.commonService.confirmAlert(warningTitle, activationPopUpMsg+'?');
          alert.setMode("ios");
          alert.present();
          alert.onDidDismiss((data) => {
              if(data == true){  
          this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
          this.manageHospitalServiceProvider.activateDeactivateHospitalAdmin(hospitalAdminObj).subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.commonService.hideLoading();
                    if(this.selectedAdminDetails.blockStatus =="UNBLOCK"){
                        console.log("in if",this.selectedAdminDetails);
                        this.selectedAdminDetails.blockStatus ="BLOCK";
                    } 
                    else if(this.selectedAdminDetails.blockStatus =="BLOCK"){
                       // console.log("in else",this.AdminDetails);
                        this.selectedAdminDetails.blockStatus ="UNBLOCK";
                    }
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
                else{
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
            }
        },
        error => {
            this.commonService.hideLoading();
            this.commonService.presentToast(error.message);
        });
              }});
      }
    
    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean) {
        this.rank++;
        this.isDefault = isDefault;
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        this.imageUpload = {
            "file":"",
            "fileName":""
        }
        myReader.onloadend = (loadEvent:any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback)=>{
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if( callback ){
                    this.isUpload = isUpload;
                }
            });
        };
         myReader.readAsDataURL(file);
    }

     /**
     * Function to upload image 
     **/
     uploadImage = ( userId: any ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                    console.log("upload image sys admin=================", res);
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
        });
    }

    /**
     * Function to upload image 
     **/
     editImage = ( fileId: number ) => {
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": fileId, //file Id
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank" : this.rank
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.editImage(imageData).subscribe(
            res => {
                if ( res.status == "success" ) {
                    console.log("upload image sys admin=================", res);
                }
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.showAlert( "Error", error.message );
        });
    }

         /***
     * Function to open camera/photo gallery
     */
     protected onCamera = () => {
         try{
            // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
             this.cameraService.loadImage(this.successCallback,this.errorCallback);
         }catch(e){
             
         }
     }

     /**
      * Function to handle camera success callback
      * @param success
      */
     private successCallback = ( base64Image: any ) => {
         this.isUpload = true;
         this.rank++;
         this.isDefault = true;
         //this.imageUpload.file = base64Image;
         this.commonService.convertImgToBase64(base64Image, (callback)=>{
             this.imageUpload.file = callback;
         })
     }
     
     /**
      * Function to handle camera error callback
      * @param error
      */
     private errorCallback = ( error: any ) => {
        console.log( 'Unable to load profile picture.' ,error);
     }

    showComingSoon = () => {
        this.commonService.presentToast();        
    }   
}