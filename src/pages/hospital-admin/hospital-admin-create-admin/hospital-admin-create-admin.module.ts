import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HospitalAdminCreateAdminPage } from './hospital-admin-create-admin';
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    HospitalAdminCreateAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminCreateAdminPage),
    TranslateModule.forChild(),
    SharedModule
  ],
  exports: [
    HospitalAdminCreateAdminPage
  ]
})
export class HospitalAdminCreateAdminPageModule {}
