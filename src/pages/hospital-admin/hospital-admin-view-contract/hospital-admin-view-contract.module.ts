import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { HospitalAdminViewContractPage } from './hospital-admin-view-contract';
import { SharedModule } from "../../components/shared.module";
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  declarations: [
       HospitalAdminViewContractPage
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminViewContractPage),
    SharedModule,
    TranslateModule.forChild(),
    CalendarModule
  ],
  exports: [
       HospitalAdminViewContractPage
  ]
})
export class HospitalAdminViewContractPageModule {}
