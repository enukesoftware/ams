import { Component, ViewChild,NgZone } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, MenuController, Events, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, DoctorListDTO, HospitalListDTO } from '../../../interfaces/user-data-dto';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";

@IonicPage( {
    name: 'HospitalAdminViewContractPage',
    segment: 'hospital-admin-view-contract/:from/:id'
} )
@Component({
    selector: 'page-hospital-admin-view-contract',
    templateUrl: 'hospital-admin-view-contract.html',
})
export class HospitalAdminViewContractPage {
    @ViewChild(Content) content: Content;

    userData: UserDataDTO = {};
    selectedHospital: HospitalListDTO = {};
    timeSlotArray: any[];
    weekdays: any[];
    visitTypes: any[];
    timeSlot: any;
    editFlag: boolean = false;
    docProfileId: string;
    createFlag: boolean = false;
    isCreateContract: any;
    viewFlag: boolean = true;
    fromPage: any;

    constructor( public navCtrl: NavController, private location: Location, public platform: Platform, public evts: Events, private _ngZone: NgZone,public manageHospitalServiceProvider: ManageHospitalServiceProvider, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
        private constants: Constants ) {
        this.editFlag = false;
        this.viewFlag = true;
        this.isCreateContract = false;
        this.fromPage = this.navParams.get('from');
        this.timeSlotArray = [];
        this.weekdays = [
            { id: 1, name: 'Monday' },
            { id: 2, name: 'Tuesday' },
            { id: 3, name: 'Wednesday' },
            { id: 4, name: 'Thursday' },
            { id: 5, name: 'Friday' },
            { id: 6, name: 'Saturday' },
            { id: 7, name: 'Sunday' }
        ]
        this.visitTypes = [
            { id: 1, name: 'Appointment' },
            { id: 2, name: 'Tokens' }
        ]
        this.timeSlot = {
            day: 'Monday',
            startTime: '',
            startTimeMinutes: '',
            endTime: '',
            endTimeMinutes: '',
            visitType: 'Appointment',
            visitValue: ''
        }
        
        this.evts.subscribe('contractFlags', (flags) => {
            this.viewFlag = flags.viewFlag;
            this.editFlag = flags.editFlag;
            this.createFlag = flags.createFlag;
        });
        
        /**
         *handle device back button
         */
        this.platform.registerBackButtonAction(() => {
             this.location.back();
          });
    }

    ionViewDidLoad() {
        this.menu.enable( true );
    }

    ionViewWillEnter = () => {
        this.menu.enable( true );
        this.fromPage = this.navParams.get('from');
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }
    
    editContract = () => {
        this._ngZone.run(() => {
            this.editFlag = true;
            this.viewFlag = false;
            this.isCreateContract = false;
            this.commonService.setPaddingTopScroll( this.content );
        });
    }
    
    /**
     * Function to handle cancel btn
     **/
    cancelEdit = () => {
        this._ngZone.run(() => {
            this.editFlag = false;
            this.viewFlag = true;
            this.createFlag = false;
        });
    }
    
    
    /**
     * Function to handle Ok btn
     **/
    createContract = () => {
        console.log('event published this.isCreateContract', this.isCreateContract);
        setTimeout(() => {
            this.isCreateContract = false;
           }, 100);
           
        setTimeout(() => {
            this.isCreateContract = true;
           }, 100);
    }
    

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn(( result ) => {
            if ( !result ) {
                this.navCtrl.setRoot( 'LoginPage' );
            } else {
                
                console.log("toContractPage on viewContractPage....", this.navParams.get("from"));
                this.commonService.setInStorage("toContractPage", this.navParams.get("from")) ;
                
                this.userData = result;
                this.commonService.isPatientAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN );
                if(this.navParams.get("from") == 'DoctorAssociatedHospital'){
                    this.commonService.fireSelectedEvent('#/doctor-associate-hospitals');
                    let hospitalId = this.navParams.get('id');
                    this.commonService.setInStorage("hospitalId", hospitalId);
                }else{
                    let doctorProfileInfo = this.navParams.get('id');
                    this.commonService.getFromStorage("hospitalId").then(( value ) => {
                     if(value){
                         let doctorProfileId = this.navParams.get('id');
                         this.docProfileId = doctorProfileId;
                         this.commonService.setInStorage("doctorProfileId", doctorProfileId);
                         this.getContractInfo(doctorProfileId, value);
                     }
                    });
                    this.commonService.fireSelectedEvent('#/hospital-admin-associated-doctor-list');
                }
                this.commonService.setPaddingTopScroll( this.content );
            }
        } );
    }
    
    
    /**
     * Function to handle on change event of time picker 
     **/
    onChangeStartTime = ( sTime ) => {
        this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes( sTime );
    }

    /**
     * Function to handle on change event of time picker 
     **/
    onChangeEndTime = ( eTime ) => {
        if ( this.commonService.getTimeInMinutes( eTime ) > this.timeSlot.startTimeMinutes ) {
            this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes( eTime );
        } else {
            this.commonService.presentToast( "Please select valid end time." )
            this.timeSlot.endTime = '';
        }
    }
    
    /**
     * Function to remove time slot from array
     * @param index of time slot
     */
    removeTimeSlot = ( index ) => {
        this.timeSlotArray.splice( index, 1 );
    }
    
    /**
     * Function to get contract info
     **/
    getContractInfo(doctorProfileId, hospitalId){
        if(this.navParams.get('from') != "hospitalAdminCreate"){
            this.createFlag = false;
            /*this.manageHospitalServiceProvider.getDoctorContractDetails(doctorProfileId, hospitalId).subscribe(res=>{
                console.log("contractDetails.....", res);
             }, err=>{
                 console.log("err in getting doctorContract....", err);
             });*/
        } else{
            this.createFlag = true;
            this.viewFlag = false;
            this.editFlag = false;
        }
    }
    
    /**
     * Function to add time slot to time array
     **/
   addTimeSlotClick = () => {
       this.commonService.addTimeSlotClick(this.timeSlot, false);
       /*if ( this.timeSlot.day != '' ) {
           if ( this.timeSlot.startTime != '' ) {
               if ( this.timeSlot.endTime != '' ) {
                   if ( this.timeSlot.visitValue != '' ) {
                       if ( this.isTimeRepeat( this.timeSlot ) ) {
                           this.commonService.presentToast( "Please select valid time slot." );
                       } else {
                           this.timeSlotArray.push( JSON.parse( JSON.stringify( this.timeSlot ) ) );
                           this.timeSlot = {
                               day: 'Monday',
                               startTime: '',
                               startTimeMinutes: '',
                               endTime: '',
                               endTimeMinutes: '',
                               visitType: 'Appointment',
                               visitValue: ''
                           }
                       }
                   } else {
                       this.commonService.presentToast( "Please enter valid value for Appointment/Tokens." );
                   }
               } else {
                   this.commonService.presentToast( "Please select the end time." )
               }
           } else {
               this.commonService.presentToast( "Please select the start time." )
           }
       } else {
           this.commonService.presentToast( "Please select a day." )
       }*/

   }
   
   /**
    * Function to validate time slots 
    **/
   isTimeRepeat = ( tempSlot ) => {
       if ( this.timeSlotArray.length == 0 ) { return false; }
       for ( let i = 0; i < this.timeSlotArray.length; i++ ) {
           if ( tempSlot.day == this.timeSlotArray[i].day && tempSlot.startTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.startTimeMinutes <= this.timeSlotArray[i].endTimeMinutes ) {
               return true;
           }
           if ( tempSlot.day == this.timeSlotArray[i].day && tempSlot.endTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes <= this.timeSlotArray[i].endTimeMinutes ) {
               return true;
           }
           if ( tempSlot.day == this.timeSlotArray[i].day && tempSlot.startTimeMinutes < this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes > this.timeSlotArray[i].endTimeMinutes ) {
               return true;
           }
       }
       return false;
   }
    
    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
        this.commonService.presentToast();
    }

}