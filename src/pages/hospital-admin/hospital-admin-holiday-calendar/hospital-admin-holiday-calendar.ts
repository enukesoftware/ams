import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, MenuController, Platform, Events } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import * as moment from 'moment';


import { CommonService } from '../../../providers/common-service/common.service';
import { UserDataDTO, DoctorDataDto, AddressDto, serviceAndSpecializationDataDto, HospitalListDTO } from "../../../interfaces/user-data-dto";
import { Constants } from '../../../providers/appSettings/constant-settings';
import { DoctorRoleServiceProvider } from '../../../providers/doctor-role-service/doctor-role-service';
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CalendarModule } from 'primeng/calendar';
//import { groupByDatePipe } from '../../../pipes/groupByDate.pipe';

//3rd party npm module
import * as _ from 'lodash';

/**
 * Generated class for the HospitalAdminHolidayCalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
    name: 'HospitalAdminHolidayCalendarPage',
    segment: 'hospital-admin-holiday-calendar/:mode/:adminId/:fromPage'
})

@Component({
    selector: 'page-hospital-admin-holiday-calendar',
    templateUrl: 'hospital-admin-holiday-calendar.html',
})
export class HospitalAdminHolidayCalendarPage {
    @ViewChild(Content) content: Content;
    loadFinishedHospital: boolean;
    hospitals: any;
    currentSegment: string = 'hospitalInfo';
    hospitalPageNo: number = 0;
    userData: UserDataDTO = {};
    doctorId: any;
    pageNo: number = 0;
    pageName: any;
    feedbackData = [];
    doctorServices: any[];
    doctorSpecialities: any[];
    viewMoreSpFlag: boolean = false;
    lessThen3Sp: boolean = true;
    initialSpecialities: any[];
    windowWidth: number;
    today: any;
    todayForWeb: any;
    todayDateStr: any;

    viewMoreServiceFlag: boolean = false;
    lessThen3Service: boolean = true;
    initialServices: any[];
    isSkipAndExplore: boolean = false;
    redirectParam: any;
    popover: any;
    adminData: any;
    adminId: any;
    fromPage: any;
    mode: any;
    viewFlag: boolean = false;
    editFlag: boolean = false;
    createFlag: boolean = false;
    formSubmitted: boolean = false;
    applyLeaveData: any = {};
    startDateValue: any;
    endDateValue: any;
    startTimeValue: any;
    endTimeValue: any;
    viewLeaves: any;
    startDateValueClone: any;
    endDateValueClone: any;
    startTimeValueClone: any;
    endTimeValueClone: any;
    hospitalsClone: any;
    leaveDescriptionClone: any;
    leaveDescription: any;
    leaveIds: any[] = [];
    fromDateView: any;
    fromTimeView: any;
    toDateView: any;
    toTimeView: any;

    backPageUrl: any;
    homePageUrl:any;
    showBackButton:boolean;

    constructor(private evts: Events, private doctorRoleServiceProvider: DoctorRoleServiceProvider, public platform: Platform, public location: Location, private _ngZone: NgZone, public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private commonService: CommonService,
        private constants: Constants, private doctorRoleService: DoctorRoleServiceProvider, public manageHospitalServiceProvider: ManageHospitalServiceProvider) {
        this.platform.registerBackButtonAction(() => {
            this.location.back();
        });
        this.menu.enable(true);
    }

    ionViewDidLoad() {
        this.menu.enable(true);
    }

    ionViewWillEnter = () => {
        this.menu.enable(true);

        this.today = this.convertDate();
        this.todayForWeb = new Date();
        // console.log("today--->", this.today);
        this.todayDateStr = this.commonService.formatDateString(new Date());

        this.adminId = this.navParams.get('adminId');
        this.fromPage = this.navParams.get('fromPage');
        this.mode = this.navParams.get('mode');
        console.log("ADMIN....................", this.adminId);

        this.changeMode(this.mode);


        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
        this.windowWidth = this.commonService.getWindowWidth();



        console.log("PAGE....................", this.navParams.get('fromPage'));


    }

    convertDate = () => {
        var d = new Date(moment().format("DD-MM-YYYY hh:mm:ss"));

        return [d.getFullYear(), this.pad(d.getMonth() + 1), this.pad(d.getDate())].join('-')[this.pad(d.getHours()), this.pad(d.getMinutes()), this.pad(d.getSeconds())];
    }

    pad = (s) => {
        return (s < 10) ? '0' + s : s;
    }

    /*
   * Function to check if user is authorized or not to access the screen
   * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if (!result) {
                this.navCtrl.setRoot('LoginPage');
            } else {
                this.userData = result;

                // this.commonService.isAuthorizedToViewPage( this.navCtrl, this.constants.ROLE_DOCTOR );
                if (this.fromPage && this.fromPage == "HAHolidayCalendar") {
                    this.showBackButton = true;
                    this.commonService.fireSelectedEvent('#/hospital-admin-manage-hospital-admin');
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN);
                    this.backPageUrl = '#/hospital-admin-manage-hospital-admin';
                    this.homePageUrl = '#/hospital-admin-dashboard/0';
                    this.getAdminDetails(this.adminId);
                } else if (this.fromPage && this.fromPage == 'HAStaffHolidayCalendar') {
                    this.showBackButton = true;
                    this.commonService.fireSelectedEvent('#/hospital-admin-manage-hospital-staff');
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_ADMIN);
                    this.backPageUrl = '#/hospital-admin-manage-hospital-staff';
                    this.homePageUrl = '#/hospital-admin-dashboard/0';
                    this.commonService.getFromStorage("selectedStaff").then((value) => {
                        if (value) {
                            console.log("Staff:", value);
                            this.adminData = value;
                            this.adminId = this.adminData.userId;
                        }
                    });
                    this.commonService.setPaddingTopScroll(this.content);
                }else if (this.fromPage && this.fromPage == 'HSRoleHolidayCalendar') {
                    this.showBackButton = false;
                    this.commonService.fireSelectedEvent('#/hospital-admin-holiday-calendar/'+this.mode+"/"+this.adminId+"/"+this.fromPage);
                    this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_STAFF);
                    this.backPageUrl = '#/staff-role-dashboard/0';
                    this.homePageUrl = '#/staff-role-dashboard/0';
                    this.commonService.getFromStorage("selectedStaff").then((value) => {
                        if (value) {
                            console.log("Staff:", value);
                            this.adminData = value;
                            this.adminId = this.adminData.userId;
                        }
                    });
                    this.commonService.setPaddingTopScroll(this.content);
                } else {
                    this.showBackButton = true;
                    this.getAdminDetails(this.adminId);
                }
                // else {
                //     this.commonService.fireSelectedEvent('#/doctor-role-holiday-calendar/create/0/DRHolidayCalendar');
                // }
                // this.commonService.setPaddingTopScroll( this.content );
                this.commonService.getFromStorage("userLoginRole").then(value => {
                    // if(value){
                    //     if(value == this.constants.ROLE_DOCTOR){
                    //         this.showDrPrefix = true;
                    //     }else{
                    //         this.showDrPrefix = false;
                    //     }
                    // } else{
                    //     this.showDrPrefix = false;
                    // } 
                });




                // this.getListOfAssociatedHospitals(this.adminId,true);
            }
        });
    }

    /**
     * Function to get hospital admin details
     */
    getAdminDetails(id) {
        let adminId = id;
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.manageHospitalServiceProvider.getHospitalAdminDetails(adminId).subscribe(
            res => {
                this.commonService.hideLoading();
                if (res.status == "success") {
                    this.adminData = res.data;
                    console.log("ADMIN-------", this.adminData);
                    //this.AdminDetails =this.selectedAdminDetails;
                    // this.address = res.data.address;
                    // console.log("address at 168...", this.address);
                    // if( this.selectedAdminDetails.imageBase64 ){
                    //     this.imageUpload.file = this.selectedAdminDetails.imageBase64;
                    // } 
                    // this.getImageBase();
                    // if(res.data.address.cityId != null){
                    //     this.address.cityId = res.data.address.cityId;
                    // }else{
                    //     this.address.cityId = this.cityList[0].id;
                    // }

                }
                this.commonService.setPaddingTopScroll(this.content);
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.setPaddingTopScroll(this.content);
            })
    }

    /*
    * getDoctor Assosciated Hospitals
    * */

    getListOfAssociatedHospitals(doctorId: any, firstPageCall?: boolean) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.doctorRoleServiceProvider.getAssociatedHospitalsList(doctorId, this.pageNo).subscribe(
            res => {
                //this.commonService.presentToast("List of cities success.......");
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if (res.status == "success") {
                    //this.listOfHospitals = [];
                    // if( res.data.list && res.data.list.length > 0 && firstPageCall ){
                    this.hospitals = res.data.list;
                    // this.getImageBase();
                    console.log("listOfHospitals", res.data);

                }
                this.commonService.setPaddingTopScroll(this.content);
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                //this.commonService.showAlert( "Error", error.message );
                this.commonService.setPaddingTopScroll(this.content);
            }
        );
    }

    changeMode = (mode: any) => {
        if (!mode || mode == "undefined") {
            this.mode = "create";
        }
        this.mode = mode;
        console.log("MODE", mode);
        if (this.mode == "view") {
            this.viewFlag = true;
            this.createFlag = false;
            this.editFlag = false;
            this.setViewMode();
        } else if (this.mode == "create") {
            this.viewFlag = false;
            this.createFlag = true;
            this.editFlag = false;
        } else if (this.mode == "edit") {
            this.viewFlag = false;
            this.createFlag = false;
            this.editFlag = true;
        }
    }

    setViewMode = () => {
        this.getViewLeaves(this.adminId);
    }

    cancelClick = () => {
        if (this.mode == "view") {
            this.viewFlag = false;
            this.createFlag = true;
            this.editFlag = false;
            this.mode = "create";
        } else if (this.mode == "edit") {
            this.revertClone();
            this.viewFlag = true;
            this.createFlag = false;
            this.editFlag = false;
            this.mode = "view";

        }
    }

    markMyLeave = (form: NgForm) => {
        console.log(form);
        // console.log(this.hospitals[]);

        this.formSubmitted = true;
        if (form.valid) {

            this.formSubmitted = false;
            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.applyLeaveData = {};
            this.applyLeaveData.description = this.leaveDescription;
            this.applyLeaveData.profileId = this.adminData.userId;
            if (this.fromPage == 'HAStaffHolidayCalendar' || this.fromPage == 'HSRoleHolidayCalendar') {
                this.applyLeaveData.roleType = this.constants.ROLE_HOSPITAL_STAFF;
            } else {
                this.applyLeaveData.roleType = this.constants.ROLE_HOSPITAL_ADMIN;

            }
            // this.applyLeaveData.hospitalIds = [];

            //convert date time to utc format
            let fromDateTimeISO = new Date(moment(form.value.fromdate).format('YYYY-MM-DD') + " " + moment(form.value.startTime, "HH:mm").format('hh:mm A')).toISOString();
            this.applyLeaveData.fromDateTime = moment.utc(fromDateTimeISO).format('YYYY-MM-DDThh:mm A');

            //convert date time to utc format
            let toDateTimeISO = new Date(moment(form.value.todate).format('YYYY-MM-DD') + " " + moment(form.value.endTime, "HH:mm").format('hh:mm A')).toISOString();
            this.applyLeaveData.toDateTime = moment.utc(toDateTimeISO).format('YYYY-MM-DDThh:mm A');

            // this.applyLeaveData.hospitalIds = [];
            // this.hospitals.forEach(element => {
            //     // if(element.hospitalDetails.isChecked){
            //         this.applyLeaveData.hospitalIds.push(element.hospitalDetails.hospitalId);
            //     // }
            // });



            // this.applyLeaveData = {
            //     "profileId": "659de03c-2bca-407d-b44d-fe0281c20a2a",
            //     "roleType": "DOCTOR",
            //     "description": "Feeling Sick",
            //     "hospitalIds": ["d971aeef-9e79-4df5-bb92-944edb910dea"],
            //     "fromDateTime": "2019-02-16T10:30 AM",
            //     "toDateTime": "2019-02-18T10:30 AM"
            //     }




            if (this.mode == "edit") {
                this.applyLeaveData.leaveIds = this.leaveIds;
                console.log("REQUEST", this.applyLeaveData);
                this.doctorRoleServiceProvider.updateLeave(this.applyLeaveData).subscribe(
                    res => {
                        if (res.message) {
                            this.commonService.presentToast(res.message);
                        }
                        if (res.status == "success") {
                            console.log("DATA", res.data);
                            this.commonService.hideLoading();
                            this.cancelClick();
                            this.getViewLeaves(this.adminId);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                        this.commonService.presentToast(errorMsg);
                    }
                );
            } else {
                console.log("REQUEST", this.applyLeaveData);
                this.doctorRoleServiceProvider.applyLeave(this.applyLeaveData).subscribe(
                    res => {
                        if (res.message) {
                            this.commonService.presentToast(res.message);
                        }
                        if (res.status == "success") {
                            console.log("DATA", res.data);
                            this.commonService.hideLoading();


                        } else {
                            this.commonService.hideLoading();
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                        this.commonService.presentToast(errorMsg);
                    }
                );
            }

        }

    }


    /*
    * get View Leaves
    * */

    getViewLeaves(adminId: any) {
        console.log("ADMIN_ID", adminId);
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.doctorRoleServiceProvider.viewLeaves(adminId).subscribe(res => {

            // let tempHospitalPageNo = this.hospitalPageNo;
            if (res.data && res.data.leaveDTOs && res.data.leaveDTOs.length > 0) {
                this.commonService.hideLoading();
                this.viewLeaves = res.data.leaveDTOs;

                // if(!this.commonService.checkIsWeb()){
                //     this.sTimeChanged = true;
                //     this.timeSlot.startTime = new Date(sTime);
                // } else{
                //     let tempDate = new Date(sTime);
                //     let hrMinStrin = tempDate.getHours() + ":" + tempDate.getMinutes();
                //     this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes( hrMinStrin );
                // }

                this.viewLeaves.forEach(element => {
                    element.fromDateView = new Date(element.fromDate);
                    element.toDateView = new Date(element.toDate);

                    let startTime = new Date(element.fromDate);
                    let endTime = new Date(element.toDate);
                    element.fromTimeView = new Date(startTime.getTime() + (startTime.getTimezoneOffset() * 60000));
                    element.toTimeView = new Date(endTime.getTime() + (endTime.getTimezoneOffset() * 60000));
                });


            } else {
                // this.loadFinishedHospital = true;
                this.commonService.hideLoading();
            }
        }, error => {
            this.commonService.hideLoading();
            this.commonService.presentToast(error);

        });
    }


    editLeave = (viewLeave: any) => {
        this.cloneData();

        this.leaveDescription = viewLeave.description;
        if (this.commonService.checkIsWeb()) {
            this.startDateValue = new Date(viewLeave.fromDate);//new Date(moment(viewLeave.fromDate).format("DD/MM/YY")).toISOString();
            this.endDateValue = new Date(viewLeave.toDate);
            let startTime = new Date(viewLeave.fromDate);
            let endTime = new Date(viewLeave.toDate);
            // this.startTimeValue = new Date(startTime.getTime() + (startTime.getTimezoneOffset()*60000));
            // this.endTimeValue = new Date(endTime.getTime() + (endTime.getTimezoneOffset()*60000));
            this.startTimeValue = new Date(startTime.getTime());
            this.endTimeValue = new Date(endTime.getTime());
        } else {

            this.startDateValue = new Date(viewLeave.fromDate).toISOString();
            this.endDateValue = new Date(viewLeave.toDate).toISOString();

            let startTime = new Date(viewLeave.fromDate);
            let endTime = new Date(viewLeave.toDate);
            this.startTimeValue = new Date(startTime.getTime() - startTime.getTimezoneOffset() * 60000).toISOString();
            this.endTimeValue = new Date(endTime.getTime() - endTime.getTimezoneOffset() * 60000).toISOString();
        }



        // this.hospitals = [];
        // this.selectedHospitals = [];

        // viewLeave.hospitalDTO.forEach(element => {
        //     element.isChecked = true;
        //     let hospitalDetails = {"hospitalDetails":element};
        //     this.selectedHospitals.push(element.hospitalId);
        //     this.hospitals.push(hospitalDetails);

        // });

        // this.getImageBaseHospitalList();

        // this.formatAvailability();

        this.leaveIds = _.cloneDeep(viewLeave.leaveIds);
        this.changeMode("edit");
    }

    cloneData = () => {
        this.startDateValueClone = _.cloneDeep(this.startDateValue);
        this.endDateValueClone = _.cloneDeep(this.endDateValue);
        this.startTimeValueClone = _.cloneDeep(this.startTimeValue);
        this.endTimeValueClone = _.cloneDeep(this.endTimeValue);
        this.leaveDescriptionClone = _.cloneDeep(this.leaveDescription);
    }

    revertClone = () => {
        this.startDateValue = _.cloneDeep(this.startDateValueClone);
        this.endDateValue = _.cloneDeep(this.endDateValueClone);
        this.startTimeValue = _.cloneDeep(this.startTimeValueClone);
        this.endTimeValue = _.cloneDeep(this.endTimeValueClone);
        this.leaveDescription = _.cloneDeep(this.leaveDescriptionClone);

    }

}
