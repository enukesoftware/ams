import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HospitalAdminHolidayCalendarPage } from './hospital-admin-holiday-calendar';
import { CalendarModule } from 'primeng/calendar';
import { PipesModule } from '../../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../components/shared.module';

@NgModule({
  declarations: [
    HospitalAdminHolidayCalendarPage
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminHolidayCalendarPage),
    TranslateModule.forChild(),
    SharedModule,
    CalendarModule,
    PipesModule
  ],
  exports: [
    HospitalAdminHolidayCalendarPage,
  ]
})
export class HospitalAdminHolidayCalendarPageModule {}
