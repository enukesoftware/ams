import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { HospitalAdminViewHospitalPage } from './hospital-admin-view-hospital';
import { SharedModule } from "../../components/shared.module";

@NgModule({
  declarations: [
    HospitalAdminViewHospitalPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminViewHospitalPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    HospitalAdminViewHospitalPage,
  ]
})
export class HospitalAdminViewHospitalPageModule {}
