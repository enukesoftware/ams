import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { HospitalAdminDoctorListPage } from './hospital-admin-doctor-list';

import { SharedModule } from "../../components/shared.module";
import { IonicSelectableModule } from 'ionic-selectable';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    HospitalAdminDoctorListPage
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminDoctorListPage),
    SharedModule,
    IonicSelectableModule,
    DropdownModule,
    TranslateModule.forChild()
  ],
  exports: [
    HospitalAdminDoctorListPage
  ]
})
export class DoctorListPageModule {}
