import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HospitalAdminCreateStaffPage } from './hospital-admin-create-staff';
import { SharedModule } from '../../components/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { DropdownModule } from 'primeng/dropdown';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    HospitalAdminCreateStaffPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalAdminCreateStaffPage),
    SharedModule,
    TranslateModule.forChild(),
    DropdownModule,
    IonicSelectableModule
  ],
  exports:[
    HospitalAdminCreateStaffPage
  ]
})
export class HospitalAdminCreateStaffPageModule {}
