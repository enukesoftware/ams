import { Component ,ViewChild} from '@angular/core';
import { IonicPage, Content, NavController, NavParams } from 'ionic-angular';

//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { UserDataDTO, PatientDataDTO, AddressDto } from '../../../interfaces/user-data-dto';
import { LocalStorageService } from '../../../providers/localStorage-service/localStorage.service';
import { CameraService } from '../../../providers/camera-service/camera.service';

/**
 * Generated class for the HospitalAdminCreateStaffPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'HospitalAdminCreateStaffPage',
  segment: 'hospital-admin-create-staff/:fromPage/:previousFromPage/:redirectPage/:redirectParam/:doctorId/:mode'
})
@Component({
  selector: 'page-hospital-admin-create-staff',
  templateUrl: 'hospital-admin-create-staff.html',
})
export class HospitalAdminCreateStaffPage {
  @ViewChild(Content) content: Content;
  address: AddressDto = {};
  patientData: any = {};
  formSubmitted = false;
  userData: UserDataDTO = {};
  inputType: any = "text";
  isDefault: boolean;
  imageUpload: any = {};
  rank: number;
  isUpload: boolean = false;

  isHospitalAdmin: boolean = false;
  isDoctorAdmin: boolean = false;
  isPatientAdmin = false;

  viewFlag: boolean = false;
  createFlag: boolean = true;
  editFlag: boolean = false;

  fromPage: any;
  previousFromPage: any;
  redirectPage: any;
  redirectParam: any;
  doctorId: any;
  mode: any;
  backPageUrl: any;
  navParamsComponent:any;

  constructor(public navCtrl: NavController, public cameraService: CameraService, public navParams: NavParams, public commonService: CommonService,
    private constants: Constants, private manageSysAdminServiceProvider: ManageSysAdminServiceProvider,
    private locstr: LocalStorageService) {
    
    this.fromPage = this.navParams.get('fromPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HospitalAdminCreateStaffPage');
  }

  ionViewWillEnter = () => {
    //check if user is authorized or not to access the screen if not redirect him to login screen
    this.rank = 1;

    this.navParamsComponent = this.navParams;
    console.log("ionViewWillEnter");

    this.isAuthorized();
}

/*
* Function to check if user is authorized or not to access the screen
* */
isAuthorized = () => {
    this.commonService.isLoggedIn((result) => {
        if (!result) {
            this.navCtrl.setRoot('LoginPage');
        } else {
            this.userData = result;
            // this.setAdminRole();
            // this.getNavigationData();
            
            if(this.fromPage == 'HAMHospitalStaff'){
              this.commonService.fireSelectedEvent( '#/hospital-admin-manage-hospital-staff' );
            }

        }
    });
}

}
