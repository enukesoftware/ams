import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from "../../components/shared.module";
import { SRDashboardPage } from './staff-role-dashboard';
 
@NgModule({
    declarations: [
       SRDashboardPage
    ],
    imports: [
       IonicPageModule.forChild(SRDashboardPage),
       TranslateModule.forChild(),
       SharedModule
    ],
    exports: [
        SRDashboardPage
    ]
})
export class HADashboardPageModule {}
