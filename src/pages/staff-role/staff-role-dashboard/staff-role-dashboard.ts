import { Component, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { IonicPage, NavController, MenuController, Content, NavParams, Events, Platform } from 'ionic-angular';
import { Location } from '@angular/common';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { UserDataDTO, HospitalListDTO } from '../../../interfaces/user-data-dto';
import { HospitalStaffServiceProvider } from '../../../providers/hospital-staff-service/hospital-staff-service';

@IonicPage({
    name: 'SRDashboardPage',
    segment: 'staff-role-dashboard/:hospitalStaffId'
})
@Component({
    selector: 'page-staff-role-dashboard',
    templateUrl: 'staff-role-dashboard.html'
})
export class SRDashboardPage {
    @ViewChild(Content) content: Content;
    @ViewChild('HSlistGrid') HSlistGrid: ElementRef;
    userData: UserDataDTO = {};
    selectedHospital: HospitalListDTO[];
    scrollHTFlag: boolean = false;
    staffId: string = "";
    userLoginRole: string;
    userActiveRole: string;
    showMyProfile: boolean = false;
    reportsRedirectionRole: any;

    constructor(private commonService: CommonService, private evts: Events, private location: Location, public platform: Platform, public hospitalStaffServiceProvider: HospitalStaffServiceProvider, public navParams: NavParams, private renderer: Renderer2, private navCtrl: NavController, private menu: MenuController, private constants: Constants) {
        this.menu.enable(true);
        /**
         *handle device back button
         */
        this.platform.registerBackButtonAction(() => {
            //             this.location.back();
        });
    }

    ionViewWillEnter = () => {
        this.menu.enable(true);
        // this.setStaffSampleData();
        //check if user is authorized or not to access the screen if not redirect him to login screen
        this.isAuthorized();
    }

    ionViewDidLoad = () => {
        //dashboard Wrapper height dynamically set based on scroll content height
        setTimeout(() => {
            this.scrollHTFlag = this.commonService.contentHeight(this.content, this.HSlistGrid);
        }, 100);
    }

    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if (!result) {
                this.navCtrl.setRoot('LoginPage');
            } else {
                this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
                this.userData = result;
                console.log("USER:",this.userData);
                this.commonService.getFromStorage("userLoginRole").then((value) => {
                    if (value) {
                        if (value == this.constants.ROLE_HOSPITAL_STAFF) {
                            this.commonService.setInStorage('userActiveRole', this.constants.ROLE_HOSPITAL_STAFF);
                        }
                    }
                });
                if (result.role == this.constants.ROLE_HOSPITAL_STAFF) {
                    this.commonService.setActiveUserRole(this.constants.ROLE_HOSPITAL_STAFF);
                }
                this.evts.publish('fire-after-login-event');

                this.userLoginRole = this.commonService.getUserLoginRole();
                this.userActiveRole = this.commonService.getActiveUserRole();

                if (this.userLoginRole == 'HOSPITAL_ADMIN') {
                    this.reportsRedirectionRole = 'hospitalAdmin';
                }
                if (this.userLoginRole == 'SYSTEM_ADMIN') {
                    this.reportsRedirectionRole = 'sysAdmHospitalAdmin';
                }

                if (this.userLoginRole == this.userActiveRole) {
                    this.showMyProfile = true;
                }

                this.commonService.fireSelectedEvent('#/staff-role-dashboard/0');
                this.commonService.isAuthorizedToViewPage(this.navCtrl, this.constants.ROLE_HOSPITAL_STAFF, true);

                console.log("STAFF_ID:",this.navParams.get('hospitalStaffId'));
                if( this.navParams.get('hospitalStaffId') && this.navParams.get('hospitalStaffId') != ":hospitalStaffId" && this.navParams.get('hospitalStaffId') != "0" ){
                    this.staffId = this.navParams.get('hospitalStaffId');
                    this.commonService.setInStorage("hospitalStaffId", this.staffId );
                    this.hospitalStaffServiceProvider.getHospitalStaffDetails(this.staffId).subscribe(res=>{
                        // this.selectedHospital = res.data;
                        this.commonService.hideLoading();
                        if(res.data){
                            this.commonService.setInStorage("selectedStaff", res.data);
                            this.commonService.setInStorage("hospitalStaffId", res.data.userId);
                            if(res.data.hospitalDTO && res.data.hospitalDTO.hospitalId){
                                this.commonService.setInStorage("hospitalId", res.data.hospitalDTO.hospitalId);
                            }
                        }
                    }, error=>{
                        this.commonService.hideLoading();
                        let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                        this.commonService.presentToast(errorMsg);
                    });
                }else{
                    this.commonService.getFromStorage("hospitalStaffId").then(( value ) => {
                        if( value ) {
                            this.staffId = value;
                        }else{
                            this.staffId = "";
                        }
                        this.hospitalStaffServiceProvider.getHospitalStaffDetails(this.staffId).subscribe(res=>{
                            // this.selectedHospital = res.data;
                            this.commonService.hideLoading();
                            if(res.data){
                                this.commonService.setInStorage("selectedStaff", res.data);
                                this.commonService.setInStorage("hospitalStaffId", res.data.userId);
                                if(res.data.hospitalDTO && res.data.hospitalDTO.hospitalId){
                                    this.commonService.setInStorage("hospitalId", res.data.hospitalDTO.hospitalId);
                                }
                            }
                        }, error=>{
                            this.commonService.hideLoading();
                            let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                            this.commonService.presentToast(errorMsg);
                        });
                    });
                }

                this.commonService.setPaddingTopScroll(this.content);


                //booking details clear from local storage
                this.commonService.removeFromStorage("appointmentDetails");
                this.commonService.removeFromStorage("isConfirmed");
                this.commonService.removeFromStorage("isUpdateOnView");
            }
        });
    }

    showComingSoon = () => {
        this.commonService.presentToast();
    }

    setStaffSampleData() {
        let data = {
            "userId": "cbddf657-3d82-4d6c-88d0-a4b540307125",
            "firstName": "Jai",
            "lastName": "Kishan",
            "email": "jaikishan@mail123.com",
            "contactNumber": "9824534534",
            "userProfilePicUrl": null,
            "userProfileThumbPicUrl": null,
            "blockStatus": "BLOCK",
            "userName": "jaikishan1234",
            "address": {
                "latitude": 0.0,
                "longitude": 0.0,
                "state": "DL",
                "street": "main street2",
                "country": "",
                "zipCode": "321313",
                "cityId": "b805fb55-7146-4393-a80c-cb9e03ff6f34",
                "cityName": "Delhi"
            },
            "imageBase64": null,
            "fileId": "85e660cd-ad7f-40f8-9e92-276dcb513959",
            "hospitalId":"595a3c7d-cb71-400d-a150-82d7a5afd687"
        }
        this.commonService.setInStorage("hospitalStaffId", data.userId);
        this.commonService.setInStorage("hospitalId", data.hospitalId);
        this.commonService.setInStorage("selectedStaff", data);
    }
}