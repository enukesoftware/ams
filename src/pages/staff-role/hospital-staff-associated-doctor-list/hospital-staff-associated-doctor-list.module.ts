import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from "../../components/shared.module";
import { HospitalStaffAssociatedDoctorListPage } from './hospital-staff-associated-doctor-list';

@NgModule({
  declarations: [
    HospitalStaffAssociatedDoctorListPage
  ],
  imports: [
    IonicPageModule.forChild(HospitalStaffAssociatedDoctorListPage),
    SharedModule,
    TranslateModule.forChild()
  ],
  exports: [
    HospitalStaffAssociatedDoctorListPage
  ]
})
export class AssociatedDoctorListPageModule {}
