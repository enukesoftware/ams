import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, Events, Platform  } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, NgForm  } from '@angular/forms';

//3rd party npm modules
import { UserIdleService } from '../../providers/angular-user-idle';

//providers services
import { CommonService } from '../../providers/common-service/common.service';
import { AuthService } from '../../providers/authService/authService';
import { Constants } from '../../providers/appSettings/constant-settings';
import { LocalStorageService } from '../../providers/localStorage-service/localStorage.service';
import { UserDataDTO } from '../../interfaces/user-data-dto';
import { HttpResponseDTO } from '../../interfaces/http-response-dto';
import { OneSignalService } from '../../providers/one-signal-service/one-signal.service';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    passwordEyeIcon: any = "eye-icon";
    inputType: any = "password";
    formSubmitted: boolean = false;
    umePlaceholder: any;
    passwordPlaceholder: any;
    emailErrorFlag: boolean = false;
    passwordErrorFlag: boolean = false;
    loginForm: FormGroup;
    loginData: UserDataDTO = {};
    guestDTO: HttpResponseDTO ={};
    isSkipAndExplore : boolean =false;


constructor( private commonService: CommonService, public platform: Platform, private evts: Events, private constants: Constants, private authService: AuthService, private navCtrl: NavController, private menu: MenuController,
        private formBuilder: FormBuilder, private locstr: LocalStorageService , public onesignal: OneSignalService) {
    this.menu.enable(false);
    this.platform.registerBackButtonAction(() => {
        //this.location.back();
      }
    );
    
    this.loginForm = formBuilder.group({

        loginIdentifier: ['', Validators.compose([
            Validators.required
        ])
        ],

        Password: [
           '', Validators.compose([
               Validators.required
           ])
       ]
    });

}
    
    ionViewWillEnter = () => {
        this.menu.enable(false);
        this.isAuthorized();
    }
    
    /*
     * Function to check if user is authorized or not to access the screen
     * */
    isAuthorized = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                console.log("login isAuthorized not ==============================>");
            }else{
                this.navCtrl.setRoot( 'SADashboardPage' );
            }
        });
    }
    
    /*
     * Function to show coming soon toast
     * */
    commingSoon = () => {
        this.commonService.presentToast();
    }
    
    /*
     * Function to handle toggle behavior of the password field
     * */
    showPasswordToggle = () => {
        this.commonService.passwordToggle( this.passwordEyeIcon,this.inputType, ( eyeIcon,inputType ) => {
            this.passwordEyeIcon = eyeIcon;
            this.inputType = inputType;
        });
    }
    
    /*
     * Function to skip login and allow user to explore the app, so navigate to home directly
     * */
    skipNProceed = () => {
        console.log("skipNProceed clicked ==========================>");
        /*this.evts.publish('fire-after-login-event');
        this.navCtrl.setRoot('SADashboardPage');*/
      //  this.navCtrl.setRoot('PatientRoleHomePage');
//        this.commonService.presentToast();
        this.isSkipAndExplore =true;
        this.commonService.setInStorage( 'isSkipAndExplore',this.isSkipAndExplore );
        this.guestDTO.data ={
                contactNo: "1234567890",
                email: "",
                firstName: "Guest",
                isContactNoVerified: false,
                isEmailVerified: false,
                lastName: "User",
                role: "PATIENT",
                signupType: "",
                token: " ",
                userId: "",
                userName: "Guest_User"
        }
        this.guestDTO.status='success';
        this.guestDTO.code = 200;
        //this.evts.publish( 'isSkipAndExplore', this.isSkipAndExplore);
        this.handleResponse (this.guestDTO);
    }
    
    /*
     * Function to make user login into the app and login functionality implementation with login api call
     * */
    doLogin = ( form: NgForm ) => {
        this.isSkipAndExplore =false;
        this.commonService.setInStorage( 'isSkipAndExplore',this.isSkipAndExplore );
        this.formSubmitted = true ;
        if( form.valid  ){
            this.formSubmitted = false ;
            console.log("doLogin clicked ==========================>",this.loginData);
            this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
            this.authService.login( this.loginData ).subscribe(
                res => {
                    this.locstr.setObj('accessToken', res.data.token);
                    this.handleResponse( res );
                },
                error => {
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);
                }
            );
        }
    }
    
    /**
     * Function to validate a form
     * @param login form
     */
    protected validateForm = ( form ) => {
       if ( this.loginData.loginIdentifier ) {
            this.emailErrorFlag = false;
        }else if( this.loginData.password ){
            this.passwordErrorFlag = false;
        }
        if ( this.loginData.loginIdentifier && this.loginData.password ) {
            this.emailErrorFlag = false;
             this.passwordErrorFlag = false;
            return true;
        } else {
            if( this.loginData.loginIdentifier == '' ||  this.loginData.loginIdentifier == undefined ){
                this.emailErrorFlag = true;
            }else if( this.loginData.password == '' ||  this.loginData.password == undefined ){
                this.passwordErrorFlag = true;
            }
            return false;
        }
    }
    
    /**
     * Function to handle response of login web service
     * param: response
     */
    private handleResponse = ( res: HttpResponseDTO ) => {
        console.log("res..",res);
        if ( res.status == "success" ) {
            this.commonService.hideLoading();
            this.commonService.checkIfUserIsIdle(res.data.role);
            this.commonService.setInStorage( 'userData',res.data );
            this.commonService.setInStorage( 'userLoginRole', res.data.role);
            this.commonService.setInStorage( 'userActiveRole',res.data.role );
            this.commonService.setActiveUserRole( res.data.role );
            this.commonService.setUserLoginRole( res.data.role );
            this.evts.publish('fire-after-login-event');
            this.onesignal.generateIDs();
            if(res.data.role == this.constants.ROLE_SYSTEM_ADMIN){
                this.navCtrl.setRoot( 'SADashboardPage' );
            } else if(res.data.role == this.constants.ROLE_HOSPITAL_ADMIN){
                this.navCtrl.setRoot( 'HADashboardPage' );
            } else if(res.data.role == this.constants.ROLE_PATIENT){
                this.navCtrl.setRoot( 'PatientRoleHomePage',{ isSkipAndExplore:this.isSkipAndExplore });
            } else if(res.data.role == this.constants.ROLE_DOCTOR){
                this.navCtrl.setRoot( 'DoctorDashboardPage' );
            }else if(res.data.role == this.constants.ROLE_HOSPITAL_STAFF){
                this.commonService.setInStorage("hospitalStaffId", res.data.userId);
                this.navCtrl.setRoot( 'SRDashboardPage', {
                    hospitalStaffId:res.data.userId
                });
                // this.navCtrl.setRoot( 'SRDashboardPage');
            }
//            this.navCtrl.setRoot( 'SADashboardPage' );
        }
        else {
            console.log( "error has occured" );
            this.commonService.hideLoading();
        }
    }
}