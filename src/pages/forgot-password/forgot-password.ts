import { Component, NgZone } from '@angular/core';

import { IonicPage, NavController, MenuController, Platform  } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';

//DTOs
import { UserDataDTO } from '../../interfaces/user-data-dto';

//providers services
import { CommonService } from '../../providers/common-service/common.service';
import { AuthService } from '../../providers/authService/authService';
import { Constants } from '../../providers/appSettings/constant-settings';

@IonicPage()
@Component({
    selector: 'page-forgot-password',
    templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
    passwordEyeIcon: any = "eye-icon";
    nPasswordEyeIcon: any = "eye-icon";
    nInputType: any = "password";
    forgotPasswordForm: FormGroup;
    inputType: any = "password";

    cPasswordEyeIcon: any = "eye-icon";
    cInputType: any = "password";
    
    umePlaceholder: any;
    newPasswordPlaceholder: any;
    confirmPasswordPlaceholder: any;
    isSubmit =false;
    showForgotPassView: boolean = true;
    showEnterOTPView: boolean = false;
    showResetPassView: boolean = false;
    formData: UserDataDTO = {};

    constructor( private _ngZone: NgZone, public platform: Platform, private location: Location, public commonService: CommonService, public navCtrl: NavController, private menu: MenuController,
            private formBuilder: FormBuilder, public authService:AuthService, private constants: Constants ) {
        this.menu.enable(false);
        this.platform.registerBackButtonAction(() => {
            this.location.back();
        });
        this.translateTheText().then(()=>{});
        
        this.forgotPasswordForm = formBuilder.group({
            loginIdentifier: ['', Validators.compose([
                //Validators.pattern(/^\s*(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/),                                                
                Validators.required,
            ])
            ],
            OTP: ['', Validators.compose([
                Validators.pattern(/^\d{4}$/),
                Validators.required,
            ])
           ],
           Password: ['', Validators.compose([
                 Validators.minLength(8),
                 Validators.required,
                 Validators.pattern(/^(?=.*\d)(?=.*[A-Z])(?!.*[^a-zA-Z0-9@#$^+=])(.{8,20})$/)
           ])
           ],
           confirmPassword: ['', Validators.compose([
                  Validators.minLength(8),
                  Validators.required
          ])
        ]
        });
        
        
    }
    
    ionViewWillEnter = () => {
        this.menu.enable(false);
    }
    
    /*
     * Function to translate the text used through components
     * */
    translateTheText = () => {
       return new Promise((resolve, reject) => {
            let promisesArr = [];
            
            //Set placeholder labels from translated files
            promisesArr.push(this.commonService.translateText("UME_LABEL").toPromise().then((translatedText)=>{
                this.umePlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("NEW_PASSWORD_LABEL").toPromise().then((translatedText)=>{
                this.newPasswordPlaceholder = translatedText;
            }));
            
            promisesArr.push(this.commonService.translateText("CONFIRM_PASSWORD_LABEL").toPromise().then((translatedText)=>{
                this.confirmPasswordPlaceholder = translatedText;
            }));
            
            Promise.all(promisesArr).then( () => {
                resolve();
            });
        });
    }
    
    /*
     * Function to handle toogle behaviour of the password field
     * */
    showNewPasswordToggle = () => {
        this.commonService.passwordToggle( this.nPasswordEyeIcon,this.nInputType, ( eyeIcon,inputType ) => {
            this.nPasswordEyeIcon = eyeIcon;
            this.nInputType = inputType;
        });
    }
    
    showConfirmPasswordToggle = () => {
        console.log("confirm password toggle-------->");
        this.commonService.passwordToggle( this.cPasswordEyeIcon,this.cInputType, ( eyeIcon,inputType ) => {
            this.cPasswordEyeIcon = eyeIcon;
            console.log("cPasswordEyeIcon------->", this.cPasswordEyeIcon);
            this.cInputType = inputType;
            console.log(".cPasswordEyeIcon------->", this.cInputType);
        });
    }
    
    
    /*
     * Function to handle toggle behavior of the password field
     * */
    showPasswordToggle = () => {
        this.commonService.passwordToggle( this.passwordEyeIcon,this.inputType, ( eyeIcon,inputType ) => {
            this.passwordEyeIcon = eyeIcon;
            this.inputType = inputType;
        });
    }
    
    /*
     * Function to resend OTP 
     * */
    resendOTP = () => {
        this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
        this.formData.type ='EMAIL';
        this.authService.resendOTP(this.formData).subscribe(
                response => {
                    console.log("response-------->",response);
                    if ( response.status == "success" ) {
                        //this.commonService.showAlert('Success', response.message);
                        this.commonService.hideLoading();
                        this.commonService.presentToast(response.message);
                        this.showForgotPassView = false;
                        this.showEnterOTPView = true;
                        this.showResetPassView = false;
                        this.formData.transactionId = response.data.transactionId;
                    }
                    else {
                        console.log( "error has occured" );
                        this.commonService.hideLoading();
                        this.commonService.presentToast(response.message);
                    }
                },
                error => {
                    console.log("error-------->",error);
                    this.commonService.hideLoading();
                    this.commonService.presentToast(error.message);
                }
        );
    }
    
    /*
     * Function to request for forgot password and request to send OTP from backend
     * */
   public requestOTP = (form:any) => {
       this._ngZone.run(() => { this.isSubmit =true; });
       if(form.controls.loginIdentifier.valid){
           this.isSubmit =false;
           console.log("formData-------->",this.formData);
           this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
           this.formData.type ='EMAIL';
           this.authService.requestOTP(this.formData).subscribe(
                   response => {
                       console.log("response-------->",response);
                       if ( response.status == "success" ) {
                           //this.commonService.showAlert('Success', response.message);
                           this.commonService.hideLoading();
                           this.commonService.presentToast(response.message);
                           this.showForgotPassView = false;
                           this.showEnterOTPView = true;
                           this.showResetPassView = false;
                           this.formData.transactionId = response.data.transactionId;
                       }
                       else {
                           console.log( "error has occured" );
                           this.commonService.hideLoading();
                           this.commonService.presentToast(response.message);
                       }
                   },
                   error => {
                       console.log("error-------->",error);
                       this.commonService.hideLoading();
                       this.commonService.presentToast(error.message);
                   }
           );
           
       }
       else{}
    }
    
    /*
     * Function to submit the OTP entered by user to backend
     * */
    submitOTP = (form:any) => {
        this._ngZone.run(() => { this.isSubmit =true; });
        if(form.controls.OTP.valid){
            console.log("in submit otp");
            this.isSubmit =false;
            this.authService.verifyOTP(this.formData).subscribe(
                    response => {
                        console.log("response-------->",response);
                        if ( response.status == "success" ) {
                            this.commonService.hideLoading();
                            this.commonService.presentToast(response.message);
                            this.showForgotPassView = false;
                            this.showEnterOTPView = false;
                            this.showResetPassView = true;
                        }
                        else{
                            console.log( "error has occured" );
                            this.commonService.hideLoading();
                            this.commonService.presentToast(response.message);
                        }
                    },
                    error => {
                        console.log("error-------->",error);
                        this.commonService.hideLoading();
                        this.commonService.presentToast(error.message);
                    }
            );
        }
        else{}
    }
    
    /*
     * Function to request for resetting password by submitting new password to backend
     * */
    resetPassword = (form:any) => {
        this._ngZone.run(() => { this.isSubmit =true; });
        if(form.controls.Password.valid && form.controls.confirmPassword.valid){
            if(form.controls.Password.value == form.controls.confirmPassword.value){
            console.log("form is valid-------->");
            this.isSubmit = false;
            //this.formData.otp ='';
            this.authService.forgotPassword(this.formData).subscribe(
                    response => {
                        console.log("response-------->",response);
                        if ( response.status == "success" ) {
                            this.commonService.hideLoading();
                            //this.commonService.showAlert('Success', response.message);
                            this.commonService.presentToast(response.message);
                            this.showForgotPassView = false;
                            this.showEnterOTPView = false;
                            this.showResetPassView = false;
                            this.navCtrl.setRoot( 'SADashboardPage' );
                        }
                        else{
                            console.log( "error has occured" );
                            this.commonService.hideLoading();
                            this.commonService.presentToast(response.message);
                            
                        }
                    },
                    error => {
                        console.log("error-------->",error);
                        this.commonService.hideLoading();
                        this.commonService.presentToast(error.message);
                    });
            }else{}
       }else{}
    }
}