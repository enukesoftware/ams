import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import {ChartModule} from "primeng/chart";
import { CalendarModule } from "primeng/calendar";
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";

import { DoctorListComponent } from "./doctor-list-component/doctor-list-component";
import { HospitalListComponent } from "./hospital-list-component/hospital-list-component";
import { ViewContractComponent } from "./view-contract-component/view-contract-component";
import { SystemAdminListComponent } from "./system-admin-list-component/system-admin-list-component";
import { UpcomingAppointmentComponent } from "./upcoming-appointments-component/upcoming-appointments-component";
import { PatientListComponent } from "./patient-list/patient-list";
import { FooterPage } from "../footer/footer";
import { HeaderPage } from "../header/header";
import { HeaderForRoleCreationPage } from "../header-for-role-creation-form/header-for-role-creation-form";

import { DoctorFormComponent } from "./doctor-form/doctor-form";
import { PopoverSliderComponentModule } from "./popover-slider/popover-slider.module";
import { AppointmentDetailsComponent } from "./appointment-details/appointment-details";
import { SearchResultComponent } from "./search-result-component/search-result-component";
import { TruncateModule } from 'ng2-truncate';
import { HospitalItemComponent } from './hospital-item-component/hospital-item-component';
import { ViewHolidayItemComponent } from './view-holiday-item-component/view-holiday-item-component';
import { StartTimeDirective } from '../../directives/StartTimeValidator';
import { EndTimeDirective } from '../../directives/EndTimeValidator';
import { DoctorItemComponent } from './doctor-item-component/doctor-item-component';
import { AddPatientComponent } from './add-patient-component/add-patient-component';
import { DropdownModule } from 'primeng/dropdown';
import { IonicSelectableComponent, IonicSelectableModule } from 'ionic-selectable';
import { CreateUserComponent } from './create-user-component/create-user-component';
import { MaxValueDirective } from '../../directives/MaxValueValidator';
import { MinValueDirective } from '../../directives/MinValueValidator';

//import { chartComponent } from "./chartComponent/chartComponent";
// import { RouterModule,Router } from '@angular/router';

@NgModule({
  declarations: [
    DoctorListComponent,
    HospitalListComponent,
    ViewContractComponent,
    SystemAdminListComponent,
    PatientListComponent,
    FooterPage,
    HeaderPage,
    HeaderForRoleCreationPage,
    DoctorFormComponent,
    UpcomingAppointmentComponent,
    SearchResultComponent,
    AppointmentDetailsComponent,
    HospitalItemComponent,
    ViewHolidayItemComponent,
    StartTimeDirective,
    EndTimeDirective,
    DoctorItemComponent,
    AddPatientComponent,
    CreateUserComponent,
    MaxValueDirective,
    MinValueDirective
    //chartComponent
  ],
  imports: [
            IonicPageModule.forChild(''),
            TranslateModule.forChild(),
            MultiSelectModule,
            PopoverSliderComponentModule,
            CalendarModule,
            TruncateModule,
            ChartModule,
            DropdownModule,
            IonicSelectableModule,
            // RouterModule.forChild()
            

        ],
  exports: [
    DoctorListComponent,
    HospitalListComponent,
    ViewContractComponent,
    SystemAdminListComponent,
    PatientListComponent,
    FooterPage,
    HeaderPage,
    HeaderForRoleCreationPage,
    DoctorFormComponent,
    UpcomingAppointmentComponent,
    SearchResultComponent,
    AppointmentDetailsComponent,
    HospitalItemComponent,
    ViewHolidayItemComponent,
    StartTimeDirective,
    EndTimeDirective,
    DoctorItemComponent,
    AddPatientComponent,
    CreateUserComponent,
    MaxValueDirective,
    MinValueDirective
    //chartComponent
  ]

})
export class SharedModule {}
