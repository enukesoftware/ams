import { Component, Input } from '@angular/core';
import { NavController, NavParams, Events } from "ionic-angular";

//Provider Services
import { CommonService } from '../../../providers/common-service/common.service';
import { HospitalListDTO } from '../../../interfaces/user-data-dto';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';



@Component({
  selector: 'patient-list',
  templateUrl: 'patient-list.html'
})
export class PatientListComponent {
  @Input() patientList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,  private evts: Events, private commonService: CommonService,
              private constants: Constants,private patientRoleService: PatientRoleServiceProvider,) {
      console.log("patientList", this.patientList);
  }

  ionViewWillEnter = () => {
       console.log("patientList", this.patientList);
    }

    comingSoonPopup(){
        this.commonService.presentToast();
    }
    
    
    /**
     * Function to activate or deactivate doctor
     */

    public activateDeactivatePatient =(patient) => {
          let activationStatus:any;
          this.commonService.getActivationStatus(patient,(cb)=>{
              activationStatus =cb;
          });
          let patientObj ={
                  patientId:patient.userId,
                  action:activationStatus.Action
        }
          
          let activationPopUpMsg = this.commonService.getTranslate('CONFIRM_AD_MSG1',{})+  this.commonService.getTranslate(activationStatus.text,{})
          let warningTitle =  this.commonService.getTranslate('WARNING',{});
          let alert = this.commonService.confirmAlert(warningTitle, activationPopUpMsg+'?');
          alert.setMode("ios");
          alert.present();
          alert.onDidDismiss((data) => {
              if(data == true){   
                  this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
                  this.patientRoleService.activateDeactivatePatient(patientObj).subscribe(
              res => {
                if ( res.status == "success" ) {
                    this.commonService.hideLoading();
                    if(patient.blockStatus =="UNBLOCK"){
                        patient.blockStatus ="BLOCK";
                    } 
                    else if(patient.blockStatus =="BLOCK"){
                        patient.blockStatus ="UNBLOCK";
                    }
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
                else{
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
            }
        },
        error => {
            this.commonService.hideLoading();
            this.commonService.presentToast(error.message);
        });
      }else{}
      });
     }

     managePatient = (patient) => {
        this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_PATIENT );
        this.commonService.setActiveUserRole( this.constants.ROLE_PATIENT );
        this.evts.publish('fire-after-login-event');
    }
}
