import { Component, Input, NgZone } from '@angular/core';
import { NavController, NavParams, Events, ViewController } from 'ionic-angular';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { SearchServiceProvider } from "../../../providers/search-service/search-service";
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';
import { PatientRoleServiceProvider } from '../../../providers/patient-role-service/patient-role-service';



@Component({
    selector: 'search-result-component',
    templateUrl: 'search-result-component.html'
})
export class SearchResultComponent {
    @Input() from: string;
    searchData: any;
    showLoader: boolean = true;
    searchObj: any;
    showResult: boolean = false;
    pageNo: number = 0;
    doctorListParams:any;
    
    constructor( private _ngZone: NgZone, public viewCtrl: ViewController, private searchServiceProvider: SearchServiceProvider, private commonService: CommonService, private evts: Events, private constants: Constants, public navCtrl: NavController, public navParams: NavParams,private manageHospitalService: ManageHospitalServiceProvider,private patientRoleService: PatientRoleServiceProvider ) {
        this.evts.subscribe('system-admin-search-hospitals', (result, doctorListParams:any = null) => {
            this._ngZone.run(() => {
                console.log("result--",result);
                this.searchObj = result.searchObj;
                if(doctorListParams){
                    this.doctorListParams = doctorListParams.doctorListParams;
                    console.log("doctorListParams--",this.doctorListParams.cityId);
                    this.getUnassociatedDoctorList(this.doctorListParams.cityId, this.doctorListParams.hospitalId, this.doctorListParams.searchText, this.doctorListParams.firstTime);
                }
                else{
                    this.getHospitalsList();
                }
            });
        });
        
        this.evts.subscribe('unsub-system-admin-search-hospitals', () => {
            this.evts.unsubscribe('system-admin-search-hospitals');
            this.showResult = false;
        });
        
        this.evts.subscribe('system-admin-search-patients', (result) => {
            this._ngZone.run(() => {
                console.log("result--",result);
                this.searchObj = result.searchObj;
                this.getPatientAsPerSearch(this.searchObj.searchText);
                
            });
            
        });
        
        this.evts.subscribe('system-admin-search-patients', () => {
            this.evts.unsubscribe('system-admin-search-patients');
            this.showResult = false;
        });
        
    }

    ngAfterViewInit = () => {
        console.log("ngAfterViewInit ========================>");
    }
    
    ionViewWillUnload() {
        this.evts.unsubscribe('system-admin-search-hospitals');
    }
    
    /*
     * Function to get hospitals list for system admin when text is entered in search field
     * */
    public getHospitalsList = () => {
        console.log("getHospitalsList ========================>");
        this.searchServiceProvider.getSearchResult(this.searchObj).subscribe(
                res=>{
                    if( res.status == "success"){
                        if(this.searchObj.type == "hospital"){
                            this.searchData = res.data.hospitals;
                        } else if(this.searchObj.type == "doctor"){
                            this.searchData = res.data.doctors;
                        }
                        console.log("this.searchData...........", this.searchData);
                        this.showResult = true;
                        this.showLoader = false;
                    }
                }, error=>{
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);
                });
    }
    
    getAllData(){
        this.evts.publish( 'system-admin-search-more-hospitals',{searchObj: this.searchData} );
    }
    
    
    /**
     * Get doctors list to associate them with particular hospital
     **/
   public getUnassociatedDoctorList( cityId: any, hospitalId:any, searchText:any, firstPageCall?: boolean ) {
        console.log("ngAfterViewInit ========================>",cityId);
        this.manageHospitalService.geUnassociatedDoctorsList( cityId, hospitalId ,this.pageNo,searchText).subscribe(
            res => {
                this.commonService.hideLoading();
                let tempPageNo = this.pageNo;
                if ( res.status == "success" ) {
                        this.searchData =res.data.list;
                        console.log("this.searchData ========================>",this.searchData);
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast( errorMsg );
            }
        );
    }
   
   /**
    * Get patient list as per search text
    **/
   public getPatientAsPerSearch( searchText:any, firstPageCall?: boolean ) {
       console.log("ngAfterViewInit ========================>",searchText);
       this.patientRoleService.getPatientsAsPerSearch( this.pageNo,searchText).subscribe(
           res => {
               this.commonService.hideLoading();
               let tempPageNo = this.pageNo;
               if ( res.status == "success" ) {
                       this.searchData =res.data.list;
                       console.log("this.searchData ========================>",this.searchData);
               }
           },
           error => {
               this.commonService.hideLoading();
               let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
               this.commonService.presentToast( errorMsg );
           }
       );
   }
   
    
   
   /*
    * goto doctor profile for mobile    
    */
    goToDoctorProfile(doctorId, isAssociatedDocList?:boolean){
        console.log("from goToDoctorProfile at 469...",isAssociatedDocList);
        this.navCtrl.setRoot("HACreateDoctorPage", {'mode': 'view', 'id':doctorId ,'from':isAssociatedDocList});
    }
    
    managePatient = (patient) => {
        this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_PATIENT );
        this.commonService.setActiveUserRole( this.constants.ROLE_PATIENT );
        this.evts.publish('fire-after-login-event');
    }
    
}