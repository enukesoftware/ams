import { Component, Input, ViewChild } from '@angular/core';
import { NavParams, Events, Nav, NavController, Platform } from 'ionic-angular';


//Provider Services
import { CommonService } from '../../../providers/common-service/common.service';
import { HospitalListDTO } from '../../../interfaces/user-data-dto';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";

@Component({
    selector: 'doctor-list-component',
    templateUrl: 'doctor-list-component.html',
})
export class DoctorListComponent {
    @Input() doctorList: any;
    @Input() isAssociateDoctorlist: any;
    @Input() isPatientDoctorList: any;
    @Input() isSADoctorlist: any;
    @Input() fromPage: any;
    @Input() hospitalId: any;
    @Input() isPatientRoleDoctorList: any;
    @Input() isHolidayCalendarDoctorList: any;
    @Input() isHospitalStaffAssociatedDoctors:boolean;
    @Input() isPatientSelected:boolean;
    @Input() fromHAOnboardPatient:boolean;
    @Input() fromPROnboardPatient:boolean;

    selectedHospital: HospitalListDTO = {};
    patientRoleHospitalId: any;
    isSkipAndExplore = false;
    bgImage: any = "assets/imgs/manage_doctors.svg";
    popover: any;
    browserName: any;
    isPatientDependents: boolean = false;
    bookingUrl: any;
    userActiveRole:any;


    @ViewChild(Nav) nav: Nav;
    constructor(private evts: Events, public navParams: NavParams, private commonService: CommonService, public platform: Platform
        , private constants: Constants, private manageSysAdminServiceProvider: ManageSysAdminServiceProvider,
        private navCtrl: NavController
    ) {
        this.browserName = this.commonService.getBrowserName();
        console.log("browser", this.browserName);
        console.log("hospitalId", this.hospitalId);
        this.commonService.getFromStorage('isSkipAndExplore').then((value) => {
            this.isSkipAndExplore = value;
        })

        this.platform.registerBackButtonAction(() => {
            this.commonService.closePopupOnBackBtn(this.popover);
        })

        this.getPatientDependent();
    }

    ionViewWillEnter = () => {
        this.browserName = this.commonService.getBrowserName();
        console.log("browser", this.browserName);
        this.commonService.getFromStorage('selectedHospital').then((value) => {
            if (value) {
                this.selectedHospital = value;
            }
        });

    }

    


    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
        this.commonService.presentToast();
    }

    /**
     * Function to show Availability Timings of Doctor
     **/
    availabilityTimings = (doctor: any) => {
        this.commonService.getFromStorage('selectedHospital').then((value) => {
            if (value) {
                this.selectedHospital = value;
                let education = "";
                let subtitle = "";
                for (let i = 0; i < doctor.educationList.length; i++) {
                    education = education + doctor.educationList[i].name;
                }
                subtitle = subtitle + '<div>' + doctor.firstName + '</div><div>' + education + '</div>';
                this.popover = this.commonService.showTimingList(this.selectedHospital.name, subtitle, doctor.formattedAvailabilities);
                this.commonService.closePopupOnBackBtn(this.popover);
            }
        });
    }

    /*
     * Function to store hospital and doctor id before navigating to associate doctor screen
     * */
    associateDoctor = (doctor: any) => {
        this.commonService.presentToast();
        this.commonService.setInStorage('selectedDoctor', doctor);
    }

    /*
     * Function to store hospital and doctor id before navigating to view contract doctor screen
     * */
    viewContract = (doctor: any) => {
        this.commonService.presentToast();
    }

    /**
     * Function to activate or deactivate doctor
     */
    public activateDeactivateDoctor = (doctor) => {
        let activationStatus: any;
        this.commonService.getActivationStatus(doctor, (cb) => {
            activationStatus = cb;
        });

        let doctorObj = {
            doctorId: doctor.doctorProfileId,
            action: activationStatus.Action
        }

        let activationPopUpMsg = this.commonService.getTranslate('CONFIRM_AD_MSG1', {}) + this.commonService.getTranslate(activationStatus.text, {})
        let warningTitle = this.commonService.getTranslate('WARNING', {});

        let alert = this.commonService.confirmAlert(warningTitle, activationPopUpMsg + '?');
        alert.setMode("ios");
        alert.present();
        alert.onDidDismiss((data) => {
            if (data == true) {
                this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
                this.manageSysAdminServiceProvider.activateDeactivateDoctor(doctorObj).subscribe(
                    res => {
                        if (res.status == "success") {
                            this.commonService.hideLoading();
                            if (doctor.blockStatus == "UNBLOCK") {
                                doctor.blockStatus = "BLOCK";
                            }
                            else if (doctor.blockStatus == "BLOCK") {
                                doctor.blockStatus = "UNBLOCK";
                            }
                            this.commonService.presentToast(res.message);
                        }
                        else {
                            this.commonService.hideLoading();
                            this.commonService.presentToast(res.message);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        this.commonService.presentToast(error.message);
                    });
            } else { }
        });
    }


    /**
     * Function to show Timing popup
     **/
    openTimingDetails = (doctor) => {
        console.log("doctor obj...", doctor);
        let title = doctor.firstName + " " + doctor.lastName;
        let subTitle = doctor.address.street + ", " + doctor.address.cityName;
        this.popover = this.commonService.showTimingList(title, subTitle, doctor.formattedAvailabilities);
        this.commonService.closePopupOnBackBtn(this.popover);
    }



    /*
     * getMoreHospitals for doctor profile
     * */
    getMoreHospitals(hospitalList, doctor) {
        let title = doctor.firstName + " " + doctor.lastName;
        let subTitle = "";
        for (let i = 0; i < doctor.educationList.length; i++) {
            if (i != doctor.educationList.length - 1) {
                subTitle = subTitle + doctor.educationList[i].name + ", ";
            } else {
                subTitle = subTitle + doctor.educationList[i].name;
            }
        }
        this.popover = this.commonService.showHospitalList(title, subTitle, hospitalList);
        this.commonService.closePopupOnBackBtn(this.popover);
    }

    /*
    * goto doctor profile for mobile    
    */
    goToDoctorProfile(doctorId, isAssociatedDocList?: boolean) {
        console.log("from goToDoctorProfile at 469...", isAssociatedDocList);
        this.navCtrl.setRoot("HACreateDoctorPage", { 'mode': 'view', 'id': doctorId, 'from': isAssociatedDocList });
    }


    showGoToLoginPopup() {
        console.log("in show popup");
        this.commonService.showGoTOLoginPopup(this.navCtrl);
    }

    getPatientDependent = () => {
        this.commonService.getFromStorage("userActiveRole").then(( value ) => {
            if( value ) {
                this.userActiveRole = value;
                if(this.userActiveRole == this.constants.ROLE_HOSPITAL_ADMIN){
                    this.commonService.getFromStorage('HospitalDependents').then((value) => {
                        if (value) {
                            this.isPatientDependents = true;
                        } else {
                            this.isPatientDependents = false;
                        }
                    });
                }else if(this.userActiveRole == this.constants.ROLE_DOCTOR){
                    this.commonService.getFromStorage('DoctorDependents').then((value) => {
                        if (value) {
                            this.isPatientDependents = true;
                        } else {
                            this.isPatientDependents = false;
                        }
                    });
                }else if(this.userActiveRole == this.constants.ROLE_PATIENT){
                    this.commonService.getFromStorage('PatientDependents').then((value) => {
                        if (value) {
                            this.isPatientDependents = true;
                        } else {
                            this.isPatientDependents = false;
                        }
                    });
                }else if(this.userActiveRole == this.constants.ROLE_HOSPITAL_STAFF){
                    this.commonService.getFromStorage('HospitalStaffDependents').then((value) => {
                        if (value) {
                            this.isPatientDependents = true;
                        } else {
                            this.isPatientDependents = false;
                        }
                    });
                }

                // this.getPatientDependent();
            }
        }); 
        // if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
        //     this.commonService.getFromStorage('HospitalDependents').then((value) => {
        //         if (value) {
        //             this.isPatientDependents = true;
        //         } else {
        //             this.isPatientDependents = false;
        //         }
        //     });
        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
        //     this.commonService.getFromStorage('DoctorDependents').then((value) => {
        //         if (value) {
        //             this.isPatientDependents = true;
        //         } else {
        //             this.isPatientDependents = false;
        //         }
        //     });

        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
        //     this.commonService.getFromStorage('PatientDependents').then((value) => {
        //         if (value) {
        //             this.isPatientDependents = true;
        //         } else {
        //             this.isPatientDependents = false;
        //         }
        //     });

        // }

    }

    onClickBookAppointment(fromPage: any, redirectPage: any, redirectParam: any, doctorId: any) {
        console.log("DATA:","FROM:"+fromPage +" REDIRECT_PAGE:" +redirectPage +" PARAM:" +redirectParam + " DOC:"+doctorId);
        
        if(this.isHospitalStaffAssociatedDoctors){
            if(this.isPatientSelected){
                this.bookingUrl = "#/booking-step1/" + doctorId + "/HSRoleAssociatedDoctorList/" + redirectParam;
                console.log("BOOKING_URL1",this.bookingUrl);
            }else{
                this.bookingUrl = "#/onboard-patient/HSRoleAssociatedDoctorList/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            }
        }else if(this.fromHAOnboardPatient){
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
        }else if(this.fromPROnboardPatient){
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
        }
        // else if (this.isPatientDependents) {
        //     this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
        //     console.log("BOOKING_URL1",this.bookingUrl);
        // } 
        else {
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            console.log("BOOKING_URL2",this.bookingUrl);
        }
    }

}