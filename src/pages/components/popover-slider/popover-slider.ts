import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, ViewController } from 'ionic-angular';
import { Slides } from 'ionic-angular';

@Component({
    selector: 'popover-slider',
    templateUrl: 'popover-slider.html'
})
export class PopoverSliderComponent {
    @ViewChild('slides') private slides: Slides;
    index: any;
    imageSlide: any = [];
    currentIndex: number = 0;
    slideLength: number = 0;

    constructor( private evts: Events, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController ) {
    
    }

    ionViewWillEnter = () => {
        if( this.navParams.get('data') ){
            let imgData = this.navParams.get('data');
            for( let i=0; i<imgData.length; i++ ){
                if( imgData[i].file != null ){
                    this.imageSlide.push(imgData[i]);
                }
           }
            console.log("this.imageSlide", this.imageSlide);
        }
        if(this.navParams.get('index')){
            this.currentIndex = this.navParams.get('index');
        }
        
    }

    slideChanged() {
        this.currentIndex = this.slides.getActiveIndex();
   }

    slideToNext() {
        this.slides.slideNext();
    }

    slideToPrev() {
        this.slides.slidePrev()
    }
}
