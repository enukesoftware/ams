import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { PopoverSliderComponent } from './popover-slider';
 
@NgModule({
    declarations: [
       PopoverSliderComponent,
    ],
    imports: [
       IonicPageModule.forChild(PopoverSliderComponent),
       TranslateModule.forChild()
    ],
    exports: [
       PopoverSliderComponent
    ]
})
export class PopoverSliderComponentModule {}
