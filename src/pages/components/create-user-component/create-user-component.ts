import { Component, ViewChild, Input, SimpleChanges, SimpleChange } from '@angular/core';
import { IonicPage, Content, NavController, NavParams } from 'ionic-angular';



//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { UserDataDTO, PatientDataDTO, AddressDto } from '../../../interfaces/user-data-dto';
import { LocalStorageService } from '../../../providers/localStorage-service/localStorage.service';
import { CameraService } from '../../../providers/camera-service/camera.service';


import * as moment from 'moment';
import * as _ from 'lodash';
import { HospitalStaffServiceProvider } from '../../../providers/hospital-staff-service/hospital-staff-service';


/**
 * Generated class for the AddPatientComponentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
    selector: 'create-user-component',
    templateUrl: 'create-user-component.html',
})
export class CreateUserComponent {
    
    @Input() navParamsComponent: any;
    @Input() isStaffData:boolean;
    // @Input() data?:any = {};


    @ViewChild(Content) content: Content;
    address: AddressDto = {};
    data: any = {};
    formSubmitted = false;
    userData: UserDataDTO = {};
    inputType: any = "text";
    isDefault: boolean;
    imageUpload: any = {};
    rank: number;
    isUpload: boolean = false;


    cityList: any = [];
    city: any = {};

    fromPage: any;
    previousFromPage: any;
    redirectPage: any;
    redirectParam: any;
    doctorId: any;
    mode: any;
    backPageUrl: any;
    saveDataRedirectUrl: any;
    saveDataRedirectParams: any;

    isSystemAdmin:boolean;
    isHospitalAdmin: boolean;
    isDoctorAdmin: boolean;
    isPatientAdmin: boolean;
    isStaffAdmin:boolean;
    editFlag: boolean;
    createFlag: boolean;
    viewFlag: boolean;




    constructor(public navCtrl: NavController, public cameraService: CameraService, public navParams: NavParams, public commonService: CommonService,
        private constants: Constants, private hospitalStaffServiceProvider: HospitalStaffServiceProvider,
        private locstr: LocalStorageService) {

        this.listOfCities();
    }

    getNavigationData() {
        console.log("NAV:", this.navParamsComponent);
        this.fromPage = this.navParamsComponent.get('fromPage');
        this.previousFromPage = this.navParamsComponent.get('previousFromPage');
        this.redirectPage = this.navParamsComponent.get('redirectPage');
        this.redirectParam = this.navParamsComponent.get('redirectParam');
        this.doctorId = this.navParamsComponent.get('doctorId');
        this.mode = this.navParamsComponent.get('mode');
        this.setPageMode();
        this.getActiveRole();
        this.setPageNavigationData();

        this.commonService.getFromStorage( "hospitalId" ).then(( value ) => {
            if ( value ) {
                this.data.hospitalId = value;
                console.log("HOSPITAL_ID:",value);
            }
        });

        
        
    }

    setPageMode() {
        if (this.mode == "create") {
            this.createFlag = true;
            this.viewFlag = false;
            this.editFlag = false;
        } else if (this.mode == "view") {
            this.createFlag = false;
            this.viewFlag = true;
            this.editFlag = false;
        } else if (this.mode == "edit") {
            this.createFlag = false;
            this.viewFlag = false;
            this.editFlag = true;
            this.commonService.getFromStorage( "selectedStaff" ).then(( value ) => {
                if ( value ) {
                    console.log("Staff:",value);
                    this.data = value;
                    this.address = this.data.address;
                    // this.city.cityId = this.data.address.cityId;
                    // this.city.cityName = this.data.address.cityName;
                    this.city = {"name":this.data.address.cityName,"id":this.data.address.cityId}
                    
                }else{
                    console.log("NO_DATA");
                }
            });
        }
    }

    setPageNavigationData() {
        if (this.fromPage) {
            if (this.fromPage == "HAMHospitalStaff"){
                this.backPageUrl = '#/hospital-admin-manage-hospital-staff';
                this.saveDataRedirectUrl = 'HospitalAdminManageHospitalStaffPage';

                this.saveDataRedirectParams = {
                    
                };

            }
           

        } else {
            this.fromPage = ""
        }


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CreateUserComponent', this.isPatientAdmin);

    }

    ngOnChanges(changes: SimpleChanges) {
        console.log('onchanges called', changes);
        let isPatientAdmin: SimpleChange;
        let isHospitalAdmin: SimpleChange;
        let isDoctorAdmin: SimpleChange;
        let navParams: SimpleChange;

        if (changes.isPatientAdmin) {
            isPatientAdmin = changes.isPatientAdmin;
        } else if (changes.isHospitalAdmin) {
            isHospitalAdmin = changes.isHospitalAdmin;

        } else if (changes.isDoctorAdmin) {
            isDoctorAdmin = changes.isDoctorAdmin;
        }

        if (changes.navParamsComponent) {
            navParams = changes.navParamsComponent;
        }

        if (navParams && navParams.currentValue) {
            this.navParamsComponent = navParams.currentValue;
            this.getNavigationData();
        }

       
    }


    getActiveRole = () => {
        this.commonService.getFromStorage('userActiveRole').then((value) => {
            if (value) {
                console.log("ROLE:", value);

                if (value == this.constants.ROLE_SYSTEM_ADMIN) {
                    this.isSystemAdmin = true;
                    this.isHospitalAdmin = false;
                    this.isDoctorAdmin = false;
                    this.isPatientAdmin = false;
                    this.isStaffAdmin = false;
                } else if (value == this.constants.ROLE_HOSPITAL_ADMIN) {
                    this.isSystemAdmin = false;
                    this.isHospitalAdmin = true;
                    this.isDoctorAdmin = false;
                    this.isPatientAdmin = false;
                    this.isStaffAdmin = false;
                } else if (value == this.constants.ROLE_DOCTOR) {
                    this.isSystemAdmin = false;
                    this.isHospitalAdmin = false;
                    this.isDoctorAdmin = true;
                    this.isPatientAdmin = false;
                    this.isStaffAdmin = false;
                } else if (value == this.constants.ROLE_PATIENT) {
                    this.isSystemAdmin = false;
                    this.isHospitalAdmin = false;
                    this.isDoctorAdmin = false;
                    this.isPatientAdmin = true;
                    this.isStaffAdmin = false;
                }else if(value == this.constants.ROLE_HOSPITAL_STAFF){
                    this.isSystemAdmin = false;
                    this.isHospitalAdmin = false;
                    this.isDoctorAdmin = false;
                    this.isPatientAdmin = false;
                    this.isStaffAdmin = true;
                }

            }
        })
       

    }

   

    /**
    * Function to get list of cities
    */
    listOfCities = () => {
        this.commonService.getListOfCities().subscribe(
            res => {
                if (res.status == "success") {
                    console.log("CITIES:", res);
                    this.cityList = res.data.list;
                    
                    // this.cityList.push({ "name": "Other", "id": "0" });
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }

    onClickSave(form: any) {
        this.formSubmitted = true;

        console.log("VALID:", form.valid);
        console.log("FORM", form);

        // if(event){
        // event.href = this.saveDataRedirectUrl
        // event.location.path = this.saveDataRedirectUrl;
        // this.location.path = this.saveDataRedirectUrl;
        // console.log("EVENT:",event.href);
        // window.location = this.saveDataRedirectUrl;
        // }

        if (form.valid) {
            // this.patientData.userId="";
            this.formSubmitted = false;
            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.data.address = this.address;
            this.data.addressRequestWrapper = this.address;
            //   this.patientData.contactNumber = this.patientData.contactNo;

            if (this.createFlag) {
                this.createUser();
            } else if (this.editFlag) {
                this.updateUser();
               
            }
        }
        else {
            console.log("invalid form", form);
        }
    }


    public createUser = () => {

        this.hospitalStaffServiceProvider.createStaff(this.data).subscribe(
            res => {
                if (res.status == "success") {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                    this.address = this.data.address;
                    console.log("RES:", res);

                    if (res.data) {
                        // res.data.isSelected = true;
                        // this.patientDependents.forEach(element => {
                        //     element.isSelected = false;
                        // });

                        // this.patientDependents.push(res.data);
                        // console.log("DATA:", this.patientDependents);
                        // this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                        // this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);

                        if (this.isUpload && res.data && res.data.userId) {
                            this.uploadImage(res.data.userId);
                        }else{
                            // this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                            
                            if(this.isStaffData && this.isHospitalAdmin){
                                this.navCtrl.setRoot(this.saveDataRedirectUrl);
                            }else{
                                this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
                            }
                        }
                    }

                   
                    // this.navCtrl.setRoot( 'SystemAdminManagePatientsPage' );
                }
                else {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);

            }
        );

    }

    public updateUser = () => {
        // this.data.imageBase64 = null;
       
        this.hospitalStaffServiceProvider.editStaff(this.data).subscribe(
            res => {
                if (res.status == "success") {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                    this.address = this.data.address;
                    console.log("RES:", res);

                    if (res.data) {
                        
                        // let index = this.patientDependents.findIndex(dependent => dependent.userProfileId == this.patientData.userProfileId);
                        // let list = _.cloneDeep(this.patientDependents);
                        // list.splice(index,1);
                        // this.patientDependents = list;
                        // this.patientDependents.push(this.patientData);


                        // this.patientDependents.push(res.data);
                        console.log("DATA:", this.data);
                       

                    // }

                    if (this.isUpload && res.data && res.data.userId) {
                        this.uploadImage(res.data.userId);
                    }else{
                        // this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                        // this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
                        if(this.isStaffData && this.isHospitalAdmin){
                            this.navCtrl.setRoot(this.saveDataRedirectUrl);
                        }else{
                            this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
                        }
                    }
                }
                    // this.navCtrl.setRoot( 'SystemAdminManagePatientsPage' );
                }
                else {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);

            }
        );

    }

    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean) {
        this.rank++;
        this.isDefault = isDefault;
        var image: any = new Image();
        var file: File = $event.target.files[0];
        var myReader: FileReader = new FileReader();
        var that = this;
        this.imageUpload = {
            "file": "",
            "fileName": ""
        }
        myReader.onloadend = (loadEvent: any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback) => {
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if (callback) {
                    this.isUpload = isUpload;
                }
            });
        };
        myReader.readAsDataURL(file);
    }

    /**
     * Function to upload image 
     **/
    uploadImage = (userId: number) => {
        console.log("USERID:" + userId);
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank": 1
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if (res.status == "success") {
                    console.log("upload image sys admin=================", res);
                    // this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                    if(this.isStaffData && this.isHospitalAdmin){
                        this.navCtrl.setRoot(this.saveDataRedirectUrl);
                    }else{
                        this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
                    }
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            });
    }

    /***
     * Function to open camera/photo gallery
     */
    protected onCamera = () => {
        try {
            // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
            this.cameraService.loadImage(this.successCallback, this.errorCallback);
        } catch (e) {

        }
    }

    /**
      * Function to handle camera success callback
      * @param success
      */
    private successCallback = (base64Image: any) => {
        this.isUpload = true;
        this.rank++;
        this.isDefault = true;
        //this.imageUpload.file = base64Image;
        this.commonService.convertImgToBase64(base64Image, (callback) => {
            this.imageUpload.file = callback;
        })
        console.log('success callback', base64Image);
    }

    /**
      * Function to handle camera error callback
      * @param error
      */
    private errorCallback = (error: any) => {
        console.log('Unable to load profile picture.', error);
    }


    /*
    * Function to handle click event on profile photo
    * */
    public comingsoonPopup = () => {
        this.commonService.presentToast();
    }

   

    onCitySelect = (city: any) => {
        console.log("CITY:", city);

        // if (city.name == "Other") {
        //     this.city = {};
        //     this.address.cityId = "";
        //     this.address.cityName = "";
        // } else {
            this.address.cityId = city.id;
            this.address.cityName = city.name;
        // }
    }

   

  

}
