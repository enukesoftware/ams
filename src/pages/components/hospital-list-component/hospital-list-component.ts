import { Component, Input } from '@angular/core';
import { NavController, NavParams, Events, Platform } from 'ionic-angular';
import {  ViewChild, ElementRef, NgZone } from '@angular/core';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition, /*
    MarkerOptions,*/
    Marker,
    ILatLng
   } from '@ionic-native/google-maps';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';
import { MapServiceProvider } from '../../../providers/map-integration-service/map-integration-service';

declare var google;
declare var marker: any;
declare var iLatLng: ILatLng;

@Component({
    selector: 'hospital-list-component',
    templateUrl: 'hospital-list-component.html'
})
export class HospitalListComponent {
    @Input() hospitals: any;
    showActivateBtn:boolean;
    //declare var google;
   // let google: any;
    //@ViewChild( 'googleMap' ) mapElement: ElementRef;
    @ViewChild('map') mapElement: ElementRef;
    
    private latitude: string;
    private longitude: string;
    map: any;
    mapInitialised: boolean = false;
    apiKey: any;
    popover:any;
    
    constructor(private commonService: CommonService, private evts: Events, private constants: Constants, public navCtrl: NavController, public navParams: NavParams, 
                 private manageHospitalService: ManageHospitalServiceProvider,public plt: Platform, public mapServiceProvider:MapServiceProvider) {
        this.plt.registerBackButtonAction(() => {
            this.commonService.closePopupOnBackBtn(this.popover);
        })
    }

    ionViewWillEnter = () => {
        
        //this.activateDeactivateHospital();
    
    }
    
    manageHospital = ( currentHospital ) => {
//        this.commonService.setInStorage( 'selectedHospital',currentHospital );
        /*this.commonService.setInStorage( 'userActiveRole',this.constants.ROLE_HOSPITAL_ADMIN );
        this.commonService.setActiveUserRole( this.constants.ROLE_HOSPITAL_ADMIN );
        this.evts.publish('fire-after-login-event');*/
    }
    
    openTimingDetails = (hospital) => {
        let title = hospital.name;
        let subTitle= hospital.address.street +", "+ hospital.address.cityName;
        this.popover = this.commonService.showTimingList(title, subTitle, hospital.formattedAvailabilities);
    //    this.commonService.closePopupOnBackBtn(this.popover);
    }
    
    comingSoonPopup(){
        this.commonService.presentToast();
    }
    
    
    /**
     * Function to activate or deactivate hospital admin 
     */

    public activateDeactivateHospital =(hospital) => {
            let activationStatus:any;
            this.commonService.getActivationStatus(hospital,(cb)=>{
                console.log("cb",cb);
                activationStatus =cb;
            });
    
            let hospitalObj ={
                    HospitalId:hospital.hospitalId,
                    action:activationStatus.Action
            }
            
           let activationPopUpMsg = this.commonService.getTranslate('CONFIRM_AD_MSG1',{})+  this.commonService.getTranslate(activationStatus.text,{})
           let warningTitle =  this.commonService.getTranslate('WARNING',{});
           let alert = this.commonService.confirmAlert(warningTitle, activationPopUpMsg+'?');
           alert.setMode("ios");
           alert.present();
           alert.onDidDismiss((data) => {
                  if(data == true){
                      this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
                      this.manageHospitalService.activateDeactivateHospital(hospitalObj).subscribe(
                      res => {
                        if ( res.status == "success" ) {
                            this.commonService.hideLoading();
                            if(hospital.blockStatus =="UNBLOCK"){
                                hospital.blockStatus ="BLOCK";
                            } 
                            else if(hospital.blockStatus =="BLOCK"){
                                hospital.blockStatus ="UNBLOCK";
                            }
                            this.commonService.hideLoading();
                            this.commonService.presentToast(res.message);
                        }
                        else{
                            this.commonService.hideLoading();
                            this.commonService.presentToast(res.message);
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(error.message);
                });
             
                 }
                  else{this.commonService.hideLoading();}
              });
         // this.commonService.showLoading( this.constants.PLEASE_WAIT_TEXT );
     }

    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public plotAddressOnMap = ( hospital: any ) => {
        let currentHospitalAddress = hospital.name+ "+" +hospital.address.street + "+" + hospital.address.cityName + "+" + 
                                     hospital.address.state + "+" + hospital.address.zipCode;
        this.commonService.setAddressOnMap(currentHospitalAddress);
    }
    
}
