import { Component, Input } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { HospitalStaffServiceProvider } from '../../../providers/hospital-staff-service/hospital-staff-service';

@Component({
    selector: 'system-admin-list-component',
    templateUrl: 'system-admin-list-component.html'
})
export class SystemAdminListComponent {
    @Input() systemAdmins: any;
    @Input() from: string;
    @Input() fromDoctorRoleAssociatedHospitalDetails: any;
    isManageSysAdmin: boolean = false;
    selectedAdmin: any;


    constructor(private commonService: CommonService, private evts: Events, private constants: Constants, public navCtrl: NavController, public navParams: NavParams, public hospitalStaffServiceProvider: HospitalStaffServiceProvider) {
    }

    manageHospital = (currentHospital) => {
        console.log("HospitalListComponent manageHospital currentHospital ================================>", currentHospital);
        this.commonService.setInStorage('selectedHospital', currentHospital);
        this.commonService.setInStorage('userActiveRole', this.constants.ROLE_HOSPITAL_ADMIN);
        this.commonService.setActiveUserRole(this.constants.ROLE_HOSPITAL_ADMIN);
        this.evts.publish('fire-after-login-event');
    }


    public goToSystemAdminItem = (selectedAdmin) => {
        this.isManageSysAdmin = true;
        this.selectedAdmin = selectedAdmin;
        console.log("in go to sys admin ", this.selectedAdmin);
        this.navCtrl.push('SystemAdminCreateSystemAdminPage', {
            selectedAdmin: this.selectedAdmin, isManageSysAdmin: this.isManageSysAdmin
        });
    }

    onClickEdit(data: any) {
        console.log("SATFF_DATA:", data);
        // this.commonService.removeFromStorage('selectedStaff');
        if (this.from == 'hospitalAdminStaffList') {
            this.commonService.setInStorage('selectedStaff', data);
            // href="#/hospital-admin-create-staff/HAMHospitalStaff/0/0/0/0/edit"
            this.navCtrl.push('HospitalAdminCreateStaffPage', {
                fromPage: 'HAMHospitalStaff',
                previousFromPage: 0,
                redirectPage: 0,
                redirectParam: 0,
                doctorId: 0,
                mode: 'edit'
            });

            // this.commonService.getFromStorage( "selectedStaff" ).then(( value ) => {
            //     if ( value ) {
            //         console.log("Staff:",value);
            //     }
            // });
        }
    }

    onClickHolidayCalendar(data:any){
        // href="#/hospital-admin-holiday-calendar/create/{{systemAdmin?.userId}}/HAStaffHolidayCalendar"
        if (this.from == 'hospitalAdminStaffList') {
            this.commonService.setInStorage('selectedStaff', data);
            this.navCtrl.push('HospitalAdminHolidayCalendarPage', {
                fromPage: 'HAStaffHolidayCalendar',
                adminId: data.userId,
                mode: 'create'
            });
        }
    }


    /**
     * Function to activate or deactivate staff
     */

    public activateDeactivateStaff = (staffData: any) => {
        let activationStatus: any;
        this.commonService.getActivationStatus(staffData, (cb) => {
            activationStatus = cb;
        });

        let staffObj = {
            userId: staffData.userId,
            action: activationStatus.Action
        }

        let activationPopUpMsg = this.commonService.getTranslate('CONFIRM_AD_MSG1', {}) + this.commonService.getTranslate(activationStatus.text, {})
        let warningTitle = this.commonService.getTranslate('WARNING', {});
        let alert = this.commonService.confirmAlert(warningTitle, activationPopUpMsg + '?');
        alert.setMode("ios");
        alert.present();
        alert.onDidDismiss((data) => {
            if (data == true) {
                this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
                this.hospitalStaffServiceProvider.activateDeactivateStaff(staffObj).subscribe(
                    res => {
                        if (res.status == "success") {
                            this.commonService.hideLoading();
                            if (staffData.blockStatus == "UNBLOCK") {
                                console.log("in if", staffData);
                                staffData.blockStatus = "BLOCK";
                            }
                            else if (staffData.blockStatus == "BLOCK") {
                                staffData.blockStatus = "UNBLOCK";
                            }
                            this.commonService.hideLoading();
                            this.commonService.presentToast(res.message);
                        }
                        else {
                            this.commonService.hideLoading();
                            this.commonService.presentToast(res.message);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        this.commonService.presentToast(error.message);
                    });
            }
        });
    }

}
