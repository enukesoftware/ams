import { Component, NgZone  } from '@angular/core';
import { ViewController, NavController, NavParams } from "ionic-angular";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { CommonService } from "../../../providers/common-service/common.service";
import { SearchServiceProvider } from "../../../providers/search-service/search-service";

@Component({
  selector: 'search-filter',
  templateUrl: 'search-filter.html'
})
export class SearchFilterComponent {
  feesSlots: any;
  availabilities: any;
  mode: any;
  inHospital: any;
  feesTickUrl: any;
  open24HrsTick: boolean = false;
  open24HrsTickImg: string;
  multiSpeciality: boolean = false;
  multiSpTickUrl: string;
  city: any;
  searchData: any = {};
  doctorExp: any;
  feesArray: any = [];

  constructor(public viewCtrl: ViewController, private searchServiceProvider: SearchServiceProvider, private constants: Constants, public manageHospitalServiceProvider: ManageHospitalServiceProvider, public navCtrl: NavController, public navParams: NavParams,  private commonService: CommonService,
           private _ngZone: NgZone) {
      this.feesArray = [];
      this.city = this.navParams.data.city;
      this.mode = this.navParams.data.mode;
      if(this.mode == "hospital"){
          this.open24HrsTickImg = "assets/imgs/01_multispacility_uncheck.png";
          this.multiSpTickUrl = "assets/imgs/01_multispacility_uncheck.png";
          if(this.navParams.data.allDayOpen){
              this.open24HrsTickImg = "assets/imgs/01_multispacility_checked.png";
              this.open24HrsTick = true;
          }
          if(this.navParams.data.multiSpeciality){
              this.multiSpTickUrl = "assets/imgs/01_multispacility_checked.png";
              this.multiSpeciality = true;
          }
      }
      if(this.mode == "doctor"){
          this.feesSlots = this.navParams.data.feesSlots;
          this.availabilities = this.navParams.data.availability;
          this.doctorExp = this.navParams.data.doctorExp;
      }
  }
  
  feesOptionClick(index){
      this._ngZone.run(() => {
      this.feesSlots[index].tick = !this.feesSlots[index].tick;
      if(this.feesSlots[index].tick){
          this.feesSlots[index].tickUrl = "assets/imgs/01_multispacility_checked.png";
      } else{
          this.feesSlots[index].tickUrl = "assets/imgs/01_multispacility_uncheck.png";
      }
      })
  }
  
  availabilityClick(index){
      this._ngZone.run(() => {
          this.availabilities[index].tick = !this.availabilities[index].tick;
          for(let i=0; i<this.availabilities.length; i++){
              if(i == index && this.availabilities[index].tick){
                  this.availabilities[index].tick = true;
                  this.availabilities[index].tickUrl = "assets/imgs/01_multispacility_checked.png";
              } else{
                  this.availabilities[i].tick = false;
                  this.availabilities[i].tickUrl = "assets/imgs/01_multispacility_uncheck.png";
              }
          }
      })
  }
  
  resetFilters(){
      if(this.mode == "doctor"){
          if(this.feesSlots){
              let feesSlotsLen = this.feesSlots.length;
              for(let i=0; i<feesSlotsLen; i++){
                  this.feesSlots[i].tick = false;
                  this.feesSlots[i].tickUrl = "assets/imgs/01_multispacility_uncheck.png";
              }
          }
          if(this.availabilities){
              let availabilitiesLen = this.availabilities.length;
              for(let i=0; i<availabilitiesLen; i++){
                  this.availabilities[i].tick = false;
                  this.availabilities[i].tickUrl = "assets/imgs/01_multispacility_uncheck.png";
              }
          }
          this.doctorExp = {
              'lower': 0,
              'upper': 50
          }
      }
      if(this.mode == "hospital"){
          this.open24HrsTickImg = "assets/imgs/01_multispacility_uncheck.png";
          this.multiSpTickUrl = "assets/imgs/01_multispacility_uncheck.png";
          this.multiSpeciality = false;
          this.open24HrsTick = false;
      }
  }
      
  /*
   * Open 24 Hrs tick handle   
   */    
  
 open24HrsClick(){
  this._ngZone.run(() => {
          this.open24HrsTick = !this.open24HrsTick;
          if(this.open24HrsTick){
              this.open24HrsTickImg = "assets/imgs/01_multispacility_checked.png";
          } else{
             this.open24HrsTickImg = "assets/imgs/01_multispacility_uncheck.png";
          }
      });
 }
      
 /*
  * Handle multispeciality click    
  */
      
  multiSpecialityClick(){
      this._ngZone.run(() => {
          this.multiSpeciality = !this.multiSpeciality;
          if(this.multiSpeciality){
              this.multiSpTickUrl = "assets/imgs/01_multispacility_checked.png";
          } else{
              this.multiSpTickUrl = "assets/imgs/01_multispacility_uncheck.png"; 
          }
      });
  }
      
 /**
 * Cancel filter
 ***/
  cancelFilter(){
      this.viewCtrl.dismiss(false);
  }
      
  /**
  * Apply filter
  ***/
 applyFilter(){
     this._ngZone.run(() => {
     
      if(this.mode == "hospital"){
          let filtersArray = [];
          if(this.open24HrsTick && !this.multiSpeciality){
              filtersArray = [{
                      filterName: "is24HrOpen",
                      rangeFrom: "",
                      rangeTo: "",
                      selectedValues: [true]
                  }];
          } else if(!this.open24HrsTick && this.multiSpeciality){
              filtersArray = [{
                      filterName: "isAMultispeciality",
                      rangeFrom: "",
                      rangeTo: "",
                      selectedValues: [true]
                 }];
          } else if(this.open24HrsTick && this.multiSpeciality){
              filtersArray = [{
                  filterName: "is24HrOpen",
                  rangeFrom: "",
                  rangeTo: "",
                  selectedValues: [true]
                 },{
                  filterName: "isAMultispeciality",
                  rangeFrom: "",
                  rangeTo: "",
                  selectedValues: [true]
                }];
          } else{
                  filtersArray = [{
                      filterName: "",
                      rangeFrom: "",
                      rangeTo: "",
                      selectedValues: [true]
             }];
          }
          let searchHospitalObj = {
              searchText: "",
              city: this.city,
              currentDocIndex: 0,
              currentPage: 0,
              sortField:"",
              type: "hospital",
              filters:filtersArray
          }
      
          this.getSearchResults(searchHospitalObj);
      } else if(this.mode == "doctor"){
          let filtersArray = [];
          let availabiltyArray = [];
          let feesRangeFrom;
          let feesRangeTo;
      
          for(let i=0; i<this.feesSlots.length; i++){
              if(this.feesSlots[i].tick){
                  this.feesArray.push(this.feesSlots[i]);
                      console.log("feesArray..",this.feesArray)
              }
          }
      
          for(let i=0; i<this.availabilities.length; i++){
              if(this.availabilities[i].tick){
                  availabiltyArray.push(this.availabilities[i]);
              }
          }
          if( this.feesArray.length > 0){
         
              feesRangeFrom = this.feesArray[0].lowerLimit;
              feesRangeTo = this.feesArray[this.feesArray.length - 1].upperLimit;
            
              if(feesRangeFrom == undefined){
                  feesRangeFrom = "";
              }
              
              if(feesRangeTo == undefined){
                  feesRangeTo = "";
              }
              let searchDoctorlObj;
              if(availabiltyArray.length == 0 && this.doctorExp.upper != 0){
                  filtersArray = [{
                      filterName: "fees",
                      rangeFrom: feesRangeFrom,
                      rangeTo: feesRangeTo,
                      selectedValues: [true]
                  }, {
                      filterName: "yearsOfExp",
                      rangeFrom: this.doctorExp.lower,
                      rangeTo: this.doctorExp.upper,
                      selectedValues: [true]
                  }];
              } else if(availabiltyArray.length == 0 && this.doctorExp.upper == 0){
                  filtersArray = [{
                      filterName: "fees",
                      rangeFrom: feesRangeFrom,
                      rangeTo: feesRangeTo,
                      selectedValues: [true]
                  }];
              } else {
                  for(let i=0; i<this.availabilities.length; i++){
                      if(this.availabilities[i].tick && this.doctorExp.upper == 0){
                          filtersArray = [{
                              filterName: "fees",
                              rangeFrom: feesRangeFrom,
                              rangeTo: feesRangeTo,
                              selectedValues: [true]
                          }, {
                              filterName: this.availabilities[i].value,
                              rangeFrom: "",
                              rangeTo: "",
                              selectedValues: [true]
                          }];
                      } else if(this.availabilities[i].tick){
                          filtersArray = [{
                              filterName: "fees",
                              rangeFrom: feesRangeFrom,
                              rangeTo: feesRangeTo,
                              selectedValues: [true]
                          }, {
                              filterName: "yearsOfExp",
                              rangeFrom: this.doctorExp.lower,
                              rangeTo: this.doctorExp.upper,
                              selectedValues: [true]
                          }, {
                              filterName: this.availabilities[i].value,
                              rangeFrom: "",
                              rangeTo: "",
                              selectedValues: [true]
                          }];
                      }
                  }
              }
                  
              this.commonService.setInStorage("doctorFiltersArray", filtersArray);     
                  
              searchDoctorlObj = {
                  searchText: "",
                  city: this.city,
                  currentDocIndex: 0,
                  currentPage: 0,
                  sortField:"",
                  type: "doctor",
                  filters:filtersArray
              };
                  
              this.getSearchResults(searchDoctorlObj);
              } else if(this.feesArray.length == 0 && availabiltyArray.length > 0 || this.doctorExp.upper > 0){
                  let onlyAvailablityOrExpArray = [];
                  let availabilityFlag = false;
                  for(let i=0; i<this.availabilities.length; i++){
                      if(this.availabilities[i].tick){
                          availabilityFlag = true;
                          onlyAvailablityOrExpArray=[{
                              filterName: this.availabilities[i].value,
                              rangeFrom: "",
                              rangeTo: "",
                              selectedValues: [true]
                          },{
                              filterName: "yearsOfExp",
                              rangeFrom: this.doctorExp.lower,
                              rangeTo: this.doctorExp.upper,
                              selectedValues: [true]
                          }];
                      }
                  }
                  if(!availabilityFlag){
                      onlyAvailablityOrExpArray = [{
                          filterName: "yearsOfExp",
                          rangeFrom: this.doctorExp.lower,
                          rangeTo: this.doctorExp.upper,
                          selectedValues: [true]
                      }];
                  }
                  this.commonService.setInStorage("doctorFiltersArray", onlyAvailablityOrExpArray);
                  let searchDoctoAvailableObj = {
                      searchText: "",
                      city: this.city,
                      currentDocIndex: 0,
                      currentPage: 0,
                      sortField:"",
                      type: "doctor",
                      filters:onlyAvailablityOrExpArray
                  }
                  this.getSearchResults(searchDoctoAvailableObj);
              } else {
                  let filterOnlyExp = [];
                  let searchDoctorObj;
                  if(this.doctorExp.upper > 0){
                      filterOnlyExp = [{
                          filterName: "yearsOfExp",
                          rangeFrom: this.doctorExp.lower,
                          rangeTo: this.doctorExp.upper,
                          selectedValues: [true]                 
                      }];
                      this.commonService.setInStorage("doctorFiltersArray", filterOnlyExp);
                  } else{
                      filterOnlyExp = [{
                          filterName: "",
                          rangeFrom: "",
                          rangeTo: "",
                          selectedValues: [true]           
                      }];
                      this.commonService.setInStorage("doctorFiltersArray", filterOnlyExp);
                  }
                  searchDoctorObj = {
                          searchText: "",
                          city: this.city,
                          currentDocIndex: 0,
                          currentPage: 0,
                          sortField:"",
                          type: "doctor",
                          filters:filterOnlyExp
                      }
                  this.getSearchResults(searchDoctorObj);
              }
          }
          });
      }

       getSearchResults(searchHospitalObj){
           this.searchServiceProvider.getSearchResult(searchHospitalObj).subscribe(
                   res=>{
                       if( res.status == "success"){
                           if(this.mode == "hospital"){
                               this.searchData.hospitals = res.data.hospitals;
                               this.searchData.searchParams = {
                                   allDayOpen:this.open24HrsTick,
                                   multiSpeciality: this.multiSpeciality
                              }
                           } else if(this.mode == "doctor"){
                               this.searchData.doctors = res.data.doctors;
                               console.log("doc filters",this.searchData.doctors);
                               this.searchData.searchParams = {
                                    "feesSlots": this.feesSlots,
                                    "availability": this.availabilities,
                                    "doctorExp": this.doctorExp
                               }
                           }
                       }
                       this.viewCtrl.dismiss(this.searchData);
                   }, error=>{
                       let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                       this.commonService.presentToast(errorMsg);
                       this.viewCtrl.dismiss(false);
                   });
       }
}
