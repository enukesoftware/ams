import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from "ionic-angular";
import { CommonService } from "../../../providers/common-service/common.service";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { Location } from '@angular/common';

@Component({
  selector: 'cancel-appointment-confirmation',
  templateUrl: 'cancel-appointment-confirmation.html'
})
export class CancelAppointmentConfirmationComponent {
    agree: boolean;
    chkImgUrl: any;
    popover: any;

  constructor(public viewCtrl: ViewController, private constants: Constants, public platform: Platform, public location: Location, public navCtrl: NavController, public navParams: NavParams,  private commonService: CommonService) {
      this.chkImgUrl = "assets/imgs/01_multispacility_uncheck.png";
      this.commonService.closePopupOnBackBtn(this.popover);
  }
    
    /*
   * Function to show comingsoon popup
   **/
  comingSoonPopup = () => {
    this.commonService.presentToast();
  }
    
    cancel( disAgree: any ){
        console.log("disAgree.........", disAgree);
        this.viewCtrl.dismiss(disAgree);
    }
    
    ok( agree: any ){
       this.viewCtrl.dismiss(agree);
    }
    
    openPrivacyPolicy(){
        this.popover = this.commonService.showPrivacyPolicyAlert("Privacy Policy", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
        this.commonService.closePopupOnBackBtn(this.popover);
    }
    
    chk24HoursClick(){
        this.agree = !this.agree;
        if(this.agree){
            this.chkImgUrl = "assets/imgs/01_multispacility_checked.png"
        } else{
            this.chkImgUrl = "assets/imgs/01_multispacility_uncheck.png"
        }
    }
}
