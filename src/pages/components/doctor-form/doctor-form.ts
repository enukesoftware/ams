import { Component, Input } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { NgForm } from '@angular/forms';
//Provider Services
import { CommonService } from '../../../providers/common-service/common.service';
import { HospitalListDTO, DoctorDataDto, AddressDto, serviceAndSpecializationDataDto, UserDataDTO } from '../../../interfaces/user-data-dto';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";

//3rd party npm module
import * as _ from 'lodash';

@Component({
    selector: 'doctor-form',
    templateUrl: 'doctor-form.html',
})
export class DoctorFormComponent {
    servicesString: any;
    specializationString: any;
    formSubmitted: boolean;
    doctorEducation: any[];
    selectSpec: boolean = false;
    selectCat: boolean = true;
    selectService: boolean = false;
    selectEdu: boolean = false;
    selectCity: boolean = false;
    userData: UserDataDTO = {};
    isWeb: boolean = false;
    cityList: any;
    doctorEdt: any;
    categories: any;
    category: any;
    selectedCategories: any = [];
    services: any;
    servicesArr: serviceAndSpecializationDataDto[];
    specialization: any;
    specializationArr: serviceAndSpecializationDataDto[];
    editFlag: boolean = false;
    createFlag: boolean = false;
    doctorId: any;
    mode: any;
    inputType: any = "text";
    @Input() doctorData: DoctorDataDto = {};
    @Input() address: AddressDto = {};
    @Input() viewFlag: any;
    
    constructor(public navCtrl: NavController, public navParams: NavParams,  private commonService: CommonService,
            private constants: Constants,private manageSysAdminServiceProvider:ManageSysAdminServiceProvider, private manageHospitalServiceProvider: ManageHospitalServiceProvider) {
        this.listOfCities();
        this.getEducation();
        this.getSpecializationServices();
        this.isWeb = this.commonService.checkIsWeb();
        this.getDoctorInfo();
        this.doctorData.serviceList = [];
        this.doctorData.specializationList = [];
    }
    
    getDoctorInfo(){
        this.manageHospitalServiceProvider.getDoctorDetails(this.doctorId).subscribe(res=>{
            this.doctorData = res.data;
            this.address = res.data.address;
            
        }, error=>{
            //this.commonService.showAlert("Error", err.message);
            let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
            this.commonService.presentToast(errorMsg);
        });
    }
    
    cityChange() {
        this.selectCity = false;
        
        let cityLen = this.cityList.length;
        for(let i=0; i<cityLen; i++){
            if(this.address.cityId == this.cityList[i].id){
                this.address.cityName = this.cityList[i].name;
            }
        }
    }
    
    listOfCities = () => {
        this.commonService.getListOfCities().subscribe(
            res => {
                if ( res.status == "success" ) {
                    this.cityList = res.data.list;
                    this.address.cityId = "0";
                }
            },
            error => {
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
                //this.commonService.showAlert( "Error", error.message );
            }
        );
    }
    
   
    /*
     ** Get list of education 
     */  
    getEducation() {
        this.manageHospitalServiceProvider.getDoctorEducation().subscribe( res => {
            if ( res.status == "success" ) {
                this.doctorEducation = res.data.list;
                if(this.doctorEducation){
                    let educationLen = this.doctorEducation.length;
                    for( let n=0;n<educationLen;n++ ){
                        
                        this.doctorEducation[n].value = this.doctorEducation[n].id;
                        this.doctorEducation[n].label = this.doctorEducation[n].name;
                    }
                }
            }
        } )
    }
    /*
     ** Get selected education 
     */
    
    getDoctorEdu( selectedValue: any ){
        this.selectEdu = false;
        let eduLen = this.doctorData.educationList.length;
        if ( eduLen == 0 ) {
            this.doctorData.educationList = "";
        }
        if ( eduLen > 1 ) {
            for ( let j = 0; j < eduLen; j++ ) {
                if ( !this.doctorData.educationList[j].id ) {
                    this.doctorData.educationList.shift();
                    this.selectEdu = true;
                    break;
                }
            }
        }
    }
    
    
    
    /*
     * Function to get category , specialization and services
     * */
    getSpecializationServices = () => {
        this.manageHospitalServiceProvider.getDoctorSpecializationServices().subscribe( res => {
            console.log("isWeb:......", this.isWeb);
            try{
                if ( res.status == "success" ) {
                    this.categories = res.data;
                    var catIdArr = [];
                    if( this.categories ){
                        let categoryLen = this.categories.length;
                        for( let n=0;n<categoryLen;n++ ){
                            this.categories[n].value = this.categories[n];
                            this.categories[n].label = this.categories[n].name;
                        }
                        var catLen = this.categories.length;
                            
                        if(this.doctorData.serviceList){  
                            var serviceLen = this.doctorData.serviceList.length;
                            if(serviceLen > 0){
                                for(let i=0; i<serviceLen; i++){
                                    if(catIdArr.indexOf( this.doctorData.serviceList[i].categoryId ) == -1 ){
                                       catIdArr.push(this.doctorData.serviceList[i].categoryId);
                                    }
                                }
                            }
                        }
                       if(this.doctorData.specializationList){
                           var specLen = this.doctorData.specializationList.length;     
                           if(specLen > 0){
                               for(let i=0; i<specLen; i++){
                                   if(catIdArr.indexOf( this.doctorData.specializationList[i].categoryId ) == -1 ){
                                       catIdArr.push(this.doctorData.specializationList[i].categoryId);
                                   }
                               }
                           }
                       }    
                        var catIdLen = catIdArr.length; 
                        var selectedCats = [];  
                        if(catLen > 0){
                           for(let i=0; i<catLen; i++){
                               for(let j=0; j<catIdLen; j++){
                                  if(this.categories[i].id == catIdArr[j]){
                                       selectedCats.push(this.categories[i]);
                                   }
                               }
                           }
                           this.category = selectedCats;
                       }
                   }
               }
               this.onChangeCategory();
           }catch(err){
             console.log("err on 166...", err);      
         }
        }, error => {
            console.log( "specialization error.....", error );
        } );
    }

   onChangeCategory(dropDownChange?: boolean){
       this.specializationArr = [];
       this.servicesArr = [];
       try{
           if( this.category ){
               for( let h=0;h<this.category.length;h++ ){
                   if( this.category[h].specializationList ){
                       for( let t=0;t<this.category[h].specializationList.length;t++ ){
                           this.category[h].specializationList[t].categoryId = this.category[h].id;
                           this.category[h].specializationList[t].label = this.category[h].specializationList[t].name;
                           this.category[h].specializationList[t].value = this.category[h].specializationList[t];
                           this.specializationArr.push( this.category[h].specializationList[t] );
                           if(this.doctorData.specializationList != null){
                               for( let r=0;r<this.doctorData.specializationList.length;r++ ){
                                   if( this.doctorData.specializationList[r].id == this.category[h].specialization[t].id && this.doctorData.specializationList[r].categoryId == this.category[h].id ){
                                       this.doctorData.specializationList.splice( r,1 );
                                       this.doctorData.specializationList.splice( r,0,this.category[h].specialization[t].value );
                                   }
                               }
                           }
                       }
                   }
                   if ( this.category[h].serviceList ) {
                       for ( let d=0;d<this.category[h].serviceList.length;d++ ){
                           this.category[h].serviceList[d].categoryId = this.category[h].id;
                           this.category[h].serviceList[d].label = this.category[h].serviceList[d].name;
                           this.category[h].serviceList[d].value = this.category[h].serviceList[d];
                           this.servicesArr.push( this.category[h].serviceList[d] );
                           for( let q=0;q<this.doctorData.serviceList.length;q++ ){
                               if( this.doctorData.serviceList[q].id == this.category[h].serviceList[d].id && this.doctorData.serviceList[q].categoryId == this.category[h].id ){
                                   this.doctorData.serviceList.splice( q,1 );
                               }
                           }
                       }
                   }
               }
               console.log( "onChangeCategory servicesArr ==========================>",this.servicesArr);
               console.log( "onChangeCategory serviceList ==========================>",this.doctorData.serviceList);
               console.log( "onChangeCategory specializationArr ==========================>",this.specializationArr);
               console.log( "onChangeCategory specializationList ==========================>",this.doctorData.specializationList);
               if( dropDownChange ){
                   if( this.specializationArr.length <= 0 ){
                       this.doctorData.specializationList = [];                        
                   }
               
                   if( this.servicesArr.length <= 0 ){
                       this.doctorData.serviceList = [];
                   }
                   for( let b=0;b<this.doctorData.specializationList.length;b++ ){
                       let foundSpecializationCat = _.findIndex(this.specializationArr,(o) => { return o.categoryId == this.doctorData.specializationList[b].categoryId; });
                       if( foundSpecializationCat == -1 ){
                           this.doctorData.specializationList.splice( b,1 );
                       }
                   }
           
                   for( let z=0;z<this.doctorData.serviceList.length;z++ ){
                       let foundServiceCat = _.findIndex(this.servicesArr,(o) => { return o.categoryId == this.doctorData.serviceList[z].categoryId; });
                       if( foundServiceCat == -1 ){
                           this.doctorData.serviceList.splice( z,1 );
                       }
                   }
               }
           }
       }catch(err){
           console.log("err on category...", err);
       }                
       
       this.getSpecialization();
       this.getServices();
   }
                
    /**
     * On specialization change display selected specialization in block
     **/
    getSpecialization = ( selectedValue?: any ) => {
        try{
            if(this.doctorData.specializationList){
                let specLen = this.doctorData.specializationList.length;
                if ( specLen == 0 ) {
                    this.selectSpec = false
                }
                
                
                this.specializationString = "";
                let selectedSpecLen = this.doctorData.specializationList.length;
                for ( let i = 0; i < selectedSpecLen; i++ ) {
                    if ( i < selectedSpecLen - 1 ) {
                        this.specializationString = this.specializationString + this.doctorData.specializationList[i].name + ", ";
                    } else {
                        this.specializationString = this.specializationString + this.doctorData.specializationList[i].name;
                    }
                }
            }
        }catch(err){
            console.log("err on specialization......", err)
        }
    }

    /**
    * On service change, display selected services in block
    **/
    getServices = ( selectedValue?: any ) => {
        try{
            if(this.doctorData.serviceList){
                let serviceLen = this.doctorData.serviceList.length;
                if ( serviceLen == 0 ) {
                    this.selectService = false;
                }
                
                this.servicesString = "";
                let selectedServiceLen = this.doctorData.serviceList.length;
                for ( let i = 0; i < selectedServiceLen; i++ ) {
                    if ( i < selectedServiceLen - 1 ) {
                        this.servicesString = this.servicesString + this.doctorData.serviceList[i].name + ", ";
                    } else {
                        this.servicesString = this.servicesString + this.doctorData.serviceList[i].name;
                    }
                }
            }
        }catch(err){
            console.log("err......", err);
        }
    }
    
    createDoctor = ( formData: NgForm ) => {
        this.formSubmitted = true;
        console.log("formData....", formData);
        if(this.address.cityId == 0 || this.address.cityId == undefined){
            this.commonService.showAlert("Error!", "Please select your city.");
            this.commonService.presentToast("Please select your city.");
        } else{
            if ( formData.valid ) {
                
                this.formSubmitted = false;
                for( let m=0;m<this.doctorData.specializationList.length;m++ ){
                    delete this.doctorData.specializationList[m].label;
                    delete this.doctorData.specializationList[m].value;
                }
                for( let n=0;n<this.doctorData.serviceList.length;n++ ){
                    delete this.doctorData.serviceList[n].label;
                    delete this.doctorData.serviceList[n].value;
                }
                for( let e=0; e<this.doctorData.educationList.length; e++ ){
                    delete this.doctorData.educationList[e].label;
                    delete this.doctorData.educationList[e].value;
                }

                this.doctorData.address = this.address;
                
                this.commonService.showLoading( "Please wait" );
                this.manageHospitalServiceProvider.createDoctor( this.doctorData ).subscribe( res => {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                    setTimeout(()=>{
                        this.navCtrl.setRoot("HospitalAdminDoctorListPage");
                    }, 3000);
                    /*let alert = this.commonService.showAlert( res.status, res.message );
                    alert.onDidDismiss(() => {
                        this.navCtrl.setRoot( "HospitalAdminDoctorListPage" );
                    } );*/
                }, error => {
                    this.commonService.hideLoading();
                    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                    this.commonService.presentToast(errorMsg);
                    //this.commonService.showAlert( "Error", err.message );
                } );
            }
        }
    }

}
