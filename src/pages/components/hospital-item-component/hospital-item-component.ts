import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController, NavParams, Events, Platform } from 'ionic-angular';
import {  ViewChild, ElementRef, NgZone } from '@angular/core';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition, /*
    MarkerOptions,*/
    Marker,
    ILatLng
   } from '@ionic-native/google-maps';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';
import { MapServiceProvider } from '../../../providers/map-integration-service/map-integration-service';

/**
 * Generated class for the HospitalItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'hospital-item-component',
  templateUrl: 'hospital-item-component.html'
})
export class HospitalItemComponent {
  @Input('hospital') hospital: any;
  @Output('hospitalSelect') hospitalSelect= new EventEmitter(); 
  text: string;
  popover:any;

  constructor(private commonService:CommonService) {
    console.log('Hello HospitalItemComponent Component');
    this.text = 'Hello World';
  }

  // select() {
  //   this.hospitalSelect.emit(this.hospital);
  // }

   /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public plotAddressOnMap = ( hospital: any ) => {
      let currentHospitalAddress = hospital.name+ "+" +hospital.address.street + "+" + hospital.address.cityName + "+" + 
                                   hospital.address.state + "+" + hospital.address.zipCode;
      this.commonService.setAddressOnMap(currentHospitalAddress);
    }

    openTimingDetails = (hospital) => {
      let title = hospital.hospitalDetails.name;
      let subTitle = hospital.hospitalDetails.address.street + ", " + hospital.hospitalDetails.address.cityName;
      this.popover = this.commonService.showTimingList(title, subTitle, hospital.formattedAvailabilities);
      this.commonService.closePopupOnBackBtn(this.popover);
    }

}
