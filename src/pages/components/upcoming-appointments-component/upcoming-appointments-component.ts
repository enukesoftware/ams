import { Component, Input } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';

//Provider Services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import * as moment from 'moment';

@Component({
    selector: 'upcoming-appointments-component',
    templateUrl: 'upcoming-appointments-component.html',
})
export class UpcomingAppointmentComponent {
    @Input() fromDoctorAppointmentHistory;
    @Input() fromDoctorUpcomingAppointment;
    @Input() fromHospitalUpcomingAppointment;
    @Input() appointmentList;
    @Input() fromHospitalStaffManageAppointment;
    @Input() fromHospitalAdminManageAppointment;
    appointmentDetails: any;


    isPatientDependents: boolean = false;
    bookingUrl: any;
    
    constructor(public navCtrl: NavController, public navParams: NavParams,  private commonService: CommonService,
                private constants: Constants) {

                    this.getPatientDependent();
    }
    
    ionViewWillEnter= () => {
    }
    /**
     * Function to show Coming Soon popup
     **/
    comingSoonPopup() {
        this.commonService.presentToast();
    }
    
    /*
     * Function to show today timings
     * */
    showTimeSlots(hospitalDetails, availabilities, appointmentDate){
        let title = hospitalDetails.name;
        let subTitle = hospitalDetails.address.street +", "+ hospitalDetails.address.cityName;
        let selectedDate = appointmentDate;
        var newAppDate = moment(appointmentDate).toDate();
        let appDate = this.commonService.formatDateString(new Date(newAppDate), true);
        
        this.commonService.showTodayTimings(title, subTitle, appDate, availabilities);
    }

    getPatientDependent = () => {
        if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            this.commonService.getFromStorage('HospitalDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });
        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            this.commonService.getFromStorage('DoctorDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            this.commonService.getFromStorage('PatientDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;
                }
            });

        }

    }

    onClickBookAppointment(fromPage: any, redirectPage: any, redirectParam: any, doctorId: any) {
        console.log("DATA:","FROM:"+fromPage +" REDIRECT_PAGE:" +redirectPage +" PARAM:" +redirectParam + " DOC:"+doctorId);
        if(this.fromHospitalStaffManageAppointment){
            this.bookingUrl = "#/onboard-patient/HSRoleManageAppointment/" + redirectPage + "/" + redirectParam + "/" + doctorId;
        }else if(this.fromHospitalAdminManageAppointment){
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
        }else if (this.isPatientDependents) {
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
            console.log("BOOKING_URL1",this.bookingUrl);
        } else {
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            console.log("BOOKING_URL2",this.bookingUrl);
        }
    }
}



