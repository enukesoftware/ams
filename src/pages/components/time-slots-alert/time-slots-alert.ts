import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from "ionic-angular";
import { CommonService } from "../../../providers/common-service/common.service";
import { timeSlotDTO } from "../../../interfaces/user-data-dto";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { Constants } from "../../../providers/appSettings/constant-settings";

@Component({
  selector: 'time-slots-alert',
  templateUrl: 'time-slots-alert.html'
})
export class TimeSlotsAlertComponent {
    hospitalTodayTimeSlot: any = [];
    hospitalAvailableTimeSlot: any;
    timeSlot: timeSlotDTO = {};
    selectedSlotId: any;
    doctorFreeTimeSlots: any[];
    selectedDay: any;
    selectedSlot: any;
    selectedDayId: any;
    doctorProfileId: any;
    doctorAvailableSlots: any;
    hospitalFromToTimeStr: any[];

  constructor(public viewCtrl: ViewController, private constants: Constants, public manageHospitalServiceProvider: ManageHospitalServiceProvider, public navCtrl: NavController, public navParams: NavParams,  private commonService: CommonService) {
      this.doctorFreeTimeSlots = [];
      this.hospitalTodayTimeSlot = [];
      this.hospitalFromToTimeStr = [];
      this.timeSlot.day = this.navParams.get('day');
      this.timeSlot.visitType = this.navParams.get('visitType');
      this.getHospitalInfo();
      this.selectedDayId = this.navParams.get('dayId');
      this.getDoctorFreeSlots();
      this.selectedDay = this.navParams.get('day');
      this.doctorAvailableSlots = this.navParams.get('doctorAvailableSlots');
      console.log("doctorAvailableSlots at 36...", this.doctorAvailableSlots);
      if(this.doctorAvailableSlots != undefined && this.doctorAvailableSlots.day == this.selectedDay){
          this.timeSlot.startTime = this.doctorAvailableSlots.fromTime;
          this.timeSlot.endTime = this.doctorAvailableSlots.toTime;
      } else{
          this.timeSlot.startTime = new Date();
          this.timeSlot.endTime = new Date();
      }
     
      if(this.doctorAvailableSlots != undefined && this.doctorAvailableSlots.visitValue){
         this.timeSlot.visitValue = this.doctorAvailableSlots.visitValue;
      }
      
      
      if(!this.commonService.checkIsWeb()){
          this.timeSlot.startTime = new Date(this.timeSlot.startTime.getTime() - this.timeSlot.startTime.getTimezoneOffset()*60000).toISOString();
          this.timeSlot.endTime = new Date(this.timeSlot.endTime.getTime() - this.timeSlot.endTime.getTimezoneOffset()*60000).toISOString();
      }
     
  }
  
  /**
   * Function to get hospital info
   **/
  getDoctorFreeSlots = ()=>{
      this.commonService.getFromStorage("doctorProfileId").then(value=>{
          if(value){
              this.manageHospitalServiceProvider.getDoctorFreeSlots(value, this.selectedDayId).subscribe(res=>{
                  this.doctorFreeTimeSlots = res.data.doctorsFreeTimeSlots;
                  let timeSlotLen = res.data.doctorsFreeTimeSlots.length;
                  for(let i=0; i<timeSlotLen; i++){
                      let fromUTCTime = {
                          hrs : new Date(res.data.doctorsFreeTimeSlots[i].fromTime).getUTCHours(),
                          mins: new Date(res.data.doctorsFreeTimeSlots[i].fromTime).getUTCMinutes()
                      }
                  
                      let toUTCTime = {
                          hrs: new Date(res.data.doctorsFreeTimeSlots[i].toTime).getUTCHours(),
                          mins: new Date(res.data.doctorsFreeTimeSlots[i].toTime).getUTCMinutes()
                      }
                  
                      let fromTime = this.commonService.convertTo12hrsFormat(fromUTCTime, true);
                      let toTime = this.commonService.convertTo12hrsFormat(toUTCTime, true);
                      this.doctorFreeTimeSlots[i].timeString = fromTime + " to " + toTime;
                  }
              }, error=>{
                  console.log("error.....", error);
              });
          }
      });
      
  }
  
  /**
   * Function to get hospital info
   **/
  getHospitalInfo = () => {
      this.commonService.getFromStorage("hospitalId").then(( value ) => {
          this.manageHospitalServiceProvider.getHospitalDetails(value).subscribe(res=>{
             this.hospitalAvailableTimeSlot = res.data.timeAvailabilityList;
             if(this.hospitalAvailableTimeSlot && this.hospitalAvailableTimeSlot.length > 0 ){
                 let hsTsLen = this.hospitalAvailableTimeSlot.length;
                 for(let i=0; i<hsTsLen; i++){
                     if(this.selectedDayId == this.hospitalAvailableTimeSlot[i].daysId){
                         this.hospitalTodayTimeSlot.push(this.hospitalAvailableTimeSlot[i]);
                     }
                 }
             }
             
             for(let i=0; i<this.hospitalTodayTimeSlot.length; i++){
                 this.hospitalFromToTimeStr[i] = "";
                 let hospitalFromTimeStr = this.commonService.convertTo12hrsFormat(new Date(this.hospitalTodayTimeSlot[i].fromTime), false, true);
                 let hospitalToTimeStr = this.commonService.convertTo12hrsFormat(new Date(this.hospitalTodayTimeSlot[i].toTime), false, true);
                 this.hospitalFromToTimeStr[i] = this.hospitalFromToTimeStr[i] + hospitalFromTimeStr + " to " + hospitalToTimeStr;
             }
                 
          }, error=>{
              let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
              this.commonService.presentToast(errorMsg);
          });
      });
  }
  
  /**
   * Function to handle on change event of time picker 
   **/
  onChangeStartTime( sTime ){
      let windowWidth = this.commonService.getWindowWidth();
      /*if(windowWidth <= 576){
          let sTimeArr = sTime.split(":");
          let hour = sTimeArr[0];
          let minutes = sTimeArr[1];
          sTime = new Date(new Date().setHours(hour, minutes, 0));
          this.timeSlot.startTime = sTime;
      }*/
      if(!this.commonService.checkIsWeb()){
          this.timeSlot.startTime = new Date(sTime);
      }
      this.timeSlot.startTimeMinutes = this.commonService.onChangeStartTime(sTime);
  }
  
  /**
   * Function to handle on change event of time picker 
   **/
  onChangeEndTime( eTime ){
      let windowWidth = this.commonService.getWindowWidth();
      /*if(windowWidth <= 576){
          let eTimeArr = eTime.split(":");
          let hour = eTimeArr[0];
          let minutes = eTimeArr[1];
          eTime = new Date(new Date().setHours(hour, minutes, 0));
          this.timeSlot.endTime = eTime;
      }*/
      if(!this.commonService.checkIsWeb()){ 
          this.timeSlot.endTime = new Date(eTime);
      }
      this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes( eTime );
  }
  
  
  /**
   * Function to add time button click 
   **/
  addTime(){
      this.selectedSlot = this.doctorFreeTimeSlots[this.selectedSlotId];
      this.timeSlot.fromTime = this.timeSlot.startTime;
      this.timeSlot.toTime = this.timeSlot.endTime;
      console.log("this.timeSlot 125...", this.timeSlot);
      if(!this.commonService.checkIsWeb()){
          this.timeSlot.startTime = new Date(new Date(this.timeSlot.startTime).getTime() + (new Date(this.timeSlot.startTime).getTimezoneOffset()*60000));
          this.timeSlot.endTime = new Date(new Date(this.timeSlot.endTime).getTime() + (new Date(this.timeSlot.endTime).getTimezoneOffset()*60000));
          this.timeSlot.fromTime = this.timeSlot.startTime;
          this.timeSlot.toTime = this.timeSlot.endTime;
          this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes(this.timeSlot.startTime);
          this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes(this.timeSlot.endTime);
      }
      
      console.log("timeSlot at 166...", this.timeSlot);
  
      let timeSlot = this.commonService.addTimeSlotClick(this.timeSlot, true, false, this.selectedSlot, this.hospitalTodayTimeSlot);
      
      console.log("timeSlot at 131....", timeSlot);
      
      timeSlot.dayId = this.selectedDayId;
      if(timeSlot.timeValidate){
          this.viewCtrl.dismiss(timeSlot);
      }
  }
  
  /**
   * Function to cancel time 
   **/
  cancelTime(){
      this.viewCtrl.dismiss(false);
  }

}
