import { Component, Input, NgZone, ViewChild, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { Nav, NavController, NavParams, ModalController, Events, MenuController, Content } from 'ionic-angular';

//3rd party npm module
import * as _ from 'lodash';

//Provider Services
import { CommonService } from '../../../providers/common-service/common.service';
import { TimeSlotsAlertComponent } from "../time-slots-alert/time-slots-alert";
import { timeSlotDTO, createContract, HospitalListDTO, DoctorDataDto } from "../../../interfaces/user-data-dto";
import { ManageHospitalServiceProvider } from "../../../providers/manage-hospital-service/manage-hospital-service";
import { Constants } from "../../../providers/appSettings/constant-settings";
import { HospitalAdminAssociatedDoctorListPage } from "../../hospital-admin/hospital-admin-associated-doctor-list/hospital-admin-associated-doctor-list";

@Component({
    selector: 'view-contract-component',
    templateUrl: 'view-contract-component.html',
})
export class ViewContractComponent implements OnChanges {
    @ViewChild(Content) content: Content;

    userData: any;
    timeSlot: any;
    weekdays: Array<{ id: number, name: string }>;
    visitTypes: Array<{ id: number, name: string }>;
    timeSlotArray: any[];
    contractData: any;
    doctorAvailableTimeSlots: timeSlotDTO = {};
    startDate: any;
    endDate: any;
    active: boolean = true;
    selectedStartTime: any;
    contractInfo: createContract = {};
    timeSlotServiceArray: any[];
    timeSlotServicObj: timeSlotDTO = {};
    hospitalAvailableTimeSlot: any;
    hospitalTodayTimeSlot: any;
    selectedEndTime: any;
    startTimeDisabled: boolean = false;
    endTimeDisabled: boolean = false;
    doctorAvailableSlotsArr: any = [];
    hospitalData: HospitalListDTO = {};
    doctorFees: any;
    doctorInfo: DoctorDataDto = {};
    expYearStr: any;
    formSubmitted: boolean = false;
    disableTimeSlot: boolean = true;
    showImg: any;
    selectedFromTime: any;
    selectedToTime: any;
    returnPage: any;
    doctorProfileId: any;
    hospitalId: any;
    oldTimeSlotArray: any[];
    oldTimeSlotServiceArray: any[];

    @Input() editFlag: boolean;
    @Input() createFlag: boolean;
    @Input() viewFlag: boolean;
    @Input() fromPage: any;
    @Input() isCreateContract: boolean;
    @ViewChild(Nav) nav: Nav;
    constructor(private _ngZone: NgZone, private menu: MenuController, public navCtrl: NavController, public evts: Events, private constants: Constants, public navParams: NavParams, public modalCtrl: ModalController, private commonService: CommonService, public manageHospitalServiceProvider: ManageHospitalServiceProvider) {
        this.timeSlotArray = [];
        this.timeSlotServiceArray = [];
        this.doctorAvailableSlotsArr = [];
        this.getDays();
        this.visitTypes = [
            { id: 1, name: 'Appointment' },
            { id: 2, name: 'Token' }
        ]
        this.timeSlot = {
            dayId: '',
            day: '',
            dayObj: '',
            fromTime: '',
            startTimeMinutes: '',
            toTime: '',
            endTimeMinutes: '',
            visitType: '',
            visitValue: '',
            showTimeString: '',
            showVisitTypeString: ''
        }
        setTimeout(() => {
            this.commonService.getFromStorage("toContractPage").then((fromPage) => {
                this.returnPage = fromPage;
                if (fromPage == "hospitalAdmin" || fromPage == "hospitalAdminCreate") {
                    this.commonService.getFromStorage("hospitalId").then((hospitalId) => {
                        setTimeout(() => {
                            this.commonService.getFromStorage("doctorProfileId").then((docProfileId) => {
                                this.doctorProfileId = docProfileId;
                                this.hospitalId = hospitalId;
                                //                                    this.getContractInfo(docProfileId, hospitalId);
                                this.getDoctorInfo(docProfileId);
                            });
                        }, 500);
                    });
                } else if (fromPage == "DoctorAssociatedHospital") {
                    setTimeout(() => {
                        this.commonService.getFromStorage("hospitalId").then((hospitalId) => {
                            this.commonService.getFromStorage("doctorProfileId").then((doctorProfileId) => {
                                this.doctorProfileId = doctorProfileId;
                                this.hospitalId = hospitalId;
                                //                              this.getContractInfo(doctorProfileId, hospitalId);
                                this.getHospitalDetails(hospitalId);
                            });
                        });
                    }, 500);
                }
            });
        }, 800);

        /*this.evts.subscribe('clickCreateContract', () => {
            console.log('event subscribed');
            this.createContract();
        });*/

    }

    ngOnChanges(changes: SimpleChanges) {
        console.log('onchanges called', changes);
        let editFlag: SimpleChange = changes.isCreateContract;
        if (editFlag && editFlag.currentValue == true) {
            console.log('falg is ', editFlag.currentValue);
            this.createContract();
        }
        this.isCreateContract = false;
    }

    /**
     * Function to get day's
     */
    getDays() {
        this.commonService.getDay().subscribe(
            res => {
                if (res.status == "success") {
                    this.weekdays = res.data.list;
                }
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.showAlert("Error", error.message);
            });
    }

    /*
     * function to get doctor info
     * 
     * */

    getDoctorInfo(doctorProfileId) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.manageHospitalServiceProvider.getDoctorDetails("", doctorProfileId).subscribe(res => {
            this.getContractInfo(doctorProfileId, this.hospitalId);
            this.commonService.hideLoading();
            this.doctorInfo = res.data;
            this.getImageBase();
            if (this.doctorInfo.imageBase64) {
                this.showImg = this.doctorInfo.imageBase64;
            } else {
                this.showImg = "assets/imgs/manage_doctors.svg";
            }
            this.expYearStr = this.doctorInfo.yearsOfExp + " Yrs Exp";
        }, error => {
            console.log("error at 92....", error);
        });
    }


    /* function to get images in async */
    public getImageBase = () => {
        //let startIndex = this.pageNo * this.constants.PAGE_SIZE;

        console.log("on get image base");
        let imageData = {
            'userType': "USER_PROFILE_IMAGE",
            'id': this.doctorInfo.doctorId,
            'rank': 1,
        }
        this.commonService.downloadImage(imageData).subscribe(
            res => {
                if (res.status == "success") {
                    console.log("image base 64=======>", res);
                    if (res.data.file) {
                        this.showImg = res.data.file;
                    }
                    else {
                        this.showImg = "assets/imgs/manage_doctors.svg";
                    }

                }
            },
            error => {
                this.commonService.hideLoading();
                /*   let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                   this.commonService.presentToast(errorMsg);*/
            });
    }


    /*
     * function to get profile info
     * */
    getContractInfo(doctorProfileId, hospitalId) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.manageHospitalServiceProvider.getDoctorContractDetails(doctorProfileId, hospitalId).subscribe(res => {
            this.commonService.hideLoading();
            this.doctorFees = res.data.fees;
            this.timeSlot = [];
            this.contractInfo = res.data;
            this.timeSlot = res.data.doctorsBookedSlotsAsPerHospital;

            let timeSlotLength = this.timeSlot.length;
            for (let i = 0; i < timeSlotLength; i++) {
                this.timeSlot[i].dayId = this.timeSlot[i].daysId;
                delete this.timeSlot[i].daysId;

                let fromTime = this.commonService.convertTo12hrsFormat(new Date(this.timeSlot[i].fromTime), false, true);
                let toTime = this.commonService.convertTo12hrsFormat(new Date(this.timeSlot[i].toTime), false, true);
                this.timeSlot[i].startTimeMinutes = this.commonService.getTimeInMinutes(new Date(this.timeSlot[i].fromTime), true);
                this.timeSlot[i].endTimeMinutes = this.commonService.getTimeInMinutes(new Date(this.timeSlot[i].toTime), true);
                this.timeSlot[i].showTimeString = fromTime + " - " + toTime;
                if (this.timeSlot[i].visitType == "APPOINTMENT") {
                    this.timeSlot[i].visitType = "Appointment";
                    this.timeSlot[i].visitValue = this.timeSlot[i].slotDuration;
                } else {
                    this.timeSlot[i].visitType = "Token";
                    this.timeSlot[i].visitValue = this.timeSlot[i].tokens;
                }

                if (this.timeSlot[i].visitType == "Appointment") {
                    this.timeSlot[i].showVisitTypeString = this.timeSlot[i].visitValue + " min - " + this.timeSlot[i].visitType;
                } else {
                    this.timeSlot[i].showVisitTypeString = this.timeSlot[i].visitValue + " -    " + this.timeSlot[i].visitType;
                }

                this.timeSlotServicObj.day = this.timeSlot[i].day;
                this.timeSlotServicObj.daysId = this.timeSlot[i].dayId;
                this.timeSlotServicObj.fromTime = new Date(this.timeSlot[i].fromTime).toISOString();
                this.timeSlotServicObj.toTime = new Date(this.timeSlot[i].toTime).toISOString();
                this.timeSlotServicObj.slot = this.timeSlot[i].slotDuration;
                this.timeSlotServicObj.tokens = this.timeSlot[i].tokens;
                this.timeSlotServicObj.visitType = this.timeSlot[i].visitType.toUpperCase();

                this.timeSlotServiceArray.push(JSON.parse(JSON.stringify(this.timeSlotServicObj)));

                this.timeSlotArray.push(this.timeSlot[i]);
            }


            this.oldTimeSlotServiceArray = _.cloneDeep(this.timeSlotServiceArray);
            this.oldTimeSlotArray = _.cloneDeep(this.timeSlotArray);
        }, error => {
            console.log("error 86...", error);
        });
    }

    /*
     * function to get hospital details
     * */
    getHospitalDetails(hospitalId) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);;
        this.manageHospitalServiceProvider.getHospitalDetails(hospitalId).subscribe(res => {
            this.getContractInfo(this.doctorProfileId, hospitalId);
            this.commonService.hideLoading();
            this.hospitalData = res.data;
            this.getImageBaseForHosp();
            if (this.hospitalData.imageBase64) {
                this.showImg = this.hospitalData.imageBase64;
            } else {
                this.showImg = "assets/imgs/hospital_default_image.png";
            }
        });
    }


    /* function to get images in async */
    public getImageBaseForHosp = () => {
        let imageData1 = {
            'userType': "HOSPITAL_IMAGE",
            'id': this.hospitalData.hospitalId,
            'rank': 1,
            'isDefault': true
        }
        this.commonService.downloadImage(imageData1).subscribe(
            res => {
                console.log("on get image base res")
                if (res.status == "success") {
                    console.log("image base 64=======>", res);
                    this.showImg = res.data.file;
                }
            },
            error => {
                this.commonService.hideLoading();
                /* let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                 this.commonService.presentToast(errorMsg);*/
            });
    }

    /**
     * Function to add time slot to time array
     **/
    addTimeSlotClick = () => {
        try {
            console.log("this.timeSlot.visitValue", this.timeSlot);
            if (this.timeSlot.day != '') {
                if (this.timeSlot.fromTime && this.timeSlot.fromTime != '') {
                    if (this.timeSlot.toTime && this.timeSlot.toTime != '') {
                        /*if(this.commonService.getTimeInMinutes(this.selectedStartTime) > this.commonService.getTimeInMinutes(this.doctorAvailableTimeSlots.fromTime)){*/
                        /*if(this.commonService.getTimeInMinutes(this.selectedEndTime) < this.commonService.getTimeInMinutes(this.doctorAvailableTimeSlots.toTime)){*/
                        for (let i = 0; i < this.weekdays.length; i++) {
                            if (this.timeSlot.day == this.weekdays[i].name) {
                                this.timeSlot.dayId = this.weekdays[i].id;
                            }
                        }

                        this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes(new Date(this.timeSlot.fromTime));
                        this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes(new Date(this.timeSlot.toTime));
                        if (this.timeSlot.visitType != '' && this.timeSlot.visitValue != '') {
                            if (this.isTimeRepeat(this.timeSlot)) {
                                this.commonService.presentToast(this.constants.ERROR_ALREADY_BOOKED_TIME_SLOT);
                            } else {
                                if (this.timeSlot.visitType != "" && this.timeSlot.visitValue != undefined) {
                                    if (this.timeSlot.visitType == "Appointment") {
                                        this.timeSlot.slot = this.timeSlot.visitValue;
                                        this.timeSlot.tokens = 0;
                                    } else {
                                        this.timeSlot.tokens = this.timeSlot.visitValue;
                                        this.timeSlot.slot = 0;
                                        if (this.timeSlot.visitValue < 0) {
                                            this.commonService.presentToast("Please enter valid Token number");
                                            return;
                                        }else if(this.timeSlot.visitValue > 200){
                                            this.commonService.presentToast("Please enter Token number less than 200");
                                            return;
                                        }
                                    }

                                    this.timeSlotServicObj.day = this.timeSlot.day;
                                    this.timeSlotServicObj.daysId = this.timeSlot.dayId;
                                    this.timeSlotServicObj.fromTime = new Date(this.timeSlot.fromTime).toISOString();
                                    this.timeSlotServicObj.toTime = new Date(this.timeSlot.toTime).toISOString();
                                    this.timeSlotServicObj.slot = this.timeSlot.slot;
                                    this.timeSlotServicObj.tokens = this.timeSlot.tokens;
                                    this.timeSlotServicObj.visitType = this.timeSlot.visitType.toUpperCase();
                                    this.timeSlotServiceArray.push(JSON.parse(JSON.stringify(this.timeSlotServicObj)));

                                    let fromTime = this.commonService.convertTo12hrsFormat(new Date(this.timeSlot.fromTime));
                                    let toTime = this.commonService.convertTo12hrsFormat(new Date(this.timeSlot.toTime));

                                    this.timeSlot.showTimeString = fromTime + " - " + toTime;

                                    if (this.timeSlot.visitType == "Appointment") {
                                        this.timeSlot.showVisitTypeString = this.timeSlot.visitValue + " min-" + this.timeSlot.visitType;
                                    } else {
                                        this.timeSlot.showVisitTypeString = this.timeSlot.visitValue + " -    " + this.timeSlot.visitType;
                                    }

                                    this.timeSlotArray.push(this.timeSlot);



                                    this.timeSlot = {
                                        dayId: '',
                                        day: '',
                                        fromTime: '',
                                        startTimeMinutes: '',
                                        toTime: '',
                                        endTimeMinutes: '',
                                        visitType: '',
                                        visitValue: '',
                                        showTimeString: '',
                                        showSelectedFromTime: '',
                                        showSelectedToTime: ''
                                    }

                                    this.disableTimeSlot = true;
                                } else {
                                    console.log("this.timeSlot.visitValue", this.timeSlot.visitValue);
                                    if (this.timeSlot.visitType == "") {
                                        this.commonService.presentToast("Please enter a value for visit type.");
                                    } else if (this.timeSlot.visitValue == undefined || this.timeSlot.visitValue == "") {
                                        if (this.timeSlot.visitType == "Appointment") {
                                            this.commonService.presentToast("Please enter slots for appointment.");
                                        } else if (this.timeSlot.visitType == "Token") {
                                            this.commonService.presentToast("Please enter number of tokens.");
                                        } else {
                                            this.commonService.presentToast("Please select the type of time slot.");
                                        }
                                    }
                                }
                            }
                        } else {
                            if (this.timeSlot.visitType == "") {
                                this.commonService.presentToast("Please enter visit type");
                            } else {
                                if (this.timeSlot.visitType == "Appointment") {
                                    this.commonService.presentToast("Please enter slots for appointment.");
                                } else if (this.timeSlot.visitType == "Token") {
                                    this.commonService.presentToast("Please enter number of tokens.");
                                }
                            }
                        }
                        /*}else{
                            this.commonService.presentToast(this.constants.ERROR_CONTRACT_ENDTIME_LESS_THEN_TIME_SLOT);
                        }*/
                        /*} else {
                            this.commonService.presentToast(this.constants.ERROR_CONTRACT_STARTTIME_GREATER_THEN_TIME_SLOT);
                        }*/
                    } else {
                        this.commonService.presentToast(this.constants.ERROR_CONTRACT_BLANK_END_TIME);
                    }
                } else {
                    this.commonService.presentToast(this.constants.ERROR_CONTRACT_BLANK_START_TIME);
                }
            } else {
                this.commonService.presentToast(this.constants.ERROR_CONTRACT_BLANK_DAY);
            }
        } catch (err) {
            console.log("error in adding time slots....", err);
        }
    }

    /*
     * onDayChange handle event
     * */

    onDayChange(selectedDay) {
        try {

            for (let i = 0; i < this.weekdays.length; i++) {
                if (this.timeSlot.dayId == this.weekdays[i].id) {
                    this.timeSlot.day = this.weekdays[i].name;
                }
            }

            if (this.doctorAvailableSlotsArr.length > 0) {
                for (let i = 0; i < this.doctorAvailableSlotsArr.length; i++) {
                    if (this.doctorAvailableSlotsArr[i].day == this.timeSlot.day) {
                        this.doctorAvailableTimeSlots = this.doctorAvailableSlotsArr[i];
                    }
                }
            }
        } catch (err) {
            console.log("error on day change...", err)
        }

    }
    /**
     * Function to handle on change event of time picker 
     **/
    onChangeStartTime(sTime) {
        if (this.doctorAvailableTimeSlots.day == this.timeSlot.day && this.doctorAvailableTimeSlots.fromTime) {
            this.selectedStartTime = sTime;
            if (this.commonService.getTimeInMinutes(sTime) > this.commonService.getTimeInMinutes(this.doctorAvailableTimeSlots.fromTime)) {
                this.startTimeDisabled = false;
                this.timeSlot.startTimeMinutes = this.commonService.getTimeInMinutes(sTime);
            } else {
                this.startTimeDisabled = true;
                this._ngZone.run(() => {
                    this.timeSlot.fromTime = this.doctorAvailableTimeSlots.fromTime;
                });
                //               this.commonService.presentToast( "Please enter start time greater then selected time slot." );
            }
        } else {
            //           this.commonService.presentToast("Please select doctor available time slot.");
        }
    }

    /**
     * Function to handle on change event of time picker 
     **/
    onChangeEndTime(eTime) {
        if (this.doctorAvailableTimeSlots.fromTime) {
            this.selectedEndTime = eTime;
            if (this.commonService.getTimeInMinutes(eTime) > this.timeSlot.startTimeMinutes) {
                if (this.commonService.getTimeInMinutes(eTime) < this.commonService.getTimeInMinutes(this.doctorAvailableTimeSlots.toTime)) {
                    this.endTimeDisabled = true;
                    this.timeSlot.endTimeMinutes = this.commonService.getTimeInMinutes(eTime);
                } else {
                    this.endTimeDisabled = false;
                    //                   this.commonService.presentToast( "Please enter end time less than selected time slot." );
                }
            } else {
                this.endTimeDisabled = false;
                //               this.commonService.presentToast( "Please select valid end time." );
            }
        } else {
            //           this.commonService.presentToast("Please select doctor available time slot.");
        }
    }

    /**
     * Function to validate time slots 
     **/
    isTimeRepeat = (tempSlot) => {
        if (this.timeSlotArray.length == 0) { return false }
        for (let i = 0; i < this.timeSlotArray.length; i++) {
            if ((tempSlot.dayId == this.timeSlotArray[i].dayId || tempSlot.day == this.timeSlotArray[i].day) && tempSlot.startTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.startTimeMinutes <= this.timeSlotArray[i].endTimeMinutes) {
                return true;
            }
            if ((tempSlot.dayId == this.timeSlotArray[i].dayId || tempSlot.day == this.timeSlotArray[i].day) && tempSlot.endTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes <= this.timeSlotArray[i].endTimeMinutes) {
                return true;
            }
            if ((tempSlot.dayId == this.timeSlotArray[i].dayId || tempSlot.day == this.timeSlotArray[i].day) && tempSlot.startTimeMinutes < this.timeSlotArray[i].startTimeMinutes && tempSlot.endTimeMinutes > this.timeSlotArray[i].endTimeMinutes) {
                return true;
            }
        }
        return false;
    }

    /**
     * Function to remove time slot from array
     * @param index of time slot
     */
    removeTimeSlot = (index, slot) => {
        var alert = this.commonService.confirmAlert("Warning!", "Are you sure you want to delete this time slot?");
        alert.setMode("ios");
        alert.present();
        alert.onDidDismiss(data => {
            if (data) {
                for (let i = 0; i < this.timeSlotServiceArray.length; i++) {
                    if (this.timeSlotArray[i].fromTime == slot.fromTime && this.timeSlotArray[i].day == slot.day) {
                        this.timeSlotServiceArray.splice(i, 1);
                        this.timeSlotArray.splice(i, 1);
                    }
                }
            }

        });
    }

    /**
     * Function to return available time
     **/
    openTimeSlots() {
        if (this.doctorAvailableSlotsArr.length > 0) {
            for (let i = 0; i < this.doctorAvailableSlotsArr.length; i++) {
                if (this.doctorAvailableSlotsArr[i].day == this.timeSlot.day) {
                    this.timeSlot.doctorAvailableSlots = this.doctorAvailableSlotsArr[i];
                }
            }
        }
        if (this.timeSlot.day) {
            let timeSlotModel = this.modalCtrl.create(TimeSlotsAlertComponent, this.timeSlot, { cssClass: 'timeSlotPopover', showBackdrop: true, enableBackdropDismiss: false });
            timeSlotModel.present();
            timeSlotModel.onDidDismiss((data) => {
                if (data) {
                    this.doctorAvailableTimeSlots = data;
                    this.timeSlot = this.doctorAvailableTimeSlots;
                    this.timeSlot.showSelectedFromTime = this.commonService.convertTo12hrsFormat(this.timeSlot.fromTime);
                    this.timeSlot.showSelectedToTime = this.commonService.convertTo12hrsFormat(this.timeSlot.toTime);
                    //                   this.selectedFromTime = 


                    var dayFoundFlag = false;
                    if (this.doctorAvailableSlotsArr) {
                        this.disableTimeSlot = false;
                        if (this.doctorAvailableSlotsArr.length > 0) {
                            for (let i = 0; i < this.doctorAvailableSlotsArr.length; i++) {
                                if (this.doctorAvailableSlotsArr[i].day == data.day) {
                                    dayFoundFlag = true;
                                    this.doctorAvailableSlotsArr[i] = data;
                                }
                            }
                        }
                        if (dayFoundFlag == false) {
                            this.doctorAvailableSlotsArr.push(data);
                        }
                    }
                }
            });
        } else {
            this.commonService.presentToast(this.constants.ERROR_SELECT_DAY);
        }
    }


    /**
     * Function to create and edit contract
     **/
    createContract = () => {
        this.formSubmitted = true;
        if ((this.contractInfo.fees && this.timeSlot.dayId) || (this.timeSlotServiceArray && this.timeSlotServiceArray.length > 0) || this.editFlag) {
            this.commonService.getFromStorage("doctorProfileId").then((doctorId) => {
                this.contractInfo.doctorProfileId = doctorId;
                this.commonService.getFromStorage("hospitalId").then((hospitalId) => {
                    this.contractInfo.hospitalId = hospitalId;
                    /* if(this.contractInfo.startDate){*/
                    if (this.contractInfo.startDate > this.contractInfo.endDate) {
                        this.commonService.presentToast(this.constants.ERROR_CONTRACT_START_LESS_THEN_CONTRACT_END_DATE);
                    } else {
                        if (this.contractInfo.startDate) {
                            this.contractInfo.startDate = new Date(this.contractInfo.startDate).toISOString();
                            if (this.contractInfo.endDate) {
                                this.contractInfo.endDate = new Date(this.contractInfo.endDate).toISOString();
                            }
                        } else {
                            this.contractInfo.startDate = new Date();
                        }
                        if (this.timeSlotServiceArray && this.timeSlotServiceArray.length > 0 || this.editFlag) {
                            this.contractInfo.timeSlots = this.timeSlotServiceArray;
                            if (this.contractInfo.fees) {
                                this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
                                this.manageHospitalServiceProvider.createContract(this.contractInfo).subscribe(res => {
                                    this.commonService.hideLoading();
                                    this.commonService.presentToast(res.message);
                                    if (this.createFlag && this.returnPage != "DoctorAssociatedHospital") {
                                        this.navCtrl.setRoot(HospitalAdminAssociatedDoctorListPage);
                                    } else if (this.editFlag) {
                                        this.timeSlotArray = [];
                                        this.timeSlotServiceArray = [];
                                        this.createFlag = false;
                                        this.viewFlag = true;
                                        this.editFlag = false;
                                        let flags = {
                                            'editFlag': this.editFlag,
                                            'viewFlag': this.viewFlag,
                                            'createFlag': this.createFlag
                                        }
                                        this.evts.publish('contractFlags', flags);
                                        this.getContractInfo(this.doctorProfileId, this.hospitalId);
                                    }
                                    //                                        this.nav.setRoot(HospitalAdminAssociatedDoctorListPage, {}, { animate: false })
                                }, error => {
                                    this.commonService.hideLoading();
                                    this.commonService.presentToast(error.message);
                                    this.timeSlotArray = _.cloneDeep(this.oldTimeSlotArray);



                                    this.timeSlotServiceArray = _.cloneDeep(this.oldTimeSlotServiceArray);
                                    //                                            this.commonService
                                });
                            } else {
                                this.commonService.presentToast("Please enter fees.");
                            }
                        } else {
                            this.commonService.presentToast("Please enter at least one time slot to create contract.");
                        }
                    }
                });
            });
        } else {
            if (this.contractInfo.fees == undefined || this.contractInfo.fees == "") {
                this.commonService.presentToast("Please enter fees.");
            } else {
                this.commonService.presentToast("Please select a day.");
            }
        }

        // this.evts.unsubscribe('clickCreateContract');
    }

    /*
     * On KeyPress restrict for negative number
     *
     * */

    _keyPress(event: any) {
        const pattern = /[0-9]/;
        let inputChar = String.fromCharCode(event.charCode);
        console.log("inputChar 519...", pattern.test(inputChar));
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }

    /**
     * Function to handle cancel btn
     **/
    cancelEdit() {
        this.timeSlotArray = _.cloneDeep(this.oldTimeSlotArray);
        this.timeSlotServiceArray = _.cloneDeep(this.oldTimeSlotServiceArray);

        this.editFlag = false;
        this.viewFlag = true;
        this.createFlag = false;
        let flags = {
            'editFlag': this.editFlag,
            'viewFlag': this.viewFlag,
            'createFlag': this.createFlag
        }
        this.evts.publish('contractFlags', flags);
    }

    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public plotAddressOnMap = (hospital: any) => {
        let currentHospitalAddress = hospital.name + "+" + hospital.address.street + "+" + hospital.address.cityName + "+" +
            hospital.address.state + "+" + hospital.address.zipCode;
        this.commonService.setAddressOnMap(currentHospitalAddress);
    }

    ionViewWillEnter = () => {
    }

    editContract = () => {
        this._ngZone.run(() => {
            this.editFlag = true;
            this.viewFlag = false;
            this.commonService.setPaddingTopScroll(this.content);
        });

    }

}