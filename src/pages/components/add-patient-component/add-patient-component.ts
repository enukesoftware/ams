import { Component, ViewChild, Input, SimpleChanges, SimpleChange } from '@angular/core';
import { IonicPage, Content, NavController, NavParams } from 'ionic-angular';



//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from "../../../providers/appSettings/constant-settings";
import { ManageSysAdminServiceProvider } from "../../../providers/manage-sysadmin-service/manage-sysadmin-service";
import { UserDataDTO, PatientDataDTO, AddressDto } from '../../../interfaces/user-data-dto';
import { LocalStorageService } from '../../../providers/localStorage-service/localStorage.service';
import { CameraService } from '../../../providers/camera-service/camera.service';


import * as moment from 'moment';
import * as _ from 'lodash';


/**
 * Generated class for the AddPatientComponentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
    selector: 'add-patient-component',
    templateUrl: 'add-patient-component.html',
})
export class AddPatientComponent {

    @Input() isHospitalAdmin: boolean;
    @Input() isDoctorAdmin: boolean;
    @Input() isPatientAdmin: boolean;
    @Input() isHospitalStaff: boolean;
    @Input() navParamsComponent: any;
    @Input() editFlag: boolean;
    @Input() createFlag: boolean;
    @Input() viewFlag: boolean;


    @ViewChild(Content) content: Content;
    address: AddressDto = {};
    patientData: any = {};
    formSubmitted = false;
    userData: UserDataDTO = {};
    inputType: any = "text";
    isDefault: boolean;
    imageUpload: any = {};
    rank: number;
    isUpload: boolean = false;


    patientDependents: any = [];
    genderList: any[];
    gender: any;
    relationList: any = [];
    relation: any;
    emergencyRelationList: any[];
    emergencyContactRelation: any;
    maritalStatusList: any = [];
    maritalStatus: any;
    employmentList: any = [];
    employment: any;
    cityList: any = [];
    city: any = {};

    calendarMaxDateForWeb: any;
    calendarMaxDateForMobile: any;
    onCancelUrl: any;
    showOtherCity: boolean = false;
    dobYearRange: any;

    fromPage: any;
    previousFromPage: any;
    redirectPage: any;
    redirectParam: any;
    doctorId: any;
    mode: any;
    backPageUrl: any;
    saveDataRedirectUrl: any;
    saveDataRedirectParams: any;

    dependentStorageKey: any;

    constructor(public navCtrl: NavController, public cameraService: CameraService, public navParams: NavParams, public commonService: CommonService,
        private constants: Constants, private manageSysAdminServiceProvider: ManageSysAdminServiceProvider,
        private locstr: LocalStorageService) {

        this.genderList = this.constants.GENDER_LIST;
        this.relationList = _.cloneDeep(this.constants.RELATION_LIST);
        this.emergencyRelationList = _.cloneDeep(this.constants.RELATION_LIST);
        this.maritalStatusList = this.constants.MARTIAL_STATUS_LIST;
        this.employmentList = this.constants.EMPLOYMENT_LIST;

        this.calendarMaxDateForWeb = new Date();
        this.calendarMaxDateForMobile = moment().format("YYYY-MM-DD");
        this.dobYearRange = "1960" + ":" + moment().format("YYYY");

        console.log("LIST:", this.emergencyContactRelation);
        this.listOfCities();


    }

    getNavigationData() {
        console.log("NAV:", this.navParamsComponent);
        this.fromPage = this.navParamsComponent.get('fromPage');
        this.previousFromPage = this.navParamsComponent.get('previousFromPage');
        this.redirectPage = this.navParamsComponent.get('redirectPage');
        this.redirectParam = this.navParamsComponent.get('redirectParam');
        this.doctorId = this.navParamsComponent.get('doctorId');
        this.mode = this.navParamsComponent.get('mode');
        this.setPageMode();
        this.setPageNavigationData();
        this.getActiveRole();
    }

    setPageMode() {
        if (this.mode == "create") {
            this.createFlag = true;
            this.viewFlag = false;
            this.editFlag = false;
        } else if (this.mode == "view") {
            this.createFlag = false;
            this.viewFlag = true;
            this.editFlag = false;
        } else if (this.mode == "edit") {
            this.createFlag = false;
            this.viewFlag = false;
            this.editFlag = true;
        }
    }

    setPageNavigationData() {
        if (this.fromPage) {
            if (this.fromPage == "OPAddPatient" && this.createFlag) {
                this.backPageUrl = '#/onboard-patient/' + this.previousFromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId;
                // this.commonService.fireSelectedEvent('#/onboard-patient/' + this.previousFromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                if (this.previousFromPage) {
                    // this.saveDataRedirectUrl = "#/booking-step1/" + this.doctorId + "/" + this.previousFromPage + "/" + this.redirectParam;
                    // console.log("SAVE_URL:", this.saveDataRedirectUrl);

                    if(this.previousFromPage == 'HSRoleDashboard'){
                        this.saveDataRedirectUrl = 'HospitalStaffAssociatedDoctorListPage';
                        this.saveDataRedirectParams ={
                            fromPage:'HSRoleOnboardPatient'
                        };
                    }else if(this.previousFromPage == 'HSRoleAssociatedDoctorList' 
                    || this.previousFromPage == 'HSRoleManageAppointment'){
                        this.saveDataRedirectUrl = 'BookingStep1Page';

                        this.saveDataRedirectParams = {
                            doctorId: this.doctorId,
                            from:'HSRoleOnboardPatient',
                            editParam: this.redirectParam
                        };
                    }else if(this.previousFromPage == 'HADashboard'){
                        console.log("PREVIOUS:",this.previousFromPage);
                        this.saveDataRedirectUrl = 'HospitalAdminAssociatedDoctorListPage';

                        this.saveDataRedirectParams = {
                            from: 'HAOnboardPatient',

                        };
                    }else{
                        console.log("CHECK_BOOK");
                        this.saveDataRedirectUrl = 'BookingStep1Page';

                        this.saveDataRedirectParams = {
                            doctorId: this.doctorId,
                            from: this.previousFromPage,
                            editParam: this.redirectParam
                        };
                    }
                   
                }
                console.log("BackPage:", this.backPageUrl);
            } else if (this.fromPage == "OPAddPatient" && this.editFlag) {
                this.backPageUrl = '#/onboard-patient/' + this.previousFromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId;
                // this.commonService.fireSelectedEvent('#/onboard-patient/' + this.previousFromPage + "/" + this.redirectPage + "/" + this.redirectParam + "/" + this.doctorId);
                if (this.previousFromPage) {
                    // this.saveDataRedirectUrl = "#/booking-step1/" + this.doctorId + "/" + this.previousFromPage + "/" + this.redirectParam;
                    // console.log("SAVE_URL:", this.saveDataRedirectUrl);
                    this.saveDataRedirectUrl = 'OnboardPatientPage';

                    this.saveDataRedirectParams = {
                        doctorId: this.doctorId,
                        fromPage: this.previousFromPage,
                        redirectPage: this.redirectPage,
                        redirectParam: this.redirectParam
                    };
                }
                console.log("BackPage:", this.backPageUrl);
            } else if (this.fromPage == "bookStep2") {
                this.backPageUrl = '#/confirm-booking-step2/' + this.previousFromPage + "/" + this.redirectParam;
                // this.saveDataRedirectUrl = '#/confirm-booking-step2/' + this.previousFromPage + "/" + this.redirectParam ;

                console.log("BACK_URL", this.backPageUrl);

                this.saveDataRedirectUrl = 'ConfirmBookingStep2Page';

                this.saveDataRedirectParams = {
                    from: this.previousFromPage,
                    redirectParam: this.redirectParam
                };
            }

        } else {
            this.fromPage = ""
        }


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddPatientComponentPage', this.isPatientAdmin);

    }

    ionViewWillEnter() {
        console.log('ionViewWillEnter AddPatientComponentPage', this.isPatientAdmin);
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log('onchanges called', changes);
        let isPatientAdmin: SimpleChange;
        let isHospitalAdmin: SimpleChange;
        let isDoctorAdmin: SimpleChange;
        let navParams: SimpleChange;

        // if (changes.isPatientAdmin) {
        //     isPatientAdmin = changes.isPatientAdmin;
        // } else if (changes.isHospitalAdmin) {
        //     isHospitalAdmin = changes.isHospitalAdmin;

        // } else if (changes.isDoctorAdmin) {
        //     isDoctorAdmin = changes.isDoctorAdmin;
        // }

        if (changes.navParamsComponent) {
            navParams = changes.navParamsComponent;
        }

        if (navParams && navParams.currentValue) {
            this.navParamsComponent = navParams.currentValue;
            this.getNavigationData();
        }

        // if (((isPatientAdmin && isPatientAdmin.currentValue)
        //     || (isHospitalAdmin && isHospitalAdmin.currentValue)
        //     || (isDoctorAdmin && isDoctorAdmin.currentValue)) && this.patientDependents.length == 0) {


        //     if (isPatientAdmin && isPatientAdmin.currentValue) {
        //         this.dependentStorageKey = "PatientDependents"
        //     } else if (isHospitalAdmin && isHospitalAdmin.currentValue) {
        //         this.dependentStorageKey = "HospitalDependents"
        //     } else if (isDoctorAdmin && isDoctorAdmin.currentValue) {
        //         this.dependentStorageKey = "DoctorDependents"
        //     }
        //     this.getPatientDependents(this.dependentStorageKey);
        // }




        // if (editFlag && editFlag.currentValue == true )
        // {
        //     console.log('falg is ',editFlag.currentValue);
        //     this.createContract();
        // }
        // this.isCreateContract = false;
    }


    getActiveRole = () => {
        this.commonService.getFromStorage('userActiveRole').then((value) => {
            if (value) {
                console.log("ROLE:", value);

                if (value == this.constants.ROLE_SYSTEM_ADMIN) {

                } else if (value == this.constants.ROLE_HOSPITAL_ADMIN) {
                    this.dependentStorageKey = "HospitalDependents";
                    this.getPatientDependents(this.dependentStorageKey);

                } else if (value == this.constants.ROLE_DOCTOR) {
                    this.dependentStorageKey = "DoctorDependents";
                    this.getPatientDependents(this.dependentStorageKey);

                } else if (value == this.constants.ROLE_PATIENT) {
                    this.dependentStorageKey = "PatientDependents";
                    this.getPatientDependents(this.dependentStorageKey);

                }else if (value == this.constants.ROLE_HOSPITAL_STAFF) {
                    this.dependentStorageKey = "HospitalStaffDependents";
                    this.getPatientDependents(this.dependentStorageKey);

                }

            }
        })
        // if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
        //     this.dependentStorageKey = "HospitalDependents";
        //     this.getPatientDependents(this.dependentStorageKey);

        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
        //     this.dependentStorageKey = "DoctorDependents";
        //     this.getPatientDependents(this.dependentStorageKey);

        // } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
        //     this.dependentStorageKey = "PatientDependents";
        //     this.getPatientDependents(this.dependentStorageKey);

        // }

    }

    getPatientDependents(dependent: any) {

        this.commonService.getFromStorage(dependent).then((value) => {
            if (value) {

                this.patientDependents = value;
                this.patientDependents.forEach(element => {
                    if (element.relation == this.constants.RELATION_SELF_VALUE && this.mode == 'create') {
                        this.patientData.userId = element.userId;
                        this.patientData.contactNo = element.contactNo;
                        this.patientData.email = element.email;
                        /**
                       * remove SELF from relation list
                       */
                        this.manageListForOtherRelation();
                    } else if (element.isSelected == true && this.mode == 'edit') {
                        console.log("PAT_DATA:",element);
                        this.patientData = _.cloneDeep(element);
                        // this.imageUpload.file = this.patientData.base64Image;
                        this.address = this.patientData.address;
                        if (this.patientData.gender) {
                            let indexGender = this.genderList.findIndex(gender => gender.value == this.patientData.gender);
                            this.gender = this.genderList[indexGender];
                        }
                        if (this.patientData.relation) {
                            let indexRelation = this.relationList.findIndex(relation => relation.value == this.patientData.relation);
                            this.relation = this.relationList[indexRelation];

                            if (this.relation && this.relation.value == this.constants.RELATION_SELF_VALUE) {
                                this.manageListForSelfRelation();
                            } else {
                                this.manageRelationListForEdit();
                            }
                        }

                        if (this.patientData.maritalStatus) {
                            let indexMaritalStatus = this.maritalStatusList.findIndex(maritalStatus => maritalStatus.value == this.patientData.maritalStatus);
                            this.maritalStatus = this.maritalStatusList[indexMaritalStatus];
                        }

                        if (this.patientData.employment) {
                            let indexEmployment = this.employmentList.findIndex(employment => employment.value == this.patientData.employment);
                            this.employment = this.employmentList[indexEmployment];
                        }

                        if (this.patientData.address) {
                            this.address = this.patientData.address;
                        }

                        if (this.patientData.address && this.patientData.address.cityId) {
                            let indexCity = this.cityList.findIndex(city => city.value == this.patientData.address.cityId);
                            this.city = this.cityList[indexCity];
                        } else if (this.patientData.otherCity) {
                            this.showOtherCity = true;
                        }

                        if (this.patientData.emergencyContactRelation) {
                            let indexEmergencyRelation = this.emergencyRelationList.findIndex(relation => relation.value == this.patientData.emergencyContactRelation);
                            this.emergencyContactRelation = this.emergencyRelationList[indexEmergencyRelation];
                        }

                        if(this.patientData.imageBase64){
                            this.imageUpload.file = this.patientData.imageBase64;                            
                        }
                        this.getImageBase();
                        

                        console.log("PAT:", this.patientData);
                    }
                });






            } else {
                /**
                 * remove other relation from relation list and remove SELF from emergency list
                 */

                this.manageListForSelfRelation();
            }
            // this.patientRoleService.getPatientProfile(patientId).subscribe(res => {
            //     this.patient = res.data;

            //     this.patient.relation = this.constants.RELATION_SELF_VALUE;


            //     this.patientDependents = this.getSampleData();

            //     this.patientDependents.forEach(element => {
            //         if (element.relation == this.constants.RELATION_SELF_VALUE) {
            //             element.isSelected = true;
            //         } else {
            //             element.isSelected = false;
            //         }
            //     });

            //     this.patientDependents.forEach(element => {
            //         if (element.isSelected) {
            //             this.patient = element;
            //         }
            //     });


            //     this.getImageBase();
            //     // this.patientDependents.push(this.patient2);


            //     // this.testList.push({"name":"Tedsst name"});
            //     this.commonService.setInStorage("patientId", res.data.userId);
            //     console.log("PATIENT_DATA", JSON.stringify(this.patientDependents));
            // }, err => {
            //     let errorMsg = err.message ? err.message : this.constants.ERROR_NETWORK_UNAVAILABLE;

            //     // if(!this.isSkipAndExplore){
            //     //     this.commonService.presentToast(errorMsg);
            //     // }
            // });
        });
    }

    /**
     * keep only self in relation list and remove self from emergency list
     */
    manageListForSelfRelation() {
        let relationList = _.cloneDeep(this.constants.RELATION_LIST);
        let indexRelation = relationList.findIndex(relation => relation.value == this.constants.RELATION_SELF_VALUE);
        this.relationList = relationList.splice(indexRelation, 1);

        let emergencyRelationList = _.cloneDeep(this.constants.RELATION_LIST);
        let indexEmergencyRelation = emergencyRelationList.findIndex(relation => relation.value == this.constants.RELATION_SELF_VALUE);
        emergencyRelationList.splice(indexEmergencyRelation, 1);
        this.emergencyRelationList = emergencyRelationList;
    }

    manageListForOtherRelation() {
        let relationList = _.cloneDeep(this.constants.RELATION_LIST);
        let index = relationList.findIndex(relation => relation.value == this.constants.RELATION_SELF_VALUE);
        relationList.splice(index, 1);
        this.relationList = relationList;
    }

    manageRelationListForEdit() {
        let relationList = _.cloneDeep(this.constants.RELATION_LIST);
        let indexRelation = relationList.findIndex(relation => relation.value == this.constants.RELATION_SELF_VALUE);
        relationList.splice(indexRelation, 1);
        this.relationList = relationList;

        // let emergencyRelationList = _.cloneDeep(this.constants.RELATION_LIST);
        // let indexEmergencyRelation = emergencyRelationList.findIndex(relation => relation.value == this.patientData.emergencyContactRelation);
        // emergencyRelationList.splice(indexEmergencyRelation, 1);
        // this.emergencyRelationList = emergencyRelationList;

    }

    /**
    * Function to get list of cities
    */
    listOfCities = () => {
        this.commonService.getListOfCities().subscribe(
            res => {
                if (res.status == "success") {
                    console.log("CITIES:", res);
                    this.cityList = res.data.list;
                    this.address.cityId = "0";
                    this.cityList.push({ "name": "Other", "id": "0" });
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            }
        );
    }

    onClickSavePatient(form: any) {
        this.formSubmitted = true;

        console.log("VALID:", form.valid);
        console.log("FORM", form);

        // if(event){
        // event.href = this.saveDataRedirectUrl
        // event.location.path = this.saveDataRedirectUrl;
        // this.location.path = this.saveDataRedirectUrl;
        // console.log("EVENT:",event.href);
        // window.location = this.saveDataRedirectUrl;
        // }

        if (form.valid && this.patientData.relation) {
            // this.patientData.userId="";
            this.formSubmitted = false;
            this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
            this.patientData.dob = moment(this.patientData.dob).format('YYYY-MM-DD');
            this.patientData.address = this.address;
            //   this.patientData.contactNumber = this.patientData.contactNo;

            if (this.createFlag) {
                this.createPatient();
            } else if (this.editFlag) {
                this.updatePatient();
                // let index = this.patientDependents.findIndex(dependent => dependent.userProfileId == this.patientData.userProfileId);
                // this.patientDependents.push(index,0,this.patientData);


                // // this.patientDependents.push(res.data);
                // console.log("DATA:", this.patientDependents);
                // this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                // this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
            }
        }
        else {
            console.log("invalid form", form);
        }
    }


    public createPatient = () => {

        this.commonService.addPatient(this.patientData).subscribe(
            res => {
                if (res.status == "success") {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                    this.address = this.patientData.address;
                    console.log("RES:", res);

                    if (res.data) {
                        res.data.isSelected = true;
                        this.patientDependents.forEach(element => {
                            element.isSelected = false;
                        });

                        this.patientDependents.push(res.data);
                        console.log("DATA:", this.patientDependents);
                        this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                        this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);

                    }

                    if (this.isUpload && res.data && res.data.userId) {
                        this.uploadImage(res.data.userId);
                    }else{
                        this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                        this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
                    }
                    // this.navCtrl.setRoot( 'SystemAdminManagePatientsPage' );
                }
                else {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);

            }
        );

    }

    public updatePatient = () => {
        this.commonService.updatePatient(this.patientData).subscribe(
            res => {
                if (res.status == "success") {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                    this.address = this.patientData.address;
                    console.log("RES:", res);

                    if (res.data) {
                        // res.data.isSelected = true;
                        // this.patientDependents.forEach(element => {
                        //     element.isSelected = false;
                        // });

                        let index = this.patientDependents.findIndex(dependent => dependent.userProfileId == this.patientData.userProfileId);
                        // this.patientDependents.push(index,0,this.patientData);
                        let list = _.cloneDeep(this.patientDependents);
                        list.splice(index,1);
                        this.patientDependents = list;
                        this.patientDependents.push(this.patientData);


                        // this.patientDependents.push(res.data);
                        console.log("DATA:", this.patientDependents);
                       

                    // }

                    if (this.isUpload && res.data && res.data.userProfileId) {
                        this.uploadImage(res.data.userId,res.data.userProfileId);
                    }else{
                        this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                        this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
                    }
                }
                    // this.navCtrl.setRoot( 'SystemAdminManagePatientsPage' );
                }
                else {
                    this.commonService.hideLoading();
                    this.commonService.presentToast(res.message);
                }
            },
            error => {
                this.commonService.hideLoading();
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);

            }
        );

    }

    /**
     * Function to browse image 
     **/
    fileChangeListener($event, isDefault?: boolean, isUpload?: boolean) {
        this.rank++;
        this.isDefault = isDefault;
        var image: any = new Image();
        var file: File = $event.target.files[0];
        var myReader: FileReader = new FileReader();
        var that = this;
        this.imageUpload = {
            "file": "",
            "fileName": ""
        }
        myReader.onloadend = (loadEvent: any) => {
            image.src = loadEvent.target.result;
            this.commonService.convertImgToBase64(image.src, (callback) => {
                this.imageUpload.file = callback;
                this.imageUpload.fileName = file.name;
                if (callback) {
                    this.isUpload = isUpload;
                }
            });
        };
        myReader.readAsDataURL(file);
    }

    /**
     * Function to upload image 
     **/
    uploadImage = (userId: number,userProfileId?:any) => {
        console.log("USERID:" + userId + " PROFILEID:",userProfileId);
        let imageData = {
            "userType": "USER_PROFILE_IMAGE",
            "mediaType": "IMAGE",
            "id": userId,
            "userProfileId":userProfileId,
            "file": this.imageUpload.file,
            "fileName": this.imageUpload.fileName,
            "isDefault": this.isDefault,
            "rank": 1
        }
        console.log("upload  sys admin imageData++++++++++++++++", imageData);
        this.commonService.uploadImage(imageData).subscribe(
            res => {
                if (res.status == "success") {
                    console.log("upload image sys admin=================", res);
                    this.commonService.setInStorage(this.dependentStorageKey, this.patientDependents);
                    this.navCtrl.push(this.saveDataRedirectUrl, this.saveDataRedirectParams);
                }
            },
            error => {
                this.commonService.hideLoading();
                //this.commonService.showAlert( "Error", error.message );
                let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                this.commonService.presentToast(errorMsg);
            });
    }

    /***
     * Function to open camera/photo gallery
     */
    protected onCamera = () => {
        try {
            // this.commonService.showLoading( this.translationData.pleaseWaitTitle );
            this.cameraService.loadImage(this.successCallback, this.errorCallback);
        } catch (e) {

        }
    }

    /**
      * Function to handle camera success callback
      * @param success
      */
    private successCallback = (base64Image: any) => {
        this.isUpload = true;
        this.rank++;
        this.isDefault = true;
        //this.imageUpload.file = base64Image;
        this.commonService.convertImgToBase64(base64Image, (callback) => {
            this.imageUpload.file = callback;
        })
        console.log('success callback', base64Image);
    }

    /**
      * Function to handle camera error callback
      * @param error
      */
    private errorCallback = (error: any) => {
        console.log('Unable to load profile picture.', error);
    }


    /*
    * Function to handle click event on profile photo
    * */
    public comingsoonPopup = () => {
        this.commonService.presentToast();
    }

    onGenderSelect = (gender: any) => {
        console.log("GENDER:", gender);
        this.patientData.gender = gender.value;
    }

    onRelationSelect = (relation: any) => {
        console.log("RELATION:", relation);
        this.patientData.relation = relation.value;
    }

    onCitySelect = (city: any) => {
        console.log("CITY:", city);

        if (city.name == "Other") {
            this.showOtherCity = true;
            this.city = {};
            this.address.cityId = "";
            this.address.cityName = "";
        } else {
            this.address.cityId = city.id;
            this.address.cityName = city.name;
        }
    }

    onCitySelectMobile(city: any) {
        if (city.value.name == "Other") {
            this.showOtherCity = true;
            this.city = {};
        } else {
            this.address.cityId = city.value.id;
            this.address.cityName = city.name;
        }
    }

    onMaritalStatusSelect = (maritalStatus: any) => {
        console.log("MARTIAL_STATUS:", maritalStatus);
        this.patientData.maritalStatus = maritalStatus.value;
    }

    onEmergencyContactRelationSelect = (emergencyContactRelation: any) => {
        console.log("EMERGENCY_RELATION:", emergencyContactRelation);
        this.patientData.emergencyContactRelation = emergencyContactRelation.value;
    }

    onEmploymentSelect = (employment: any) => {
        console.log("EMPLOYMENT:", employment);
        this.patientData.employment = employment.value;
    }

    onCancelClick = () => {
        // console.log("PAGE:", this.fromPage)
        // if (this.backPageUrl) {
        //     this.onCancelUrl = this.backPageUrl;
        //     console.log("CANCEL:", this.onCancelUrl)
        // }
    }

    onClickCloseOtherCity = () => {
        this.showOtherCity = false;
    }

    /* function to get images in async */
    public getImageBase = () => {
        
        // let startIndex = this.pageNo * this.constants.PAGE_SIZE;
        // for (let i = startIndex; i < this.patientDependents.length; i++) {
            // console.log("USER_ID:",this.patientDependents[i].userId);
            // console.log("USER_PROFILE_ID:",this.patientDependents[i].userProfileId);
            let imageData = {
                'userType': "USER_PROFILE_IMAGE",
                'id': this.patientData.userId,
                'userProfileId': this.patientData.userProfileId,
                'rank': 1,
            }

            if(this.patientData.relation == this.constants.RELATION_SELF_VALUE){
                this.commonService.downloadImage(imageData).subscribe(
                    res => {
                        if (res.status == "success") {
                            console.log("image base 64 PATIENT=======>", res);
                            // for (let j = startIndex; j < this.patientDependents.length; j++) {
                                // if (this.patientDependents[j].userId == res.data.userId) {
                                    // if(this.patientDependents[j].imageBase64){
                                        this.patientData.imageBase64 = res.data.file;
                                        this.imageUpload.file = res.data.file;
                                    // }
                                    
                                // }
                            // }
                            console.log("this.patientDependents=======>", this.patientDependents);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        /*    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                            this.commonService.presentToast(errorMsg);*/
                    });
            }else{
                this.commonService.downloadDependentImage(imageData).subscribe(
                    res => {
                        if (res.status == "success") {
                            console.log("image base 64 DEPENDENT=======>", res);
                            // for (let j = startIndex; j < this.patientDependents.length; j++) {
                                // if (this.patientDependents[j].userProfileId == res.data.userProfileId) {
                                    // if(this.patientDependents[j].imageBase64){
                                        console.log("RELATION: "+this.patientData.relation + " NAME:" +this.patientData.firstName);

                                        this.patientData.imageBase64 = res.data.file;
                                        this.imageUpload.file = res.data.file;
                                        // console.log("IMAGE:",this.patientDependents[j].imageBase64);
                                    // }
                                    
                                // }
                            // }
                            console.log("this.patientDependents=======>", this.patientDependents);
                        }
                    },
                    error => {
                        this.commonService.hideLoading();
                        /*    let errorMsg = error.message ? error.message : this.constants.ERROR_NETWORK_UNAVAILABLE;
                            this.commonService.presentToast(errorMsg);*/
                    });
            }
            

        // }
    }

}
