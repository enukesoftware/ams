import { Component, Input } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

//providers
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { CancelAppointmentConfirmationComponent } from "../cancel-appointment-confirmation/cancel-appointment-confirmation";
@Component({
    selector: 'page-appointment-details',
    templateUrl: 'appointment-details.html',
})
export class AppointmentDetailsComponent {
    @Input() fromAppointmentHistory: any;
    @Input() fromUpcomingAppointment: any;
    @Input() fromHospitalAdminAppointmentVisitDetails: any;
    @Input() appointmentInfo: any;
    @Input() details: any;
    @Input() date: any;
    @Input() noDataFound: boolean;
    @Input() fromHospitalStaffManageAppointment:boolean;
    isPatientDependents:boolean =false;
    bookingUrl:any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private commonService: CommonService, private constants: Constants) {
        console.log('ionViewDidLoad AppointmentDetailsPage', this.appointmentInfo);

        this.getPatientDependent();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AppointmentDetailsPage', this.appointmentInfo);
    }

    /*
      * Function to redirect to native map app on mobile device and on web open google map in new tab
      **/
    public plotAddressOnMap = (hospitalName: any, address: any) => {
        let currentHospitalAddress = hospitalName + "+" + address.street + "+" + address.cityName + "+" +
            address.state + "+" + address.zipCode;
        this.commonService.setAddressOnMap(currentHospitalAddress);
    }

    /*
     * Function to show confirmation popup on cancel appointment
     **/
    openCancelAppointmentConfirmPopup(appointmentId: any) {
        let confirmation = this.modalCtrl.create(CancelAppointmentConfirmationComponent, '', { cssClass: 'cancelAppointmentPopover', showBackdrop: true, enableBackdropDismiss: false });
        confirmation.onDidDismiss((data) => {
            if (data) {
                this.cancelAppointment(appointmentId);
            }
        });
        confirmation.present();
    }

    /*
     * Function to cancel appointment
     **/
    cancelAppointment(appointmentId: any) {
        this.commonService.showLoading(this.constants.PLEASE_WAIT_TEXT);
        this.commonService.cancelAppointment(appointmentId).subscribe(
            res => {
                this.commonService.hideLoading();
                this.commonService.presentToast(res.message);
                setTimeout(() => {
                    // if( this.fromHospitalAdminAppointmentVisitDetails ){
                    this.navCtrl.setRoot("HospitalAdminAppointmentVisitDetailsPage");
                    // }
                    /*else if( this.fromUpcomingAppointment){
                        this.navCtrl.setRoot("DoctorRoleUpcomingAppointmentsPage");
                    }else{
                        this.navCtrl.setRoot("DoctorRoleAppointmentsHistoryPage");
                    }*/
                }, 3000);
            },
            error => {
                this.commonService.hideLoading();
                this.commonService.presentToast(error.message);
                console.log("error....", error);
            });
    }

    comingSoonPopup() {
        this.commonService.presentToast("commingSoon");
    }

    getPatientDependent = () => {
        if (this.commonService.getActiveUserRole() == this.constants.ROLE_SYSTEM_ADMIN) {

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_HOSPITAL_ADMIN) {
            this.commonService.getFromStorage('HospitalDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });
        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_DOCTOR) {
            this.commonService.getFromStorage('DoctorDependents').then((value) => {
                if (value) {
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });

        } else if (this.commonService.getActiveUserRole() == this.constants.ROLE_PATIENT) {
            this.commonService.getFromStorage('PatientDependents').then((value) => {
                if (value) {
                    console.log("DEP:", value);
                    this.isPatientDependents = true;
                } else {
                    this.isPatientDependents = false;;
                }
            });

        }

    }

    onClickBookAppointment(fromPage: any, redirectPage: any, redirectParam: any, doctorId: any) {
        if (this.isPatientDependents) {
            this.bookingUrl = "#/booking-step1/" + doctorId + "/" + fromPage + "/" + redirectParam;
            console.log("BOOKING_URL1", this.bookingUrl);
        } else {
            this.bookingUrl = "#/onboard-patient/" + fromPage + "/" + redirectPage + "/" + redirectParam + "/" + doctorId;
            console.log("BOOKING_URL2", this.bookingUrl);
        }
    }
}