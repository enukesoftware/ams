import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController, NavParams, Events, Platform } from 'ionic-angular';
import {  ViewChild, ElementRef, NgZone } from '@angular/core';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition, /*
    MarkerOptions,*/
    Marker,
    ILatLng
   } from '@ionic-native/google-maps';

//providers services
import { CommonService } from '../../../providers/common-service/common.service';
import { Constants } from '../../../providers/appSettings/constant-settings';
import { ManageHospitalServiceProvider } from '../../../providers/manage-hospital-service/manage-hospital-service';
import { MapServiceProvider } from '../../../providers/map-integration-service/map-integration-service';

/**
 * Generated class for the ViewHolidayItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'view-holiday-item-component',
  templateUrl: 'view-holiday-item-component.html'
})
export class ViewHolidayItemComponent {
  @Input('viewLeave') viewLeave: any;
  @Output('editLeave') editLeave= new EventEmitter(); 

  popover:any;
  showHospital:boolean = false;

  constructor(private commonService:CommonService) {
    console.log('Hello ViewHolidayItemComponent Component');
  }

  ionViewDidLoad(){
    console.log("VIEWLEAVE==>",this.viewLeave);
  }

  editClick() {
    this.editLeave.emit(this.viewLeave);
  }

  itemClick(){
      this.showHospital = !this.showHospital;
  }

}
