import { Component, Input } from '@angular/core';
import { Platform,Events } from 'ionic-angular';

//providers services
import { CommonService } from '../../providers/common-service/common.service';
import { Constants } from '../../providers/appSettings/constant-settings';
import { Location, CommonModule } from '@angular/common';

@Component({
    selector: 'header-for-role-creation-form',
    templateUrl: 'header-for-role-creation-form.html'
})
export class HeaderForRoleCreationPage {
    @Input() firstName: any;
    @Input() lastName: any;
    @Input() pageTitle: any;
    @Input() link: any;
    @Input() linkLabel: any;
    linkLabelText: any;
    @Input() showButton:boolean;
    @Input() showBackButton:boolean;
    @Input() backBtnLink:any;
    @Input() showDrPrefix:boolean;
    @Input() isSamePage:boolean;
    @Input() showHomeIcon:boolean;
    @Input() homeLink:any;
    @Input() showRatings:boolean;
    @Input() showDrImg:boolean;
    @Input() rating:any;
    @Input() isFromHospitalProfile: boolean;
    @Input() drImgPath:any;
    @Input() isBookingConfirmed: boolean;
    
    constructor( public commonService: CommonService, private location: Location, private constants: Constants, public platform: Platform,public evts: Events ){
        
    }
    
    
    comingSoon = () => {
        this.commonService.presentToast();
    }
    
    getBackEvent = () =>{
        this.evts.publish( 'backToPage' );
    }
    
    backFromHospitalDoctorProfile = () =>{
        console.log("backFromHospitalDoctorProfile at 44.....");
        this.location.back();
    }
    
    afterConfirmAppointmentBack(){
        this.commonService.presentToast("Once appointment is confirmed, you cannot hit back button");
        
    }
}
