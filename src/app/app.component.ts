import { Component, ViewChild, HostListener, NgZone } from '@angular/core';
import { Nav, Platform, Events} from 'ionic-angular';
import { Location } from '@angular/common';

// 3rd party npm modules
import { TranslateService } from '@ngx-translate/core';
import { UserIdleService } from '../providers/angular-user-idle';

// cordova native plugin
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Globalization } from '@ionic-native/globalization';

// components that are not lazy loaded

//providers services
import { LocalStorageService } from '../providers/localStorage-service/localStorage.service';
import { CommonService } from '../providers/common-service/common.service';
import { Constants } from '../providers/appSettings/constant-settings';
import { NetworkService } from '../providers/networkService/networkService';

@Component({
    templateUrl: 'app.html'
})
export class DoctorPatientApp {
    menuExtFromPatientRoleSA: any;
    @ViewChild(Nav) nav: Nav;
    //@HostListener('window:resize', ['$event']) windowResize: any;
    
    rootPage: any = "";
    showDesktopViewFooter: boolean = false;
    isWeb: boolean = false;
    browserName: string = "";
    
    menuDashboardLabel: any;
    menuHome: any;
    menuManageHospitals: any;
    menuManageHospital: any;
    menuManageSysAdmins: any;
    menuManageAdmins: any;
    menuManageAppointments: any;
    menuManageDoctors: any;
    menuManageHospitalStaff:any;
    menuMyProfile: any;
    menuAppointmentHist: any;
    menuAssoiatedHosp: any;
    menuUpcomingAppointments: any;
    menuHolidayCalendar:any;
    menuOnboardPatient:any;
    menuFeedbackHistory: any;
    menuManagePatients: any;
    menuManageReports: any;
    menuExitFromHA: any;
    menuLogout: any;
    menuPatientHome: any;
    userId: any;
    menuDoctorDashboard: any;
    menuExtFromDocRoleSA: any;
    menuExtFromDocRoleHA: any;
    isSkipAndExplore:boolean=false;
    menuAssociatedDoctors:any;
    
    // Window width and height variables
    windowWidth: any;
    windowHeight: any;
    halfOfWidthDifference: any; // half of the width difference
    menuWidth: any; // calculated width for menu
    path: any;
    
    pages: Array<{title: string, menuItemClassName: string, componentLink: string, component: any, selected: boolean}>;
    componentList : Array<{component: string, componentLink: string, segment: string}>;
    
    constructor( private _ngZone: NgZone, public commonService: CommonService, public networkService: NetworkService,  private location: Location, private evts: Events, private constant: Constants, public platform: Platform, public statusBar: StatusBar, private localStorageService: LocalStorageService, private globalization: Globalization, private translate: TranslateService, public splashScreen: SplashScreen) {
        this.translate.setDefaultLang('en');
        this.evts.subscribe('fire-after-login-event', () => {
            this._ngZone.run(() => { this.setMenuItems(); });
            this.getDays();
        });
        this.evts.subscribe('set-menu-selected', (path) => {
            this.path = path;
            this.setSelectedMenuFor(path);
        });
        this.evts.subscribe('user:logout', (path) => {
            this.commonService.doLogout( this.nav );
        });
        
        this.translateTheText().then(()=>{
            
            this.commonService.getFromStorage('userActiveRole').then((value)=>{
                if( value ){
                    this.commonService.setActiveUserRole( value );
                }else{
                    this.commonService.setActiveUserRole( this.constant.ROLE_PATIENT );
                    this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_PATIENT );
                }
                this.commonService.getFromStorage('userLoginRole').then((value)=>{
                   if(value){
                       this.commonService.setUserLoginRole(value);
                   }
                   this.initializeApp();
                   this.setMenuItems();
                });
            });
        });
        
        this.componentList = [
                              {componentLink: '#/system-admin-dashboard', component: 'SADashboardPage',segment: 'system-admin-dashboard'},
                              {componentLink: '#/system-admin-manage-hospitals', component: 'SAManageHospitalsPage', segment: 'system-admin-manage-hospitals'},
                              {componentLink: '#/hospital-admin-dashboard', component: 'HADashboardPage', segment: 'hospital-admin-dashboard/0'},
                              {componentLink: '#/hospital-admin-view-hospital', component: 'HospitalAdminViewHospitalPage', segment: 'hospital-admin-view-hospital'},
                              {componentLink: '#/hospital-admin-associated-doctor-list', component: 'HospitalAdminAssociatedDoctorListPage', segment: 'hospital-admin-associated-doctor-list'},
                              {componentLink: '#/hospital-admin-doctor-list',component: 'HospitalAdminDoctorListPage', segment: 'hospital-admin-doctor-list'},
//                              {componentLink: '#/hospital-admin-doctor-list/hospital-admin', component: 'HospitalAdminDoctorListPage', segment: 'hospital-admin-doctor-list'},
                              {componentLink: '#/system-admin-create-hospital',component: 'SACreateHospitalPage', segment: 'system-admin-create-hospital'},
                              {componentLink: '#/hospital-admin-create-contract',component: 'HospitalAdminCreateContractPage', segment: 'hospital-admin-create-contract'},
                              {componentLink: '#/hospital-admin-create-doctor',component: 'HACreateDoctorPage', segment: 'hospital-admin-create-doctor'},
                              {componentLink: '#/hospital-admin-edit-hospital',component: 'HospitalAdminEditHospitalPage', segment: 'hospital-admin-edit-hospital'},
                              {componentLink: '#/system-admin-manage-sysadmins',component: 'SystemAdminManageSysadminsPage', segment: 'system-admin-manage-sysadmins'},
                              {componentLink: '#/doctor-dashboard',component: 'DoctorDashboardPage', segment: 'doctor-dashboard'},
                              {componentLink: '#/docror-role-upcoming-appointments',component: 'DoctorRoleUpcomingAppointmentsPage', segment: 'docror-role-upcoming-appointments'},
                              {componentLink: '#/doctor-role-appointments-history',component: 'DoctorRoleAppointmentsHistoryPage', segment: 'doctor-role-appointments-history'},
                            ]
    }
    
     /*
     * Function to set menu items selected
     * */
    setSelectedMenuFor = (path) =>{
      //  console.log("on setSelectedMenuFor path...", path);
        for(let i = 0; this.pages && i<= this.pages.length; i++){
            if(this.pages && this.pages[i] && this.pages[i].componentLink  == path ){
                //console.log("on 123 path...", this.pages[i].componentLink);
                this.pages[i].selected = true;
            }else{                
                if(this.pages[i]){
                    this.pages[i].selected = false;
                }                
            }
        }
    }
    /*
     * Function to set menu items array based on user active role
     * */
    setMenuItems = () => {
     // used for an example of ngFor and navigation
       // console.log("on setMenuItems activeRole...", this.commonService.getActiveUserRole());
        if( this.commonService.getActiveUserRole() == this.constant.ROLE_SYSTEM_ADMIN ){
            this.pages = [
              /*********************** System Admin Menus START **************************************************/
              { title: this.menuDashboardLabel, menuItemClassName: 'menu-dashboard', componentLink: '#/system-admin-dashboard', component: 'SADashboardPage', selected: true },
              { title: this.menuManageHospitals, menuItemClassName: 'menu-manage-hospitals', componentLink: '#/system-admin-manage-hospitals', component: 'SAManageHospitalsPage', selected: false },
              { title: this.menuManageSysAdmins, menuItemClassName: 'menu-manage-admins', componentLink: '#/system-admin-manage-sysadmins', component: '', selected: false },
              { title: this.menuManageDoctors, menuItemClassName: 'menu-manage-doctors', componentLink: '#/system-admin-doctor-list', component: '', selected: false },
              { title: this.menuManagePatients, menuItemClassName: 'menu-manage-patients', componentLink: '#/system-admin-manage-patients', component: 'SystemAdminManagePatientsPage', selected: false },
              { title: this.menuManageReports, menuItemClassName: 'menu-reports', componentLink: '#/reports/systemAdmin', component: 'reportsPage', selected: false },
              { title: this.menuMyProfile, menuItemClassName: 'menu-my-profile', componentLink: '#/system-admin-create-system-admin/view/0', component: '', selected: false },
              { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }
              /*********************** System Admin Menus END **************************************************/
            ];
        } else if(this.commonService.getActiveUserRole() == this.constant.ROLE_HOSPITAL_ADMIN && this.commonService.getUserLoginRole() == this.constant.ROLE_HOSPITAL_ADMIN){
            this.pages = [
              /*********************** Hospital Admin Only Menus START **************************************************/
              { title: this.menuDashboardLabel, menuItemClassName: 'menu-dashboard', componentLink: "#/hospital-admin-dashboard/0", component: 'HADashboardPage', selected: true },
              { title: this.menuManageHospital, menuItemClassName: 'menu-manage-hospitals', componentLink: '#/hospital-admin-view-hospital', component: 'HospitalAdminViewHospitalPage', selected: false },
              { title: this.menuManageDoctors, menuItemClassName: 'menu-manage-doctors', componentLink: '#/hospital-admin-associated-doctor-list', component: 'HospitalAdminAssociatedDoctorListPage', selected: false },
              { title: this.menuManageAdmins, menuItemClassName: 'menu-manage-admins', componentLink: '#/hospital-admin-manage-hospital-admin', component: '', selected: false },
              { title: this.menuManageHospitalStaff, menuItemClassName: 'menu-manage-hospital-staff', componentLink: '#/hospital-admin-manage-hospital-staff', component: '', selected: false },

             /* { title: this.menuManageAppointments, menuItemClassName: 'menu-manage-appointments', componentLink: 'javascript: void(0)', component: 'HospitalAdminManageAppointmentPage', selected: false },*/
             { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/HADashboard/HAAssociatedDoctorList/0/0', component: 'OnboardPatientPage', selected: false }, 
             { title: this.menuManageAppointments, menuItemClassName: 'menu-manage-appointments', componentLink: '#/hospital-admin-manage-appointment', component: 'HospitalAdminManageAppointmentPage', selected: false },
              { title: this.menuHolidayCalendar, menuItemClassName: 'menu-manage-appointments', componentLink: '#/hospital-admin-doctor-holiday-calendar', component: 'HospitalAdminDoctorHolidayCalendarPage', selected: false },
              { title: this.menuManageReports, menuItemClassName: 'menu-reports', componentLink: '#/reports/hospitalAdmin', component: '', selected: false },
              { title: this.menuMyProfile, menuItemClassName: 'menu-my-profile', componentLink: '#/hospital-admin-create-admin/view/0', component: '', selected: false },
              { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }
              /*********************** Hospital Admin Only Menus END **************************************************/
            ];
        }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_HOSPITAL_ADMIN && this.commonService.getUserLoginRole() == this.constant.ROLE_SYSTEM_ADMIN ){
            this.pages = [
              /*********************** Hospital Admin and SysAdmin Menus START **************************************************/
              { title: this.menuDashboardLabel, menuItemClassName: 'menu-dashboard', componentLink: "#/hospital-admin-dashboard/0", component: 'HADashboardPage', selected: true },
              { title: this.menuManageHospital, menuItemClassName: 'menu-manage-hospitals', componentLink: '#/hospital-admin-view-hospital', component: 'HospitalAdminViewHospitalPage', selected: false },
              { title: this.menuManageDoctors, menuItemClassName: 'menu-manage-doctors', componentLink: '#/hospital-admin-associated-doctor-list', component: 'HospitalAdminAssociatedDoctorListPage', selected: false },
              { title: this.menuManageAdmins, menuItemClassName: 'menu-manage-admins', componentLink: '#/hospital-admin-manage-hospital-admin', component: '', selected: false },
              { title: this.menuManageHospitalStaff, menuItemClassName: 'menu-manage-hospital-staff', componentLink: '#/hospital-admin-manage-hospital-staff', component: '', selected: false },

              /*{ title: this.menuManageAppointments, menuItemClassName: 'menu-manage-appointments', componentLink: 'javascript: void(0)', component: 'HospitalAdminManageAppointmentPage', selected: false },*/
              { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/HADashboard/HAAssociatedDoctorList/0/0', component: 'OnboardPatientPage', selected: false }, 
              { title: this.menuManageAppointments, menuItemClassName: 'menu-manage-appointments', componentLink: '#/hospital-admin-manage-appointment', component: 'HospitalAdminManageAppointmentPage', selected: false },
              { title: this.menuHolidayCalendar, menuItemClassName: 'menu-manage-appointments', componentLink: '#/hospital-admin-doctor-holiday-calendar', component: 'HospitalAdminDoctorHolidayCalendarPage', selected: false },
              { title: this.menuManageReports, menuItemClassName: 'menu-reports', componentLink: '#/reports/sysAdmHospitalAdmin', component: '', selected: false },
              { title: this.menuExitFromHA, menuItemClassName: 'menu-exit-from-role', componentLink: '#/system-admin-dashboard', component: '', selected: false },
              { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }
              /*********************** Hospital Admin and SysAdmin Menus END **************************************************/
            ];
        }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_DOCTOR && this.commonService.getUserLoginRole() == this.constant.ROLE_SYSTEM_ADMIN ){
            /*this.commonService.getFromStorage("userPrevRole").then(val=>{
               
            });*/
            this.pages = [
                          /*********************** Doctor Menus from system admin START **************************************************/
                          { title: this.menuDashboardLabel, menuItemClassName: 'menu-home', componentLink: '#/doctor-dashboard/0', component: 'DoctorDashboardPage', selected: true },
                         /* { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-doctor-upcoming-appointments', componentLink: 'javascript: void(0)', component: 'DoctorRoleUpcomingAppointmentsPage', selected: false },
                          { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: 'javascript: void(0)', component: 'DoctorRoleAppointmentsHistoryPage', selected: false },
                          { title: this.menuAssoiatedHosp, menuItemClassName: 'menu-manage-hospitals', componentLink: 'javascript: void(0)', component: 'DoctorAssociateHospitalsPage', selected: false },*/
                          { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-doctor-upcoming-appointments', componentLink: '#/doctor-role-upcoming-appointments', component: 'DoctorRoleUpcomingAppointmentsPage', selected: false },
                          { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: '#/doctor-role-appointments-history', component: 'DoctorRoleAppointmentsHistoryPage', selected: false },
                          { title: this.menuAssoiatedHosp, menuItemClassName: 'menu-manage-hospitals', componentLink: '#/doctor-associate-hospitals', component: 'DoctorAssociateHospitalsPage', selected: false },
                          { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/doctorDashboard/doctorRoleAppointmentHistory/0/0', component: 'OnboardPatientPage', selected: false }, 
                          { title: this.menuHolidayCalendar, menuItemClassName: 'menu-manage-appointments', componentLink: '#/doctor-role-holiday-calendar/create/0/DRHolidayCalendar', component: 'DoctorRoleHolidayCalendarPage', selected: false },
                          { title: this.menuManageReports, menuItemClassName: 'menu-reports', componentLink: '#/reports/sysAdmDoctor', component: '', selected: false },
                          { title: this.menuExtFromDocRoleSA, menuItemClassName: 'menu-exit-from-role', componentLink: '#/system-admin-dashboard', component: '', selected: false },
                          { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }
                          /*********************** Doctor Menus END **************************************************/
                        ];
        }else if(this.commonService.getActiveUserRole() == this.constant.ROLE_DOCTOR && this.commonService.getUserLoginRole() == this.constant.ROLE_HOSPITAL_ADMIN){
            this.pages = [
                  /*********************** Doctor Menus from hospital admin START **************************************************/
                  { title: this.menuDashboardLabel, menuItemClassName: 'menu-home', componentLink: '#/doctor-dashboard/0', component: 'DoctorDashboardPage', selected: true },
                 /* { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-doctor-upcoming-appointments', componentLink: 'javascript: void(0)', component: 'DoctorRoleUpcomingAppointmentsPage', selected: false },
                  { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: 'javascript: void(0)', component: 'DoctorRoleAppointmentsHistoryPage', selected: false },*/
                  { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-doctor-upcoming-appointments', componentLink: '#/doctor-role-upcoming-appointments', component: 'DoctorRoleUpcomingAppointmentsPage', selected: false },
                  { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: '#/doctor-role-appointments-history', component: 'DoctorRoleAppointmentsHistoryPage', selected: false },
                  { title: this.menuAssoiatedHosp, menuItemClassName: 'menu-manage-hospitals', componentLink: '#/doctor-associate-hospitals', component: 'DoctorAssociateHospitalsPage', selected: false },
                  { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/doctorDashboard/doctorRoleAppointmentHistory/0/0', component: 'OnboardPatientPage', selected: false }, 
                  { title: this.menuHolidayCalendar, menuItemClassName: 'menu-manage-appointments', componentLink: '#/doctor-role-holiday-calendar/create/0/DRHolidayCalendar', component: 'DoctorRoleHolidayCalendarPage', selected: false },
                  { title: this.menuManageReports, menuItemClassName: 'menu-reports', componentLink: 'javascript: void(0)', component: '', selected: false },
                  { title: this.menuExtFromDocRoleHA, menuItemClassName: 'menu-exit-from-role', componentLink: '#/hospital-admin-dashboard/0', component: '', selected: false },
                  { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }
                  /*********************** Doctor Menus END **************************************************/
                ];
        }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_DOCTOR ){
            this.pages = [
              /*********************** Doctor Menus START **************************************************/
              { title: this.menuDashboardLabel, menuItemClassName: 'menu-home', componentLink: '#/doctor-dashboard/0', component: 'DoctorDashboardPage', selected: true },
             /* { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-doctor-upcoming-appointments', componentLink: 'javascript: void(0)', component: 'DoctorRoleUpcomingAppointmentsPage', selected: false },
              { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: 'javascript: void(0)', component: 'DoctorRoleAppointmentsHistoryPage', selected: false },*/
              { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-doctor-upcoming-appointments', componentLink: '#/doctor-role-upcoming-appointments', component: 'DoctorRoleUpcomingAppointmentsPage', selected: false },
              { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: '#/doctor-role-appointments-history', component: 'DoctorRoleAppointmentsHistoryPage', selected: false },
              { title: this.menuAssoiatedHosp, menuItemClassName: 'menu-manage-hospitals', componentLink: '#/doctor-associate-hospitals', component: 'DoctorAssociateHospitalsPage', selected: false },
              { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/doctorDashboard/doctorRoleAppointmentHistory/0/0', component: 'OnboardPatientPage', selected: false }, 
              { title: this.menuHolidayCalendar, menuItemClassName: 'menu-manage-appointments', componentLink: '#/doctor-role-holiday-calendar/create/0/DRHolidayCalendar', component: 'DoctorRoleHolidayCalendarPage', selected: false },
              { title: this.menuManageReports, menuItemClassName: 'menu-reports', componentLink: '#/reports/doctor', component: '', selected: false },
              { title: this.menuMyProfile, menuItemClassName: 'menu-my-profile', componentLink: '#/doctor-role-profile', component: 'DoctorRoleProfilePage', selected: false },
              { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: 'javascript: void(0)', component: '', selected: false }
              /*********************** Doctor Menus END **************************************************/
            ];
        }else if(this.commonService.getActiveUserRole() == this.constant.ROLE_PATIENT && this.commonService.getUserLoginRole() == this.constant.ROLE_SYSTEM_ADMIN){
            this.pages = [
                  /*********************** Patient Menus START **************************************************/
                  { title: this.menuHome, menuItemClassName: 'menu-home', componentLink: '#/patient-role-home/0', component: 'PatientRoleHomePage', selected: true },
                 /* { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-upcoming-appointments', componentLink: 'javascript: void(0)', component: 'AppointmentList', selected: false },
                  { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: 'javascript: void(0)', component: 'AppointmentList', selected: false },
                  { title: this.menuFeedbackHistory, menuItemClassName: 'menu-feedback-history', componentLink: 'javascript: void(0)', component: 'PatientRoleFeedbackHistoryPage', selected: false },*/
                  { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-upcoming-appointments', componentLink: '#/appointment-list/patientUpcoming', component: 'AppointmentList', selected: false },
                  { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: '#/appointment-list/patientHistory', component: 'AppointmentList', selected: false },
                  { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/patientHome/patientHome/0/0', component: 'OnboardPatientPage', selected: false }, 
                  { title: this.menuFeedbackHistory, menuItemClassName: 'menu-feedback-history', componentLink: '#/patient-role-feedback-history', component: 'PatientRoleFeedbackHistoryPage', selected: false },
                  { title: this.menuExtFromPatientRoleSA, menuItemClassName: 'menu-exit-from-role', componentLink: '#/system-admin-dashboard', component: '', selected: false },
                  { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }
                  /*********************** Patient Menus END **************************************************/
                ];
        }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_PATIENT ){
            this.pages = [
              /*********************** Patient Menus START **************************************************/
              { title: this.menuHome, menuItemClassName: 'menu-home', componentLink: '#/patient-role-home/0', component: 'PatientRoleHomePage', selected: true },
             /* { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-upcoming-appointments', componentLink: 'javascript: void(0)', component: 'AppointmentList', selected: false },
              { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: 'javascript: void(0)', component: 'AppointmentList', selected: false },
              { title: this.menuFeedbackHistory, menuItemClassName: 'menu-feedback-history', componentLink: 'javascript: void(0)', component: 'PatientRoleFeedbackHistoryPage', selected: false },*/
              { title: this.menuUpcomingAppointments, menuItemClassName: 'menu-upcoming-appointments', componentLink: '#/appointment-list/patientUpcoming', component: 'AppointmentList', selected: false },
              { title: this.menuAppointmentHist, menuItemClassName: 'menu-appointment-history', componentLink: '#/appointment-list/patientHistory', component: 'AppointmentList', selected: false },
              { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/patientHome/patientHome/0/0', component: 'OnboardPatientPage', selected: false }, 
              { title: this.menuFeedbackHistory, menuItemClassName: 'menu-feedback-history', componentLink: '#/patient-role-feedback-history', component: 'PatientRoleFeedbackHistoryPage', selected: false },
               { title: this.menuMyProfile, menuItemClassName: 'menu-my-profile', componentLink: '#/patient-role-profile/view/0', component: 'PatientRoleProfilePage', selected: false },
              { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }
              /*********************** Patient Menus END **************************************************/
            ];
        }else if(this.commonService.getActiveUserRole() == this.constant.ROLE_HOSPITAL_STAFF && this.commonService.getUserLoginRole() == this.constant.ROLE_HOSPITAL_STAFF){
            this.pages = [
              /*********************** Staff Role Only Menus START **************************************************/
              { title: this.menuDashboardLabel, menuItemClassName: 'menu-home', componentLink: "#/staff-role-dashboard/0", component: 'SRDashboardPage', selected: true },
              { title: this.menuOnboardPatient, menuItemClassName: 'menu-onboard-patient', componentLink: '#/onboard-patient/HSRoleDashboard/HSRoleAssociatedDoctorList/0/0', component: 'OnboardPatientPage', selected: false }, 
              { title: this.menuManageAppointments, menuItemClassName: 'menu-manage-appointments', componentLink: '#/hospital-admin-manage-appointment', component: 'HospitalAdminManageAppointmentPage', selected: false },
              { title: this.menuHolidayCalendar, menuItemClassName: 'menu-holiday-calendar', componentLink: '#/hospital-admin-holiday-calendar/create/0/HSRoleHolidayCalendar', component: 'HospitalAdminHolidayCalendarPage', selected: false },
              { title: this.menuAssociatedDoctors, menuItemClassName: 'menu-associated-doctors', componentLink: '#/hospital-staff-associated-doctor-list/HSRoleDashboard', component: 'HospitalStaffAssociatedDoctorListPage', selected: false },

              { title: this.menuLogout, menuItemClassName: 'menu-logout', componentLink: '#/login', component: '', selected: false }

              /*********************** Staff Role Only Menus END **************************************************/
            ];
        }
        this.setSelectedMenuFor(this.path);
    }
    
    /*
     * Function to show/hide desktop footer based on platform width
     * max 768 is width. Also set the event when resizing the window in web view.
     * */
    manageDFooterLogic = () => {
        if( this.platform.width() >= 768 ){
            this.commonService.setBottomScroll();
            this.showDesktopViewFooter =  true;
        }else{
            this.commonService.actionAfterHidingDesktopFooter();
            this.showDesktopViewFooter =  false;
        }
        
        /*this.windowResize = (event) => {
            if( this.platform.width() >= 768 ){
                this.commonService.setBottomScroll();
                this.showDesktopViewFooter =  true;
            }else{
                this.commonService.actionAfterHidingDesktopFooter();
                this.showDesktopViewFooter =  false;
            }
        }*/
    }
    
    /*
     * Function to get language set on device using globalization plugin and set
     * it for the multi-language npm module
     * */
    setDeviceLanguage = () => {
        this.globalization.getPreferredLanguage().then( (res)=>{
            let langArr = res.value.split( "-" );
           // console.log("globalization.getPreferredLanguage langArr ========================>",langArr);
            let langStr = langArr[0];
            this.translate.use( langStr );
        }).catch( (e)=>{
            this.translate.use('en');
           // console.log("globalization.getPreferredLanguage catch ======================>",e);
        });
    }
    
    /*
     * Function to check if userData is present in ionic storage if yes then redirect to Home
     * or else redirect to login page
     * */
    checkIfLoggedIn = () => {
        this.commonService.isLoggedIn((result) => {
            if( !result ){
                //this.showDesktopViewFooter = false;
                this.rootPage = "LoginPage";
                // this.rootPage = "LandingCopyPage"
            }else{
                // this.rootPage = "LoginPage";
                console.log("currentpath : ",this.location.path(true));
                this.commonService.checkIfUserIsIdle(this.commonService.getActiveUserRole());
                if( this.isWeb ){
                    
                    //this.manageDFooterLogic();
                }
                let currentPath = this.location.path(true);
                if( currentPath == '/system-admin-dashboard' ){
                    this.commonService.setUserData( result );
                    this.rootPage = "SADashboardPage";
                }else if( currentPath != '' ){
                    this.location.go(currentPath);
                    /*let component = this.getComponentOf('#'+currentPath);
                    this.rootPage = component;*/
                }else{
                    if( this.commonService.getActiveUserRole() == this.constant.ROLE_SYSTEM_ADMIN ){
                        this.rootPage = "SADashboardPage";
                    }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_HOSPITAL_ADMIN ){
                        this.rootPage = "HADashboardPage";                          
                    }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_DOCTOR ){
                        this.rootPage = "DoctorDashboardPage";                          
                    }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_PATIENT ){
                        this.rootPage = "PatientRoleHomePage";
                    }else if( this.commonService.getActiveUserRole() == this.constant.ROLE_HOSPITAL_STAFF ){
                        this.rootPage = "SRDashboardPage";
                    }
                }
            }
        });
    }
     /*
     * Function to to get associated component of url
     * */
    getComponentOf = (path) => {
        
        for(let i = 0; this.componentList && i<= this.componentList.length; i++){
            if(this.componentList && this.componentList[i] && this.componentList[i].componentLink  == path ){
                return this.componentList[i].component;
            }
        }
    }
    /*
     * Function to translate the text used through components
     * */
    translateTheText = () => {
       return new Promise((resolve, reject) => {
            let promisesArr = [];
            
            //Set menu labels from translated files
            promisesArr.push(this.commonService.translateText("DASHBOARD_TITLE").toPromise().then((translatedText)=>{
                this.menuDashboardLabel = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("HOME_TITLE").toPromise().then((translatedText)=>{
                this.menuHome = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_HOSPITALS_TITLE").toPromise().then((translatedText)=>{
                this.menuManageHospitals = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_HOSPITAL_TITLE").toPromise().then((translatedText)=>{
                this.menuManageHospital = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_SYS_ADMINS_TITLE").toPromise().then((translatedText)=>{
                this.menuManageSysAdmins = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_ADMINS_TITLE").toPromise().then((translatedText)=>{
                this.menuManageAdmins = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_APPOINTMENTS_TITLE").toPromise().then((translatedText)=>{
                this.menuManageAppointments = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_DOCTORS_TITLE").toPromise().then((translatedText)=>{
                this.menuManageDoctors = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MY_PROFILE_TITLE").toPromise().then((translatedText)=>{
                this.menuMyProfile = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("APPOINTMENT_HISTORY_TITLE").toPromise().then((translatedText)=>{
                this.menuAppointmentHist = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("ASSOCIATED_HOSPITAL_TITLE").toPromise().then((translatedText)=>{
                this.menuAssoiatedHosp = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("UPCOMING_APPOINTMENT_TITLE").toPromise().then((translatedText)=>{
                this.menuUpcomingAppointments = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("FEEDBACK_HISTORY_TITLE").toPromise().then((translatedText)=>{
                this.menuFeedbackHistory = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_PATIENTS_TITLE").toPromise().then((translatedText)=>{
                this.menuManagePatients = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("REPORTS_TITLE").toPromise().then((translatedText)=>{
                this.menuManageReports = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("EXIT_FROM_HA_TITLE").toPromise().then((translatedText)=>{
                this.menuExitFromHA = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("LOGOUT_TITLE").toPromise().then((translatedText)=>{
                this.menuLogout = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("PATIENT_HOME_PAGE").toPromise().then((translatedText)=>{
                this.menuPatientHome = translatedText;
            }));
             promisesArr.push(this.commonService.translateText("DOCTOR_DASHBOARD").toPromise().then((translatedText)=>{
                this.menuDoctorDashboard = translatedText;
            }));
             promisesArr.push(this.commonService.translateText("EXIT_FROM_DOC_TITLE").toPromise().then((translatedText)=>{
                 this.menuExtFromDocRoleSA = translatedText;
                 this.menuExtFromDocRoleHA = translatedText;
             }));
             promisesArr.push(this.commonService.translateText("EXIT_FROM_PATIENT_ROLE_TITLE").toPromise().then((translatedText)=>{
                 this.menuExtFromPatientRoleSA = translatedText;
             }));
             promisesArr.push(this.commonService.translateText("HOLIDAY_CALENDAR_TITLE").toPromise().then((translatedText)=>{
                this.menuHolidayCalendar = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("ONBOARD_PATIENT_TITLE").toPromise().then((translatedText)=>{
                this.menuOnboardPatient = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("MANAGE_HOSPITAL_STAFF_TITLE").toPromise().then((translatedText)=>{
                this.menuManageHospitalStaff = translatedText;
            }));
            promisesArr.push(this.commonService.translateText("ASSOCIATED_DOCTORS_LABEL").toPromise().then((translatedText)=>{
                this.menuAssociatedDoctors = translatedText;
            }));
            Promise.all(promisesArr).then( () => {
                resolve();
            });
        });
    }
    
    /*
     * Function to get the window width & height and save it to ionic storage
     * */
    getWindowWidthNHeight = () => {
        this.windowWidth = this.platform.width();
        this.windowHeight = this.platform.height();
        
        this.commonService.setInStorage( "windowWidth",this.windowWidth );
        this.commonService.setInStorage( "windowHeight",this.windowHeight );
        
        this.makeContentCenterLogic();
    }
    
    /*
     * Function to make content center if window width goes beyond 1200px so that content will
     * strict to CONTENT_MAX_WIDTH = 1200px
     * */
    makeContentCenterLogic = () => {
        let tempHalfOfWidthDifference = (this.windowWidth - this.constant.CONTENT_MAX_WIDTH) / 2;
        if( tempHalfOfWidthDifference > 0 ){
            this.halfOfWidthDifference = tempHalfOfWidthDifference;
            this.menuWidth = this.constant.MIN_MENU_WIDTH + this.halfOfWidthDifference;
        }
    }
    
    initializeApp = () => {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            
            this.networkService.monitorNetworkStatus();
            
            //this.nav.setRoot('HomePage');
            this.isWeb = this.commonService.checkIsWeb();
            this.browserName = this.commonService.getBrowserName();
            this.getWindowWidthNHeight();
            
            this.setDeviceLanguage();
            this.checkIfLoggedIn();
            
        });
    }
    
    /*
     * Function to get days
     * */
    
    getDays(){
        this.commonService.getDay().subscribe(
                res => {
                    if ( res.status == "success" ) {
                        console.log("weekdays on launch...", res);
                        this.commonService.setInStorage("weekdays", res.data.list);
                    }
                },
                error => {
                    this.commonService.hideLoading();
                    this.commonService.showAlert( "Error", error.message );
            });
    }
    
    /*
     * Function for menu item click and redirect to that specific screen
     * */
    openPage = ( page: any ) => {
        this.commonService.getFromStorage('isSkipAndExplore').then((value)=>{
                this.isSkipAndExplore =value;
                console.log("value of isSkipAndExplore",value);
                console.log("page",page);
                // Reset the content nav to have just this page
                // we wouldn't want the back button to show in this scenario
                for( let i=0;i < this.pages.length;i++ ){
                    this.pages[i].selected = false;
                }
                page.selected = true;
               // console.log("openPage ====================>",this.pages);
                
                if( page.title == this.menuLogout ){
                    let logoutPopUpMsg = this.commonService.getTranslate('LOGOUTCONFIRMATIONMSG',{});
                    let warningTitle =  this.commonService.getTranslate('WARNING',{});
                    let alert = this.commonService.confirmAlert(warningTitle, logoutPopUpMsg);
                    alert.setMode("ios");
                    alert.present();
                    alert.onDidDismiss((data) => {
                        if(data == true){
                            this.commonService.doLogout( this.nav );
                          //  navCtrl.setRoot( 'LoginPage' );
                   }  else{}
                   });
                }else if( page.title == this.menuExitFromHA){
                    this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_SYSTEM_ADMIN );
                    this.commonService.setActiveUserRole( this.constant.ROLE_SYSTEM_ADMIN );
                    this.commonService.removeFromStorage("HospitalDependents")
                    this.setMenuItems();
                }else if(page.title == this.menuExtFromDocRoleSA){
                    this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_SYSTEM_ADMIN );
                    this.commonService.setActiveUserRole( this.constant.ROLE_SYSTEM_ADMIN );
                    this.commonService.removeFromStorage("DoctorDependents")
                    this.setMenuItems();
                }else if(page.title == this.menuExtFromPatientRoleSA){
                    this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_SYSTEM_ADMIN );
                    this.commonService.setActiveUserRole( this.constant.ROLE_SYSTEM_ADMIN );
                    this.commonService.removeFromStorage("PatientDependents")
                    this.setMenuItems();
                }
                else if(page.title == this.menuUpcomingAppointments && this.isSkipAndExplore){
                    console.log("in patient upcoming role");
                   this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_PATIENT );
                   this.commonService.setActiveUserRole( this.constant.ROLE_PATIENT );
                   this.setMenuItems();
                    this.commonService.showGoTOLoginPopup(this.nav);
                }
                else if(page.title == this.menuAppointmentHist && this.isSkipAndExplore){
                    console.log("in patient history");
                    this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_PATIENT );
                   this.commonService.setActiveUserRole( this.constant.ROLE_PATIENT );
                   this.setMenuItems();
                    this.commonService.showGoTOLoginPopup(this.nav);
                }
                else if(page.title == this.menuFeedbackHistory && this.isSkipAndExplore){
                    console.log("in patient history");
                    this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_PATIENT );
                    this.commonService.setActiveUserRole( this.constant.ROLE_PATIENT );
                    this.setMenuItems();
                    this.commonService.showGoTOLoginPopup(this.nav);
                }
                else if(page.title == this.menuMyProfile && this.isSkipAndExplore){
                    console.log("in patient history");
                    this.commonService.setInStorage( 'userActiveRole',this.constant.ROLE_PATIENT );
                    this.commonService.setActiveUserRole( this.constant.ROLE_PATIENT );
                    this.setMenuItems();
                    this.commonService.showGoTOLoginPopup(this.nav);
                }
                else if( page.componentLink == "javascript: void(0)" ){
                    this.commonService.presentToast();
                }
                else{
                    //this.nav.setRoot(page.component);
                }
        });
    }
}