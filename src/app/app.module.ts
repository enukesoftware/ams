import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import { OneSignal } from '@ionic-native/onesignal';

// 3rd party npm modules
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from 'primeng/calendar';
import { ChartModule } from "primeng/chart";
import { UserIdleModule } from '../providers/angular-user-idle';

import { DoctorPatientApp } from './app.component';
//import { FooterPageModule } from '../pages/footer/footer.module';
import { SharedModule } from '../pages/components/shared.module';
import { TimeSlotsAlertComponent } from "../pages/components/time-slots-alert/time-slots-alert";
import { SearchFilterComponent } from "../pages/components/search-filter/search-filter";
import { CancelAppointmentConfirmationComponent } from "../pages/components/cancel-appointment-confirmation/cancel-appointment-confirmation";

// providers services
import { LocalStorageService } from '../providers/localStorage-service/localStorage.service';
import { CommonService } from '../providers/common-service/common.service';
import { AuthService } from '../providers/authService/authService';
import { ManageHospitalServiceProvider } from '../providers/manage-hospital-service/manage-hospital-service';
import { ManageSysAdminServiceProvider } from '../providers/manage-sysadmin-service/manage-sysadmin-service';
import { RestService } from '../providers/restService/restService';
import { NetworkService } from '../providers/networkService/networkService';
import { DeviceInformationProvider } from '../providers/device-information/device-information';
import { Constants } from '../providers/appSettings/constant-settings';
import { PatientRoleServiceProvider } from '../providers/patient-role-service/patient-role-service';
import { MapServiceProvider } from '../providers/map-integration-service/map-integration-service';
import { DoctorRoleServiceProvider } from '../providers/doctor-role-service/doctor-role-service';
import { CameraService } from '../providers/camera-service/camera.service';
import { SearchServiceProvider } from '../providers/search-service/search-service';
import { OneSignalService } from '../providers/one-signal-service/one-signal.service';

// cordova native plugin

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Globalization } from '@ionic-native/globalization';
import { Network } from '@ionic-native/network';
import { Device } from '@ionic-native/device';
import { CallNumber } from '@ionic-native/call-number';
import { Camera } from '@ionic-native/camera';

//image gallery modal
import * as ionicGalleryModal from 'ionic-gallery-modal';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { IonicSelectableModule } from 'ionic-selectable';

//Pipe 
import { PipesModule } from '../pipes/pipes.module';
import { HospitalAdminViewHospitalPage } from "../pages/hospital-admin/hospital-admin-view-hospital/hospital-admin-view-hospital";
import { HospitalAdminViewHospitalPageModule } from "../pages/hospital-admin/hospital-admin-view-hospital/hospital-admin-view-hospital.module";
import { DropdownModule } from 'primeng/dropdown'
import { HospitalStaffServiceProvider } from '../providers/hospital-staff-service/hospital-staff-service';
// import { RouterModule,Router } from '@angular/router';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        DoctorPatientApp,
        TimeSlotsAlertComponent,
        SearchFilterComponent,
        CancelAppointmentConfirmationComponent,

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        HttpClientModule,
        MultiSelectModule,
        CalendarModule,
        SharedModule,
        PipesModule,
        ChartModule,
        SelectSearchableModule,
        IonicSelectableModule,
        HospitalAdminViewHospitalPageModule,
        //FooterPageModule,
        ionicGalleryModal.GalleryModalModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient],
            }
        }),
        IonicStorageModule.forRoot(),
        UserIdleModule.forRoot({ idle: 50, timeout: 10, ping: 5 }),
        IonicModule.forRoot(DoctorPatientApp, {
            mode: 'md'
        })
        //    RouterModule.forChild(ProjectRoutes)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        DoctorPatientApp,
        TimeSlotsAlertComponent,
        SearchFilterComponent,
        CancelAppointmentConfirmationComponent,

    ],
    providers: [
        StatusBar,
        SplashScreen,
        Globalization,
        Network,
        Device,
        OneSignal,
        LocalStorageService,
        CommonService,
        AuthService,
        RestService,
        Constants,
        CameraService,
        NetworkService,
        DeviceInformationProvider,
        ManageHospitalServiceProvider,
        ManageSysAdminServiceProvider,
        PatientRoleServiceProvider,
        MapServiceProvider,
        Geolocation,
        CallNumber,
        Camera,
        OneSignalService,
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: ionicGalleryModal.GalleryModalHammerConfig,
        },
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        DoctorRoleServiceProvider,
        SearchServiceProvider,
        HospitalStaffServiceProvider
    ]
})
export class AppModule { }

