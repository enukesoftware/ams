/*--------------------Ionic related components---------------*/
import { Injectable } from '@angular/core';
import { Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ToastController, AlertController, LoadingController, ModalController, Events, Platform} from 'ionic-angular';
import { Nav} from 'ionic-angular';
import { UserIdleService } from '../../providers/angular-user-idle';
import { Location } from '@angular/common';

import { LocalStorageService } from '../../providers/localStorage-service/localStorage.service';
import { Constants } from '../appSettings/constant-settings';
import { AppSettings } from '../appSettings/appSettings';
import { RestService } from '../restService/restService';
import { HttpResponseDTO } from '../../interfaces/http-response-dto';

import { UserDataDTO } from '../../interfaces/user-data-dto';
import * as moment from 'moment';

/**
 * CommonService class : contains common functionalities
 */
@Injectable()
export class CommonService {
    timeSlotArray: any[];
    public userData: UserDataDTO = {};
    public activeUserRole: string;
    public userLoginRole: string;
    public sessionResumeAlertFlag: boolean = false;
    public sessionTimeoutCounter: any;
    public timeoutObj: any;
    public timestartObj: any;
    public timepingObj: any;

    public loader: any;
    public inAppBrowser: any;
    public isWeb: boolean = false;
    public selectedTime: any;
    public isDevice: boolean = false;
    public isMobileWeb :boolean = false; 

    loading = this.loadingCtrl.create({
        content: 'Please wait...'
    });
    
    public showFooter: boolean = false;
    
    constructor( private userIdle: UserIdleService, private restService: RestService, public locstr: LocalStorageService, public evts: Events, private storage: Storage, private toastCtrl: ToastController, public alertCtrl: AlertController, private loadingCtrl: LoadingController,
                 private platform: Platform, public location: Location, private translate: TranslateService, private constant: Constants, public localStorageService: LocalStorageService ) {
        this.timeSlotArray = [];
    }
    
    /*
     * Setter function for logged in user data
     * */
    public setUserData = ( userData: UserDataDTO ) => {
        this.userData = userData;
        //console.log("commonService setUserData ==============================>",this.userData);
    }
    
    /*
     * Getter function for logged in user data
     * */
    public getUserData = () => {
        return this.userData;
    }
    
    /*
     * Function to return role of logged in user
     * */
    public getUserRole = () => {
        return this.userData.role;
    }
    
    /*
     * Function to set active role of logged in user
     * */
    public setActiveUserRole = ( activeRole: any ) => {
        this.activeUserRole = activeRole;
    }
    
    /*
     * Function to set active role of logged in user
     * */
    public setUserLoginRole = ( LoginRole: any ) => {
        this.userLoginRole = LoginRole;
    }
    
    /*
     * Function to return role of logged in user
     * */
    public getActiveUserRole = () => {
        return this.activeUserRole;
    }
    
    
    /*
     * Function to return role of logged in user
     * */
    public getUserLoginRole = () => {
        return this.userLoginRole;
    }
    
    /*
     * Function to fire event for showing selected menu item in menu list
     * */
    public fireSelectedEvent = ( screenURL: any ) => {
        this.evts.publish('set-menu-selected', screenURL);
    }
    
    public closePopupOnBackBtn = (popover) => {
        this.platform.registerBackButtonAction(() => {
            if(popover){
                console.log("before popover dismiss...", popover);
                popover.dismiss();
                popover.onDidDismiss((data)=>{
                    this.platform.registerBackButtonAction(() => {
                        this.location.back();
                    });
                });
            } else{
                console.log("73 before popover dismiss...", popover);
                this.location.back();
            }
        });
    }
    /*
     * Function to check whether the logged in user is authorized to access the current screen
     * */
    public isAuthorizedToViewPage = ( navCtrl: any, pageOfRole: any, isDashboard?: boolean ) => {
        //console.log("isAuthorizedToViewPage =========================>",this.activeUserRole);
        if( this.activeUserRole == pageOfRole ){
            //return true;
        }else{
            //return false;
            if( this.activeUserRole == this.constant.ROLE_SYSTEM_ADMIN && !isDashboard ){
                navCtrl.setRoot( 'SADashboardPage' );
            }else if( this.activeUserRole == this.constant.ROLE_HOSPITAL_ADMIN && !isDashboard ){
                navCtrl.setRoot( 'HADashboardPage' );
            }else if( this.activeUserRole == this.constant.ROLE_DOCTOR && !isDashboard ){
                navCtrl.setRoot( 'DoctorDashboardPage' );
            }else if( this.activeUserRole == this.constant.ROLE_PATIENT && !isDashboard ){
                setTimeout(()=>{
                    navCtrl.setRoot( 'PatientRoleHomePage');
                },500);
            }
        }
    }

    /*
     * Function to check whether the patient is authorized to access the current screen
     * */
     public isPatientAuthorizedToViewPage = ( navCtrl: any, pageOfRole: any ) => {
        if( this.activeUserRole == this.constant.ROLE_PATIENT ){
            navCtrl.setRoot( 'PatientRoleHomePage' );
        }
    }

    /*
     * Function to calculate header height and assign that height to scroll content as padding top
     * */
    public setPaddingTopScroll = ( content: any ) => {
        let contentChilds = content._elementRef.nativeElement.childNodes;
        let scrollPaddingTop: any;
        let mainContentHt: any;
        let actualScrollHt: any;
        for ( let i = 0; i < contentChilds.length; i++ ) {
            if ( contentChilds[i].className == "scroll-content" ) {
                let scrollChilds = contentChilds[i].childNodes;
                for ( let j = 0; j < scrollChilds.length; j++ ) {
                    if ( scrollChilds[j].classList ) {
                        for ( let k = 0; k < scrollChilds[j].classList.length; k++ ) {
                            if ( scrollChilds[j].classList[k] == "header" ) {
                                if( this.getBrowserName() == "ie-11" || this.getBrowserName() == "ie-10" ){
                                    let tempHalfOfWidthDifference = (this.platform.width() - this.constant.CONTENT_MAX_WIDTH) / 2;
                                    if( tempHalfOfWidthDifference > 0 ){
                                        let menuWidth = this.constant.MIN_MENU_WIDTH + tempHalfOfWidthDifference;
                                        scrollChilds[j].style.left = menuWidth + "px";
                                    }
                                }
                                scrollPaddingTop = scrollChilds[j].clientHeight + 1; // + 16
                                contentChilds[i].style.paddingTop = scrollPaddingTop + "px";
                            }
                            
                            if( scrollChilds[j].classList[k] == "common-header-component" ){
                                let commonHeaderChilds = scrollChilds[j].childNodes;
                                for ( let n = 0; n < commonHeaderChilds.length; n++ ) {
                                    if ( commonHeaderChilds[n].classList ) {
                                        for ( let m = 0; m < commonHeaderChilds[n].classList.length; m++ ) {
                                            if ( commonHeaderChilds[n].classList[m] == "header" ) {
                                                scrollPaddingTop = commonHeaderChilds[n].clientHeight + 1; // + 16
                                                contentChilds[i].style.paddingTop = scrollPaddingTop + "px";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if( this.checkIsWeb() ){
            setTimeout(() => {
                for ( let i = 0; i < contentChilds.length; i++ ) {
                    if ( contentChilds[i].className == "scroll-content" ) {
                        let scrollChilds = contentChilds[i].childNodes;
                        for ( let j = 0; j < scrollChilds.length; j++ ) {
                            if ( scrollChilds[j].classList ) {
                                for ( let k = 0; k < scrollChilds[j].classList.length; k++ ) {
                                    if ( scrollChilds[j].classList[k] == "main-content" ) {
                                        mainContentHt = scrollChilds[j].clientHeight;
                                    }
                                }
                                actualScrollHt = contentChilds[i].clientHeight - scrollPaddingTop;
                            }
                        }
                    }
                }
                for ( let i = 0; i < contentChilds.length; i++ ) {
                    if ( contentChilds[i].className == "scroll-content" ) {
                        let scrollChilds = contentChilds[i].childNodes;
                        for ( let j = 0; j < scrollChilds.length; j++ ) {
                            if ( scrollChilds[j].classList ) {
                                for ( let k = 0; k < scrollChilds[j].classList.length; k++ ) {
                                    if ( scrollChilds[j].classList[k] == "header" ) {
                                        if ( actualScrollHt && mainContentHt ) {
                                            if ( actualScrollHt <= mainContentHt && !this.platform.is('mobileweb')) {
                                                scrollChilds[j].style.paddingRight = "17px";
                                            } else {
                                                scrollChilds[j].style.paddingRight = "0";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }, 1500 );
        }
    }
    
    /*
     * Function to calculate footer height and assign that height to scroll content as margin bottom
     * */
    public setBottomScroll = () => {
        setTimeout(()=>{
            /*let footer = <HTMLElement>document.getElementsByClassName('footer-parent')[0];
            let scrollCont = document.getElementsByClassName('scroll-content');
            for( let i=0;i<scrollCont.length;i++ ){
                (<HTMLElement>scrollCont[i]).style.marginBottom = ""+footer.offsetHeight+"px";
            }*/
            if( this.checkIsWeb() && this.platform.width() >= 1286 ){
                // since main footer i.e. copyright footer has fix height of 36px we are assigning fix bottom 36px
                // scroll content
                let scrollCont = document.getElementsByClassName('scroll-content');
                for( let i=0;i<scrollCont.length;i++ ){
                    (<HTMLElement>scrollCont[i]).style.marginBottom = "36px";
                }
            }
        },500);
    }

    /*
     * Function to check whether ion-content will have height auto or 100%
     * */
    contentHeight = ( content: any, HAlistGrid: any ) => {
        let dashboardContentHT = content.contentHeight - 36;
        let listGridHeight = HAlistGrid.nativeElement.offsetHeight;
       // console.log( "listGridHeight", listGridHeight );
        if ( listGridHeight < dashboardContentHT ) {
            //console.log( "grid height less or Gr", listGridHeight );
            return true;
        } else {
            return false;
        }
    }

    /*
     * Function to take action after hiding the desktop footer
     * */
    public actionAfterHidingDesktopFooter = () => {
        if( this.checkIsWeb() ){
            // since main footer i.e. copyright footer has fix height of 36px we are assigning fix bottom 36px
            // scroll content
            let scrollCont = document.getElementsByClassName('scroll-content');
            for( let i=0;i<scrollCont.length;i++ ){
                (<HTMLElement>scrollCont[i]).style.marginBottom = "0";
            }
        }
    }

    /*
     * Function to check if app is running as web browser app or mobile app
     * */
    public checkIsWeb = () => {
        if (this.getWindowWidth() <= 768) {
            this.isWeb = false;
        } else {
            this.isWeb = true;
        }
        return this.isWeb;
    }
    
    /*
     * Function to check if app is running as web browser app or mobile app
     * */
    public checkPlatform = () => {
        if (this.platform.is('android') || this.platform.is('ios')) {
            this.isDevice = true;
        } else {
            this.isDevice = false;
        }
        
        //console.log("device",this.isDevice)
        // console.log("platform",this.platform.is('android'))
       /* console.log("platform ios",this.platform.is('ios'))*/
        return this.isDevice;
    }
    
    
    public checkMobileweb = () =>{
       if( this.platform.is('mobileweb')){
           this.isMobileWeb =true;
       }else{
           this.isMobileWeb =false;
       }
       
       console.log("isMobileWeb",this.isMobileWeb)
       return this.isMobileWeb;
    }
    
    
    
    /*
     * Function to check if user is idle or not
     * */
    public checkIfUserIsIdle = ( currentRole?:any ) => {
        let timeoutCount = this.userIdle.getConfigValue().timeout;
        console.log("checkIfUserIsIdle ======================>",timeoutCount);
        if( currentRole == this.constant.ROLE_DOCTOR || currentRole == this.constant.ROLE_PATIENT ){
            let config: any = {
                idle: "",
                timeout: "",
                ping: ""
            };
            if( currentRole == this.constant.ROLE_DOCTOR ){
                config.idle = 3600;
                config.timeout = 10;
                config.ping = 900;
            }else if( currentRole == this.constant.ROLE_PATIENT ){
                /*config.idle = 60;
                config.timeout = 10;
                config.ping = 20;*/
                config.idle = 900;
                config.timeout = 10;
                config.ping = 300;
            }
            this.userIdle.constructor(config);
            this.userIdle.startWatching();
            this.timestartObj = this.userIdle.onTimerStart().subscribe( (count) => {
                this.sessionTimeoutCounter = timeoutCount - count;
                this.sessionResumeAlertFlag = true;
            });
            this.timepingObj = this.userIdle.ping$.subscribe(() => {
                console.log("PING")
            });
            this.timeoutObj = this.userIdle.onTimeout().subscribe(() => {
                this.userIdle.stopWatching();
                this.timeoutObj.unsubscribe();
                this.timestartObj.unsubscribe();
                this.timepingObj.unsubscribe();
                this.sessionResumeAlertFlag = false;
                this.evts.publish( 'user:logout' );
            });
            
        }
    }
    
    /*
     * Function to reset session timer
     * */
    public resetTheSessionTimer = () => {
        this.userIdle.stopTimer();
        this.sessionResumeAlertFlag = false;
    }
    
    /**
     * Function to show toast
     */
    public presentToast = (message?: any) => {
        let toast = this.toastCtrl.create({
            message: message ? message : 'Coming soon',
            duration: 10000,
            position: 'top',
            cssClass: 'comingSoon',
            showCloseButton: true,
            dismissOnPageChange: true
        });
        toast.present();
    }
    
    /**
     * Function to show alert Pop up
     * @param: title
     * @param: error message 
     */
    public showAlert = (title: string, errorMessage: string) => {
        let alert = this.alertCtrl.create({
            title: title,
            enableBackdropDismiss: false,
            cssClass: 'alertPopUp',
            message: errorMessage,
            buttons: ['Ok']
        });
        alert.present();
        return alert;
    }
    
    /**
     * Function to show alert Pop up
     * @param: title
     * @param: error message 
     */
    public showPrivacyPolicyAlert = (title: string, errorMessage: string) => {
        let alert = this.alertCtrl.create({
            title: title,
            enableBackdropDismiss: true,
            cssClass: 'privacyPolicyPopup',
            message: errorMessage,
            buttons: ['Ok']
        });
        alert.present();
        return alert;
    } 
    
    /**
     * Function to show confirm alert Pop up
     * @param: title
     * @param: error message 
     */
    public confirmAlert = (title: string, errorMessage: string) => {
        let alert = this.alertCtrl.create({
            title: title,
            enableBackdropDismiss: false,
            cssClass: 'alertPopUp',
            message: errorMessage,
            buttons:  [
                       {
                           text: 'No',
                           role: 'cancel',
                           handler: () => {
                               alert.dismiss(false);
                               return false;
                           }
                         },
                         {
                           text: 'Yes',
                           handler: () => {
                               alert.dismiss(true);
                               return false;
                           }
                         }
                       ]
        });
        return alert;
        
    }
    
    
    
    /**
     * Function to show go to login popup on skip and explore page
     * @param: title
     * @param: error message 
     */
    public goToLoginAlert = (title: string, errorMessage: string) => {
        let alert = this.alertCtrl.create({
            title: title,
            enableBackdropDismiss: false,
            cssClass: 'alertPopUp',
            message: errorMessage,
            buttons:  [
                       {
                           text: 'Not now',
                           role: 'cancel',
                           handler: () => {
                               alert.dismiss(false);
                               return false;
                           }
                         },
                         {
                           text: 'Login',
                           handler: () => {
                               alert.dismiss(true);
                               return false;
                           }
                         }
                       ]
        });
        return alert;
        
    }
    
    
    
    /**
     * Translate given string
     */
    getTranslate(key, params) {
        return this.translate.instant(key, params);
    }
    
    /*
     * Timing List Popup
     * */
    
    public showTimingList = (title: string, subTitle: string, timingObj: any)=>{
        var timingStr = "<table>";
        console.log("timingObj..",timingObj)
        let timingLength = timingObj.length;
        let closeTableTag = "</table>";
        if(timingLength > 0){
            for(let i=0; i<timingLength; i++){
                let availability = timingObj[i].availability;
                if(timingObj[i].availability == "00:00AM - 11:59PM"){
                    availability = "24 Hrs Open";
                }
                if(timingObj[i].dayCount == new Date().getDay() - 1){
                    timingStr = timingStr + '<tr class="currentDate"><td>'+ timingObj[i].day + "</td><td>" + availability +"</td> </tr>";
                } else {
                    timingStr = timingStr + '<tr><td>'+ timingObj[i].day + "</td><td>" + availability +"</td> </tr>";
                }
            }
        }
    
        timingStr = timingStr + closeTableTag;
        
        let alert = this.alertCtrl.create({
           title: title,
           enableBackdropDismiss: false,
           cssClass: 'InfoPopUp',
           subTitle: subTitle,
           message: timingStr,
           buttons: ['Ok']
        });
        alert.present();
        return alert;
    }
    
    /*
     * Show today timings
     * */
    public showTodayTimings = (title: string, subTitle: string, date: any, timeObj: any)=>{
        let subTitleWithDate = subTitle + "<br><br>" + date;
        
        var timeStr = "<table>";
        let timeLength = timeObj.length;
        console.log("timeObj", timeObj)
        if(timeLength > 0){
            console.log("timeLength.............  526", timeLength)
            for(let i=0; i<timeLength; i++){
                console.log("timeLength...........528", timeLength)
                var newTimeObj = moment(timeObj[i].fromTime).toDate();
                var finalTimeObj = moment(timeObj[i].toTime).toDate();
                timeStr = timeStr + "<tr><td>" + this.convertTo12hrsFormat(new Date(newTimeObj)) + " - "+ this.convertTo12hrsFormat(new Date(finalTimeObj)) +"</td></tr>";
            }
        }
        timeStr = timeStr + "</table>";
        console.log("timeStr", timeStr)
        let alert = this.alertCtrl.create({
           title: title,
           enableBackdropDismiss: false,
           cssClass: 'InfoPopUp todayPopup',
           subTitle: subTitleWithDate,
           message: timeStr,
           buttons: ['Ok']
        });
        alert.present();
            return alert;
    }
    
    /*
     * format common date string
     * */
    formatDateString = (date, forPopup?:any) =>{
        var d = date,
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        
        if(!forPopup){    
            return [year, month, day].join('-');
        } else{
            return [day, month, year].join('-');
        }
        
    }
    
    /*
     * Hospital List Popup
     * */
    public showHospitalList = (title: string, subTitle: string, hospitalList: any)=>{
        
        var hospitalStr = "<table class='hospital-details-tbl'>";
        let closeTableTag = "</table>";
        
        for(var i=0; i<hospitalList.length; i++){
            
            if(hospitalList[i].todaysTimeAvailabilityList && hospitalList[i].todaysTimeAvailabilityList.length > 0){
                let timeSlotFound = false;
                let currentTime = new Date();
                let currentTimeInMinutes = this.getTimeInMinutes(currentTime);
                let todayTimeLen = hospitalList[i].todaysTimeAvailabilityList.length;
                for(let j=0; j<todayTimeLen; j++){
                    let startTimeInMinutes = this.getTimeInMinutes(new Date(hospitalList[i].todaysTimeAvailabilityList[j].fromTime), true);
                    let endTimeInMinutes = this.getTimeInMinutes(new Date(hospitalList[i].todaysTimeAvailabilityList[j].toTime), true);
                
                    if(currentTimeInMinutes >= startTimeInMinutes && currentTimeInMinutes <= endTimeInMinutes){
                        timeSlotFound = true;
                        var hospFromTimeObj = moment(hospitalList[i].todaysTimeAvailabilityList[j].fromTime).toDate();
                        var hospToTimeObj = moment(hospitalList[i].todaysTimeAvailabilityList[j].toTime).toDate();
                        
                        hospitalList[i].todaysAvailabilityString = this.convertTo12hrsFormat(new Date(hospFromTimeObj), false, true) + " - " + this.convertTo12hrsFormat(new Date(hospToTimeObj), false, true);
                    }
                }
                if(!timeSlotFound){
                    hospitalList[i].todaysAvailabilityString = "Unavailable";
                }
            } else{
                hospitalList[i].todaysAvailabilityString = "Unavailable";
            }
        
        
        
            hospitalStr = hospitalStr + "<tr><th colspan='2'><div class='hospitalTitle'>"+hospitalList[i].name+"</div></th></tr>"+
                          "<tr><td class='hosp-td' valign='top'><label class='hospDetailsLabel'>Address:</label></td><td class='hosp-td' valign='top'>" + hospitalList[i].address.street + ", "+hospitalList[i].address.cityName+", "+hospitalList[i].address.state+", "+ hospitalList[i].address.state +" ,"+ hospitalList[i].address.zipCode +"</td></tr>"+
                          "<tr><td class='hosp-td' valign='top'><label class='hospDetailsLabel'>Visit Type:</label></td><td class='hosp-td' valign='top'>" + hospitalList[i].visitType +"</td></tr>"+
                          "<tr class='set-border-bottom'><td class='hosp-td' valign='top'><label class='hospDetailsLabel'>Today:</label></td><td class='hosp-td' valign='top'>" +  hospitalList[i].todaysAvailabilityString + "</td></tr>"
        }
            hospitalStr = hospitalStr + closeTableTag;
        
        let alert = this.alertCtrl.create({
            title: title,
            enableBackdropDismiss: false,
            cssClass: 'hospitalAlert',
            subTitle: subTitle,
            message: hospitalStr,
            buttons: ['Ok']
         });
         alert.present();
         return alert;
    }
    
    /*
     * Function to toggle the password input type on eye icon
     * */
    public passwordToggle = ( eyeIcon?: any, inputType?: any, callBack?: any ) => {
        if( eyeIcon == "eye-icon" ){
            eyeIcon = "eye-off-icon";
            inputType = "text";
            callBack( eyeIcon,inputType );
        }else{
            eyeIcon = "eye-icon";
            inputType = "password";
            callBack( eyeIcon,inputType );
        }
    }
  
    /**
     * Function to translate text from component
     * @param translateWord
     */
    public translateText( translateWord: string ){
        return this.translate.get(translateWord);
    }
    
    /*
     * Function to save values in ionic storage
     * */
    public setInStorage = ( key: any, value: any ) => {
        //this.userData = value;
        this.storage.set( key,value );
    }
    
    /*
     * Function to get saved values from ionic storage
     * */
    public getFromStorage = ( key: any ) => {
        return this.storage.get(key);
    }
    
    /*
     * Function to remove individual values from ionic storage
     * */
    public removeFromStorage = ( key: any ) => {
        return this.storage.remove(key);
    }
    
    /*
     * Function to clear all from ionic storage
     * */
    public clearStorage = () => {
        return this.storage.clear();
    }
    
    /*
     * Function to logout user from app
     * */
    public doLogout = ( navCtrl: any ) => {
        this.logout().subscribe(
                res => {
                    console.log("logout res ==============================>",res);
                },
                error => {
                    console.log("logout error ==============================>",error);
                }
            );
        this.clearStorage().then(() => {
            this.locstr.clearAllLocalStorage();
            if( this.userIdle && this.timeoutObj && this.timestartObj && this.timepingObj ){
                this.userIdle.stopWatching();
                this.timeoutObj.unsubscribe();
                this.timestartObj.unsubscribe();
                this.timepingObj.unsubscribe();
                this.sessionResumeAlertFlag = false;
            }
            navCtrl.setRoot( 'LoginPage' );
        });
    }
    
    /*
     * Function to check if userData present in storage and if not present redirect to user to specified screen
     * */
    public isLoggedIn = ( cb: any ) => {
        this.getFromStorage('userData').then((result) => {
            console.log("commonService isLoggedIn ==============================>",result);
            if( !result ){
                cb(false);
            }else{
                cb(result);
            }
        });
    }
    
    public isLoggedIn1 = ( navCtrl: any, screenName?: any ) => {
        this.getFromStorage('userData').then((result) => {
           // console.log("commonService isLoggedIn1 ==============================>",result);
            if( !result ){
                navCtrl.setRoot( 'LoginPage' );
            }
        });
    }
    
    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public setAddressOnMap = ( currentHospitalAddress: any ) => {
        /*if( this.checkIsWeb() ){
            // web app related code ....
            this.openNewTab(currentHospitalAddress);
        }else{
            //mobile app related code ....
        }*/
        this.openNewTab(currentHospitalAddress);
    }
    
    public openNewTab = ( currentHospitalAddress: any ) => {
        let googleMapLink = "https://www.google.com/maps/search/?api=1&query="+currentHospitalAddress;
        if( this.checkPlatform() ){
            //mobile app related code ....
            window.open( googleMapLink,'_system' );
        }else{
            // web app related code ....
            window.open( googleMapLink,'_blank' );
        }
    }
    
    /**
     * Function to open a link in-app browser
     * @param: link
     */
    public openLinkInAppBrowser = (link: any, target: any, options: any) => {
        var me = this;
        try{
            //me.inAppBrowser = me.iab.create(link, target, options);
            me.inAppBrowser.on("loadstart").subscribe(type => {
                //console.log("openLinkInAppBrowser loadstart =======================>",type);
                me.showLoading("Please wait");
            });
            me.inAppBrowser.on("loadstop").subscribe(type => {
                //console.log("openLinkInAppBrowser loadstop =======================>",type);
                me.hideLoading();
                me.inAppBrowser.show();
            });
            me.inAppBrowser.on("loaderror").subscribe(type => {
               // console.log("openLinkInAppBrowser loaderror =======================>",type);
                me.hideLoading();
            });
        }catch(exc){
            console.log('Error in openLinkInAppBrowser =======================>',exc);
        }
    }
    
    /**
     * Function to get windowHeight
     * 
     */
    
    public getWindowHeight(){
        return window.innerHeight;
    }
    
    /**
     * Function to get windowWidth
     * 
     */
    public getWindowWidth(){
        return window.innerWidth;
    }
    
 
    /**
     * Function to show loader
     * @param: message
     */
    public showLoading = message => {
        if (this.isLoaderUndefined()) {
            this.loader = this.loadingCtrl.create({
                content: message,
                duration: 60000
            });
        }
        this.loader.present();
    }
 
    /**
     * Function to hide loader
     */
    public hideLoading = () => {
        let myThis = this;
        try {
            if (!this.isLoaderUndefined()) {
                myThis.loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));
            }
            this.loader = undefined;
        } catch (e) {
            this.loader = undefined;
            console.log("aa -----hideLoading--e=", e);
        }
    }
  
    /**
     * Function to check if loader undefined or not
     */
    isLoaderUndefined(): boolean {
        return (this.loader == null || this.loader == undefined);
    }
    
    /**
     * Function to Get list of cities rest API call
     * @param data
     */
     calculateTime = (timeArray): any => {
         console.log("timeArray at 766....", timeArray);
         let timeLength: number;
     
         if(timeArray != null && timeArray.length > 0){
             timeLength = timeArray.length;
         }else{
             timeLength = 0;
         }
     
         var matchDays = [];
         var weekday = [];
         let timeObject: any;
         let dayObj: any = [];
         
        /* this.getFromStorage("weekdays").then((value)=>{
             if(value){
                 weekday =  value.name;
                console.log("weekdays at 781...", value);
             }
         });*/
         
         /*weekday[0] = "Sunday";
         weekday[1] = "Monday";
         weekday[2] = "Tuesday";
         weekday[3] = "Wednesday";
         weekday[4] = "Thursday";
         weekday[5] = "Friday";
         weekday[6] = "Saturday";*/
         
         weekday[0] = "Monday";
         weekday[1] = "Tuesday";
         weekday[2] = "Wednesday";
         weekday[3] = "Thursday";
         weekday[4] = "Friday";
         weekday[5] = "Saturday";
         weekday[6] = "Sunday";
         
         
         var currentDate =  new Date();
         var today = currentDate.getDay();
         for(let i=0; i<timeLength; i++){
             if(i < timeLength){
                 let day = timeArray[i].day;
                 let fromTime = timeArray[i].fromTime;
                 let endTime = timeArray[i].toTime;
                 let formattedFromTimeObj = this.convertTimeFormat(fromTime);
                 let formattedToTimeObj = this.convertTimeFormat(endTime);
                 
                 
         
                 let startTimeStr = formattedFromTimeObj.startHours + ":" + formattedFromTimeObj.startMinutes;
                 let endTimeStr = formattedToTimeObj.startHours + ":" + formattedFromTimeObj.startMinutes;
                 let availability;
                 let startTimeSuffix = "am";
                 let endTimeSuffix = "am";
         
                 var unformattedStartTime = moment(fromTime).toDate();
                 var unformattedOldTime = moment(endTime).toDate();
         
                 let startTime = this.convertTo12hrsFormat(new Date(unformattedStartTime), false, true);
                 let lastTime = this.convertTo12hrsFormat(new Date(unformattedOldTime), false, true);
                 console.log("startTime",startTime);
                 console.log("startTime",lastTime);    
                 
                 //console.log("startTime 723.....", startTime);
                // console.log("lastTime 724....", lastTime);
         
                 if( parseInt(formattedFromTimeObj.startHours) > 12){
                     startTimeSuffix = "pm";
                 }
                 if( parseInt(formattedToTimeObj.startHours) > 12 ){
                     endTimeSuffix = "pm";
                 }
                 
                 availability = startTime + " - " + lastTime;
                 let matchDay;
                 let matchedDayCount;
                 for(let j=0; j<7; j++){
                     if(weekday[j] == day){
                         matchDay = day;
                         matchedDayCount = j;
                         if(dayObj.indexOf(day) === -1) {
                             dayObj.push({'day': matchDay, 'startTime': startTimeStr, 'endTime': endTimeStr, 'availability': availability, 'dayCount': matchedDayCount});
                             matchDays.push(matchDay);
                         }    
                     }
                 }
                 
             }
         }
                 
         if(matchDays.length > 0){
             for(let k=0; k<weekday.length; k++){
                 if(matchDays.indexOf(weekday[k]) === -1){
                     let dayData = {'day': weekday[k], 'startTime': "", 'endTime': "", 'availability': "Closed", 'dayCount': k};
                     dayObj.splice(k, 0, dayData);
                 }
             }
         } else{
             for(let k=0; k<weekday.length; k++){
                 let dayData = {'day': weekday[k], 'startTime': "", 'endTime': "", 'availability': "Closed", 'dayCount': k};
                 dayObj.splice(k, 0, dayData);
             }
         }
             
         dayObj.sort(this.compare);
         return dayObj;
     }
      
      /*
      * sort time object       
      */ 
    
      compare = (a,b) =>{
         if (a.dayCount < b.dayCount)
           return -1;
         if (a.dayCount > b.dayCount)
           return 1;
         return 0;
       }
    
     convertTimeFormat = (milSec) =>{
         let miliSec: number = parseInt(milSec);   
         let startMilliseconds: number;
         let startSeconds: number;
         let startMinutes: number;
         let startHours: number;
         startMilliseconds = (miliSec % 1000) / 100;
         startSeconds = (miliSec / 1000) % 60;
         startMinutes = (miliSec / (1000 * 60)) % 60;
         startHours = (miliSec / (1000 * 60 * 60)) % 24;
         
         var formattedTime = {"startHours": startHours.toFixed(0), "startMinutes": startMinutes.toFixed(0), "startSeconds": startSeconds.toFixed(0)};
         
         return formattedTime;
     }    
         
    /**
     * Function to Get list of cities rest API call
     * @param data
     */
    getListOfCities = (): Observable<any> => {
        var path = AppSettings.GET_CITIES;
        return this.restService.getCall( path, null, false, null )
        .map( res => res.json() )
        .catch( this.handleError );
    } 

    
    /*
     *  function to get activation or deactivation action   
     **/
    public getActivationStatus = (person,cb) =>{
        let activateionStatus={
                Action:'',
                confirmationText:''
        };
        if(person.blockStatus =="UNBLOCK"){
            activateionStatus.Action ="DEACTIVATE";
            activateionStatus.confirmationText="deactivate"
            cb({Action:activateionStatus.Action,text:activateionStatus.confirmationText});
        } 
        else if(person.blockStatus =="BLOCK"){
            activateionStatus.Action ="ACTIVATE";
            activateionStatus.confirmationText="activate"
            cb({Action:activateionStatus.Action,text:activateionStatus.confirmationText});
        }
        else{
            activateionStatus.Action ="ACTIVATE";
            activateionStatus.confirmationText="activate"
            cb({Action:activateionStatus.Action,text:activateionStatus.confirmationText});
        }
        
    }

    /**
   * Function to Upload image
   */
  /*  uploadImage = ( data: any, file:any, imageData: any ): Observable<any> => {
      console.log("data", file);
      var path = AppSettings.IMAGE_UPLOAD + '?file=' + file +'&fileName=' + file.name + '&userType=' + imageData.userType + '&mediaType=' + imageData.mediaType + '&id=' + imageData.id + '&rank=' + imageData.rank + '&isDefault=' + imageData.isDefault;
      return this.restService.postCallForFormData( path, data, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
  } */
   uploadImage = ( data: any ): Observable<any> => {
      // console.log("data", data);
      var path = AppSettings.IMAGE_UPLOAD;
      return this.restService.postCall( path, data, false, 60000 )
      .map( res => res.json() )
      .catch( this.handleError );
  }

  /**
   * Function to get Day's form monday to sunday with Id
   */
    getDay = (): Observable<any> => {
        var path = AppSettings.GET_DAY;
        return this.restService.getCall( path, null, false, null )
        .map( res => res.json() )
        .catch( this.handleError );
    } 

  /**
   * Function to Upload image
   */
  downloadImage = ( imageData: any ): Observable<any> => {
      let checkDefaultImg = imageData.isDefault ? '&isDefault='+ imageData.isDefault : '';
      console.log("downloadImage ====================>",checkDefaultImg,imageData);
      var path = AppSettings.IMAGE_DOWNLOAD + '?id=' + imageData.id +'&rank=' + imageData.rank + '&userType=' + imageData.userType + checkDefaultImg;
      return this.restService.getCall( path, null, false, null )
      .map( res => res.json());
  }

  /**
   * Function to Upload image
   */
  downloadDependentImage = ( imageData: any ): Observable<any> => {
    let checkDefaultImg = imageData.isDefault ? '&isDefault='+ imageData.isDefault : '';
    console.log("downloadImage ====================>",checkDefaultImg,imageData);
    var path = AppSettings.IMAGE_DOWNLOAD + '?id=' + imageData.id + '&userProfileId=' + imageData.userProfileId +'&rank=' + imageData.rank + '&userType=' + imageData.userType + checkDefaultImg;
    return this.restService.getCall( path, null, false, null )
    .map( res => res.json());
}

 /**
   * Function to Edit Profile image
   */
   editImage = ( data: any ): Observable<any> => {
      var path = AppSettings.EDIT_IMAGE;
      return this.restService.putCall( path, data, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
  }
  
     /**
   * Function to Edit Profile image
   */
   deleteImage = ( userType:any, hospitalId: any ): Observable<any> => {
      var path = AppSettings.DELETE_IMAGE + '?userType=' + userType + '&id=' + hospitalId;
      return this.restService.deleteCall( path, false, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
  }
  
    /**
   * Function to Edit Profile image
   */
   convertTo12hrsFormat = ( utcTime, isUTC?: boolean, isIncoming?: boolean) => {
       let hrs;
       let mins;
       let time = utcTime;
       if(isIncoming){
           time = this.convertToLocalDate(utcTime);
       }
       if(isUTC){
           hrs = time.hrs;
           mins = time.mins;
       }else{
           hrs = time.getHours();
           mins = time.getMinutes();
       }       
       if( mins < 10 ){
           mins = "0" + mins;
       }
       if( hrs < 10 ){
           hrs = "0" + hrs;
       }
       if( parseInt(hrs) > 12 ){
          time = (parseInt(hrs) - 12) + ":" + mins + "PM";
          if((parseInt(hrs) - 12) < 10){
              time = "0"+time;
          }
       }else if(parseInt(hrs) == 0){
           time = "00" +  ":" + mins + "AM";
       }else if(parseInt(hrs) == 12){
           time = parseInt(hrs) +  ":" + mins + "PM";
       }else{
           time = hrs +  ":" + mins + "AM";
       }
       return time;
  }

   /**
   * Function to Edit Profile image
   */
   getHospitalListWithTimeslots = ( data: any, pageNo: any ): Observable<any> => {
      var path = AppSettings.GET_HOSPITAL_LIST_WITH_TIMESLOTS  + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE;
      return this.restService.postCall( path, data, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
  }
    
  /**
   * Function to Create Appointment
   */
   createAppointment = ( data:any ): Observable<any> => {
      var path = AppSettings.CREATE_APPOINTMENT;
      return this.restService.postCall( path, data, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
  }
    
   /**
   * Function to Get Appointment Details
   */
   getAppointmentDetails = ( data:any ): Observable<any> => {
      var path = AppSettings.GET_APPOINTMENT_DETAILS + '?appointmentId='+ data;
      return this.restService.getCall( path, false, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
  }
    
  /**
   * Function to Edit Appointment Details
   */
   editAppointmentDetails = ( data:any ): Observable<any> => {
      var path = AppSettings.EDIT_APPOINTMENT;
      return this.restService.putCall( path, data, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
  }
  
  /*
   *  Function to cancel Appointment
   **/
    cancelAppointment( data: any ){ 
      var path = AppSettings.CANCEL_APPOINTMENT + '?appointmentId='+ data;
      return this.restService.getCall( path, false, false, null )
      .map( res => res.json() )
      .catch( this.handleError );
    }

    /*
    * Function to search patient
    */
    searchPatient = (data:any): Observable<any> => {
        var path = AppSettings.SEARCH_PATIENT;
        return this.restService.postCall( path, data, false, null )
        .map( res => res.json() )
        .catch( this.handleError  );
    }

    /**
     * Function to add patient web service
     * @param data
     */
    addPatient = (data:any): Observable<any> => {
        //console.log("in createPatient",data);
        var path = AppSettings.ADD_PATIENT;
        return this.restService.postCall( path, data, false, null )
        .map( res => res.json() )
        .catch( this.handleError );
    }

    /**
     * Function to update patient web service
     * @param data
     */
    updatePatient = (data:any): Observable<any> => {
        //console.log("in createPatient",data);
        var path = AppSettings.UPDATE_PATIENT + "/"+data.userProfileId;
        return this.restService.putCall( path, data, false, null )
        .map( res => res.json() )
        .catch( this.handleError );
    }

    /**
     * Function to update patient web service
     * @param data
     */
    deletePatient = (id:any): Observable<any> => {
        //console.log("in createPatient",data);
        var path = AppSettings.DELETE_PATIENT + '/' + id;
        return this.restService.deleteCall( path, false, false, null )
        .map( res => res.json() )
        .catch( this.handleError );
    }
    
  /*
   * Function to get start time
   * */
   onChangeStartTime( sTime ){
       console.log("sTime 839....", sTime);
       return this.getTimeInMinutes( sTime );
   }
   
   /**
    * Function to return time in minutes
    **/
   getTimeInMinutes = ( getTimeInStr, incoming?:boolean ) => {
       let timeInStr = getTimeInStr;
       if(incoming){
           timeInStr = this.convertToLocalDate(getTimeInStr);
       }
       let hr = 0;
       let min = 0;
       hr = new Date(timeInStr).getHours();
       min = new Date(timeInStr).getMinutes();
       return ( hr * 60 ) + min;
   }
   
   /**
    * Function to return utctime in minutes
    **/
    getUTCTimeInMinutes = ( hrs, minutes ) => {
       let hr = hrs;
       let min = minutes;
       return ( hr * 60 ) + min;
   }
   
   /**
    * Function to add time slot to time array
    **/
  addTimeSlotClick = (timeSlot: any, isDoctorSlot?: boolean, isHospitalSlot?: boolean, selectedRadioSlot?: any, hospitalTimeSlot?: any, storedSlots?: any) => {
      if(storedSlots && storedSlots.length > 0){
          this.timeSlotArray = storedSlots;
      }
      /*if(timeSlot.effectiveFrom != undefined || !isHospitalSlot){*/
          if ( timeSlot.day != '' ) {
              if ( timeSlot.startTime != '' ) {
                  if ( timeSlot.endTime != '' ) {
                      if(!isDoctorSlot && !isHospitalSlot){
                          if ( timeSlot.visitValue != '' ) {
                              if ( this.isTimeRepeat( timeSlot ) ) {
                                  timeSlot.timeValidate = false;
                                  this.presentToast( this.constant.ERROR_ALREADY_BOOKED_TIME_SLOT );
                              } else {
                                  this.timeSlotArray.push( JSON.parse( JSON.stringify(timeSlot ) ) );
                              }
                          } else {
                              timeSlot.timeValidate = false;
                              this.presentToast( this.constant.ERROR_VALID_VALUE_FOR_APP_TOKEN );
                          }
                      } else if( isHospitalSlot ){
                          /*if(timeSlot.effectiveFrom != undefined){*/
                          if(timeSlot.day != ''){
                              if(this.getTimeInMinutes(new Date(timeSlot.startTime)) < this.getTimeInMinutes(new Date(timeSlot.endTime)) || this.getTimeInMinutes(new Date(timeSlot.endTime)) > this.getTimeInMinutes(new Date(timeSlot.startTime)) ){
                                  if ( this.isTimeRepeat( timeSlot ) ) {
                                      timeSlot.timeValidate = false;
                                      this.presentToast( this.constant.ERROR_ALREADY_BOOKED_TIME_SLOT );
                                  } else {
                                      timeSlot.timeValidate = true;
                                      this.timeSlotArray.push( JSON.parse( JSON.stringify(timeSlot ) ) );
                                  }
                              }else{
                                  timeSlot.timeValidate = false;
                                  if(this.getTimeInMinutes(new Date(timeSlot.startTime)) > this.getTimeInMinutes(new Date(timeSlot.endTime))){
                                      this.presentToast(this.constant.ERROR_STRAT_TIME_LESS_THEN_END_TIME);
                                  } else if(this.getTimeInMinutes(new Date(timeSlot.startTime)) < this.getTimeInMinutes(new Date(timeSlot.endTime))){
                                      this.presentToast(this.constant.ERROR_END_TIME_GREATEER_THEN_START_TIME);
                                  }
                              }
                              /*} else{
                                  this.presentToast("Please select day.");
                              }*/
                          } else{
                              this.presentToast(this.constant.ERROR_SELECT_DAY);
                          } 
                      } else{
                          /*if(hospitalTimeSlot && hospitalTimeSlot.length > 0 && hospitalTimeSlot[0].fromTime && hospitalTimeSlot[0].fromTime != ""){*/
//                              let selectedSlot = selectedRadioSlot;
//                              
//                              let getSelectedSlotStartHrs = new Date(selectedSlot.fromTime).getUTCHours();
//                              let getSelectedSlotStartMins = new Date(selectedSlot.fromTime).getUTCMinutes();
//                              
//                              let getSelectedSlotEndHrs = new Date(selectedSlot.toTime).getUTCHours();
//                              let getSelectedSlotEndMins = new Date(selectedSlot.toTime).getUTCMinutes();
//                              
//                              let selectedSlotStartTimeMinutes = this.getUTCTimeInMinutes(getSelectedSlotStartHrs, getSelectedSlotStartMins);
//                              
//                              let selectedSlotEndTimeMinutes = this.getUTCTimeInMinutes(getSelectedSlotEndHrs, getSelectedSlotEndMins);
//                             
                              let sTimeMinutes;
                              let eTimeMinutes;
                              
                              if(timeSlot.fromTime){
                                  sTimeMinutes = this.getTimeInMinutes(new Date(timeSlot.fromTime)); 
                              } else{
                                  sTimeMinutes = this.getTimeInMinutes(new Date(timeSlot.startTime));
                                  timeSlot.fromTime = timeSlot.startTime;
                              }
                              
                              
                              if(timeSlot.toTime){
                                  eTimeMinutes = this.getTimeInMinutes(new Date(timeSlot.toTime)); 
                              } else{
                                  eTimeMinutes = this.getTimeInMinutes(new Date(timeSlot.endTime));
                                  timeSlot.toTime = timeSlot.endTime;
                              }
                              
                              
//                              let selectedEndTimeMinutes = this.getTimeInMinutes(new Date(selectedSlot.toTime));
                              
                              /*let hospitalStartTimeMinutes = this.getTimeInMinutes(new Date(hospitalTimeSlot[0].fromTime));
                              let hospitalEndTimeMinutes = this.getTimeInMinutes(new Date(hospitalTimeSlot[0].toTime));
                              
                              let checkHospitalAvailability = this.checkHospitalAvailability(hospitalTimeSlot, sTimeMinutes, eTimeMinutes)
                              
                              console.log("checkHospitalAvailability at 1051...", checkHospitalAvailability);*/
//                              let hospitalFromTimeStr = this.convertTo12hrsFormat(new Date(hospitalTimeSlot[0].fromTime));
//                              let hospitalToTimeStr = this.convertTo12hrsFormat(new Date(hospitalTimeSlot[0].toTime));
                              
//                              let hospitalFromToTimeStr = hospitalFromTimeStr + " " + hospitalToTimeStr;
                              
                              /*if(!checkHospitalAvailability){*/
                                  /*if( sTimeMinutes > selectedSlotStartTimeMinutes && sTimeMinutes < selectedSlotEndTimeMinutes ){*/
                                      /*if(eTimeMinutes < selectedSlotEndTimeMinutes){*/
                              /*if ( this.isTimeRepeat( timeSlot ) ) {*/
//                                  timeSlot.timeValidate = false;
//                                  this.presentToast( this.constant.ERROR_ALREADY_BOOKED_TIME_SLOT );
//                              } else {
                                  timeSlot.timeValidate = true;
                                  this.timeSlotArray.push( JSON.parse( JSON.stringify( timeSlot ) ) );
//                              }
                                      /*} else{
                                          timeSlot.timeValidate = false;
                                          this.presentToast( this.constant.ERROR_SELECT_END_TIME_BETWEEN_SLOTS );
                                      }*/
                                  /*} else{
                                      timeSlot.timeValidate = false;
                                      this.presentToast( this.constant.ERROR_SELECT_START_TIME_BETWEEN_SLOTS );
                                  }*/
                              /*} else{
                                  timeSlot.timeValidate = false;
                                  this.presentToast( this.constant.ERROR_SELECT_DOCTOR_SLOT_BETWEEN_HOSPITAL_TIME_SLOT );
                              }*/
                         /* } else{
                              this.presentToast( this.constant.ERROR_HOSPITAL_AVAILABLE_TIME_SLOTS_NOT_FOUND );
                          }*/
                      }
                  } else {
                      timeSlot.timeValidate = false;
                      this.presentToast( this.constant.ERROR_CONTRACT_BLANK_END_TIME );
                  }
              } else {
                  timeSlot.timeValidate = false;
                  this.presentToast( this.constant.ERROR_CONTRACT_BLANK_START_TIME );
              }
          } else {
              timeSlot.timeValidate = false;
              this.presentToast( this.constant.ERROR_CONTRACT_BLANK_DAY );
          }
      /*} else {
          timeSlot.timeValidate = false;
          this.presentToast( "Please select effective from date." );
      }*/
      
      return timeSlot;
  }
  
  /*
   * remove slot
   * */
  
  removeTimeSlot = ( index: any ) => {
      console.log("this.timeSlotArray before remove....", this.timeSlotArray);
      this.timeSlotArray.splice(index, 1);
      console.log("this.timeSlotArray after remove....", this.timeSlotArray);
  }
  
  /*
   * clear time slot array 
   * */
  clearTimeSlotArr(){
      if(this.timeSlotArray.length > 0){
          this.timeSlotArray = [];
      }
  }
  
  /**
   * check hospital availability
   */
  checkHospitalAvailability(hospitalSlots, startTime, endTime){
      let timeSlotFound = true;
      
      for(let i=0; i<hospitalSlots.length; i++){
          let hospitalStartTimeMinutes = this.getTimeInMinutes(new Date(hospitalSlots[i].fromTime));
          let hospitalEndTimeMinutes = this.getTimeInMinutes(new Date(hospitalSlots[i].toTime));
      
          if(startTime > hospitalStartTimeMinutes && endTime < hospitalEndTimeMinutes){
              timeSlotFound = false;
          }
      }
      if(timeSlotFound){
          return true;
      } else{
          return false;
      }
  }
  
  /**
   * Function to validate time slots 
   **/
  isTimeRepeat = ( tempSlot ) => {
      let currentSlot = tempSlot;
      
      if ( this.timeSlotArray.length == 0 ) { return false }
          for ( let i = 0; i < this.timeSlotArray.length; i++ ) {
              if ( currentSlot.day == this.timeSlotArray[i].day && currentSlot.startTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && currentSlot.startTimeMinutes <= this.timeSlotArray[i].endTimeMinutes ) {
                  return true;
              }
              if ( currentSlot.day == this.timeSlotArray[i].day && currentSlot.endTimeMinutes >= this.timeSlotArray[i].startTimeMinutes && currentSlot.endTimeMinutes <= this.timeSlotArray[i].endTimeMinutes) {
                  return true;
              }
              if ( currentSlot.day == this.timeSlotArray[i].day && currentSlot.startTimeMinutes < this.timeSlotArray[i].startTimeMinutes && currentSlot.endTimeMinutes > this.timeSlotArray[i].endTimeMinutes) {
                  return true;
              }
          }
          return false;
  }
  
  /*
   * Function to convert date to local
   * 
   * */
  convertToLocalDate = (utcDate) => {
      let checkDst = moment(new Date()).isDST(); 
      let selectedDate = utcDate;
      if(checkDst){
          selectedDate = new Date(utcDate);
          selectedDate.setHours(selectedDate.getHours() + 1);
      }
      return selectedDate;
  }
  
  /*
   * Function to getDoctor filters
   * 
   * */
  
  getDoctorFilters = (cityName) => {
      let dataForDoctor = {
              mode: "doctor",
              city: cityName,
              doctorExp: {
                  'lower': 0,
                  'upper': 50
              },
              feesSlots: [
                          {'id': 1, 'slot':"200 and below", 'tickUrl':"assets/imgs/01_multispacility_uncheck.png", 'tick': false, 'lowerLimit': 0, 'upperLimit': 200},
                          {'id': 2, 'slot':"201 - 500", 'tickUrl': 'assets/imgs/01_multispacility_uncheck.png', 'tick': false, 'lowerLimit': 201, 'upperLimit': 500},
                          {'id': 3, 'slot':"501 and above", 'tickUrl': 'assets/imgs/01_multispacility_uncheck.png', 'tick': false, 'lowerLimit': 501, 'upperLimit': 50000}
                      ],
              availability: [
                         {'id': 1, 'slot':"Available today", 'tickUrl':"assets/imgs/01_multispacility_uncheck.png", 'tick': false, 'value': 'isAvailableToday'},
                         {'id': 2, 'slot':"Available for next 3 days", 'tickUrl':"assets/imgs/01_multispacility_uncheck.png", 'tick': false, 'value': 'isAvailableNextThreeDays'},
                         {'id': 3, 'slot':"Available on weekends", 'tickUrl': "assets/imgs/01_multispacility_uncheck.png", 'tick': false, 'value': 'isAvailableWeekend'}
                       ]
            };
      return dataForDoctor;
  }
  
  /*
   * Function to getDoctor filters
   * 
   * */
  getHospitalFilters = (cityName) => {
      let dataForHospital = {
              mode: "hospital",
              city: cityName,
              allDayOpen: false,
              multiSpeciality: true
            };
      return dataForHospital;
  }
   
   /**
     * Function to perform common error handling
     * @param error
     */
/*    private handleError = ( error: Response | any ) => {
        if( error && error.status == 401 ){
            setTimeout(() => {
                this.evts.publish( 'user:logout' );
            }, 500);
            this.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
        }else{
            if( error ){
                if( error.message == this.constant.ERROR_NETWORK_UNAVAILABLE ){
                    return Observable.throw( error );
                }else{
                    var err: any;
                    err = error.json();
                    if( error && err ){
                        if( error.code == 401 ){
                            setTimeout(() => {
                                this.evts.publish( 'user:logout' );
                            }, 500);
                        }else{
                            return Observable.throw( err );
                        }
                    }else{
                        this.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                    }
                }
            }else{
                this.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
            }
        }
        return Observable.throw( err );
    }*/
    
    
    public handleError = ( error: Response | any ) => {
        console.log( "status-------------------", error );
        /*if ( error && error.status == 401 ) {
                setTimeout(() => {
                    //this.evts.publish( 'user:logout' );
                }, 500 );
                let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNAUTHORIZED;
              this.presentToast(errorMsg);
                console.log('error------------------------------',error);
              //  return Observable.throw(error);
        } else {
            console.log('get default------------------------------',error);*/
            return this.getDefaultErrorMsg(error);
        //}
    }
    
    /**
     * Function to get default error message for error handling
     * @param error
     */
    public getDefaultErrorMsg = error =>{
        console.log('error------------------------------',error);
        /*if((typeof(error) == "string")){
            //console.log("string typeOf(error):: ",typeof(error));
            try{
                error = JSON.stringify(error);
            }catch(err){
                console.log("Exception in handleError:: ",err);
                return Observable.throw( { "message":this.constant.ERROR_MSG_UNABLE_TO_CONNECT});
            }
        }*/
        if ( error && error._body ) {
            console.log('error------------------------------',error);
            let errorJson = error.json();
            if ( error && error.status == 401 ) {
                //let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNAUTHORIZED;
                //this.presentToast(errorMsg);
                setTimeout(() => {
                    this.evts.publish( 'user:logout' );
                }, 500 );
                return Observable.throw( { "message":this.constant.ERROR_MSG_UNAUTHORIZED} );
            }else if ( errorJson && errorJson.message ) {
                return Observable.throw( errorJson );
            } else {
                return Observable.throw( { "message":this.constant.ERROR_MSG_UNABLE_TO_CONNECT});
            }
        } else if ( error && error.message ) {
            let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNAUTHORIZED;
            if ( error && error.status == 401 ) {
                errorMsg = this.constant.ERROR_MSG_UNAUTHORIZED;
            }else{
                errorMsg = this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
            }
            this.presentToast(errorMsg);
            if ( error && error.status == 401 ) {
                setTimeout(() => {
                    this.evts.publish( 'user:logout' );
                }, 500 );
            }
            return Observable.throw( error );
        } else {
            let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNAUTHORIZED;
            if ( error && error.status == 401 ) {
                errorMsg = this.constant.ERROR_MSG_UNAUTHORIZED;
            }else{
                errorMsg = this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
            }
            this.presentToast(errorMsg);
            if ( error && error.status == 401 ) {
                setTimeout(() => {
                    this.evts.publish( 'user:logout' );
                }, 500 );
            }
            return Observable.throw( { "message": this.constant.ERROR_MSG_UNABLE_TO_CONNECT});
        }
    }
    /*
     * Function to get browser name in which web app is running
     * */
    public getBrowserName = () => {
        /*var browserName = 'unknown';

        var isOpera = ( !!window.opr && !!opr.addons ) || !!window.opera
            || navigator.userAgent.indexOf( ' OPR/' ) >= 0;

        var isIE = @cc_on!@false || !!document.documentMode;

        var isChrome = !!window.chrome && !!window.chrome.webstore;

        if ( ( !!window.opr && !!opr.addons ) || !!window.opera
            || navigator.userAgent.indexOf( ' OPR/' ) >= 0 ) {
            browserName = 'opera';
        }
        if ( typeof InstallTrigger !== 'undefined' ) {
            browserName = 'firefox';
        }
        if ( Object.prototype.toString.call( window.HTMLElement ).indexOf( 'Constructor' ) > 0 ) {
            browserName = 'safari';
        }
        if (@cc_on!@false || !!document.documentMode ) {
            browserName = 'ie';
        }
        if ( !isIE && !!window.StyleMedia ) {
            browserName = 'edge';
        }
        if ( !!window.chrome && !!window.chrome.webstore ) {
            browserName = 'chrome';
        }
        return browserName;*/
        let browserName = 'unknown';
        if (navigator.userAgent.search("MSIE") >= 0){
            browserName = 'ie-10';
            /*var position = navigator.userAgent.search("MSIE") + 5;
            var end = navigator.userAgent.search("; Windows");
            var version = navigator.userAgent.substring(position,end);
            document.write(version + '"');*/
        }else if (navigator.userAgent.search("Trident") >= 0){
            browserName = 'ie-11';
            /*var position = navigator.userAgent.search("MSIE") + 5;
            var end = navigator.userAgent.search("; Windows");
            var version = navigator.userAgent.substring(position,end);
            document.write(version + '"');*/
        }else if (navigator.userAgent.search("Edge") >= 0){
            browserName = 'edge';
            /*var position = navigator.userAgent.search("MSIE") + 5;
            var end = navigator.userAgent.search("; Windows");
            var version = navigator.userAgent.substring(position,end);
            document.write(version + '"');*/
        }else if (navigator.userAgent.search("Chrome") >= 0){
            console.log('"Google Chrome ');// For some reason in the browser identification Chrome contains the word "Safari" so when detecting for Safari you need to include Not Chrome
            browserName = 'chrome';
            let position = navigator.userAgent.search("Chrome") + 7;
            let end = navigator.userAgent.search(" Safari");
            let version = navigator.userAgent.substring(position,end);
            let versionArr = version.split(".");
            if( versionArr && versionArr.length > 0){
                let versionNumber = parseInt( versionArr[0] );
                let chromeVersion =  parseInt( version );
                
                if( chromeVersion >= versionNumber){
                    browserName = 'chrome-above-70';
                }
            }
            ;
        }else if (navigator.userAgent.search("Firefox") >= 0){
            console.log('"Mozilla Firefox ');
            browserName = 'firefox';
            /*var position = navigator.userAgent.search("Firefox") + 8;
            var version = navigator.userAgent.substring(position);
            document.write(version + '"');*/
        }else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0){//<< Here
            console.log('"Apple Safari ');
            browserName = 'safari';
            /*var position = navigator.userAgent.search("Version") + 8;
            var end = navigator.userAgent.search(" Safari");
            var version = navigator.userAgent.substring(position,end);
            document.write(version + '"');*/
        }else if (navigator.userAgent.search("Opera") >= 0){
            console.log('"Opera ');
            browserName = 'opera';
            /*var position = navigator.userAgent.search("Version") + 8;
            var version = navigator.userAgent.substring(position);
            document.write(version + '"');*/
        }else{
            console.log('"Other"');
            browserName = 'other';
        }
        return browserName;
    }
    
    /*
     * Function to to show go to login popup 
     * */
    showGoTOLoginPopup =(navCtrl: any)=>{
        console.log("navCtrl..",navCtrl);
        let goToLoginPopUpMsg = this.getTranslate('GOTO_SIGNUP_UPCOMINGAPP',{})
        let warningTitle =  this.getTranslate('WARNING',{});
         
        let alert = this.goToLoginAlert(warningTitle, goToLoginPopUpMsg);
        alert.setMode("ios");
        alert.present();
        alert.onDidDismiss((data) => {
          if(data == true){  
              this.doLogout(navCtrl);
           }else{
               navCtrl.setRoot( 'PatientRoleHomePage' );
           }
       });
    }
    
    /**
     * function to clear fcm token on  Logout
     * */ 
    logout = () : Observable<HttpResponseDTO> => {
        var path = AppSettings.LOGOUT;
        return this.restService.getCall( path, false, false, null )
        .map( res => <HttpResponseDTO>res.json() )
        .catch( this.handleError  );
    }
    
    
    public getImageListBase = (list, role, cb) =>{
       // console.log("in get Image base list ....--- >",list ,"role ---- >" ,role);
        let param:any;
        for( let i=0; i < list.length; i++ ){
            if(role == 'Patient'){
                param = list[i].patientId
            }else if(role == 'Doctor'){
                param = list[i].doctorId
            }
            else if(role == 'HA'){
                param = list[i].patientId
            }else if(role == 'SysAdm'){
                param = list[i].patientId
            }
            let imageData = {
                'userType': "USER_PROFILE_IMAGE",
                'id': param,
                'rank': 1
             } 
            this.downloadImage( imageData ).subscribe(
                res => {
                    
                    if ( res.status == "success" ) {
                       // console.log("image base 64=======>", res);
                        for( let j=0; j < list.length; j++ ){
                                if(role == 'Doctor' && list[j].doctorId == res.data.userId){
                                    cb(j,res.data.file);
                                }
                                else if(role == 'Patient' && list[j].patientId == res.data.userId){
                                    cb(j,res.data.file);
                                }else if(role == 'HA' && list[j].doctorId == res.data.userId){
                                    cb(j,res.data.file);
                                }else if(role == 'SysAdm' && list[j].doctorId == res.data.userId){
                                    cb(j,res.data.file);
                                }
                              }
                        }
                },
                error => {
                    this.hideLoading();
                    let errorMsg = error.message ? error.message : this.constant.ERROR_NETWORK_UNAVAILABLE;
                    this.presentToast(errorMsg);
            });
        
        }}
    
    /***
     * function to convert and compress image to Base64
     */
   public convertImgToBase64 = ( url, callback ) => {
       // var canvas = document.createElement('CANVAS'),
       //ctx = canvas.getContext('2d'),
       var image = new Image;
       //img.crossOrigin = 'Anonymous';
       image.onload = function() {

           // Resize the image
           var canvas = document.createElement( 'canvas' ),
               max_size = 1024, // TODO : pull max size from a site config
               width = image.width,
               height = image.height;
           if ( width > height ) {
               if ( width > max_size ) {
                   height *= max_size / width;
                   width = max_size;
               }
           } else {
               if ( height > max_size ) {
                   width *= max_size / height;
                   height = max_size;
               }
           }
           canvas.width = width;
           canvas.height = height;
           //canvas.rotate(180 * Math.PI / 180);
           canvas.getContext( '2d' ).drawImage( image, 0, 0, width, height );
           var dataUrl = canvas.toDataURL( 'image/jpeg' );
           // var resizedImage = dataURLToBlob(dataUrl);
           callback( dataUrl );
       }
       image.src = url;
   }

   
        
}