/**
 * OneSignalService - Push Notification related function
 * Added for creating map related common functions
 * Added by Biz-1166
 * Date: 19 September 2018
 */

import { Injectable } from '@angular/core';
import { OneSignal, OSNotification, OSDisplayType } from '@ionic-native/onesignal';
import { Platform, Events  } from 'ionic-angular';
//import { AppSettings } from '../auth-service/app-settings';
import { AuthService } from '../authService/authService';
declare var WebOneSignal: any;

@Injectable()
export class OneSignalService {
    private isWeb: boolean = false;
    public onseSignalAppId: string = '77e6b007-7b15-42f5-a867-7173569017b7';   // -------- Client account
    private googleProjectId: string = '951281662914';     //FIREBASE_SENDER_ID
    public data:any = {
            'playerId':''
    }

constructor( public oneSignal: OneSignal, private platform: Platform, private event: Events,public authService:AuthService ) {

}

generateIDs = () => {
  
  if(this.platform.is('cordova')) {
        if (this.platform.is('android')) {
            this.oneSignal.startInit(this.onseSignalAppId, this.googleProjectId);
        }
        else if (this.platform.is('ios')) {
            this.oneSignal.startInit(this.onseSignalAppId);
        }
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    
        this.oneSignal.handleNotificationReceived().subscribe((notification) => {
            // do something when notification is received
             console.log("notification " ,notification)
            this.event.publish('updateNotificationCount');
        });
    
        this.oneSignal.handleNotificationOpened().subscribe(result => {
            // do something when a notification is opened 
             console.log("notification opened" ,result)
        });
    
        this.oneSignal.endInit();
    
        // AND THIS METHOD RETURN YOUR DEVICES USER_ID
        
        this.oneSignal.getIds().then(identity => {
            console.log("PUSHTOKEN " ,identity.pushToken);
            console.log("USERID " ,identity.userId);
            this.data.playerId = identity.userId; 
           this.authService.SendUserId(this.data).subscribe(
                response => {
                      console.log(" register device", response)            
                },
                error => {
                     console.log(" register device error", error)     
                },
                () => { }
            );
            
            
        });
  }else{
    //is platform web
    //WebOneSignal
    WebOneSignal.push(() => {
//        WebOneSignal.init({
//            appId : "b995681e-7e26-4c15-bad2-7e216e850673",
//            autoRegister : true,
//            notifyButton : {
//                enable : false,
//            },
//        });
         
        WebOneSignal.getUserId().then(userId => {
            this.data.playerId = userId; 
            console.log("WEB OneSignal User ID:", userId);
            // (Output) OneSignal User ID: 270a35cd-4dda-4b3f-b04e-41d7463a2316
           this.authService.SendUserId(this.data).subscribe(
                response => {
                      console.log(" register device", response)            
                },
                error => {
                     console.log(" register device error", error)     
                },
                () => { }
            );
        });
      
        WebOneSignal.on('notificationDisplay', (noti) => {
            // This callback fires every time the event occurs
            console.log("WEB notification opened" ,noti)
            this.event.publish('updateNotificationCount');
        });
        
    });
               
    
  }
}
}