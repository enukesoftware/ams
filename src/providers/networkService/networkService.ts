import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';

@Injectable()
export class NetworkService {
    private networkFlag = false;
    private networkType: any;
    private isWeb: boolean = false;
    
    constructor( private network: Network, private platform: Platform ) { 
        if ( this.platform.is('android') || this.platform.is('ios') ){
            this.isWeb = false;
        } else {
            this.isWeb = true;
        }
        //console.log("NetworkService constructor ============================>",this.isWeb);
    }
    
    isNetworkAvailable = () => {
        if ( !this.isWeb ) {
            return true;//this.networkFlag; 
        } else {
            return navigator.onLine;  //uncomment for mobile build
//            return true;    //comment for mobile build
        }
    }
    
    getNetworkType = () => {
        return this.networkType;
    }
    
    monitorNetworkStatus = () => {
        this.networkType = this.network.type;
        if( this.network.type != 'none' ){
            this.networkFlag = true;         
        }
     
        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
           // console.log('network was disconnected :-(');
            this.networkFlag = false;
        });
     
        let connectSubscription = this.network.onConnect().subscribe(() => {
          //  console.log('network connected!');
            this.networkFlag = true;
            // We just got a connection but we need to wait briefly
            // before we determine the connection type. Might need to wait.
            // prior to doing any api requests as well.
            setTimeout(() => {
                this.networkType = this.network.type;
                if( this.network.type === 'wifi' ){
                    console.log('we got a wifi connection, woohoo!');
                }
            }, 3000);
        });  
    }
}