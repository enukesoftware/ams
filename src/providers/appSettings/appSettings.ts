/*--------------------Ionic related components---------------*/
import {Injectable} from '@angular/core';

/**
 * AppSettings class : contains common url
 */
@Injectable()
export class AppSettings {
    public static get BASE_URL(): string {
//      return 'http://192.168.1.33:9002/api/v1/'; // Rahul
//        return 'http://192.168.1.33:9003/api/v1/'; // Rahul elastic search
    //   return 'http://192.168.1.130:9003/api/v1/'; // Rahul temp
            //  return 'http://18.217.135.90:8080/api/v1/'; // Biz4dev
    // return 'http://18.217.154.224:8080/api/v1/' //UAT server
    return 'http://18.217.135.90:8080/api/v1/'
 
    }// client server 
    
    // AWS configuration
    public static get REGION_NAME(): string {return '';}
    public static get IDENTITY_POOL_ID(): string {return '';}
    public static get BUCKET_NAME(): string {return '';}
  
    //--AWS
    public static get SIGN_UP_URL(): string {return 'users/signup';}
    public static get LOGIN_URL(): string {return 'users/login';}
    
    public static get COUNTRY_CODES_URL(): string {return '/authless/countryCodes';}
    public static get FORGOT_PASWORD_URL(): string {return 'api/v1/user/forgotPasswordEmail';}
    public static get REQUEST_OTP_URL(): string {return 'otp/requestOtp';}
    public static get VERIFY_OTP_URL(): string {return 'otp/verify';}
    public static get FORGOT_PASSWORD_URL(): string {return 'user/forgotPassword';}
    public static get CHANGE_PASSWORD_URL(): string {return 'user/changePassword';}
    public static get RESEND_OTP_URL(): string {return 'otp/resendOtp';}
    public static get UPDATE_PROFILE(): string {return 'users/profile';}
    public static get LOGOUT(): string {return 'users/logout';}

    //Social sharing URL's
    public static get FACEBOOK_URL(): string {return 'https://www.facebook.com/biz4appdevelopment/?ref=br_rs';}
    public static get TWITTER_URL(): string {return 'https://www.twitter.com';}
    public static get SMS_URL(): string {return 'https://www.twitter.com';}
    public static get WHATSAPP_URL(): string {return 'https://www.twitter.com';}

    public static get SOCIAL_APP_LOGIN(): string {return 'users/socialAppLogin';}
    public static get NOTIFICATION_URL(): string {return 'users/notification/fcm/demoMessage';}
  
    public static get CREATE_SUBSCRIPTION(): string {return 'subscriptions/createSubscription';}
    public static get GET_SUBSCRIPTION(): string {return 'subscriptions/getSubscription';}
    public static get VERIFY_SUBSCRIPTION(): string {return 'subscriptions/verifySubscription';}

    //Stripe Integration services
    public static get ADD_CREDIT_CARD(): string { return 'stripe/payment/addCreditCard'; }
    public static get GET_ALL_CREDIT_CARDS(): string { return 'stripe/payment/getAllCreditCards'; }
    public static get REMOVE_CREDIT_CARD(): string { return 'stripe/payment/removeCreditCard'; }
    public static get SET_DEFAULT_CREDIT_CARD(): string { return 'stripe/payment/setDefaultCreditCard'; }
    
    //City service
    public static get GET_CITIES(): string { return 'master/city'; }
    
    //Education service
    public static get GET_EDUCATION(): string { return 'master/data/education' }
    
    public static get GET_SPECIALIZATION_SERVICES(): string { return 'master/data/specializationServices' }
        
    //Create Doctor Service
    public static get CREATE_DOCTOR(): string { return 'doctor/create' }
    
    //Image upload
    public static get IMAGE_UPLOAD(): string { return 'file/uploadFiles' }
    public static get IMAGE_DOWNLOAD(): string { return 'file/downloadFiles' }
    public static get EDIT_IMAGE(): string { return 'file/editFiles' }
    public static get DELETE_IMAGE(): string { return 'file/deleteFiles' }
    
    //Hospital services
    public static get CREATE_HOSPITAL(): string { return 'hospital/create'; }
    public static get GET_ASSOCIATED_DOCTORS_LIST(): string { return 'doctor/listDoctorsAsPerCityAndHospital'; }
    public static get GET_UNASSOCIATED_DOCTORS_LIST(): string { return 'doctor/listUnAssociatedDoctorsAsPerCity'; }
    public static get GET_SYS_ADMIN_DOCTORS_LIST(): string { return 'doctor/listDoctorsAsPerCity'; }
    public static get VIEW_HOSPITAL(): string { return 'hospital/details?hospitalId='; }
    public static get UPDATE_HOSPITAL(): string { return 'hospital/edit'; }
    public static get HOSPITAL_ADMINS_LIST(): string { return 'hospital/listHospitalAdmins' }
    public static get HOSPITAL_ADMINS_DETAILS(): string { return 'hospital/detailsHospitalAdmins' }
    public static get HOSPITAL_ADMINS_SAVE_DETAILS(): string { return 'hospital/createHospitalAdmins' }
    public static get HOSPITAL_ADMIN_EDIT_DETAILS(): string { return 'hospital/editHospitalAdmins' }
    public static get HOSPITAL_DETAILS(): string { return 'hospital/details' }
    public static get GET_DOCTOR_DETAILS(): string { return 'doctor/details' }
    public static get UPDATE_DOCTOR_PROFILE(): string { return 'doctor/edit' }
    public static get GET_HOSPITAL_ADMIN_ACTIVATE_DEACTIVATE(): string {return 'hospital/admin/activateDeactivate'}
    public static get GET_HOSPITAL_APPOINTMENT_VISIT_DETAILS(): string { return 'appointment/doctorAndHospital/upcomingAppointments/details' }
     public static get GET_HOSPITAL_UPCOMING_APPOINTMENT(): string {return 'appointment/hospital/upcomingAppointments'}
     public static get GET_CONTRACT_DETAILS(): string { return 'doctorHospital/contractDetails' }
     public static get CREATE_CONTRACT(): string { return 'doctorHospital/contract' }
     public static get DOCTOR_FREE_TIME_SLOT(): string { return 'doctorHospital/doctorsFreeTimeSlots' }
     
     
    //System-admin services
    public static get GET_SYSTEMADMINS_LIST(): string { return 'systemAdmin/listSystemAdmin'; }
    public static get GET_SYSTEMADMIN_BY_ID(): string { return 'systemAdmin/details'; }
    public static get CREATE_SYSTEMADMIN(): string { return 'systemAdmin/create' }
    public static get EDIT_SYSADMIN(): string { return 'systemAdmin/edit' }   
    public static get GET_PATIENT_LIST(): string { return 'patient/listPatientsAsPerCity' }  
    public static get GET_SYSADMIN_ACTIVATE_DEACTIVATE(): string {return 'systemAdmin/activateDeactivate'}
    public static get GET_PATIENT_AS_PER_SEARCH(): string { return 'patient/listPatientsAsPerCity' }  

    
    //Patient Role
    public static get GET_DOCTORS_LIST(): string { return 'doctor/listDoctorsAsPerCity'; }
    public static get GET_HOSPITALS_LIST(): string { return 'hospital/listHospitalsAsPerCity'; }
    public static get CREATE_PATIENT(): string { return 'patient/createPatient'; }
    public static get GET_PATIENT_ACTIVATE_DEACTIVATE():string{return 'patient/activateDeactivate';}
    public static get GET_PATIENT_DETAILS():string{return 'patient/details';}
    public static get GET_PATIENT_UPCOMING_APPOINTMENTS():string{return 'appointment/patient/upcomingAppointments';}
    public static get GET_PATIENT_APPOINTMENT_HISTORY():string{return 'appointment/patient/historyAppointments';}
    public static get GET_ASSOCIATED_HOSPITALS_LIST():string{return 'hospital/listHospitalsAsPerCityAndDoctor';}
    public static get UPDATE_PATIENT_PROFILE(): string { return 'patient/edit';}
    
    //feedback
    public static get CREATE_FEEDBACK(): string { return 'feedback/create';}
    public static get GET_PATIENT_FEEDBACK_HISTORY(): string { return 'feedback/patient/feedbackHistory';}
    public static get GET_DOCTOR_FEEDBACKS(): string { return 'feedback/doctor/feedbackHistory';}
    public static get GET_FEEDBACKS(): string { return 'feedback/details';}
    public static get  UPDATE_FEEDBACK(): string { return 'feedback/update';}
    
    //system admin hospital activate deactivate
    public static get GET_HOSPITAL_ACTIVATE_DEACTIVATE():string{return 'hospital/activateDeactivate';}
    
    //system admin doctor activate deactivate
    public static get GET_DOCTOR_ACTIVATE_DEACTIVATE():string{return 'doctor/activateDeactivate';}

    //Get day Service
    public static get GET_DAY():string{return 'master/data/day';}
    
    //Booking flow service
    public static get GET_HOSPITAL_LIST_WITH_TIMESLOTS():string{return 'appointment/hospoitalListWithTimeSlots';}
    public static get CREATE_APPOINTMENT():string{return 'appointment/createAppointment';}
    public static get GET_APPOINTMENT_DETAILS():string{return 'appointment/detailsAppointment';}
    public static get EDIT_APPOINTMENT():string{return 'appointment/editAppointment';}
    public static get CANCEL_APPOINTMENT():string{return 'appointment/cancelAppointment';}
    
    //Doctor Role
    public static get GET_DOCTOR_UPCOMING_APPOINTMENTS():string { return 'appointment/doctor/upcomingAppointments';}
    public static get GET_DOCTOR_APPOINTMENT_HISTORY():string{return 'appointment/doctor/historyAppointments';}
    public static get GET_HOSPITAL_DOCTOR_DETAIL():string{return 'appointment/doctorAndHospital/Appointments/details';}
    public static get GET_HISTORY_DETAIL():string{return 'appointment/doctorAndHospital/historyAppointments/details';}
    public static get GET_DOCTOR_ASSOCIATED_HOSPITALS():string{return 'hospital/listHospitalsAsPerCityAndDoctor';}

    //Reports
    public static get GET_REPORT_FOR_DOCTOR():string{return 'doctor/report/get';}
    public static get GET_REPORT_FOR_HOSPITALADM():string{return 'hospital/report/get';}
    public static get GET_REPORT_FOR_SYSADM():string{return 'systemAdmin/report/get';}
    
   //send one signal userId
    public static get SEND_USER_ID():string{return 'users/saveFcmToken';}
    
    //get search results
    public static get GET_SEARCH_RESULTS(): string { return 'search'; }

    //Apply Leave
    public static get APPLY_LEAVE(): string { return 'leaves'; }

    //View Leaves
    public static get VIEW_LEAVES(): string { return 'leaves?id='; }

    //Add Patient
    public static get ADD_PATIENT(): string { return 'profiles/profile'; }

    //Update Patient
    public static get UPDATE_PATIENT(): string { return 'profiles/profile'; }

    //Delete Patient
    public static get DELETE_PATIENT(): string { return 'profiles/profile'; }

    //Search Patient
    public static get SEARCH_PATIENT(): string { return 'profiles/profiles-user'; }

    //Hospital Staff
    public static get HOSPITAL_STAFF_LIST(): string { return 'hospitalStaff/listHospitalStaff' }
    public static get CREATE_STAFF():string{return 'hospitalStaff/create'}
    public static get EDIT_STAFF():string{return 'hospitalStaff/edit'}
    public static get ACTIVATE_DEACTIVATE_STAFF():string {return 'hospitalStaff/activateDeactivate'}
    public static get HOSPITAL_STAFF_DETAILS(): string { return 'hospitalStaff/details' }

}