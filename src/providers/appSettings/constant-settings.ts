/*--------------------Ionic related components---------------*/
import { Injectable } from '@angular/core';

/**
 * Constants class : contains constants and strings
 */
@Injectable()
export class Constants {
    FEEDBACK_ERROR: string;
    error_msg_facebook_login_fail: string;
    ok_label: string;
    cancel_label: string;
    whatsapp_error_msge: string;
    facebook_error_msge: string;
    facebook_share_error_msge: string;
    twitter_error_msge: string;
    twitter_share_error_msge: string;
    linkedin_error_msge: string;
    cordova_error_msge: string;
    sms_error_msge: string;
    logout_message: string;
    facebook_close_dialog_msge: string;

    //reset password screen constants
    Error_MSG_BLANK_PIN: string;

    //signup screen constants
    EDIT_PROFILE_TEXT: string;
    Error_MSG_FIRST_NAME: string;
    Error_MSG_MIN_MAX_LENGTH_FIRST_NAME: string;
    Error_MSG_BLANK_EMAIL: string;
    Error_MSG_PASSWORD_MATCH: string;
    Error_MSG_BLANK_PASSWORD: string;
    Error_MSG_MINMAX_PASSWORD: string;
    Error_MSG_INVALID_EMAIL: string;
    EMAIL_REGEX: any;
    ERROR_MSG_LAST_NAME: string;
    ERROR_MSG_INVALID_PHONE: string;

    //login with mobile screen constants
    Error_msg_blank_mobileNo_field: string;
    Error_msg_invalid_mobileNo_field: string;

    ERROR_MSG_UNABLE_TO_CONNECT: string;
    ERROR_NETWORK_UNAVAILABLE: string;
    ERROR_MSG_UNAUTHORIZED: string;

    COMING_SOON_TEXT: string;
    PLEASE_WAIT_TEXT: string;


    //contract validations
    ERROR_CONTRACT_INVALID_VISIT_TYPE: string
    ERROR_CONTRACT_ENDTIME_LESS_THEN_TIME_SLOT: string
    ERROR_CONTRACT_STARTTIME_GREATER_THEN_TIME_SLOT: string
    ERROR_CONTRACT_BLANK_END_TIME: string
    ERROR_CONTRACT_BLANK_START_TIME: string
    ERROR_CONTRACT_BLANK_DAY: string
    ERROR_CONTRACT_START_LESS_THEN_CONTRACT_END_DATE: string
    ERROR_ALREADY_BOOKED_TIME_SLOT: string
    ERROR_STRAT_TIME_LESS_THEN_END_TIME: string
    ERROR_END_TIME_GREATEER_THEN_START_TIME: string
    ERROR_VALID_VALUE_FOR_APP_TOKEN: string
    ERROR_SELECT_DAY: string
    ERROR_SELECT_END_TIME_BETWEEN_SLOTS: string
    ERROR_SELECT_START_TIME_BETWEEN_SLOTS: string
    ERROR_SELECT_DOCTOR_SLOT_BETWEEN_HOSPITAL_TIME_SLOT: string
    ERROR_HOSPITAL_AVAILABLE_TIME_SLOTS_NOT_FOUND: string

    //login with Username / email / mobileNo required common Error message
    Error_MSG_BLANK_LOGIN_IDENTIFIER: string;


    //forgot paassword screen
    Error_MSG_BLANK_FORGOT_PASSWORD: string;
    Error_MSG_INVALID_EMAIL_MOBILENO: string;

    //change password in settings screen
    Error_MSG_BLANK_OLD_PASSWORD: string;
    Error_MSG_BLANK_NEW_PASSWORD: string;

    //signup types
    APP_SIGNUP_TYPE_TEXT: string;
    FB_SIGNUP_TYPE_TEXT: string;
    TWITTER_SIGNUP_TYPE_TEXT: string;
    GOOGLE_SIGNUP_TYPE_TEXT: string;
    LINKEDIN_SIGNUP_TYPE_TEXT: string;
    INSTAGRAM_SIGNUP_TYPE_TEXT: string;
    //Manage Card screen messages
    DELETE_CARD_WARNING_MSG: string;

    UNABLE_TO_PERFORM_REQUEST_TEXT: string;
    trial_subscription_long_time_msg: string;
    trial_subscription_accept_msg: string;

    FILE_UPLOAD_TYPE_ERROR_MSG: string;
    FILE_UPLOAD_SIZE_ERROR_MSG: string;
    FILE_UPLOAD_UNABLE_ERROR_MSG: string;

    CONTENT_MAX_WIDTH: any;
    MIN_MENU_WIDTH: any;

    PAGE_SIZE: number;

    /************************** Roles *****************************/
    ROLE_SYSTEM_ADMIN: string = "SYSTEM_ADMIN";
    ROLE_HOSPITAL_ADMIN: string = "HOSPITAL_ADMIN";
    ROLE_DOCTOR: string = "DOCTOR";
    ROLE_PATIENT: string = "PATIENT";
    ROLE_HOSPITAL_STAFF: string = "HOSPITAL_STAFF";

    /************************** Get time zone *****************************/
    GET_TIMEZ_ZONE: string = Intl.DateTimeFormat().resolvedOptions().timeZone;

    /************************** RELATION Data *****************************/
    RELATION_SELF_LABEL: string;
    RELATION_SPOUSE_LABEL: string;
    RELATION_CHILD_LABEL: string;
    RELATION_PARENT_LABEL: string;
    RELATION_OTHER_LABEL: string;

    RELATION_SELF_VALUE: string;
    RELATION_SPOUSE_VALUE: string;
    RELATION_CHILD_VALUE: string;
    RELATION_PARENT_VALUE: string;
    RELATION_OTHER_VALUE: string;

    RELATION_LIST: any = [];

    /************************** Gender *****************************/
    GENDER_MALE_LABEL: string;
    GENDER_FEMALE_LABEL: string;
    GENDER_MALE_VALUE: string;
    GENDER_FEMALE_VALUE: string;

    GENDER_LIST: any = [];

    /************************** Employment *****************************/
    EMPLOYMENT_EMPLOYED_LABEL: string;
    EMPLOYMENT_RETIRED_LABEL: string
    EMPLOYMENT_UNEMPLOYED_LABEL: string;
    EMPLOYMENT_STUDENT_LABEL: string;
    EMPLOYMENT_UNKNOWN_LABEL: string
    

    EMPLOYMENT_EMPLOYED_VALUE: string;
    EMPLOYMENT_RETIRED_VALUE: string
    EMPLOYMENT_UNEMPLOYED_VALUE: string;
    EMPLOYMENT_STUDENT_VALUE: string;
    EMPLOYMENT_UNKNOWN_VALUE: string;
    

    EMPLOYMENT_LIST: any = [];

    /************************** Martial Status *****************************/
    MARTIAL_STATUS_SINGLE_LABEL: string;
    MARTIAL_STATUS_MARRIED_LABEL: string;
    MARTIAL_STATUS_DIVORCED_LABEL: string;
    MARTIAL_STATUS_WIDOWED_LABEL: string;
    MARTIAL_STATUS_UNKNOWN_LABEL: string;
    MARTIAL_STATUS_SEPERATED_LABEL: string;
    MARTIAL_STATUS_DOMESTIC_PARTNER_LABEL: string;
    MARTIAL_STATUS_DECLINED_LABEL: string;

    MARTIAL_STATUS_SINGLE_VALUE: string;
    MARTIAL_STATUS_MARRIED_VALUE: string;
    MARTIAL_STATUS_DIVORCED_VALUE: string;
    MARTIAL_STATUS_WIDOWED_VALUE: string;
    MARTIAL_STATUS_UNKNOWN_VALUE: string;
    MARTIAL_STATUS_SEPERATED_VALUE: string;
    MARTIAL_STATUS_DOMESTIC_PARTNER_VALUE: string;
    MARTIAL_STATUS_DECLINED_VALUE: string;

    MARTIAL_STATUS_LIST:any = [];


    constructor() {
        this.error_msg_facebook_login_fail = 'Facebook login fail. Please try again.';
        this.ok_label = "OK";
        this.cancel_label = "Cancel";
        this.whatsapp_error_msge = 'Please install WhatsApp.';
        this.facebook_error_msge = 'Please install facebook app.';
        this.twitter_error_msge = 'Please install twitter app.';
        this.linkedin_error_msge = 'Please install Linkedin app.';
        this.cordova_error_msge = 'Cordova not available.';
        this.sms_error_msge = 'Unable to share with sms.';
        this.logout_message = 'Are you sure, you want to logout?';

        //forgot password constants
        this.Error_MSG_BLANK_FORGOT_PASSWORD = 'Please enter Email Id or Phone number';
        this.Error_MSG_INVALID_EMAIL_MOBILENO = 'Please enter valid Email Id or Phone number';

        //reset password screen constants
        this.Error_MSG_BLANK_PIN = 'Please enter OTP that you have received.';
        this.facebook_close_dialog_msge = 'User cancelled login.';
        this.Error_MSG_PASSWORD_MATCH = 'Password and confirm password does not match.';

        //change password constants
        this.Error_MSG_BLANK_OLD_PASSWORD = 'Please enter old password.';
        this.Error_MSG_BLANK_NEW_PASSWORD = 'Please enter new password.';

        //signup screen constants
        this.EDIT_PROFILE_TEXT = "Edit Profile";
        this.Error_MSG_FIRST_NAME = 'Please enter first name.';
        this.ERROR_MSG_LAST_NAME = 'Please enter last name.';
        this.Error_MSG_MIN_MAX_LENGTH_FIRST_NAME = 'Please enter first name between 2 to 30 characters.';
        this.Error_MSG_BLANK_EMAIL = 'Please enter email id.';
        this.Error_MSG_BLANK_LOGIN_IDENTIFIER = 'Please enter Email, Username or Mobile Number.';
        this.Error_MSG_BLANK_PASSWORD = 'Please enter password.';
        this.Error_MSG_MINMAX_PASSWORD = 'Please enter password between 8 to 20 characters.';
        this.Error_MSG_INVALID_EMAIL = 'Please enter valid email id.';
        this.EMAIL_REGEX = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
        this.ERROR_MSG_INVALID_PHONE = 'Please enter valid mobile number.';
        //login with mobile screen constants
        this.Error_msg_blank_mobileNo_field = 'Mobile No. is required.';
        this.Error_msg_invalid_mobileNo_field = 'Please enter valid Mobile No.';

        this.ERROR_MSG_UNABLE_TO_CONNECT = 'Unable to connect with server. Please try again.';
        this.ERROR_NETWORK_UNAVAILABLE = 'Please check your internet connection and try again.';
        this.ERROR_MSG_UNAUTHORIZED = 'Unauthorized.';

        this.COMING_SOON_TEXT = 'Coming soon.';
        this.PLEASE_WAIT_TEXT = 'Please wait.';

        //signup types
        this.APP_SIGNUP_TYPE_TEXT = 'APP';
        this.FB_SIGNUP_TYPE_TEXT = 'FB';
        this.TWITTER_SIGNUP_TYPE_TEXT = 'TW';
        this.GOOGLE_SIGNUP_TYPE_TEXT = 'GP';//'GP';  //need to change it once back-end finished it
        this.LINKEDIN_SIGNUP_TYPE_TEXT = 'LI';
        this.INSTAGRAM_SIGNUP_TYPE_TEXT = 'IG';

        // Manage Card screen messages
        this.DELETE_CARD_WARNING_MSG = "Are you sure you want to delete this card?";

        this.UNABLE_TO_PERFORM_REQUEST_TEXT = 'User cancelled login or Twitter app not installed.';
        this.trial_subscription_long_time_msg = 'In order to make this challenge you must be a premium member. The first 30 days are a free trial and you are free to cancel. After that it is $2.99 a month. Subscribe now?';
        this.trial_subscription_accept_msg = 'In order to accept this challenge you must be a premium member. The first 30 days are a free trial and you are free to cancel. After that it is $2.99 a month. Subscribe now?';

        // AWS cognito uload file
        this.FILE_UPLOAD_TYPE_ERROR_MSG = 'Please select file type .pdf, .doc, .docx, .jpg, .jpeg or .png with maximum size 5mb.';
        this.FILE_UPLOAD_SIZE_ERROR_MSG = 'File size should be less than 5 MB.';
        this.FILE_UPLOAD_UNABLE_ERROR_MSG = 'Unable to upload file.';

        // Contract validations
        this.ERROR_CONTRACT_ENDTIME_LESS_THEN_TIME_SLOT = "Please enter end time less then selected time slot.";
        this.ERROR_CONTRACT_STARTTIME_GREATER_THEN_TIME_SLOT = "Please enter start time greater then selected time slot.";
        this.ERROR_CONTRACT_BLANK_END_TIME = "Please select the end time.";
        this.ERROR_CONTRACT_BLANK_START_TIME = "Please select the start time.";
        this.ERROR_CONTRACT_BLANK_DAY = "Please select a day.";
        this.ERROR_CONTRACT_START_LESS_THEN_CONTRACT_END_DATE = "Contract start date can not be greater then contract end date.";
        this.ERROR_ALREADY_BOOKED_TIME_SLOT = "This time slot is already booked, please chose another one.";
        this.ERROR_STRAT_TIME_LESS_THEN_END_TIME = "Start time should be less then end time.";
        this.ERROR_END_TIME_GREATEER_THEN_START_TIME = "End time should be greater then start time.";
        this.ERROR_VALID_VALUE_FOR_APP_TOKEN = "Please enter valid value for Appointment/Tokens.";
        this.ERROR_SELECT_DAY = "Please select day.";
        this.ERROR_SELECT_END_TIME_BETWEEN_SLOTS = "Please select end time in between slots.";
        this.ERROR_SELECT_START_TIME_BETWEEN_SLOTS = "Please select start time in between slots.";
        this.ERROR_SELECT_DOCTOR_SLOT_BETWEEN_HOSPITAL_TIME_SLOT = "Please select doctor slot between hospital start and end time: ";
        this.ERROR_HOSPITAL_AVAILABLE_TIME_SLOTS_NOT_FOUND = "Hospital available time slots not found.";


        this.CONTENT_MAX_WIDTH = 1200;
        this.MIN_MENU_WIDTH = 300;

        this.PAGE_SIZE = 30;

        //feedback message
        this.FEEDBACK_ERROR = "Please rate the doctor and hospital.";

        this.RELATION_SELF_LABEL = "Self";
        this.RELATION_SPOUSE_LABEL = "Spouse";
        this.RELATION_CHILD_LABEL = "Child";
        this.RELATION_PARENT_LABEL = "Parent";
        this.RELATION_OTHER_LABEL = "Other";

        this.RELATION_SELF_VALUE = "SELF";
        this.RELATION_SPOUSE_VALUE = "SPOUSE";
        this.RELATION_CHILD_VALUE = "CHILD";
        this.RELATION_PARENT_VALUE = "PARENT";
        this.RELATION_OTHER_VALUE = "OTHER";
        this.RELATION_LIST = [{ "name": this.RELATION_SELF_LABEL, "value": this.RELATION_SELF_VALUE }, 
            { "name": this.RELATION_SPOUSE_LABEL, "value": this.RELATION_SPOUSE_VALUE },
            { "name": this.RELATION_CHILD_LABEL, "value": this.RELATION_CHILD_VALUE },
            { "name": this.RELATION_PARENT_LABEL, "value": this.RELATION_PARENT_VALUE },
            { "name": this.RELATION_OTHER_LABEL, "value": this.RELATION_OTHER_VALUE }
        ]

        this.GENDER_MALE_LABEL = "Male";
        this.GENDER_FEMALE_LABEL = "Female";
        this.GENDER_MALE_VALUE = "MALE";
        this.GENDER_FEMALE_VALUE = "FEMALE";

        this.GENDER_LIST = [{ "name": this.GENDER_MALE_LABEL, "value": this.GENDER_MALE_VALUE }, { "name": this.GENDER_FEMALE_LABEL, "value": this.GENDER_FEMALE_VALUE }];


        this.EMPLOYMENT_EMPLOYED_LABEL = "Employed";
        this.EMPLOYMENT_RETIRED_LABEL = "Retired";
        this.EMPLOYMENT_UNEMPLOYED_LABEL = "Unemployed";
        this.EMPLOYMENT_STUDENT_LABEL = "Student";
        this.EMPLOYMENT_UNKNOWN_LABEL = "Unknown";
       

        this.EMPLOYMENT_EMPLOYED_VALUE = "EMPLOYED";
        this.EMPLOYMENT_RETIRED_VALUE = "RETIRED";
        this.EMPLOYMENT_UNEMPLOYED_VALUE = "UNEMPLOYED";
        this.EMPLOYMENT_STUDENT_VALUE = "STUDENT";
        this.EMPLOYMENT_UNKNOWN_VALUE = "UNKNOWN";

        this.EMPLOYMENT_LIST = [{ "name": this.EMPLOYMENT_EMPLOYED_LABEL, "value": this.EMPLOYMENT_EMPLOYED_VALUE }, 
            { "name": this.EMPLOYMENT_RETIRED_LABEL, "value": this.EMPLOYMENT_RETIRED_VALUE },
            { "name": this.EMPLOYMENT_UNEMPLOYED_LABEL, "value": this.EMPLOYMENT_UNEMPLOYED_VALUE },
            { "name": this.EMPLOYMENT_STUDENT_LABEL, "value": this.EMPLOYMENT_STUDENT_VALUE },
            { "name": this.EMPLOYMENT_UNKNOWN_LABEL, "value": this.EMPLOYMENT_UNKNOWN_VALUE }
        ]
        

        this.MARTIAL_STATUS_SINGLE_LABEL = "Single";
        this.MARTIAL_STATUS_MARRIED_LABEL = "Married";
        this.MARTIAL_STATUS_DIVORCED_LABEL = "Divorced";
        this.MARTIAL_STATUS_WIDOWED_LABEL = "Widowed";
        this.MARTIAL_STATUS_UNKNOWN_LABEL = "Unknown";
        this.MARTIAL_STATUS_SEPERATED_LABEL = "Separated";
        this.MARTIAL_STATUS_DOMESTIC_PARTNER_LABEL = "Domentic Partner";
        this.MARTIAL_STATUS_DECLINED_LABEL = "Declined";

        this.MARTIAL_STATUS_SINGLE_VALUE = "SINGLE";
        this.MARTIAL_STATUS_MARRIED_VALUE = "MARRIED";
        this.MARTIAL_STATUS_DIVORCED_VALUE = "DIVORCED";
        this.MARTIAL_STATUS_WIDOWED_VALUE = "WIDOWED";
        this.MARTIAL_STATUS_UNKNOWN_VALUE = "UNKNOWN";
        this.MARTIAL_STATUS_SEPERATED_VALUE = "SEPERATED";
        this.MARTIAL_STATUS_DOMESTIC_PARTNER_VALUE = "DOMESTIC_PARTNER";
        this.MARTIAL_STATUS_DECLINED_VALUE = "DECLINED";

        this.MARTIAL_STATUS_LIST = [{ "name": this.MARTIAL_STATUS_SINGLE_LABEL, "value": this.MARTIAL_STATUS_SINGLE_VALUE }, 
            { "name": this.MARTIAL_STATUS_MARRIED_LABEL, "value": this.MARTIAL_STATUS_MARRIED_VALUE },
            { "name": this.MARTIAL_STATUS_DIVORCED_LABEL, "value": this.MARTIAL_STATUS_DIVORCED_VALUE },
            { "name": this.MARTIAL_STATUS_WIDOWED_LABEL, "value": this.MARTIAL_STATUS_WIDOWED_VALUE },
            { "name": this.MARTIAL_STATUS_UNKNOWN_LABEL, "value": this.MARTIAL_STATUS_UNKNOWN_VALUE },
            { "name": this.MARTIAL_STATUS_SEPERATED_LABEL, "value": this.MARTIAL_STATUS_SEPERATED_VALUE },
            { "name": this.MARTIAL_STATUS_DOMESTIC_PARTNER_LABEL, "value": this.MARTIAL_STATUS_DOMESTIC_PARTNER_VALUE },
            { "name": this.MARTIAL_STATUS_DECLINED_LABEL, "value": this.MARTIAL_STATUS_DECLINED_VALUE }        
        ]
    }
}
