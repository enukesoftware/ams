import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { AppSettings } from "../appSettings/appSettings";
import { RestService } from "../restService/restService";
import { Events } from "ionic-angular";
import { Constants } from "../appSettings/constant-settings";
import { CommonService } from "../common-service/common.service";

@Injectable()
export class ManageHospitalServiceProvider {

    constructor(public http: HttpClient, public event: Events, private constant: Constants, public restService: RestService, private commonService: CommonService) {
        // console.log('Hello ManageHospitalServiceProvider Provider');
    }

    /**
     * Function to Create New Hospital
     * @param data
     */
    createHospital = (data: any): Observable<any> => {
        var path = AppSettings.CREATE_HOSPITAL;
        return this.restService.postCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to Create New Doctor
     * @param data
     */
    createDoctor = (data: any): Observable<any> => {
        var path = AppSettings.CREATE_DOCTOR;
        return this.restService.postCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get Hospital List
     */
    getHospitalList = (cityId: any, pageNo: number): Observable<any> => {
        var path = AppSettings.GET_HOSPITALS_LIST + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&cityId=' + cityId;
        return this.restService.getCall(path, false, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get appointments List
     */
    getAppointmentList = (hospitalId: any, pageNo: number, startDate?: string, endDate?: string): Observable<any> => {
        var path = AppSettings.GET_HOSPITAL_UPCOMING_APPOINTMENT + '?fromDateMilliSec=' + startDate + '&toDateMilliSec=' + endDate + '&page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&hospitalId=' + hospitalId;
        return this.restService.getCall(path, false, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get Doctor Education
     */
    getDoctorEducation = (): Observable<any> => {
        var path = AppSettings.GET_EDUCATION;
        return this.restService.getCall(path, false, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }


    /**
     * Function to get Doctor Specialization
     */
    getDoctorSpecializationServices = (): Observable<any> => {
        var path = AppSettings.GET_SPECIALIZATION_SERVICES;
        return this.restService.getCall(path, false, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get associated doctors list to particular Hospital
     */
    getAssociatedDoctorsList = (data: any, pageNo: number): Observable<any> => {
        var path = AppSettings.GET_ASSOCIATED_DOCTORS_LIST + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&hospitalId=' + data;
        return this.restService.getCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get doctors list to associate them with particular hospital
     */
    geUnassociatedDoctorsList = (cityId: any, hospitalId: any, pageNo: number, searchText: any): Observable<any> => {
        var path = AppSettings.GET_UNASSOCIATED_DOCTORS_LIST + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&cityId=' + cityId + '&hospitalId=' + hospitalId + '&searchText=' + searchText;
        return this.restService.getCall(path, false, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to View Hospital Details
     */
    viewHospital = (data: any): Observable<any> => {
        //console.log("data", data);
        var path = AppSettings.VIEW_HOSPITAL + data;
        //   console.log("path", path);

        return this.restService.getCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }


    /**
     * Function to View Hospital Details
     */
    updateHospital = (data: any): Observable<any> => {
        var path = AppSettings.UPDATE_HOSPITAL;
        //   console.log("data", data);

        return this.restService.putCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }


    /**
     * Function to get system admin doctors list
     */
    getSysAdmDoctorsList = (pageNo: number, cityId: number): Observable<any> => {
        var path = AppSettings.GET_SYS_ADMIN_DOCTORS_LIST + "?page=" + pageNo + "&size=" + this.constant.PAGE_SIZE + "&cityId=" + cityId;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get Hospital Admins List
     * @param data
     */
    getHospitalAdminsList = (data: any, pageNo: number): Observable<any> => {
        var path;
        if (data == "" || data == undefined) {
            path = AppSettings.HOSPITAL_ADMINS_LIST + "?page=" + 0 + "&size=" + this.constant.PAGE_SIZE + "&cityId=";
        } else {
            path = AppSettings.HOSPITAL_ADMINS_LIST + "?page=" + pageNo + "&size=" + this.constant.PAGE_SIZE + "&hospitalId=" + data + "&cityId=";
        }

        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
    * Function to get Hospital Admins Details
    * @param data
    */
    getHospitalAdminDetails = (id: any): Observable<any> => {
        var path = AppSettings.HOSPITAL_ADMINS_DETAILS + "?userId=" + id;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get Hospital Details
     * @param data
     */

    getHospitalDetails = (id: any): Observable<any> => {
        var path = AppSettings.HOSPITAL_DETAILS + "?hospitalId=" + id;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get Doctor Details
     * @param data
     */

    getDoctorDetails = (doctorId?: any, doctorPorfileId?: any): Observable<any> => {
        var path = AppSettings.GET_DOCTOR_DETAILS + "?doctorId=" + doctorId + "&doctorProfileId=" + doctorPorfileId;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }



    /**
     * Function to save Hospital Admins Details
     * @param data
     */
    saveHospitalAdminDetails = (data: any, mode: any): Observable<any> => {
        var path;
        if (mode == "view") {  //for edit mode
            path = AppSettings.HOSPITAL_ADMIN_EDIT_DETAILS;
            delete data.address;
            return this.restService.putCall(path, data, false, null)
                .map(res => res.json())
                .catch(this.commonService.handleError);
        } else {
            delete data.address;
            delete data.blockStatus;
            path = AppSettings.HOSPITAL_ADMINS_SAVE_DETAILS;
            return this.restService.postCall(path, data, false, null)
                .map(res => res.json())
                .catch(this.commonService.handleError);
        }

    }

    /*
     *  function to get activate or deactivate hospital by sysadmin
     * */

    public activateDeactivateHospital = (data: any) => {
        // console.log("data",data);
        var path = AppSettings.GET_HOSPITAL_ACTIVATE_DEACTIVATE + "?hospitalId=" + data.HospitalId + "&action=" + data.action;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }


    /*
     *  function to get activate or deactivate hospital-admin by sysadmin
     * */

    public activateDeactivateHospitalAdmin = (data: any) => {
        // console.log("data",data);
        var path = AppSettings.GET_HOSPITAL_ADMIN_ACTIVATE_DEACTIVATE + "?userId=" + data.userId + "&action=" + data.action;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /*
     *  function to get hospital appointment visit details
     **/
    public getHospitalAppointmentVisitDetails = (hospitalId: any, doctorProfileId: any, date: any, pageNo: number, detailMode: any): Observable<any> => {
        var path;
        //console.log('detailMode-------->',detailMode);
        if (detailMode == 'historyDetails' || detailMode == 'doctorHistoryDetails') {
            path = AppSettings.GET_HISTORY_DETAIL + "?page=" + pageNo + "&size=" + this.constant.PAGE_SIZE + "&doctorProfileId=" + doctorProfileId + "&hospitalId=" + hospitalId + "&date=" + date;
        } else {
            path = AppSettings.GET_HOSPITAL_APPOINTMENT_VISIT_DETAILS + "?page=" + pageNo + "&size=" + this.constant.PAGE_SIZE + "&doctorProfileId=" + doctorProfileId + "&hospitalId=" + hospitalId + "&date=" + date;
        }

        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /*
     * function to get associated hospitals specific to a patient 
     * */
    public getAssociatedHospitalList = (id: any, pageNo: number) => {
        var path = AppSettings.GET_ASSOCIATED_HOSPITALS_LIST + "?page=" + pageNo + '&doctorId=' + (id ? id : "") + "&size=" + this.constant.PAGE_SIZE;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);

    }

    /*
     *  function to get doctor contract details
     *  
     **/
    public getDoctorContractDetails = (doctorProfileId: any, hospitalId: any, dayId?: any): Observable<any> => {
        var path = AppSettings.GET_CONTRACT_DETAILS + "?doctorProfileId=" + doctorProfileId + "&hospitalId=" + hospitalId + "&dayId=";
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }


    /*
     *  function to get hospital appointment visit details
     **/
    public getHospitalForDetails = (hospitalId: any, doctorProfileId: any, date: any): Observable<any> => {
        var path;
        path = AppSettings.GET_HOSPITAL_DOCTOR_DETAIL + "?hospitalId=" + hospitalId + "&doctorProfileId=" + doctorProfileId + "&appointmentDateMillisec=" + date;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /*
     *  function to get hospital appointment visit details
     **/
    public createContract = (data: any): Observable<any> => {
        var path = AppSettings.CREATE_CONTRACT;
        return this.restService.postCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /*
     *  function to get doctor free timeslots
     **/
    public getDoctorFreeSlots = (doctorProfileId, dayId): Observable<any> => {
        var path = AppSettings.DOCTOR_FREE_TIME_SLOT + "?doctorProfileId=" + doctorProfileId + "&dayId=" + dayId;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

   
    /**
     * Function to perform common error handling
     * @param error
     */
    public handleError = (error: Response | any) => {
        if (error && error.status == 401) {
            setTimeout(() => {
                this.event.publish('user:logout');
            }, 500);
            //          this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
        } else {
            if (error) {
                if (error.message == this.constant.ERROR_NETWORK_UNAVAILABLE) {
                    return Observable.throw(error);
                } else {
                    var err: any;
                    err = error.json();
                    if (error && err) {
                        if (error.code == 401) {
                            setTimeout(() => {
                                this.event.publish('user:logout');
                            }, 500);
                        } else {
                            return Observable.throw(err);
                        }
                    } else {
                        //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                        let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                        this.commonService.presentToast(errorMsg);
                    }
                }
            } else {
                //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                this.commonService.presentToast(errorMsg);
            }
        }
        return Observable.throw(err);
    }


}