import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";

import { Constants } from "../appSettings/constant-settings";
import { CommonService } from "../common-service/common.service";
import { Platform } from 'ionic-angular';
declare var google;
@Injectable()

export class MapServiceProvider {
    public isWeb: boolean = false;
    constructor( public http: HttpClient, private constant: Constants, private commonService: CommonService, public platform: Platform ) {
        //console.log( 'Hello MapService Provider' );
        if (this.platform.is('cordova')) {
            this.isWeb = false;
        } else {
            this.isWeb = true;
        }
    }
    
    
    /**
     * Function: Used to get current location details
     * 
     */
    public getCurrentLocation = (successCb, errorCb) => {
        var geoLocationOptions = { maximumAge: 60000, timeout: 10000, enableHighAccuracy: true };
        if (!this.isWeb) {
            let myThis = this;
            if (this.platform.is('android')) {
                navigator.geolocation.getCurrentPosition((r) => {
                    successCb(r);
                }, (e) => {
                    // console.log( 'erro', e );
                    myThis.getLocationFromBGLocationSender(successCb, errorCb);
                }, geoLocationOptions);
            } else {
                navigator.geolocation.getCurrentPosition((r) => {
                    successCb(r);
                }, (e) => {
                    console.log('erro', e);
                    myThis.getLocationFromBGLocationSender(successCb, errorCb);
                });
            }

        } else {
            navigator.geolocation.getCurrentPosition((r) => { successCb(r) }, (e) => { errorCb(e) }, geoLocationOptions);
        }
    }
    
    public getLocationFromBGLocationSender = (successCb, errorCb) => {
        try {
            if (!this.isWeb) {
                errorCb("BGLocationSender plugin is not added.");
            } else {
                errorCb("plugin is only for mobile");
            }
        } catch (e) {
           // console.log("aa --stopBGLocationSender---e=", e)
        }
    }
    

    /**
     * Function: used to return formatted_address according to latlog
     * @param: lat contains latitude.
     * @param: lag contains longitude.
     * @param: successCallBackMethod used to handle call back method.
     * **/
    public reverseGeocoding = (lat, lng, successCallBackMethod) => {
        var city;
        let geocoder = new google.maps.Geocoder();
       // console.log('lat, lng',lat, lng);
        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({ 'latLng': latlng }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results && results[1]) {
                  //find country name
                    for (var i=0; i< results[1].address_components.length; i++) {
                   for (let b=0;b< results[1].address_components[i].types.length;b++) {

                   //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                       if (results[1].address_components[i].types[b] == "administrative_area_level_2") {
                           //this is the object you are looking for
                           city= results[1].address_components[i];
                           break;
                       }
                   }
               }
               //city data
                    if(city && city.long_name!= undefined){
                        var responseData = { "address": city.long_name};
                        successCallBackMethod(true, responseData);
                    }
                    else{
                        successCallBackMethod(true, '');
                    }
                } else {
                    successCallBackMethod(false, "Could not fetch address details for your geo-location.");
                }
            } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                successCallBackMethod(false, "Could not fetch address details for your geo-location.");
            } else {
                successCallBackMethod(false, "Could not fetch address details for your geo-location.");
            }
        });
    }
    

    
    /*
     * Function to redirect to native map app on mobile device and on web open google map in new tab
     * */
    public setAddressOnMap = ( currentHospitalAddress: any ) => {
        if( this.commonService.checkIsWeb() ){
            // web app related code ....
            this.openNewTab(currentHospitalAddress);
        }else{
            //mobile app related code ....
        }
    }
    
    public openNewTab = ( currentHospitalAddress: any ) => {
        let googleMapLink = "https://www.google.com/maps/search/?api=1&query="+currentHospitalAddress;
       // console.log('openNewTab ============================>',window);
        window.open( googleMapLink,'_blank' );
    }
    
}