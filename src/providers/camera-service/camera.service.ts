import { Injectable } from '@angular/core';
import { Camera,CameraOptions } from '@ionic-native/camera';
import {ActionSheetController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
//import { Crop } from '@ionic-native/crop';
//import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet';
//import { Constants } from '../appSettings/constant-settings';
//import { AWS } from 'aws-sdk';

@Injectable()
export class CameraService {  
    constructor( private actionSheetCtrl: ActionSheetController,private camera: Camera,public platform:Platform) {
     
    }

    //function to load image actionSheet
    public loadImage = ( successCallback: any, errorCallback: any) => {
        let myThis= this;
        let actionSheet = this.actionSheetCtrl.create( {
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: () => {
                        console.log('handler call===Load from Library=>');
                         myThis.uploadImage(myThis.camera.PictureSourceType.PHOTOLIBRARY,(data)=>{
                             successCallback(data)
                         }, errorCallback);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: () => {
                        console.log('handler call===Use Camera=>');
                        myThis.uploadImage(myThis.camera.PictureSourceType.CAMERA, successCallback, errorCallback);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    }

    /**
     * Function to return image url
     * @param: sourceType
     * @param: successCallback
     * @param: errorCallback
     */
    public uploadImage = ( sourceType: any, successCallback: any, errorCallback: any, isBase64?: boolean, isSkipCrop?: boolean ) => {
        let options: CameraOptions;
        if ( this.platform.is( 'ios' ) ) {
            options = {
                quality: 50,
                sourceType: sourceType,
                mediaType: this.camera.MediaType.PICTURE,
                destinationType: this.camera.DestinationType.DATA_URL,
                allowEdit: isSkipCrop ? false : true,
                correctOrientation: true
            }
        } else {
            options = {
                quality: 50,
                sourceType: sourceType,
                mediaType: this.camera.MediaType.PICTURE,
                destinationType: isBase64 ? this.camera.DestinationType.DATA_URL : this.camera.DestinationType.FILE_URI,
                allowEdit: false,
                correctOrientation: true
            }
        }

        this.camera.getPicture( options ).then(( imageData ) => {
            // We don't need to crop image in case of iOS platform, it is a inbuild feature
            let myThis = this;
            if ( myThis.platform.is( 'ios' ) ) {
                let base64Image = 'data:image/jpeg;base64,' + imageData;
                successCallback( base64Image );
            } else { // android platform, we need to do manually
                    /*              if ( !isSkipCrop && !isBase64 ) {
                    let optobj: any = {
                        quality: 75
                    }
                    this.crop.crop( imageData, optobj )
                        .then(
                        newImageURL => {
                            myThis.convertImgToBase64( newImageURL, successCallback );
                        },
                        error => console.error( 'Error cropping image', error )
                        );
                } else {*/
                    if ( isBase64 ) {
                        let base64Image = 'data:image/jpeg;base64,' + imageData;
                        successCallback( base64Image );
                    } else {
                        console.log('imageData===>',imageData);
                        myThis.convertImgToBase64( imageData, successCallback );
                    }
                //}
            }

        }, ( err ) => {
            errorCallback( err );
            // Handle error
        });
    }

    /***
      * function to convert and compress image to Base64
      */
    public convertImgToBase64 = ( url, callback ) => {
        var image = new Image;
        image.onload = function() {

            // Resize the image
            var canvas = document.createElement( 'canvas' ),
                max_size = 544, // TODO : pull max size from a site config
                width = image.width,
                height = image.height;
            if ( width > height ) {
                if ( width > max_size ) {
                    height *= max_size / width;
                    width = max_size;
                }
            } else {
                if ( height > max_size ) {
                    width *= max_size / height;
                    height = max_size;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext( '2d' ).drawImage( image, 0, 0, width, height );
            var dataUrl = canvas.toDataURL( 'image/jpeg' );
            callback( dataUrl );
        }
        image.src = url;
    }



}    