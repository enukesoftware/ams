import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { AppSettings } from "../appSettings/appSettings";
import { RestService } from "../restService/restService";
import { AuthService } from "../authService/authService";
import { Events } from "ionic-angular";
import { Constants } from "../appSettings/constant-settings";
import { CommonService } from "../common-service/common.service";

@Injectable()
export class ManageSysAdminServiceProvider {

  constructor(public http: HttpClient, public event: Events, private constant: Constants, public restService: RestService, private authServices: AuthService, private commonService: CommonService ) {
   // console.log('Hello ManageHospitalServiceProvider Provider');
  }
  
  /**
   * Function to get System-Admins List
   * @param data
   */
  getSysAdminsList = (page:any): Observable<any> => {
      var path = AppSettings.GET_SYSTEMADMINS_LIST  +"?page="+page + '&size=' + this.constant.PAGE_SIZE;
      return this.restService.getCall(path, null, false, null)
      .map(res=>res.json())
      .catch(this.commonService.handleError );
  }
  

 /**
   * Function to get System-Admins List
   * @param data
   */
  
  getSysAdminFromId = (id?: any): Observable<any> => {
      if(id == undefined){
          id = "";
      }
      var path = AppSettings.GET_SYSTEMADMIN_BY_ID  +"?userId="+id ;
      return this.restService.getCall(path, null, false, null)
      .map(res=>res.json())
      .catch(this.commonService.handleError);
  }
  
  /**
   * Function to get Hospital List
   */
  getPatientList = (pageNo: number): Observable<any> => {
      var path = AppSettings.GET_PATIENT_LIST + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE;
      return this.restService.getCall(path, false, false, null)
              .map(res=>res.json())
              .catch(this.commonService.handleError );
  }

  
  /**
   * Function to create new patient web service
   * @param data
   */
  createPatient = (data:any): Observable<any> => {
      //console.log("in createPatient",data);
      var path = AppSettings.CREATE_PATIENT;
      return this.restService.postCall( path, data, false, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError );
  }
  
  /**
   * Function to create or edit patient web service
   * @param data and mode
   */
  
    createEditSysAdmin = (data: any, mode: any): Observable<any> => {
      var path;
      if(mode == "create"){
             path = AppSettings.CREATE_SYSTEMADMIN;
             return this.restService.postCall(path, data, false, null)
             .map(res=>res.json())
             .catch(this.commonService.handleError );
          } else{
             path = AppSettings.EDIT_SYSADMIN;
             delete data.address;
             return this.restService.putCall(path, data, false, null)
             .map(res=>res.json())
             .catch(this.commonService.handleError );
          }
          
      }
      
  
    
   /*
    *  function to get activate or deactivate doctor
    * */
    
    public activateDeactivateDoctor =(data:any) =>{
      //  console.log("data",data);
        var path = AppSettings.GET_DOCTOR_ACTIVATE_DEACTIVATE + "?doctorId="+ data.doctorId + "&action="+ data.action;
        return this.restService.getCall( path, null, false, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError);
    }
    
    
    /*
     *  function to get activate or deactivate sysadmin
     * */
     
     public activateDeactivateSysAdmin =(data:any) =>{
        // console.log("data",data);
         var path = AppSettings.GET_SYSADMIN_ACTIVATE_DEACTIVATE + "?userId="+ data.userId + "&action="+ data.action;
         return this.restService.getCall( path, null, false, null )
         .map( res => res.json() )
         .catch( this.commonService.handleError );
     }

     
    
    
  /**
   * Function to perform common error handling
   * @param error
   */
/*  public handleError = ( error: Response | any ) => {
      if( error && error.status == 401 ){
          setTimeout(() => {
              this.event.publish( 'user:logout' );
          }, 500);
//          this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
      }else{
          if( error ){
              if( error.message == this.constant.ERROR_NETWORK_UNAVAILABLE ){
                  return Observable.throw( error );
              }else{
                  var err: any;
                  err = error.json();
                  if( error && err ){
                      if( error.code == 401 ){
                          setTimeout(() => {
                              this.event.publish( 'user:logout' );
                          }, 500);
                      }else{
                          return Observable.throw( err );
                      }
                  }else{
                      //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                      let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                      this.commonService.presentToast(errorMsg);
                  }
              }
          }else{
              //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
              let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
              this.commonService.presentToast(errorMsg);
          }
      }
      return Observable.throw( err );
  }*/
  
}