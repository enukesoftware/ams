import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { AppSettings } from "../appSettings/appSettings";
import { RestService } from "../restService/restService";
import { Events } from "ionic-angular";
import { Constants } from "../appSettings/constant-settings";
import { CommonService } from "../common-service/common.service";

@Injectable()
export class HospitalStaffServiceProvider {

    constructor(public http: HttpClient, public event: Events, private constant: Constants, public restService: RestService, private commonService: CommonService) {
        // console.log('Hello HospitalStaffServiceProvider Provider');
    }

    /**
     * Function to get Hospital Staff Details
     * @param data
     */
    getHospitalStaffDetails = (id: any): Observable<any> => {
        var path = AppSettings.HOSPITAL_STAFF_DETAILS + "?staff=" + id;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to get Hospital Staff List
     * @param data
     */
    getHospitalStaffList = (hospitalId: any, pageNo: number): Observable<any> => {
        var path;
        if (hospitalId == "" || hospitalId == undefined) {
            path = AppSettings.HOSPITAL_STAFF_LIST + "?page=" + pageNo + "&size=" + this.constant.PAGE_SIZE;
        } else {
            path = AppSettings.HOSPITAL_STAFF_LIST + "?page=" + pageNo + "&size=" + this.constant.PAGE_SIZE + "&hospitalId=" + hospitalId;
        }

        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
    * Function to create staff web service
    * @param data
    */
    createStaff = (data: any): Observable<any> => {
        //console.log("in createPatient",data);
        var path = AppSettings.CREATE_STAFF;
        return this.restService.postCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.handleError);
    }

    /**
     * Function to edit staff web service
     * @param data
     */
    editStaff = (data: any): Observable<any> => {
        //console.log("in createPatient",data);
        var path = AppSettings.EDIT_STAFF;
        return this.restService.putCall(path, data, false, null)
            .map(res => res.json())
            .catch(this.handleError);
    }

    /**
     * Function to activate/deactivate staff web service
     * @param data
     */
    public activateDeactivateStaff = (data: any) => {
        // console.log("data",data);
        var path = AppSettings.ACTIVATE_DEACTIVATE_STAFF + "?userId=" + data.userId + "&action=" + data.action;
        return this.restService.getCall(path, null, false, null)
            .map(res => res.json())
            .catch(this.commonService.handleError);
    }

    /**
     * Function to perform common error handling
     * @param error
     */
    public handleError = (error: Response | any) => {
        if (error && error.status == 401) {
            setTimeout(() => {
                this.event.publish('user:logout');
            }, 500);
            //          this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
        } else {
            if (error) {
                if (error.message == this.constant.ERROR_NETWORK_UNAVAILABLE) {
                    return Observable.throw(error);
                } else {
                    var err: any;
                    err = error.json();
                    if (error && err) {
                        if (error.code == 401) {
                            setTimeout(() => {
                                this.event.publish('user:logout');
                            }, 500);
                        } else {
                            return Observable.throw(err);
                        }
                    } else {
                        //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                        let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                        this.commonService.presentToast(errorMsg);
                    }
                }
            } else {
                //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                this.commonService.presentToast(errorMsg);
            }
        }
        return Observable.throw(err);
    }


}