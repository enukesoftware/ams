import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { RestService } from "../restService/restService";
import { Events } from "ionic-angular";
import { Constants } from "../appSettings/constant-settings";
import { CommonService } from "../common-service/common.service";
import { AppSettings } from "../appSettings/appSettings";

@Injectable()
export class DoctorRoleServiceProvider {

  constructor(public http: HttpClient, public event: Events, private constant: Constants, public restService: RestService, 
              private commonService: CommonService ) {
              
  }

  /**
   * Function to get Doctors profile details
   * @param cityId
   **/
  getDoctorProfile = (id?: any): Observable<any> => {
      var path = AppSettings.GET_DOCTOR_DETAILS +"?doctorId="+ (id ? id : "");
      return this.restService.getCall(path, null, false, null)
              .map(res=>res.json())
              .catch(this.commonService.handleError);
  }
  
  /**
   * function to get upcoming appointments
   */
  
  
  public getDoctorUpcomingAppointments =(doctorProfileId:any,pageNo: any, startDate?: any, endDate?: any) =>{
      console.log("startDate....", startDate);
      console.log("endDate....", endDate);
      var path = AppSettings.GET_DOCTOR_UPCOMING_APPOINTMENTS + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&doctorProfileId='+ doctorProfileId+'&fromDateMilliSec='+startDate+'&toDateMilliSec='+endDate;
      return this.restService.getCall(path, null, false, null)
              .map(res=>res.json())
              .catch(this.commonService.handleError );
  }
  
  
  public getDoctorAppointmentHistory =(doctorProfileId:any,pageNo: any, startDate?: any, endDate?: any)=>{
      var path = AppSettings.GET_DOCTOR_APPOINTMENT_HISTORY + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&doctorProfileId=' + doctorProfileId+"&fromDateMilliSec="+startDate+"&toDateMilliSec="+endDate;
      return this.restService.getCall(path, null, false, null)
      .map(res=>res.json())
      .catch(this.commonService.handleError );
  }
  
  
  public getAssociatedHospitalsList = (id: any,pageNo: any,) =>{
      var path = AppSettings.GET_DOCTOR_ASSOCIATED_HOSPITALS + '?page=' + pageNo + '&doctorId=' + (id ? id : "") + '&size=' + this.constant.PAGE_SIZE;
      return this.restService.getCall(path, null, false, null)
      .map(res=>res.json())
      .catch(this.commonService.handleError );
      
  }
  
  
  /**
   * Function to update Doctors profile
   **/
  updateDoctorProfile = ( data:any ): Observable<any> => {
      var path = AppSettings.UPDATE_DOCTOR_PROFILE;
      return this.restService.putCall(path, data, false, null)
              .map(res=>res.json())
              .catch(this.commonService.handleError );
  }

    /**
     * Function to Apply Leave
     */
    applyLeave = ( data:any ): Observable<any> => {
        var path = AppSettings.APPLY_LEAVE;
        return this.restService.postCall( path, data, false, null )
        .map( res => res.json() )
        .catch(this.commonService.handleError);
    }

  /**
   * function to get leaves
   */
  
  public viewLeaves =(doctorProfileId:any) =>{
    var path = AppSettings.VIEW_LEAVES + doctorProfileId ;
    return this.restService.getCall(path, null, false, null)
            .map(res=>res.json())
            .catch(this.commonService.handleError );
  }   

  /**
     * Function to Update Leave
     */
    updateLeave = ( data:any ): Observable<any> => {
        var path = AppSettings.APPLY_LEAVE;
        return this.restService.putCall( path, data, false, null )
        .map( res => res.json() )
        .catch(this.commonService.handleError);
    }

  /**
   * Function to perform common error handling
   * @param error
   **/
 /* public handleError = ( error: Response | any ) => {
      if( error && error.status == 401 ){
          setTimeout(() => {
              this.event.publish( 'user:logout' );
          }, 500);
          //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
      }else{
          if( error ){
              if( error.message == this.constant.ERROR_NETWORK_UNAVAILABLE ){
                  return Observable.throw( error );
              }else{
                  var err: any;
                  err = error.json();
                  if( error && err ){
                      if( error.code == 401 ){
                          setTimeout(() => {
                              this.event.publish( 'user:logout' );
                          }, 500);
                      }else{
                          return Observable.throw( err );
                      }
                  }else{
                      //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                      this.commonService.presentToast(this.constant.ERROR_MSG_UNABLE_TO_CONNECT);
                  }
              }
          }else{
              //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
              this.commonService.presentToast(this.constant.ERROR_MSG_UNABLE_TO_CONNECT);
          }
      }
      return Observable.throw( err );
  }*/
}
