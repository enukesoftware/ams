import { Injectable, NgZone } from '@angular/core';
import {Http, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import { CommonService } from '../common-service/common.service';
import { AppSettings } from '../appSettings/appSettings';
import { Constants } from '../appSettings/constant-settings';
import { LocalStorageService } from '../localStorage-service/localStorage.service';
import { NetworkService } from '../networkService/networkService';
import { DeviceInformationProvider } from '../device-information/device-information';
import { Storage } from '@ionic/storage';

@Injectable()
export class RestService {
    private isWeb: boolean = false;
    private networkError: any;
    role:any;
    finalRole:any;

    constructor(  private http: Http, public locstr: LocalStorageService, private network: NetworkService,
                 private constants: Constants, private deviceInfo: DeviceInformationProvider, private platform: Platform, private storage: Storage,
                 private _ngZone: NgZone){
        if ( this.platform.is('cordova') ) {
            this.isWeb = false;
        } else {
            this.isWeb = true;
        }
        this.networkError = {
            "message": this.constants.ERROR_NETWORK_UNAVAILABLE
        }
        //this.getActiveUserRole();
        
    }
    
    
    public getActiveUserRole =(callback)=> {
        
        console.log("timeZone..",this.constants.GET_TIMEZ_ZONE);
        //this._ngZone.run(() => {
            //setTimeout(() => {
            this.getFromStorage('userActiveRole').then((value)=>{
                console.log("value",value);
                this.role =value;
                console.log("value",this.role );
                callback(this.role)
                //return this.role;
            })
            //, 2000 })
        //})
        //return this.role;
    }
    
    /*
     * Function to get saved values from ionic storage
     * */
    public getFromStorage = ( key: any ) => {
        return this.storage.get(key);
    }
    
  
    getCall = ( path:any, data:any, skipAuth:any, timeout:any ): Observable<any> => {
        if( this.network.isNetworkAvailable() ){
         //   this.finalRole = this.getActiveUserRole();
            this.getActiveUserRole((res)=>{
                console.log("ROLE:",res);
                this.finalRole =res;
            });
            var url = AppSettings.BASE_URL + path;
            timeout = timeout || 40000;
            data = data || {};
            skipAuth = skipAuth || false;
            var headerObj:any  = {
                 'Content-Type': 'application/json',
                 'Accept-Language': 'en',
                 'deviceId' : this.deviceInfo.getDeviceID(),
                 'deviceType' : this.deviceInfo.getDevicePlatform(),
                 'appVersion' : this.deviceInfo.getAppVersion(),
                 'deviceName' : this.deviceInfo.getDeviceModel(),
                 'Device-Id-TimeZone': this.constants.GET_TIMEZ_ZONE,
                 'role' : this.role
            }
            if( skipAuth == false ){
                var accessToken = this.locstr.getObj('accessToken');
                if(accessToken == undefined || accessToken == '' ){
                     headerObj["Authorization"] = ""; 
                }
				else{
				    headerObj["Authorization"] = "Bearer " + accessToken;   
        		    }
				}
            let payload = JSON.stringify(data);
            let headers = new Headers(headerObj);
            let options = new RequestOptions({ headers: headers });
            return this.http.get(url, options).timeout(timeout);
        }else{
            return Observable.throw(this.networkError);
        }
    }
    
    getCallForNotification = ( path:any, data:any, skipAuth:any, timeout:any ): Observable<any> => {
        if( this.network.isNetworkAvailable() ){
          //  this.finalRole = this.getActiveUserRole();
            this.getActiveUserRole((res)=>{
                console.log("res",res);
                this.finalRole =res;
            });
            var url = AppSettings.BASE_URL + path;
            timeout = timeout || 40000;
            data = data || {};
            skipAuth = skipAuth || false;
            var headerObj:any  = {
                 'Accept-Language': 'en',
                 'deviceId' : this.locstr.getObj('pushToken'),
                 'deviceType' : this.locstr.getObj('deviceType'),
                 'appVersion' : this.locstr.getObj('appVersion'),
                 'Device-Id-TimeZone': this.constants.GET_TIMEZ_ZONE,
                 'role' : this.role
            }
            if( skipAuth == false ){
                var accessToken = this.locstr.getObj('accessToken');
                if(accessToken == undefined || accessToken == '' ){
                    headerObj["Authorization"] = ""; 
               }
               else{
                   headerObj["Authorization"] = "Bearer " + accessToken;   
                   }
            }
            let payload = JSON.stringify(data);
            let headers = new Headers(headerObj);
            let options = new RequestOptions({ headers: headers });
            return this.http.get(url, options).timeout(timeout);
        }else{
            return Observable.throw(this.networkError);
        }
    }
    
    postCall = ( path:any, data:any, skipAuth:any, timeout:any ): Observable<any> => {
        if( this.network.isNetworkAvailable() ){
           // this.finalRole = this.getActiveUserRole();
            this.getActiveUserRole((res)=>{
                console.log("res",res);
                this.finalRole =res;
            });
            var url = AppSettings.BASE_URL + path;
            timeout = timeout || 40000;
            data = data || {};
            skipAuth = skipAuth || false;
            var headerObj:any  = {
                 'Content-Type': 'application/json',
                 'Accept-Language': 'en',
                 'deviceId' : this.deviceInfo.getDeviceID(),
                 'deviceType' : this.deviceInfo.getDevicePlatform(),
                 'appVersion' : this.deviceInfo.getAppVersion(),
                 'deviceName' : this.deviceInfo.getDeviceModel(),
                 'Device-Id-TimeZone': this.constants.GET_TIMEZ_ZONE,
                 'role' : this.role
            }
            if( skipAuth == false ){
                var accessToken = this.locstr.getObj('accessToken');
                if(accessToken == undefined || accessToken == '' ){
                    headerObj["Authorization"] = ""; 
               }
               else{
                   headerObj["Authorization"] = "Bearer " + accessToken;   
                   }
            }
            let payload = JSON.stringify(data);
            let headers = new Headers(headerObj);
            let options = new RequestOptions({ headers: headers });

            console.log("HEADERS:",headers);
            console.log("OPTIONS",options);

            return this.http.post(url, payload, options).timeout(timeout);
        }else{
            return Observable.throw(this.networkError);
        }
    }
    
    putCall = ( path:any, data:any, skipAuth:any,timeout:any ): Observable<any> => {
        if( this.network.isNetworkAvailable() ){
           // this.finalRole = this.getActiveUserRole();
            this.getActiveUserRole((res)=>{
                console.log("res",res);
                this.finalRole =res;
            });
            var url = AppSettings.BASE_URL + path;
            data = data || {};
            timeout = timeout || 40000;
            skipAuth = skipAuth || false;
            var headerObj:any  = {
                 'Content-Type': 'application/json',
                 'Accept-Language': 'en',
                 'deviceId' : this.deviceInfo.getDeviceID(),
                 'deviceType' : this.deviceInfo.getDevicePlatform(),
                 'appVersion' : this.deviceInfo.getAppVersion(),
                 'deviceName' : this.deviceInfo.getDeviceModel(),
                 'Device-Id-TimeZone': this.constants.GET_TIMEZ_ZONE,
                 'role' : this.role
            }
            if( skipAuth == false ){
                var accessToken = this.locstr.getObj('accessToken');
                if(accessToken == undefined || accessToken == '' ){
                    headerObj["Authorization"] = ""; 
               }
               else{
                   headerObj["Authorization"] = "Bearer " + accessToken;   
                   }
            }
            let payload = JSON.stringify(data);
            let headers = new Headers(headerObj);
            let options = new RequestOptions({ headers: headers });
            return this.http.put(url, payload, options).timeout(timeout);
        }else{
            return Observable.throw(this.networkError);
        }
    }
    
    deleteCall( path: any, data: any, skipAuth: any, timeout: any ): Observable<any> {
        if( this.network.isNetworkAvailable() ){
            this.finalRole = this.getActiveUserRole((res)=>{
                console.log("res",res);
            });
            var url = AppSettings.BASE_URL + path;
            data = data || {};
            timeout = timeout || 40000;
            skipAuth = skipAuth || false;
            var headerObj:any  = {
                 'Content-Type': 'application/json',
                 'Accept-Language': 'en',
                 'deviceId' : this.deviceInfo.getDeviceID(),
                 'deviceType' : this.deviceInfo.getDevicePlatform(),
                 'appVersion' : this.deviceInfo.getAppVersion(),
                 'deviceName' : this.deviceInfo.getDeviceModel(),
                 'Device-Id-TimeZone': this.constants.GET_TIMEZ_ZONE,
                 'role' : this.role
            }
            if( skipAuth == false ){
                var accessToken = this.locstr.getObj('accessToken');
                headerObj["Authorization"] = "Bearer " + accessToken;   
            }
            let payload = JSON.stringify(data);
            let headers = new Headers(headerObj);
            let options = new RequestOptions({ headers: headers });
            return this.http.delete(url, new RequestOptions({ headers: headers,body: payload}))
                            .timeout(timeout);

        }else{
            return Observable.throw(this.networkError);
        }
    }
    
    externalCall = ( url:any, params:any, options:any ): Observable<any> => {
        return null;
    }

    /**
     * All POST API call for sens xlx file will be done using this function
     * @param1: Path of URL
     * @param2: Data to pass to URL if any
     * @param3: skip authorization if needed
     * @param4: timeout - default will be 40 sec if need more or less pass as argument
     * */
    postCallForFormData( path: any, data: any, skipAuth: any, timeout: any ): Observable<any> {
        if ( this.network.isNetworkAvailable() ) {
            //this.finalRole = this.getActiveUserRole();
            this.getActiveUserRole((res)=>{
                console.log("res",res);
                this.finalRole =res;
            });
            var url = AppSettings.BASE_URL + path;
            timeout = timeout || 40000;
            data = data || {};
            skipAuth = skipAuth || false;
           
            var headerObj: any = {
                'Accept-Language': 'en',
                'deviceId' : this.deviceInfo.getDeviceID(),
                'deviceType' : this.deviceInfo.getDevicePlatform(),
                'appVersion' : this.deviceInfo.getAppVersion(),
                'deviceName' : this.deviceInfo.getDeviceModel(),
                'Device-Id-TimeZone': this.constants.GET_TIMEZ_ZONE,
                'role' : this.role
            }
            if( skipAuth == false ){
                var accessToken = this.locstr.getObj('accessToken');
                headerObj["Authorization"] = "Bearer " + accessToken;   
            }

            let headers = new Headers( headerObj );
            let options = new RequestOptions( { headers: headers } );
            return this.http.post( url, data, options )
                .timeout( timeout );
        } else {
            return Observable.throw( this.networkError );
        }
    }
}

