import { Injectable } from '@angular/core';
import { Device } from '@ionic-native/device';
import { Platform } from 'ionic-angular';

/*
  Generated class for the DeviceInformationProvider provider.
  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DeviceInformationProvider {
    public isWeb: boolean = false;

    constructor( private device: Device, private platform: Platform ) {
        console.log('Hello DeviceInformationProvider Provider');
        if( this.platform.is('cordova') ){
            this.isWeb = false;
        }else{
            this.isWeb = true;
        }
    }
  
    /**
     * Function to get device information (UUID, device model, platform..)
     * @param - none
     * */
    public getDeviceInformation = (): any => {
        if( !this.isWeb ){
            return this.device;
        }
        return 'WEB';
    }
  
    public getOSVersion = (): any => {
        if( !this.isWeb ){
            return this.device.version;
        }else{
            return 'WEB';
        }
    }
  
    public getDeviceModel = (): any => {
        if( !this.isWeb ){
            return this.device.model;
        }else{
            let browser = navigator.appName;
            let browserVersion = navigator.appVersion;
            return browser + " " + browserVersion;
        }
    }
  
    public getDeviceID = (): any => {
        if( !this.isWeb ){
            return this.device.uuid;
        } else {
            let date = new Date();
            let timestamp = date.getTime();
            return timestamp;
        }
    }
  
    public getDevicePlatform = (): any => {
        if( !this.isWeb ){
            return this.device.platform;
        } else {
            return 'WEB';
        }
    }
  
    public getAppVersion = (): any => {
        if( !this.isWeb ){
            return '1.0';
        } else {
            return '1.0';
        }
    }

}
