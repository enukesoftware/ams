/*--------------------Ionic related components---------------*/
import {Injectable, Inject} from '@angular/core';
import { Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { ModalController, Events, AlertController } from 'ionic-angular';

import { HttpResponseDTO } from '../../interfaces/http-response-dto';

/*-----------------App Providers---------------------*/
import { AppSettings } from '../appSettings/appSettings';
import { RestService } from '../restService/restService';
import { LocalStorageService } from '../localStorage-service/localStorage.service';
import { Constants } from '../appSettings/constant-settings';
import { CommonService } from '../common-service/common.service';

/*
  Generated class for the DbService provider.
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

    constructor( private restService: RestService, public locstr: LocalStorageService,
                 public modalCtrl: ModalController, public event: Events, private constant: Constants, private alertCtrl: AlertController, private commonService: CommonService ) {

    }

    /**
     * Function to perform login
     * @param data
     */
    login = ( data: any ): Observable<any> => {
        var path = AppSettings.LOGIN_URL;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError );
    }

    /**
     * Function to perform social app login
     * @param data
     */
    socialAppLoginService = ( data: any ): Observable<any> => {
        var path = AppSettings.SOCIAL_APP_LOGIN;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError  );
    }

    /**
     * Function to send OTP web-service integration
     * @param data
     */
    requestOTP = ( data: any ): Observable<any> => {
        var path = AppSettings.REQUEST_OTP_URL;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError  );
    }
    
    /**
     * Function to send OTP web-service integration
     * @param data
     */
    resendOTP = ( data: any ): Observable<any> => {
        var path = AppSettings.RESEND_OTP_URL;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch(this.commonService.handleError   );
    }

    /**
     * Function to perform OTP verification
     * @param data
     */
    verifyOTP = ( data: any ): Observable<any> => {
        var path = AppSettings.VERIFY_OTP_URL;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError  );
    }

    /**
     * Function to perform forgot password web-service integration
     * @param data
     */
    forgotPassword = ( data: any ): Observable<any> => {
        var path = AppSettings.FORGOT_PASSWORD_URL;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError  );
    }
  
    /**
     * Function to perform resend OTP web-service integration
     * @param data
     */
    resendOtp = ( data: any ): Observable<any> => {
        var path = AppSettings.RESEND_OTP_URL;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch(this.commonService.handleError  );
    }
  
    /**
     * Function to perform change password web-service integration
     * @param data
     */
    changePassword = ( data: any ): Observable<any> => {
        var path = AppSettings.CHANGE_PASSWORD_URL;
        return this.restService.postCall( path, data, false, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError  );
    }

    /**
     * Function to perform signup web-service integration
     * @param data
     */
    signup = ( data: any ): Observable<any> => {
        var path = AppSettings.SIGN_UP_URL;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError  );
    }
  
    /**
     * Function to perform update profile web-service integration
     * @param data
     */
    updateProfile = ( data: any ): Observable<any> => {
        var path = AppSettings.UPDATE_PROFILE;
        return this.restService.putCall( path, data, false, null )
        .map( res => res.json() )
        .catch(this.commonService.handleError  );
    }
  
    /**
     * Function to get push notification for demo
     * @param data (deviceId, deviceType)
     */
    getPushNotification = ( data: any ): Observable<any> => {
        var path = AppSettings.NOTIFICATION_URL;
        return this.restService.getCallForNotification( path, data, false, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError  );
    }
  
    /**
     * Function to get Subscription for In app purchase
     * @param data (productId, productType)
     */
    getSubscription = () : Observable<HttpResponseDTO> => {
        var path = AppSettings.GET_SUBSCRIPTION;
        return this.restService.getCall( path, false, false, null )
        .map( res => <HttpResponseDTO>res.json() )
        .catch( this.commonService.handleError  );
    }
  
    /**
     * Function to create Subscription for In app purchase
     * @param data (productId, productType)
     */
    createSubscription = ( data: any ): Observable<any> => {
        var path = AppSettings.CREATE_SUBSCRIPTION;
        return this.restService.postCall( path, data, true, null )
        .map( res => res.json() )
        .catch(this.commonService.handleError  );
    }
  
    /**
     * Function to verify Subscription for In app purchase
     * @param data (productId, productType)
     */
    verifySubscription = ( data: any ): Observable<HttpResponseDTO> => {
        var path = AppSettings.VERIFY_SUBSCRIPTION;
        return this.restService.getCall( path, false, false, null )
        .map( res => <HttpResponseDTO>res.json() )
        .catch( this.commonService.handleError  );
    } 

    /**
     * Function to send one signal userid
     * @param data
     */
    SendUserId = (data:any) : Observable<HttpResponseDTO> => {
        var path = AppSettings.SEND_USER_ID;
        return this.restService.putCall( path, data, false, null )
        .map( res => res.json() )
        .catch( this.commonService.handleError );
    }
    /**
     * Function to perform common error handling
     * @param error
     */
   /* public handleError = ( error: Response | any ) => {
        if( error && error.status == 401 ){
            setTimeout(() => {
                this.event.publish( 'user:logout' );
            }, 500);
//            this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
        }else{
            if( error ){
                if( error.message == this.constant.ERROR_NETWORK_UNAVAILABLE ){
                    return Observable.throw( error );
                }else{
                    var err: any;
                    err = error.json();
                    if( error && err ){
                        if( error.code == 401 ){
                            setTimeout(() => {
                                this.event.publish( 'user:logout' );
                            }, 500);
                        }else{
                            return Observable.throw( err );
                        }
                    }else{
                        //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                        let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                        this.commonService.presentToast(errorMsg);
                    }
                }
            }else{
                //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                this.commonService.presentToast(errorMsg);
            }
        }
        return Observable.throw( err );
    }*/
}