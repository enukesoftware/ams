import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { RestService } from "../restService/restService";
import { Events } from "ionic-angular";
import { Constants } from "../appSettings/constant-settings";
import { CommonService } from "../common-service/common.service";
import { AppSettings } from "../appSettings/appSettings";

@Injectable()
export class SearchServiceProvider {

    constructor( public http: HttpClient, public event: Events, private constant: Constants, public restService: RestService,
        private commonService: CommonService ) {

    }

    /**
     * Function to get Doctors list as per search text
     **/
    getDoctorList = (): Observable<any> => {
        var path = AppSettings.GET_SYS_ADMIN_DOCTORS_LIST +"?page=0&size=5&cityId=1";
        return this.restService.getCall( path, null, false, null )
            .map( res => res.json() )
            .catch( this.commonService.handleError  );
    }
    
    /*
     * Function to get search result on keychange
     * 
     * */
    
    getSearchResult = (searchObj?: any):Observable<any> => {
        console.log("searchObj at 34...", searchObj);
        var path = AppSettings.GET_SEARCH_RESULTS;
        return this.restService.postCall( path, searchObj, false, null )
            .map( res => res.json() )
            .catch( this.commonService.handleError );
    }

    /**
     * Function to perform common error handling
     * @param error
     **/
    /*public handleError = ( error: Response | any ) => {
        if ( error && error.status == 401 ) {
            setTimeout(() => {
                this.event.publish( 'user:logout' );
            }, 500 );
            //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
        } else {
            if ( error ) {
                if ( error.message == this.constant.ERROR_NETWORK_UNAVAILABLE ) {
                    return Observable.throw( error );
                } else {
                    var err: any;
                    err = error.json();
                    if ( error && err ) {
                        if ( error.code == 401 ) {
                            setTimeout(() => {
                                this.event.publish( 'user:logout' );
                            }, 500 );
                        } else {
                            return Observable.throw( err );
                        }
                    } else {
                        //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                        this.commonService.presentToast( this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                    }
                }
            } else {
                //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                this.commonService.presentToast( this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
            }
        }
        return Observable.throw( err );
    }*/
}
