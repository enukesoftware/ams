import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { RestService } from "../restService/restService";
import { Events } from "ionic-angular";
import { Constants } from "../appSettings/constant-settings";
import { CommonService } from "../common-service/common.service";
import { AppSettings } from "../appSettings/appSettings";

@Injectable()
export class PatientRoleServiceProvider {

  constructor(public http: HttpClient, public event: Events, private constant: Constants, public restService: RestService, 
              private commonService: CommonService ) {}

  /**
   * Function to get Doctors List based on City
   * @param cityId
   **/
  getDoctorsList = ( data:any,pageNo: number, role?: any ): Observable<any> => {
      if(!role){
          role = "";
      }
      var path = AppSettings.GET_DOCTORS_LIST + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&cityId=' + data+"&role="+role;
      return this.restService.getCall(path, data, false, null)
              .map(res=>res.json())
              .catch(this.commonService.handleError );
  }
  /*
   *  function to get activate or deactivate patient  
   * */
  
  public activateDeactivatePatient =(data:any) =>{
     // console.log("data",data);
      var path = AppSettings.GET_PATIENT_ACTIVATE_DEACTIVATE + "?patientId="+ data.patientId + "&action="+ data.action;
      return this.restService.getCall( path, null, false, null )
      .map( res => res.json() )
      .catch( this.commonService.handleError  );
  } 
  
  /*
   *  function to get  patient  details
   * */
  
  public getPatientProfile =(id?: any) =>{
      var path = AppSettings.GET_PATIENT_DETAILS +"?patientId="+ (id ? id : "");
      return this.restService.getCall(path, null, false, null)
              .map(res=>res.json())
              .catch(this.commonService.handleError );
  }
  
  /**  function to get  patient  upcoming appointments
  * */
 
 public getPatientUpcomingAppointments =( patientId:any, pageNo: any) =>{
    // console.log('---patientId----',patientId);
     var path = AppSettings.GET_PATIENT_UPCOMING_APPOINTMENTS + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&patientId=' + patientId;
     return this.restService.getCall(path, null, false, null)
             .map(res=>res.json())
             .catch(this.commonService.handleError );
 }
 
 /*
  *  function to get  patient appointment history
  * */
 
 public getPatientAppointmentHistory =(patientId:any,pageNo: any) =>{
     var path = AppSettings.GET_PATIENT_APPOINTMENT_HISTORY + '?page=' + pageNo + '&size=' + this.constant.PAGE_SIZE + '&patientId=' + patientId;
     return this.restService.getCall(path, null, false, null)
             .map(res=>res.json())
             .catch(this.commonService.handleError );
 }

 
 /**
  * Function to update patient profile
  **/
 public updatePatientProfile = ( data:any ): Observable<any> => {
     var path = AppSettings.UPDATE_PATIENT_PROFILE;
     return this.restService.putCall(path, data, false, null)
             .map(res=>res.json())
             .catch(this.commonService.handleError );
 }
 
 /*
  * Function to create feedback
  * */
 
 createFeedback = (data:any): Observable<any> => {
     var path = AppSettings.CREATE_FEEDBACK;
     return this.restService.postCall( path, data, false, null )
       .map( res => res.json() )
       .catch( this.commonService.handleError  );
 }
 
 /*
  * function to get Feedback History
  * */
 getFeedbackHistory = (patientId:any,pageNo: any): Observable<any> => {
     var path = AppSettings.GET_PATIENT_FEEDBACK_HISTORY+ '?page=' + pageNo + '&patientId=' + patientId + '&size=' + this.constant.PAGE_SIZE;
     return this.restService.getCall(path, null, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError );
 }
  
/*
 * function to get doctor feedbacks on doctor profile
 * */ 

 getDoctorFeedbacks = (doctorId:any,pageNo: any): Observable<any> => {
     var path = AppSettings.GET_DOCTOR_FEEDBACKS+  '?doctorId=' + doctorId + '&size=' + this.constant.PAGE_SIZE+ '&page=' + pageNo;
     return this.restService.getCall(path, null, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError );
 }
 
 
 /*
  * function to get feedback 
  * */
 
 getFeedback = (userFeedbackId:any): Observable<any> => {
     console.log(" Id ",userFeedbackId);
     var path = AppSettings.GET_FEEDBACKS+ '?userFeedbackId=' + userFeedbackId;
     return this.restService.getCall(path, null, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError );
 }
 

     /*
      * function to update feedback 
      * */
 
     updateFeedback  = ( data:any ): Observable<any> => {
         var path = AppSettings.UPDATE_FEEDBACK;
         return this.restService.putCall(path, data, false, null)
                 .map(res=>res.json())
                 .catch(this.commonService.handleError );
     }
 
     /*
      * function to get Report 
      * */
 
     getReportForDoctor  = ( Id:any,dates?:any ): Observable<any> => {
         var path = AppSettings.GET_REPORT_FOR_DOCTOR+'?doctorProfileId=' + Id + '&fromDateMilliSec='+dates.fromDate +'&toDateMilliSec='+dates.toDate;
         return this.restService.getCall(path, null, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError ); 
     }
     
     
     /*
      * function to get doctor Report 
      * */
 
     getReportDoctor  = ( Id:any ): Observable<any> => {
         var path = AppSettings.GET_REPORT_FOR_DOCTOR+'?doctorProfileId=' + Id;
         return this.restService.getCall(path, null, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError ); 
     }
 
     
     /*
      * function to get report for Hospital Admin
      * */
     
     getReportForHospitalAdmin  = ( Id:any,dates:any): Observable<any> => {
         var path = AppSettings.GET_REPORT_FOR_HOSPITALADM+'?hospitalId=' + Id + '&fromDateMilliSec='+dates.fromDate +'&toDateMilliSec='+dates.toDate;
         return this.restService.getCall(path, null, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError ); 
     }
     
     
     getReportForSysAdmin = (dates:any): Observable<any> => {
         var path = AppSettings.GET_REPORT_FOR_SYSADM + '?page=0&size=' + this.constant.PAGE_SIZE + '&fromDateMilliSec='+dates.fromDate +'&toDateMilliSec='+dates.toDate;
         return this.restService.getCall(path, null, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError ); 
     }
     
     getPatientsAsPerSearch = (pageNo:any,searchText:any): Observable<any> => {
         console.log("in get patient",pageNo);
         var path = AppSettings.GET_PATIENT_AS_PER_SEARCH +'?page=' + pageNo + '&searchText='+ searchText + '&size=' + this.constant.PAGE_SIZE ;
         return this.restService.getCall(path, false, false, null)
         .map(res=>res.json())
         .catch(this.commonService.handleError );
     }

    
     
 
  /**
   * Function to perform common error handling
   * @param error
   **/
/*  public handleError = ( error: Response | any ) => {
      if( error && error.status == 401 ){
          setTimeout(() => {
              this.event.publish( 'user:logout' );
          }, 500);
//          this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNAUTHORIZED );
      }else{
          if( error ){
              if( error.message == this.constant.ERROR_NETWORK_UNAVAILABLE ){
                  return Observable.throw( error );
              }else{
                  var err: any;
                  err = error.json();
                  if( error && err ){
                      if( error.code == 401 ){
                          setTimeout(() => {
                              this.event.publish( 'user:logout' );
                          }, 500);
                      }else{
                          return Observable.throw( err );
                      }
                  }else{
                      //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
                      let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
                      this.commonService.presentToast(errorMsg);
                  }
              }
          }else{
              //this.commonService.showAlert( "Error", this.constant.ERROR_MSG_UNABLE_TO_CONNECT );
              let errorMsg = error.message ? error.message : this.constant.ERROR_MSG_UNABLE_TO_CONNECT;
              this.commonService.presentToast(errorMsg);
          }
      }
      return Observable.throw( err );
  }*/
}