import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'groupByDatePipe',
})
export class GroupByDatePipe implements PipeTransform {
    list =[];
    transform(appointmenList) {     
    this.list = appointmenList;
    console.log("list--->",this.list);
        let groups = {};
        //console.log("groups--->",groups);
        let grouped = [];
        if(this.list){
        this.list.forEach(function (o) {
        if (!groups[o.appointmentDate]) {
            groups[o.appointmentDate] = [];
            grouped.push({ date: o.appointmentDate, details: groups[o.appointmentDate] });
        }
            groups[o.appointmentDate].push(o);
        });
        }
    console.log('myarray===>',grouped);
    return  grouped;
  }  
}
