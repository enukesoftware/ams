import { NgModule } from '@angular/core';
import { GroupByDatePipe } from './group-by-date/group-by-date';
@NgModule({
	declarations: [GroupByDatePipe],
	imports: [],
	exports: [GroupByDatePipe]
})
export class PipesModule {}
