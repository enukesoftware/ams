import { Directive,Input,SimpleChanges } from '@angular/core';
import {Validator,AbstractControl} from "@angular/forms";
import {NG_VALIDATORS} from "@angular/forms";
import * as moment from 'moment';
import { CommonService } from '../providers/common-service/common.service';


@Directive({
  selector: '[validDateTime]',
   providers: [
        {provide: NG_VALIDATORS,useExisting:StartTimeDirective, multi: true}
    ]
})
export class StartTimeDirective implements Validator {


  constructor(private commonService:CommonService) { }

  @Input("validDateTime")
    validDateTime: any;

    validate(c:AbstractControl) {
      let value = moment(this.validDateTime).isSame(moment().format('YYYY-MM-DD'), 'day');
      if(value) {
        if(!this.commonService.checkIsWeb()){
          console.log(c.value);
          if(c.value) {
            let time = (c.value.split(':'))
            var abc = moment();
            abc.set({h: time[0], m: time[1]});        
            let currentTime = moment();
            if(moment().isAfter(abc)) {
              return {  
                 validDateTime: {
                  condition:false
                 }
               }
             }

          }

      }else{
        let time = moment(c.value)        
        let currentTime = moment();

        if(moment(time).isBefore(currentTime)) {
         return {
            validDateTime: {
             condition:false
            }
          }
        }
      }
      }
      

       return null;
    }

  registerOnValidatorChange(fn: () => void): void { this._onChange = fn; }
  private _onChange: () => void;

  ngOnChanges(changes: SimpleChanges): void {
 
    if ('validDateTime' in changes) {
      
      if (this._onChange) this._onChange();
    }
  }
}