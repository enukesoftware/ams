import { Directive,Input,SimpleChanges } from '@angular/core';
import {Validator,AbstractControl} from "@angular/forms";
import {NG_VALIDATORS} from "@angular/forms";
import * as moment from 'moment';
import { CommonService } from '../providers/common-service/common.service';


@Directive({
  selector: '[endDate]',
   providers: [
        {provide: NG_VALIDATORS,useExisting:EndTimeDirective, multi: true}
    ]
})
export class EndTimeDirective implements Validator {

  constructor(private commonService:CommonService) { }

  @Input("endDate")
    endDate: any;

    @Input("startTime")
    startTime: any;

    validate(c:AbstractControl) {
      console.log('validatetimeinput', this.endDate)
       let value = moment(this.endDate).isSame(moment().format('YYYY-MM-DD'), 'day');
       if(value) {
        if(!this.commonService.checkIsWeb()){
          if(c.value) {
            let onlyTime = (c.value.split(':'))
            var time = moment();
            time.set({h: onlyTime[0], m: onlyTime[1]}); 

            if(this.startTime){
              let startTimeHourMin = this.startTime.split(':');
              var startTimeComplete = moment();
              startTimeComplete.set({h: startTimeHourMin[0], m: startTimeHourMin[1]});  
  
              if(moment(time).isBefore(startTimeComplete) ) {
                return {  
                   validDateTime: {
                    condition:false
                   }
                 }
               }
            }else{
              if(moment(time).isBefore(moment()) ) {
                return {  
                   validDateTime: {
                    condition:false
                   }
                 }
               }
            }
            

          }

      }else{
         let time = moment(c.value)
         if(moment(time).isBefore(this.startTime) || moment(time).isBefore(moment())) {
          return {
             validDateTime: {
              condition:false
             }
           }
         }
       }
      }

       return null;
    }

  registerOnValidatorChange(fn: () => void): void { this._onChange = fn; }
  private _onChange: () => void;

  ngOnChanges(changes: SimpleChanges): void {
 
    if ('endDate' in changes) {
      
      if (this._onChange) this._onChange();
    }

    if ('startTime' in changes) {
      
      if (this._onChange) this._onChange();
    }
  }
}