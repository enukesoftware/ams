import { Directive,Input,SimpleChanges } from '@angular/core';
import {Validator,AbstractControl} from "@angular/forms";
import {NG_VALIDATORS} from "@angular/forms";
import * as moment from 'moment';
import { CommonService } from '../providers/common-service/common.service';


@Directive({
  selector: '[minValue]',
   providers: [
        {provide: NG_VALIDATORS,useExisting:MinValueDirective, multi: true}
    ]
})
export class MinValueDirective implements Validator {


  constructor(private commonService:CommonService) { }

  @Input("minValue")
  minValue: any;

    validate(c:AbstractControl) {
      if(c.value<this.minValue){
        return {  
          minValue: {
           condition:false
          }
        }
      }else{
        return null;
      }
      

    }

  registerOnValidatorChange(fn: () => void): void { this._onChange = fn; }
  private _onChange: () => void;

  ngOnChanges(changes: SimpleChanges): void {
 
    if ('minValue' in changes) {
      
      if (this._onChange) this._onChange();
    }
  }
}