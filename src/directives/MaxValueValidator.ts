import { Directive,Input,SimpleChanges } from '@angular/core';
import {Validator,AbstractControl} from "@angular/forms";
import {NG_VALIDATORS} from "@angular/forms";
import * as moment from 'moment';
import { CommonService } from '../providers/common-service/common.service';


@Directive({
  selector: '[maxValue]',
   providers: [
        {provide: NG_VALIDATORS,useExisting:MaxValueDirective, multi: true}
    ]
})
export class MaxValueDirective implements Validator {


  constructor(private commonService:CommonService) { }

  @Input("maxValue")
  maxValue: any;

    validate(c:AbstractControl) {
      if(c.value>this.maxValue){
        return {  
          maxValue: {
           condition:false
          }
        }
      }else{
        return null;
      }
      

    }

  registerOnValidatorChange(fn: () => void): void { this._onChange = fn; }
  private _onChange: () => void;

  ngOnChanges(changes: SimpleChanges): void {
 
    if ('maxValue' in changes) {
      
      if (this._onChange) this._onChange();
    }
  }
}