export interface HttpResponseDTO {
    code?: any;
    data?: any;
    message?: string;
    status?: string;
    token?: string;
    error_message?: string;
    success?: boolean;
}