export interface UserDataDTO {
    firstName?: any;
    lastName?: any;
    userName?: string;
    contactNumber?: string;
    email?: string;
    city?: string;
    password?: string;
    loginIdentifier?: any;
    signupType?: any;
    role?: any;
    type?:any;
    otp?:any;
    transactionId?:any;
    userId?: any;
    imageBase64?: any;
    contactNo?: any;
    doctorId?: any;
    fileId?: any;
}


export interface AddressDto{
    street?: string;
    state?: string;
    zipCode?: number;
    cityName?: string;
    cityId?: any;
    country?: string;
    latitude?: any;
    longitude?: any;
} 

/**
 * Address DTO
 **/
interface AddressDataDTO {
    street?: string;
    state?: string;
    zipCode?: number;
    city?: string;
    cityId?: number;
    country?: string;
    latitude?: any;
    longitude?: any;
}

/**
 * Hospital list DTO
 **/

export interface HospitalListDTO{
    address? : any;
    active?: any;
    avgRating?: number;
    hospitalId?: number;
    imageUrl?: number;
    name?: string;
    availabilities?: any;
    serviceList?: any;
    specializationList?: any; 
    formattedAvailabilities?: any;
    dayCount?: number;
    todayAvailability?: any;
    timeAvailabilityList?: any;
    isAMultispeciality?: any;
    todaysAvailabilityString?: any;
    todaysTimeAvailabilityList?: any;
    imageBase64?: any;
}

export interface HospitalDTO{
    address? : any;
    addressDTO? : any;
    active?: any;
    avgRating?: number;
    hospitalId?: number;
    imageUrl?: number;
    name?: string;
    availabilities?: any;
    serviceList?: any;
    specializationList?: any; 
    formattedAvailabilities?: any;
    dayCount?: number;
    todayAvailability?: any;
    timeAvailabilityList?: any;
    overview?: any;
    websiteLink?: any;
    timeAvailability?: any;
    imagesCount?: any;
    effectiveFrom?: any;
    isAMultispeciality?:any;
    todaysAvailabilityString?: any;
    todaysTimeAvailabilityList?: any;
    is24HrOpen?: any;
    imageBase64?:any;
}
/**
 * Associated Doctor list DTO
 **/

export interface DoctorListDTO{
    address?: any;
    avgRating?: any;
    educationList?: any;
    imageUrl?:any;
    firstName?: any;
    lastName?: any;
    serviceList?: any;
    specializationList?: any;
    yearsOfExp?: any;
    availabilities?: any;
    formattedAvailabilities?: any;
    todayAvailabilityList?: any;
    hospitalAvailableList?: any;
    todayAvailability?: any;
    timeAvailabilityList?: any;
    advanceBookingDays?: any;
    todaysTimeAvailabilityList?: any;
    todaysAvailabilityString?: any;
    doctorId?: any;
    imageBase64?: any;
    userId?: any;
}

/**
 * Create Hospital DTO
 **/

export interface HospitalDataDTO extends AddressDataDTO{
    name?: any;
    hospitalName?: any;
    hospitalEmail?: string;
    hospitalContactNo?: string;
    adminFirstName?: string;
    adminLastName?: string;
    adminEmail?: string;
    adminContactNo?: string;
    specialization?: any;
    services?: any;
}

export interface serviceAndSpecializationDataDto{
    categoryId?: any,
    id?: any,
    name?: string
}

export interface DoctorDataDto extends AddressDataDTO{
    address?: any;
    firstName?: string;
    lastName?: string;
    localName?: string;
    contactNumber?: number;
    daysForAdvanceBooking?: number;
    educationList?: any;
    email?: string;
    username?: string;
    registrationNumber?: number;
    registrationState?: string;
    specializationList?: any;
    serviceList?: any;
    categoryId?: any;
    category?: any;
    yearsOfExp?: any;
    doctorId?: any;
    doctorProfileId?: any;
    advanceBookingDays?: any;
    fees?: any;
    imageBase64?:any;
    fileId?: any;
    profileInfo?:any;
}

export interface SystemAdminsListDto{
    userId?:string;
    firstName?:string;
    lastName?:string;    
    email?:string;
    contactNumber?:string;
    userProfilePicUrl?:string; 
    userProfileThumbPicUrl?:string ;
    blockStatus?:string;
    imageBase64?: any;
    fileId?:any;
}

/**
 * Create Hospital DTO
 **/

export interface HospitalAdminDto{
    addressRequestWrapper?: any;
    userId?:string;
    contactNumber?: string;
    email?: string;
    firstName?: string;
    hospitalId?: string;
    lastName?: string;
    userName?: string;
    userProfilePicUrl?: string;
    userProfileThumbPicUrl?: string;
    blockStatus?: any; 
    fileId?: any;
    imageBase64?: any;
}

export interface PatientDataDTO{
    city?: string;
    contactNumber?: string;
    country?: string;
    email?: string;
    firstName?: string;
    lastName?: string;
    state?: string;
    street?: string;
    userName?: string;
    zipCode?: string;
    imageBase64?: any;
    fileId?: any;
    userId?: any;
    address?:any;
    userProfilePicUrl?:any;
    booldGroup?:string;
    dob?:string;
    emergencyContactName?:string;
    emergencyContactNum?:string;
    emergencyContactRelation?:string;
    employment?:string;
    maritalStatus?:string;
    gender?:string;
    relation?:string;
    otherCity?:string;

}

export interface AppointmentDetailsDTO{
    appointmentDate?: any;
    appointmentId?: any;
    appointmentType?: string;
    fromTime?: any;
    patientContactNumber?: any;
    patientName?: string;
    paymentStatus?: any;
    toTime?: any;
    totalFilesCount?: any;
}

export interface AppointmentDTO{
    appointmentDetails?: any;
    doctorsDetails?: any;
    hospitalDetails?: any;
    noOfPatients?: any;
    visitType?: any;
}
/**
 * Time Slot DTO
 **/
export interface timeSlotDTO{
    dayId?: any;
    daysId?: any;
    day?: any;
    fromTime?: any;
    slot?: any;
    startTimeMinutes?: any;
    toTime?: any;
    endTimeMinutes?: any;
    tokens?: any;
    visitType?: any;
    visitValue?: any;
    timeValidate?: boolean;
    startTime?: any;
    endTime?: any;
    showVisitTypeString?:any;
}

/**
 * Create Contract DTO
 **/
export interface createContract{
    doctorProfileId?: any;
    endDate?: any;
    fees?: any;
    hospitalId?: any;
    startDate?: any;
    timeSlots?: any
}

/*
 * Create feedback DTO
 * */

export interface FeedbackDataDTO{
    appointmentId?: string;
    doctorFeedbackComments?: string;
    doctorFeedbackRating?: string;
    hospitalId?: string;
    doctorProfileId?: string;
    hospitalFeedbackComments?: string;
    hospitalFeedbackRating?: string;  
    patientId?:string;
    userFeedbackId?:string;
    doctorFeedbackRatingPoints?:string;
}

export interface LeaveDTO{
    userProfileDTO:UserDataDTO;
    fromDate:any;
    toDate:any;
    description:any;
    hospitalDTO:any;
    leaveIds:any;

}

export interface PatientDependentDataDTO{
    userId?: any;
    userProfileId:any;
    userProfilePicUrl:any;
    userProfileThumbPicUrl:any;
    address?:any;
    booldGroup?:string;
    contactNo?: string;
    dob?:string;
    email?: string;
    emergencyContactName?:string;
    emergencyContactNum?:string;
    emergencyContactRelation?:string;
    employment?:string;
    firstName?: string;
    lastName?: string;
    gender?:string;
    imageBase64?: any;
    maritalStatus?:string;
    relation?:string;
    otherCity?:string;
    isSelected:boolean;
}

export interface StaffDataDTO{
    userId?: any;
    firstName?: string;
    lastName?: string;
    email?: string;
    contactNumber?: string;
    userProfilePicUrl:any;
    userProfileThumbPicUrl:any;
    blockStatus: any,
    userName?: any,
    address?:any;
    addressRequestWrapper?:any;
    imageBase64?: any;
    fileId:any;
    hospitalId?:any;
    isSelected:boolean;
}