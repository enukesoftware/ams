$(window).on('scroll', function() 
{
    if ($(this).scrollTop() > 100 ){
        $('.scroll-top.active').removeClass('active');
        $('.scroll-top').addClass('active');
    } else {
        $('.scroll-top').removeClass('active');
    }

    // On Scroll Fiexd Search Form

    if ($(this).scrollTop() > 460) {
        $("#realtive-form").addClass("hidden");
        $("#fixed-form").addClass("visible");
    } else {
        $("#realtive-form").removeClass("hidden");
        $("#fixed-form").removeClass("visible");
    }
});

// Scroll To Top

$('.scroll-top').on('click', function() {
    $("html, body").animate({scrollTop:0}, 500 );
    return false;
});

// Custom Flag Select Dropdown

$('#basic').flagStrap({
    placeholder: {
        value: "",
        text: "Country of origin"
    }
});

// Stop Propagation

$('.dropdown-menu').click(function(e) {
    e.stopPropagation();
});
  